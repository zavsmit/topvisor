package ru.handh.topvisor.utils;

/**
 * Created by sergey on 03.02.16.
 * константы приложения
 */
public class Constants {
    public final static String SERVER_RU = "https://topvisor.ru/";
    public final static String SERVER_US = "https://topvisor.com/";

    public static final String RU_LANGUAGE = "русский";
    public static final String US_LANGUAGE = "English";

    public static final String RU = "ru";
    public static final String US = "en";

    public static final String COUNT_IN_PAGE = "50";

    public final static int VIEW_PROGRESS = 0;
    public final static int VIEW_CONTENT = 1;
    public final static int VIEW_ERROR = 2;


    public final static int UPLOADING = 50;
}
