package ru.handh.topvisor.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by sergey on 30.01.16.
 * служебные методы
 */
public class Utils {

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public static Drawable getCustomDrawable(int resourceID, Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return context.getResources().getDrawable(resourceID, context.getTheme());
        } else {
            return context.getResources().getDrawable(resourceID);
        }
    }

    public static int getCustomColor(Context context, int id) {

        if (Build.VERSION.SDK_INT >= 23) {
            return ContextCompat.getColor(context, id);
        } else {
            return context.getResources().getColor(id);
        }
    }
}
