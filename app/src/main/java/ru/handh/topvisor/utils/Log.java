package ru.handh.topvisor.utils;

import ru.handh.topvisor.BuildConfig;

/**
 * Created by sergey on 18.02.16.
 * логи для дебага
 */
public class Log {

    public static void deb(String message) {
        if (BuildConfig.TVDEBUG)
            android.util.Log.d("TV", message);
    }

    public static void deb(String title, String message) {
        if (BuildConfig.TVDEBUG)
            android.util.Log.d(title, message);
    }

    public static void deb(String title, String message, Throwable e) {
        if (BuildConfig.TVDEBUG)
            android.util.Log.d(title, message, e);
    }

    public static void edeb(String message) {
        if (BuildConfig.TVDEBUG)
            android.util.Log.d("TV", message);
    }
}