package ru.handh.topvisor.ui.project.selectDate.listDates;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ViewFlipper;

import java.util.ArrayList;
import java.util.List;

import ru.handh.topvisor.R;
import ru.handh.topvisor.ui.base.ParentBackActivity;

/**
 * Created by sergey on 30.03.16.
 * список дат провоерок
 */
public class ActivityListDates extends ParentBackActivity implements ViewListDates {
    private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_yandex_inspect);
        viewFlipperMain = (ViewFlipper) findViewById(R.id.viewFlipper);
        showData();
        mRecyclerView = (RecyclerView) findViewById(R.id.recicleView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabSearchSystem);
        fab.setVisibility(View.GONE);

        ArrayList<String> listDates = new ArrayList<>();
        int selected = 0;
        int typeSelect = 0;

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            listDates = extras.getStringArrayList("listDates");
            selected = extras.getInt("selectPosition");
            typeSelect = extras.getInt("typeSelect");
        }



        initToolbar(R.id.toolbar, getString(R.string.selectDate));
        PresenterListDates presenter = new PresenterListDatesImpl(this, listDates, selected, typeSelect);
    }

    @Override
    public void initAdapter(List<String> list, AdapterListDates.ItemGroupClickListener listener, int select) {
        RecyclerView.Adapter mAdapter = new AdapterListDates(list, listener, select);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void selectGroup(int idSelected, int typeSelect) {
        Intent intent = new Intent();
        intent.putExtra("idSelected", idSelected);
        intent.putExtra("typeSelect", typeSelect);
        setResult(RESULT_OK, intent);
        finish();
    }


}
