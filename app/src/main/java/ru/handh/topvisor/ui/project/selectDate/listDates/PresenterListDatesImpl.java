package ru.handh.topvisor.ui.project.selectDate.listDates;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by sergey on 30.03.16.
 * логика списка дат
 */
public class PresenterListDatesImpl implements PresenterListDates,
        AdapterListDates.ItemGroupClickListener {

    private ViewListDates mView;
    private int typeSelect;

    public PresenterListDatesImpl(ViewListDates view, ArrayList<String> list, int selected, int typeSelect) {
        mView = view;
        this.typeSelect = typeSelect;


        List<String> formatDates = new ArrayList<>();
        Date date = new Date();
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        DateFormat dfNew = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH);
        for (int i = 0; i < list.size(); i++) {

            try {
                date = format.parse(list.get(i));
            } catch (ParseException e) {
                e.printStackTrace();
            }


            formatDates.add(dfNew.format(date));
        }


        mView.initAdapter(formatDates, this, selected);
    }

    @Override
    public void clickItem(int idSelected) {
        mView.selectGroup(idSelected, typeSelect);
    }

}