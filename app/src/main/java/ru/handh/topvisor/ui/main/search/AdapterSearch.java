package ru.handh.topvisor.ui.main.search;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.BackgroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.network.model.currentProject.Row;

/**
 * Created by sergey on 15.02.16.
 * адаптер поиска по проектам
 */
public class AdapterSearch extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_PROGECT_NAME = 1;
    private static final int TYPE_LABEL_PROGECT = 2;
    private static final int TYPE_DIVIDER_SEARCH = 3;

    private final int TYPE_TITLE_ARCHIVE = -1;
    private final int TYPE_TITLE_AUTO = 1;
    private final int TYPE_TITLE_HAND = 0;
    private final int TYPE_TITLE_YANDEX = 2;

    private final int TYPE_LIST_TITLE = 0;
    private final int TYPE_LIST_ITEM = 1;
    private final int TYPE_LIST_DIVIDER = 2;


    private ItemSearchClickListener listener;
    private List<ItemSearch> listItems;
    private String userId;
    private Context context;


    public AdapterSearch(ArrayList<Row> dataset, ItemSearchClickListener listener, String userId, String search, Context context) {
        this.context = context;
        this.listener = listener;
        this.userId = userId;
        initData(dataset, search);
    }

    private void initData(ArrayList<Row> dataset, String search) {
        List<Row> auto = new ArrayList<>();
        List<Row> hand = new ArrayList<>();
        List<Row> guest = new ArrayList<>();
        List<Row> archive = new ArrayList<>();
        listItems = new ArrayList<>();

        for (int i = 0; i < dataset.size(); i++) {

            if (!dataset.get(i).getUser().equals(userId)) {
                guest.add(dataset.get(i));
            } else {
                switch (Integer.valueOf(dataset.get(i).getOn())) {
                    case TYPE_TITLE_ARCHIVE:
                        archive.add(dataset.get(i));
                        break;
                    case TYPE_TITLE_AUTO:
                        auto.add(dataset.get(i));
                        break;
                    case TYPE_TITLE_HAND:
                        hand.add(dataset.get(i));
                        break;
                    case TYPE_TITLE_YANDEX:
                        auto.add(dataset.get(i));
                        break;
                }
            }
        }

        addElements(auto, context.getString(R.string.autoInspection), R.drawable.auto_vector, search);
        addElements(hand, context.getString(R.string.handInspection), R.drawable.hand_vector, search);
        addElements(guest, context.getString(R.string.guest), R.drawable.guest_vector, search);
        addElements(archive, context.getString(R.string.archive), R.drawable.archive_vector, search);
    }


    private void addElements(List<Row> list, String name, int idImage, String search) {
        if (list.size() != 0) {

            listItems.add(new ItemSearch(TYPE_LIST_TITLE, name, idImage, list.size()));

            for (int i = 0; i < list.size(); i++) {
                int start = 0;
                int end = 0;

                if (!search.isEmpty()) {
                    start = list.get(i).getName().toLowerCase().indexOf(search);
                    end = start + search.length();
                }

                listItems.add(new ItemSearch(list.get(i), TYPE_LIST_ITEM, i == list.size() - 1, start, end));
            }

            listItems.add(new ItemSearch(TYPE_LIST_DIVIDER));
        }
    }


    public void notifyAllData(ArrayList<Row> dataset, String search) {
        initData(dataset, search);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case TYPE_PROGECT_NAME:
                return new ViewHolderProject(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_search, parent, false));
            case TYPE_LABEL_PROGECT:
                return new ViewHolderLabel(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_search_title, parent, false));
            case TYPE_DIVIDER_SEARCH:
                return new ViewHolderDivider(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_divider, parent, false));
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        final ItemSearch item = listItems.get(position);

        switch (holder.getItemViewType()) {

            case TYPE_PROGECT_NAME:

                ViewHolderProject mHolder = ((ViewHolderProject) holder);

                Spannable str = new SpannableStringBuilder(item.modelProject.getName());
                str.setSpan(new BackgroundColorSpan(R.color.colorButton), item.startPosition,
                        item.endPositoin, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                mHolder.text.setText(str);

                if (item.isLastElement) {
                    mHolder.divider.setVisibility(View.GONE);
                } else {
                    mHolder.divider.setVisibility(View.VISIBLE);
                }

                mHolder.container.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.clickItem(item.modelProject.getId());
                    }
                });

                break;

            case TYPE_LABEL_PROGECT:

                ViewHolderLabel mHolderLabel = ((ViewHolderLabel) holder);

                mHolderLabel.title.setText(item.nameTitle);
                mHolderLabel.label.setBackgroundResource(item.idImage);

                break;
        }

    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    @Override
    public int getItemViewType(int position) {

        switch (listItems.get(position).typeList) {
            case TYPE_LIST_TITLE:
                return TYPE_LABEL_PROGECT;
            case TYPE_LIST_ITEM:
                return TYPE_PROGECT_NAME;
            case TYPE_LIST_DIVIDER:
                return TYPE_DIVIDER_SEARCH;
            default:
                return TYPE_DIVIDER_SEARCH;
        }
    }


    public interface ItemSearchClickListener {
        void clickItem(String id);
    }

    public class ViewHolderProject extends RecyclerView.ViewHolder {
        public TextView text;
        public View divider;
        public RelativeLayout container;

        public ViewHolderProject(View v) {
            super(v);
            text = (TextView) itemView.findViewById(R.id.tv_itemSearch);
            divider = itemView.findViewById(R.id.v_itemSearch);
            container = (RelativeLayout) itemView.findViewById(R.id.rl_itemSearch);
        }
    }

    public class ViewHolderLabel extends RecyclerView.ViewHolder {
        public TextView title;
        public ImageView label;

        public ViewHolderLabel(View v) {
            super(v);
            label = (ImageView) itemView.findViewById(R.id.iv_itemSearch);
            title = (TextView) itemView.findViewById(R.id.tv_itemSearch_title);
        }
    }

    public class ViewHolderDivider extends RecyclerView.ViewHolder {
        public ViewHolderDivider(View v) {
            super(v);
        }
    }

    class ItemSearch {
        Row modelProject;
        int typeList;
        String nameTitle;
        int idImage;
        boolean isLastElement;
        int startPosition;
        int endPositoin;

        public ItemSearch(int typeList, String nameTitle, int idImage, int countGroup) {
            this.typeList = typeList;
            this.nameTitle = nameTitle + " (" + countGroup + ")";
            this.idImage = idImage;
        }

        public ItemSearch(Row modelProject, int typeList, boolean isLastElement, int startPos, int endPos) {
            this.modelProject = modelProject;
            this.typeList = typeList;
            this.isLastElement = isLastElement;
            startPosition = startPos;
            endPositoin = endPos;
        }

        public ItemSearch(int typeList) {
            this.typeList = typeList;
        }
    }
}