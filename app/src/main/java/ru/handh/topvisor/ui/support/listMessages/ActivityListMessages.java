package ru.handh.topvisor.ui.support.listMessages;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.kbeanie.imagechooser.api.ChooserType;
import com.kbeanie.imagechooser.api.ChosenImage;
import com.kbeanie.imagechooser.api.ChosenImages;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.kbeanie.imagechooser.api.ImageChooserManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.network.model.ModelItemMessage;
import ru.handh.topvisor.ui.base.ParentBackActivity;
import ru.handh.topvisor.utils.Log;

/**
 * Created by sergey on 01.04.16.
 * диалог с поддержкой
 */
public class ActivityListMessages extends ParentBackActivity implements ViewListMessages {

    public final static int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 11111;

    private RecyclerView mRecyclerView;
    private RecyclerView mRecyclerViewAddPhoto;
    private PresenterListMessages presenter;
    private AdapterListMessages mAdapter;
    private AdapterPhoto adapterPhoto;
    private AppCompatEditText etNewMessage;

    private ImageChooserManager imageChooserManager;
    private String originalFilePath;
    private String thumbnailSmallFilePath;

    private BroadcastReceiver br;

    public final static String BROADCAST_MESSAGE = "broadcastNewGcmMessage";
    public final static String PARAM_STATUS = "statusGCM";
    public final static String NEW_MESSAGE = "newMessage";
    public final static String ID_TICKET = "id_TICKET";
    public final static int STATYS_NEW_GCM_MESSAGE = 100;

    private String name = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_messeges);

        mRecyclerView = (RecyclerView) findViewById(R.id.recicleView);
        mRecyclerView.setHasFixedSize(true);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setStackFromEnd(true);

        mRecyclerView.setLayoutManager(linearLayoutManager);


        mRecyclerViewAddPhoto = (RecyclerView) findViewById(R.id.recyclerviewAddPhoto);
        assert mRecyclerViewAddPhoto != null;
        mRecyclerViewAddPhoto.setHasFixedSize(true);

        LinearLayoutManager staggerLayoutManager = new LinearLayoutManager(this);
        staggerLayoutManager.setOrientation(0);
        mRecyclerViewAddPhoto.setLayoutManager(staggerLayoutManager);

        etNewMessage = (AppCompatEditText) findViewById(R.id.et_newMessage);
        viewFlipperMain = (ViewFlipper) findViewById(R.id.viewFlipperMessages);


        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            name = extras.getString("nameId");
        }

        br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int status = intent.getIntExtra(PARAM_STATUS, 0);

                switch (status) {
                    case STATYS_NEW_GCM_MESSAGE:
                        String text = intent.getStringExtra(NEW_MESSAGE);
                        String idTicket = intent.getStringExtra(ID_TICKET);

                        if (idTicket.equals(name)) {
                            presenter.addSupportMessage(text, ActivityListMessages.this);
                        }

                        break;
                }
            }
        };

        IntentFilter intentFilter = new IntentFilter(BROADCAST_MESSAGE);
        registerReceiver(br, intentFilter);


        assert name != null;
        if (name.isEmpty()) {
            initToolbar(R.id.toolbar, getString(R.string.newTicketTitle));
            presenter = new PresenterListMessagesImpl(this, 0);
        } else {
            initToolbar(R.id.toolbar, "#" + name);
            presenter = new PresenterListMessagesImpl(this, Integer.valueOf(name));
        }


        ImageButton ib_newMessage = (ImageButton) findViewById(R.id.ib_newMessage);
        assert ib_newMessage != null;
        ib_newMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.sendMessage(etNewMessage.getText().toString().trim());
            }
        });

    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(br);
        super.onDestroy();
    }

    @Override
    public void initAdapter(List<ModelItemMessage> groups) {
        mAdapter = new AdapterListMessages(groups);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void initPhotoAdapter(List<String> list, AdapterPhoto.ItemClickListener listener) {
        adapterPhoto = new AdapterPhoto(list, listener);
        mRecyclerViewAddPhoto.setAdapter(adapterPhoto);
    }

    @Override
    public void addMessage(String text, List<String> photos) {
        if (mAdapter != null) {
            mAdapter.addMessage(text, photos);
            etNewMessage.setText("");
            mRecyclerView.scrollToPosition(mAdapter.getItemCount() - 1);
        } else {
            Toast.makeText(this, getString(R.string.notGetServerData), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void addSupportMessage(String text) {
        mAdapter.addSupportMessage(text);
        mRecyclerView.scrollToPosition(mAdapter.getItemCount() - 1);
    }

    @Override
    public void initToolbar(String name) {
        initToolbar(R.id.toolbar, "#" + name);
    }

    @Override
    public void openIntentPhoto() {
        startGallery();
    }

    @Override
    public void addPhotoToAdapter(String string) {
        adapterPhoto.appPhoto(string);
    }

    @Override
    public void showPhotoToAdapter(boolean isShow) {
        if (isShow) {
            mRecyclerViewAddPhoto.setVisibility(View.VISIBLE);
        } else {
            mRecyclerViewAddPhoto.setVisibility(View.GONE);
        }
    }


    public void clickButtons(View view) {

        switch (view.getId()) {
            case R.id.ib_newMessage:
//                presenter.sendMessage(etNewMessage.getText().toString().trim());
                break;

            case R.id.iv_newMessage_AddPhoto:
                presenter.addPhoto();
                break;

            case R.id.button_reqError:
                presenter.errorRefresh();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK
                && (requestCode == ChooserType.REQUEST_PICK_PICTURE || requestCode == ChooserType.REQUEST_CAPTURE_PICTURE)) {
            imageChooserManager.submit(requestCode, data);
            showPhotoToAdapter(true);
        }
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    private void chooseImage() {
        imageChooserManager = new ImageChooserManager(this,
                ChooserType.REQUEST_PICK_PICTURE, true);
        Bundle bundle = new Bundle();
        bundle.putBoolean(Intent.EXTRA_ALLOW_MULTIPLE, true);
        imageChooserManager.setExtras(bundle);
        imageChooserManager.setImageChooserListener(new ImageChooserListener() {
            @Override
            public void onImageChosen(ChosenImage chosenImage) {
                originalFilePath = chosenImage.getFilePathOriginal();
                thumbnailSmallFilePath = chosenImage.getFileThumbnailSmall();
                presenter.resultIntent(originalFilePath, thumbnailSmallFilePath);
            }

            @Override
            public void onError(String s) {
                Log.deb("error" + s);
            }

            @Override
            public void onImagesChosen(ChosenImages chosenImages) {

                for (int i = 0; i < chosenImages.size(); i++) {
                    onImageChosen(chosenImages.getImage(i));
                }

            }
        });
        imageChooserManager.clearOldFiles();
        try {
            imageChooserManager.choose();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void startGallery() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            final List<String> permissionsList = new ArrayList<>();
            addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE);

            if (permissionsList.size() > 0) {
                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            } else {
                chooseImage();
            }
        } else {
            chooseImage();
        }


    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean addPermission(List<String> permissionsList, String permission) {
        if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            // Check for Rationale Option
            if (!shouldShowRequestPermissionRationale(permission))
                return false;
        }
        return true;
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<>();
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for ACCESS_FINE_LOCATION
                if (perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    chooseImage();
                } else {
                    Toast.makeText(this, R.string.notPermissionsToGallery, Toast.LENGTH_SHORT).show();
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {

            Intent upIntent = NavUtils.getParentActivityIntent(this);
            if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                // This activity is NOT part of this app's task, so create a new task
                // when navigating up, with a synthesized back stack.
                TaskStackBuilder.create(this)
                        // Add all of this activity's parents to the back stack
                        .addNextIntentWithParentStack(upIntent)
                        // Navigate up to the closest parent
                        .startActivities();
            } else {
                // This activity is part of this app's task, so simply
                // navigate up to the logical parent activity.
                NavUtils.navigateUpTo(this, upIntent);
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}