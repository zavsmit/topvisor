package ru.handh.topvisor.ui.bank;

/**
 * Created by sergey on 07.04.16.
 * презентер экрана банка
 */
public interface PresenterBank {
    void init();

    void clickInHeader(int idView);

    void newLoadList();

    void payClick();
}
