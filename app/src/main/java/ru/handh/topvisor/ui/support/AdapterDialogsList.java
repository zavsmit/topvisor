package ru.handh.topvisor.ui.support;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.network.model.ModelTicket;
import ru.handh.topvisor.utils.Utils;

/**
 * Created by sergey on 31.03.16.
 */
public class AdapterDialogsList extends RecyclerSwipeAdapter<AdapterDialogsList.ViewHolder> {
    private List<ModelTicket> mDataset;
    private ItemClickListener mListener;
    private Context context;

    public AdapterDialogsList(List<ModelTicket> list, ItemClickListener listener, Context context) {
        mDataset = list;
        mListener = listener;
        this.context = context;
    }

    @Override
    public AdapterDialogsList.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_support, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int positios) {


        Calendar cal = Calendar.getInstance();
        int dayToday = cal.get(Calendar.DAY_OF_MONTH);
        int monthToday = cal.get(Calendar.MONTH);
        int yearToday = cal.get(Calendar.YEAR);

        cal.setTime(mDataset.get(holder.getAdapterPosition()).getTime());

        String date;
        if (dayToday == cal.get(Calendar.DAY_OF_MONTH) && monthToday == cal.get(Calendar.MONTH) && yearToday == cal.get(Calendar.YEAR)) {
            date = String.format(Locale.US, "%02d:%02d", cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE));
        } else if (cal.get(Calendar.YEAR) == yearToday) {
            date = cal.get(Calendar.DAY_OF_MONTH) + " " + getMonthForInt(cal.get(Calendar.MONTH));
        } else {
            int month = cal.get(Calendar.MONTH) + 1;
            date = String.format(Locale.US, "%2d.%02d", cal.get(Calendar.DAY_OF_MONTH), month) + "." + cal.get(Calendar.YEAR);
        }


        holder.date.setText(date);


        boolean isClose = mDataset.get(holder.getAdapterPosition()).getStatus().equals("1");

        if (isClose) {
            holder.title.setTextColor(Utils.getCustomColor(holder.title.getContext(), R.color.textGray));
            holder.description.setTextColor(Utils.getCustomColor(holder.title.getContext(), R.color.textGray));

            holder.status.setText(R.string.closed);
            holder.status.setTextColor(Utils.getCustomColor(holder.title.getContext(), R.color.textRed));
        } else {
            //если не закрыт диалог
            holder.title.setTextColor(Utils.getCustomColor(holder.title.getContext(), R.color.textBlack));
            holder.description.setTextColor(Utils.getCustomColor(holder.title.getContext(), R.color.textBlack));
            holder.status.setText("");
        }


        SpannableString spanTitle = new SpannableString("#" + mDataset.get(holder.getAdapterPosition()).getId());
        SpannableString spanDescription = new SpannableString(mDataset.get(holder.getAdapterPosition()).getText());

        if (!isClose && mDataset.get(holder.getAdapterPosition()).getOwner_readed().equals("0")) {
            spanTitle.setSpan(new StyleSpan(Typeface.BOLD), 0, spanTitle.length(), 0);
            spanDescription.setSpan(new StyleSpan(Typeface.BOLD), 0, spanDescription.length(), 0);


            holder.status.setText(context.getString(R.string.newStr));
            holder.status.setTextColor(Utils.getCustomColor(holder.title.getContext(), R.color.project_green));
        }


        holder.title.setText(spanTitle);
        holder.description.setText(spanDescription);

        holder.rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.clickItem(holder.getAdapterPosition());
            }
        });

    }

    private String getMonthForInt(int num) {
        String month = "wrong";
        DateFormatSymbols dfs = new DateFormatSymbols();
        String[] months = dfs.getMonths();
        if (num >= 0 && num <= 11) {
            month = months[num];
        }
        return month;
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe_project_request;
    }

    public interface ItemClickListener {
        void clickItem(int idSelected);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView title, description, date, status;
        public RelativeLayout rl;

        public ViewHolder(View v) {
            super(v);
            title = (TextView) itemView.findViewById(R.id.tv_itemSupport_title);
            description = (TextView) itemView.findViewById(R.id.tv_itemSupport_description);
            date = (TextView) itemView.findViewById(R.id.tv_itemSupport_date);
            status = (TextView) itemView.findViewById(R.id.tv_itemSupport_status);
            rl = (RelativeLayout) itemView.findViewById(R.id.rl_itemSupport);
        }
    }
}
