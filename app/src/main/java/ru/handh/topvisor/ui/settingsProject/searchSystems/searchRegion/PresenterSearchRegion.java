package ru.handh.topvisor.ui.settingsProject.searchSystems.searchRegion;

/**
 * Created by sergey on 25.03.16.
 */
public interface PresenterSearchRegion {
    void loadListRegion(String term);
    void clickEnter();
}
