package ru.handh.topvisor.ui.listResultSelected;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ViewFlipper;

import java.util.ArrayList;
import java.util.List;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.model.DateItem;
import ru.handh.topvisor.ui.base.ParentBackActivity;

/**
 * Created by sergey on 17.03.16.
 */
public class ActivityListResult extends ParentBackActivity implements ViewListResult {

    private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_yandex_inspect);

        mRecyclerView = (RecyclerView) findViewById(R.id.recicleView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        viewFlipperMain = (ViewFlipper) findViewById(R.id.viewFlipper);
        showData();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabSearchSystem);
        fab.setVisibility(View.GONE);

        ArrayList<DateItem> dateItemArrayList = new ArrayList<>();
        String title = "";
        String idSearcher = "";
        String idProject = "";
        int iSearcher = 0;
        String idCurrentRegion = "";
        int type = 0;

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            dateItemArrayList = extras.getParcelableArrayList("dateItemArrayList");
            title = extras.getString("name");
            idSearcher = extras.getString("idSearcher");
            idProject = extras.getString("idProject");
            iSearcher = extras.getInt("iSearcher");
            idCurrentRegion = extras.getString("idCurrentRegion");
            type = extras.getInt("type");
        }




        initToolbar(R.id.toolbar, title);
        PresenterListResult presenter = new PresenterListResultImpl(this,
                dateItemArrayList,
                idSearcher,
                idProject,
                iSearcher,
                idCurrentRegion,
                type);
    }

    @Override
    public void initAdapter(List<DateItem> groups, AdapterListResult.ItemGroupClickListener listener) {
        RecyclerView.Adapter mAdapter = new AdapterListResult(groups, listener);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void selectGroup(int idSelected) {
        finish();
    }
}

