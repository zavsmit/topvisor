package ru.handh.topvisor.ui.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import ru.handh.topvisor.R;
import ru.handh.topvisor.ui.main.ActivityMainView;
import ru.handh.topvisor.ui.project.ActivityViewProject;

/**
 * Created by sergey on 14.02.16.
 * дилог добавления нового домена
 */
public class NewTicketDialog extends DialogFragment {

    public final static int NEW_DOMEN = 0;
    public final static int NEW_REQUEST = 1;

    public EditText name;
    public String groupId;
    private DialogResult mListener;
    private String title, hint, textToast;

    public NewTicketDialog() {
    }

    public static NewTicketDialog newInstance(int typeDialog, String groupId) {
        NewTicketDialog f = new NewTicketDialog();

        Bundle args = new Bundle();
        args.putInt("typeDialog", typeDialog);
        args.putString("groupId", groupId);
        f.setArguments(args);

        return f;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        groupId = getArguments().getString("groupId");
        try {

            switch (getArguments().getInt("typeDialog")) {
                case NEW_DOMEN:
                    title = getActivity().getString(R.string.newDomen);
                    hint = getActivity().getString(R.string.writeDimen);
                    textToast = getActivity().getString(R.string.writeNameDomen);
                    mListener = (DialogResult) ((ActivityMainView) activity).presenter;
                    break;
                case NEW_REQUEST:
                    title = activity.getString(R.string.newRequest);
                    hint = activity.getString(R.string.writeRequest);
                    textToast = activity.getString(R.string.writeNameRequest);
                    mListener = (DialogResult) ((ActivityViewProject) activity).presenter;
                    break;
            }


        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_new_ticket, null);

        name = (EditText) view.findViewById(R.id.et_newDialog_ticket);
        name.setHint(hint);

        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
        adb.setView(view);

        adb.setTitle(title)
                .setPositiveButton(getActivity().getString(R.string.add), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        if (name.getText().toString().isEmpty()) {
                            Toast.makeText(getActivity(), textToast, Toast.LENGTH_SHORT).show();
                            return;
                        }
                        mListener.onDialogPositiveClick(NewTicketDialog.this);
                    }
                })
                .setNegativeButton(getActivity().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mListener.onDialogNegativeClick(NewTicketDialog.this);
                    }
                });


        return adb.create();
    }

}

