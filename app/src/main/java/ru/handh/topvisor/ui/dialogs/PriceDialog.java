package ru.handh.topvisor.ui.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import ru.handh.topvisor.R;
import ru.handh.topvisor.ui.main.ActivityMainView;

/**
 * Created by sergey on 16.02.16.
 * диалог для показа стоимости провеки позиции
 */
public class PriceDialog extends DialogFragment implements ActivityMainView.SendDataToDialog {

    public TextView description;
    public ProgressBar progressBar;
    private DialogResult mListener;

    public PriceDialog() {
    }

    public static PriceDialog newInstance(String name) {
        PriceDialog f = new PriceDialog();

        Bundle args = new Bundle();
        args.putString("name", name);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (DialogResult) ((ActivityMainView) activity).presenter;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_desription_progress, null);

        description = (TextView) view.findViewById(R.id.tv_dialog_description);
        progressBar = (ProgressBar) view.findViewById(R.id.progress_dialog_description);


        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
        adb.setView(view);

        adb.setTitle(getContext().getString(R.string.inspectionPisitions) + getArguments().getString("name") + "\"?")
                .setPositiveButton(getContext().getString(R.string.inspect), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mListener.onDialogPositiveClick(PriceDialog.this);
                    }
                })
                .setNegativeButton(getActivity().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mListener.onDialogNegativeClick(PriceDialog.this);
                    }
                });


        return adb.create();
    }


    @Override
    public void showPrice(double price, double xmlPrice) {
        progressBar.setVisibility(View.GONE);
        description.setVisibility(View.VISIBLE);
        description.setText(getContext().getString(R.string.debitedMoney) + " " + price);
    }
}
