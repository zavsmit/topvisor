package ru.handh.topvisor.ui.settingsProject.searchSystems.searchRegion;

import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.network.model.ModelSearchRegion;

/**
 * Created by sergey on 25.03.16.
 */
public class AdapterSearchRegion extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ItemSearchRegionClickListener mListener;
    private List<ModelSearchRegion> dataset;

    public AdapterSearchRegion(ItemSearchRegionClickListener listener) {
        dataset = new ArrayList<>();
        mListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new ViewHolderProject(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_search_region, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {


        final ViewHolderProject mHolder = ((ViewHolderProject) holder);

        ModelSearchRegion region = dataset.get(position);


        mHolder.text.setText(region.getName());


        if (region.getCountryCode().isEmpty()) {
            mHolder.imageView.setVisibility(View.INVISIBLE);
        } else {
            mHolder.imageView.setVisibility(View.VISIBLE);
            String urlImage = "https://topvisor.ru/images/common/flags/" + region.getCountryCode() + ".png";
            Picasso.with(mHolder.imageView.getContext()).load(urlImage).into(mHolder.imageView);
        }


        if (position == dataset.size() - 1) {
            mHolder.divider.setVisibility(View.GONE);
        } else {
            mHolder.divider.setVisibility(View.VISIBLE);
        }

        mHolder.rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mHolder.checkBox.setChecked(!isSelected(mHolder));


            }
        });

        mHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mListener.clickItem(dataset.get(position), isChecked);
            }
        });

    }

    private boolean isSelected(ViewHolderProject mHolder){
        return mHolder.checkBox.isChecked();
    }

    public void notifyAllData(List<ModelSearchRegion> list, String search) {
        dataset.clear();
        dataset.addAll(list);
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return dataset.size();
    }


    public interface ItemSearchRegionClickListener {
        void clickItem(ModelSearchRegion region, boolean isAdd);
    }

    public class ViewHolderProject extends RecyclerView.ViewHolder {
        public TextView text;
        public ImageView imageView;
        public View divider;
        public AppCompatCheckBox checkBox;
        public RelativeLayout rl;

        public ViewHolderProject(View v) {
            super(v);
            text = (TextView) itemView.findViewById(R.id.tv_searchRegion);
            imageView = (ImageView) itemView.findViewById(R.id.iv_searchRegion);
            divider = itemView.findViewById(R.id.v_searchRegion);
            checkBox = (AppCompatCheckBox) itemView.findViewById(R.id.ch_searchRegion);
            rl = (RelativeLayout) itemView.findViewById(R.id.rl_searchRegion);
        }
    }
}