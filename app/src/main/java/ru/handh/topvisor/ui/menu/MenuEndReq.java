package ru.handh.topvisor.ui.menu;

import ru.handh.topvisor.data.network.model.ModelList;
import ru.handh.topvisor.data.network.model.ModelTicket;
import ru.handh.topvisor.data.network.model.ModelUserData;
import ru.handh.topvisor.data.network.model.ReturnResponce;

/**
 * Created by sergey on 10.04.16.
 */
public interface MenuEndReq {
    void getTicketsResponce(ReturnResponce<ModelList<ModelTicket>> result);

    void getUserDataResponce(ReturnResponce<ModelUserData> result);

}
