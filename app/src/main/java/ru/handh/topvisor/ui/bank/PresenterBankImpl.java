package ru.handh.topvisor.ui.bank;


import android.content.Context;
import android.support.v4.app.DialogFragment;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.DataManager;
import ru.handh.topvisor.data.database.ItemProject;
import ru.handh.topvisor.data.model.SearcherItemDialog;
import ru.handh.topvisor.data.network.model.ModelBankPrice;
import ru.handh.topvisor.data.network.model.ModelItemBank;
import ru.handh.topvisor.data.network.model.ModelList;
import ru.handh.topvisor.data.network.model.ReturnResponce;
import ru.handh.topvisor.ui.dialogs.AddListPSDialog;
import ru.handh.topvisor.ui.dialogs.DialogResult;

/**
 * Created by sergey on 07.04.16.
 * презентр экрана банка
 */
public class PresenterBankImpl implements
        PresenterBank,
        BankEndReq,
        AdapterBank.ItemClickListener,
        DateSet,
        DialogResult {

    public final static int START_DATE_DIALOG = 0;
    public final static int END_DATE_DIALOG = 1;
    private int page = 1;
    private Date startDate, endDate;
    private ViewBank mView;
    private ArrayList<SearcherItemDialog> listProjects;
    private SearcherItemDialog selectedProject;
    private boolean isNewReq = true;

    public PresenterBankImpl(ViewBank view, Context context) {
        mView = view;
        startDate = DataManager.getDB().getUserStartDate();
        endDate = new Date();
        mView.initHeader(startDate, endDate);

        mView.initAdapter(new ArrayList<ModelItemBank>(), this);


        listProjects = new ArrayList<>();
        listProjects.add(new SearcherItemDialog("-1", context.getString(R.string.allProjects)));
        selectedProject = listProjects.get(0);
        List<ItemProject> projectList = DataManager.getDB().getProjectsStartDate();

        for (int i = 0; i < projectList.size(); i++) {
            listProjects.add(new SearcherItemDialog(projectList.get(i).getId(), projectList.get(i).getName()));
        }
    }


    public void clickInHeader(int idView) {
        switch (idView) {
            case R.id.ll_bank_project:
                mView.showRegionsDialog(listProjects);
                break;
            case R.id.tv_bank_startDate:
                mView.showDateDialog(startDate, START_DATE_DIALOG);
                break;
            case R.id.tv_bank_endDate:
                mView.showDateDialog(endDate, END_DATE_DIALOG);
                break;
        }
    }

    @Override
    public void newLoadList() {
        ++page;
        loadList(isNewReq);
    }


    private void loadList(boolean isNew) {

        if (isNew) {
            page = 1;
            mView.showProgress();
        }

        if (selectedProject.getIdSearcher().equals("-1")) {
            DataManager.getBankRest().getBankListDate(page, this, makeDateList());
        } else {
            DataManager.getBankRest().getBankListAllParam(page, this, selectedProject.getIdSearcher(), makeDateList());
        }

    }

    @Override
    public void payClick() {
        mView.openPay();
    }

    @Override
    public void init() {
        loadList(isNewReq);
        DataManager.getBankRest().getBankPriceDate(this, makeDateList());
    }

    @Override
    public void listBanksResponce(ReturnResponce<ModelList<ModelItemBank>> result) {
        if (!result.isFailure()) {
            List<ModelItemBank> list = result.getResult().getRows();

            if (list == null) {
                mView.showEmpty(true);
            } else {
                mView.showEmpty(false);
                mView.addDataToAdapter(list, isNewReq);
            }

            if (list != null && page == 1 && list.size() == 0) {
                mView.showEmpty(true);
            }

            isNewReq = false;
            mView.showData();
        } else {
            mView.showError();
        }


    }

    @Override
    public void priceBanksResponce(ReturnResponce<List<ModelBankPrice>> result) {

        if (!result.isFailure()) {
            List<ModelBankPrice> list = result.getResult();
            mView.initFooter(list.get(0).isSum(), list.get(list.size() - 1).isSum());
        }

    }

    @Override
    public void clickItem(String text) {
        mView.showDialogDiscription(text);
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        if (dialog instanceof AddListPSDialog) {

            int selectDialogPosition = ((AddListPSDialog) dialog).mPosition;

            selectedProject = listProjects.get(selectDialogPosition);
            mView.hideAddPSDialog();
            mView.setProjectname(selectedProject.getNameSearcher());
            loadList(true);

        }
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
    }

    @Override
    public void setDate(int year, int monthOfYear, int dayOfMonth, int type) {
        ++monthOfYear;

        String string = dayOfMonth + "." + monthOfYear + "." + year;
        DateFormat format = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH);
        Date date = null;
        try {
            date = format.parse(string);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        switch (type) {
            case START_DATE_DIALOG:
                if (date != null && !date.after(endDate)) {
                    startDate = date;
                }
                break;
            case END_DATE_DIALOG:
                if (date != null && !date.after(new Date())) {
                    endDate = date;
                }
                break;
        }

        mView.initHeader(startDate, endDate);

        isNewReq = true;
        loadList(isNewReq);
        DataManager.getBankRest().getBankPriceDate(this, makeDateList());
    }

    private List<String> makeDateList() {
        List<String> list = new ArrayList<>();


        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

        format.format(endDate);
        list.add(">" + format.format(startDate));
        list.add("<" + format.format(endDate));

        return list;
    }

}
