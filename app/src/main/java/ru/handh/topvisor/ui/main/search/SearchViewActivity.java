package ru.handh.topvisor.ui.main.search;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.DataManager;
import ru.handh.topvisor.data.network.model.currentProject.Row;
import ru.handh.topvisor.ui.base.ParentBackActivity;
import ru.handh.topvisor.ui.project.ActivityViewProject;

/**
 * Created by sergey on 15.02.16.
 * активити с поиском по проектам
 */
public class SearchViewActivity extends ParentBackActivity implements SearchView {

    private RecyclerView.Adapter mAdapter;
    private PresenterSearchMain presenter;
    private RecyclerView mRecyclerView;
    private EditText search;
    private TextView tvEmpty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        ArrayList<Row> dataset;
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            dataset = extras.getParcelableArrayList("EXTRA_DATA_MAIN");
        } else {
            dataset = new ArrayList<>();
        }

        presenter = new PresenterSearchMainImpl(this, dataset);

        initToolbar(R.id.toolbar_search);

        search = (EditText) findViewById(R.id.et_search);
        tvEmpty = (TextView) findViewById(R.id.tv_empty_search);
        mRecyclerView = (RecyclerView) findViewById(R.id.recicleView_search);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mAdapter = new AdapterSearch(presenter.getDataEnd(), (PresenterSearchMainImpl) presenter, DataManager.getPref().getUserId(), "", this);
        mRecyclerView.setAdapter(mAdapter);
        presenter.changedEndData("");

        if (presenter.isAddListener()) {

            search.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    presenter.changedEndData(s.toString());
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }

    }

    @Override
    public void changeList(ArrayList<Row> myDataset, String s) {
        ((AdapterSearch) mAdapter).notifyAllData(myDataset, s);
    }

    @Override
    public void showEmpty(boolean isEmpty) {
        if (isEmpty) {
            tvEmpty.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        } else {
            tvEmpty.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void openNewActivity(Row item) {
        Intent intent = new Intent(this, ActivityViewProject.class);
        intent.putExtra("row", item);
        startActivity(intent);
    }
}
