package ru.handh.topvisor.ui.project.selectDate;

/**
 * Created by sergey on 15.03.16.
 */
public interface PresenterSelectDate {
    void returnSelectItem(int positionSelected, int typeSelect);
    void close();
}
