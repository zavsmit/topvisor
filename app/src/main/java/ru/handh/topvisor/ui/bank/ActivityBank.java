package ru.handh.topvisor.ui.bank;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.DataManager;
import ru.handh.topvisor.data.model.SearcherItemDialog;
import ru.handh.topvisor.data.network.model.ModelItemBank;
import ru.handh.topvisor.ui.dialogs.AddListPSDialog;
import ru.handh.topvisor.ui.dialogs.DateDialog;
import ru.handh.topvisor.ui.dialogs.TextDialog;
import ru.handh.topvisor.ui.menu.ParentLeftMenu;
import ru.handh.topvisor.ui.pay.ActivityRefill;
import ru.handh.topvisor.utils.Constants;

/**
 * Created by sergey on 07.04.16.
 * активити экрана транзакция (банк)
 */
public class ActivityBank extends ParentLeftMenu implements ViewBank {
    public PresenterBank presenter;
    protected ViewFlipper viewFlipperMain;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    private RecyclerView mRecyclerView;
    private TextView footerAdd, footerDec, project, startDate, endDate, emptyTransaction;
    private LinearLayoutManager mLayoutManager;
    private boolean loading = true;
    private AdapterBank mAdapter;
    private AddListPSDialog addListPSDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mRecyclerView = (RecyclerView) findViewById(R.id.recicleViewBank);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        viewFlipperMain = (ViewFlipper) findViewById(R.id.viewFlipper);
        footerAdd = (TextView) findViewById(R.id.tv_bank_footer_add);
        footerDec = (TextView) findViewById(R.id.tv_bank_footer_dec);
        project = (TextView) findViewById(R.id.tv_bank_project);
        startDate = (TextView) findViewById(R.id.tv_bank_startDate);
        endDate = (TextView) findViewById(R.id.tv_bank_endDate);
        emptyTransaction = (TextView) findViewById(R.id.tv_transaction_empty);

        presenter = new PresenterBankImpl(this, this);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });


        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            mAdapter.showProgress(true);
                            presenter.newLoadList();
                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        });

    }

    @Override
    protected void initView() {
        isMainScreen = false;
        title = getString(R.string.bank);
        LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout v = (LinearLayout) vi.inflate(R.layout.activity_bank, null);
        drawer.addView(v, 0, new ViewGroup.LayoutParams(DrawerLayout.LayoutParams.MATCH_PARENT, DrawerLayout.LayoutParams.MATCH_PARENT));

    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.init();
    }

    public void clickButtons(View view) {

        switch (view.getId()) {
            case R.id.button_reqError:
                presenter.init();
                break;
        }
    }


    public void initFooter(double inc, double dec) {

        String server = DataManager.getPref().getServerShort();

        String symbol = "";

        if (server.equals(Constants.RU)) {

            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                symbol = getString(R.string.valuteSimbol) + " ";
            } else {
                footerAdd.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_rouble_green_vector), null, null, null);
                footerDec.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_rouble_red_vector), null, null, null);
            }

        } else {
            symbol = getString(R.string.simbolUS) + " ";
        }


        footerAdd.setText(symbol + inc);
        footerDec.setText(symbol + dec);
    }

    @Override
    public void initHeader(Date startD, Date endD) {

        DateFormat df = new SimpleDateFormat("dd.MM.yy", Locale.US);

        startDate.setText(df.format(startD));
        endDate.setText(df.format(endD));
    }


    public void bankClick(View view) {
        presenter.clickInHeader(view.getId());
    }

    @Override
    public void initAdapter(List<ModelItemBank> groups, AdapterBank.ItemClickListener listener) {
        mAdapter = new AdapterBank(groups, listener);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void addDataToAdapter(List<ModelItemBank> list, boolean isNew) {
        mAdapter.showProgress(false);
        mAdapter.addData(list, isNew);
        loading = true;
    }

    public void showData() {
        if (viewFlipperMain.getDisplayedChild() != Constants.VIEW_CONTENT)
            viewFlipperMain.setDisplayedChild(Constants.VIEW_CONTENT);
    }

    public void showProgress() {
        if (viewFlipperMain.getDisplayedChild() != Constants.VIEW_PROGRESS)
            viewFlipperMain.setDisplayedChild(Constants.VIEW_PROGRESS);
    }

    public void showError() {
        if (viewFlipperMain.getDisplayedChild() != Constants.VIEW_ERROR)
            viewFlipperMain.setDisplayedChild(Constants.VIEW_ERROR);
    }

    @Override
    public void showEmpty(boolean isShow) {
        if (isShow) {
            mRecyclerView.setVisibility(View.GONE);
            emptyTransaction.setVisibility(View.VISIBLE);
        } else {
            mRecyclerView.setVisibility(View.VISIBLE);
            emptyTransaction.setVisibility(View.GONE);
        }
    }

    public void showDialogDiscription(String text) {
        DialogFragment dialog = TextDialog.newInstance(text);
        String dialogTag = "TextDialog";
        dialog.show(getSupportFragmentManager(), dialogTag);
    }

    @Override
    public void showDateDialog(Date date, int type) {
        DateDialog dateDialog = DateDialog.newInstance(date, type);
        dateDialog.show(getSupportFragmentManager(), "dateDialog");
    }

    @Override
    public void showRegionsDialog(ArrayList<SearcherItemDialog> list) {
        addListPSDialog = AddListPSDialog.newInstance(list, getString(R.string.selectProject));
        addListPSDialog.show(getSupportFragmentManager(), "addListPSDialog");
    }

    @Override
    public void hideAddPSDialog() {
        addListPSDialog.dismiss();
    }

    public void setProjectname(String name) {
        project.setText(name);
    }

    @Override
    public void openPay() {
        Intent intent = new Intent(this, ActivityRefill.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_button, menu);
        MenuItem bedMenuItem = menu.findItem(R.id.action_button);
        bedMenuItem.setTitle(R.string.refill);

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_button) {
            presenter.payClick();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}


