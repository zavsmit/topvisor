package ru.handh.topvisor.ui.settingsProject.interval;

import java.util.ArrayList;

/**
 * Created by sergey on 16.03.16.
 */
public interface ViewIntervalInspect {
    void openChangeDate(String hour, ArrayList<String> listDate, String idProject, String autoCond);
    void openChangeYandex(int selectPosition, ArrayList<String> list, String idProject);
    void setChecked(int position);
    void changeYandexDiscription(String text);
    void changeDateDiscription(String text);
}
