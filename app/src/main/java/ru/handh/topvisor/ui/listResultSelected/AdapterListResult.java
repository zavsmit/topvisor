package ru.handh.topvisor.ui.listResultSelected;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;

import java.util.List;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.model.DateItem;

/**
 * Created by sergey on 17.03.16.
 */
public class AdapterListResult extends RecyclerSwipeAdapter<AdapterListResult.ViewHolder> {
    private List<DateItem> mDataset;
    private ItemGroupClickListener mListener;

    public AdapterListResult(List<DateItem> myDataset, ItemGroupClickListener listener) {
        mDataset = myDataset;
        mListener = listener;
    }

    @Override
    public AdapterListResult.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main_settings, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {


        holder.text.setText(mDataset.get(holder.getAdapterPosition()).getName());

        if (mDataset.get(holder.getAdapterPosition()).isEnable()) {
            holder.radioButton.setChecked(true);
        } else {
            holder.radioButton.setChecked(false);
        }

        holder.ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redraveList(holder.getAdapterPosition());
            }
        });

        holder.radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redraveList(holder.getAdapterPosition());
            }
        });


    }

    private void redraveList(int position) {

        mListener.clickItem(position);
        for (int i = 0; i < mDataset.size(); i++) {

            if (i == position) {
                mDataset.get(i).setIsEnable(true);
            } else {
                mDataset.get(i).setIsEnable(false);
            }

        }
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe_project_request;
    }

    public interface ItemGroupClickListener {
        void clickItem(int idSelected);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView text;
        public RadioButton radioButton;
        public LinearLayout ll;

        public ViewHolder(View v) {
            super(v);
            text = (TextView) itemView.findViewById(R.id.tv_item_mainSettings);
            radioButton = (RadioButton) itemView.findViewById(R.id.rb_item_mainSettings);
            ll = (LinearLayout) itemView.findViewById(R.id.ll_item_mainSettings);
        }
    }

}