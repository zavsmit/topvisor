package ru.handh.topvisor.ui.main;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.network.model.currentProject.Row;

/**
 * Created by sergey on 10.02.16.
 * адаптер главного экрана
 */
public class AdapterMain extends RecyclerSwipeAdapter<AdapterMain.ViewHolder> {
    private List<Row> mDataset;
    private ItemSwipeClickListener mListener;
    private int percent;

    public AdapterMain(List<Row> myDataset, ItemSwipeClickListener listener) {
        mDataset = myDataset;
        mListener = listener;
    }

    @Override
    public AdapterMain.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.text.setText(mDataset.get(position).getName());

        DateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm", new Locale("ru"));

        Date today = mDataset.get(position).getUpdate();
        String reportDate = df.format(today);

        holder.refreshDate.setText(reportDate);

        if (position == mDataset.size() - 1) {
            holder.divider.setVisibility(View.GONE);
        } else {
            holder.divider.setVisibility(View.VISIBLE);
        }
        percent = -1;
        if (mDataset.get(position).getPercentOfParse() != null) {

            percent = Integer.valueOf(mDataset.get(position).getPercentOfParse());

            holder.progress.setVisibility(View.VISIBLE);
            holder.progress.setProgress(percent);
            holder.divider.setVisibility(View.INVISIBLE);


            if (percent == 0) {
                holder.percent.setText("...");
            } else {
                holder.percent.setText(percent + "%");
            }

        } else {
            holder.progress.setVisibility(View.GONE);

        }

        if (percent == -1) {
            holder.percent.setText("");
        }

        if (mDataset.get(position).getOn().equals("-1")) {
            holder.refresh.setVisibility(View.GONE);
            holder.trash.setVisibility(View.GONE);
            holder.rename.setVisibility(View.GONE);
            holder.recovery.setVisibility(View.VISIBLE);
        } else {
            holder.refresh.setVisibility(View.VISIBLE);
            holder.trash.setVisibility(View.VISIBLE);
            holder.rename.setVisibility(View.VISIBLE);
            holder.recovery.setVisibility(View.GONE);
        }


        holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);

        holder.content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.clickItem(Integer.valueOf(mDataset.get(position).getId()), mDataset.get(position).getName(), mDataset.size(), position);
            }
        });

        holder.refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (percent == -1) {
                    mListener.clickRefresh(Integer.valueOf(mDataset.get(position).getId()), mDataset.get(position).getName());
                }
                holder.swipeLayout.close();
            }
        });

        holder.rename.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.clickRename(Integer.valueOf(mDataset.get(position).getId()), mDataset.get(position).getName(), position);
                holder.swipeLayout.close();
            }
        });

        holder.trash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.clickTrash(Integer.valueOf(mDataset.get(position).getId()), mDataset.get(position).getName(), position);
                holder.swipeLayout.close();
            }
        });

        holder.recovery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.clickRecovery(Integer.valueOf(mDataset.get(position).getId()), mDataset.get(position).getName(), position);
                holder.swipeLayout.close();
            }
        });


        mItemManger.bindView(holder.itemView, position);


//        if (mDataset.get(holder.getAdapterPosition()).getId() == null) {
//            holder.progressPagination.setVisibility(View.VISIBLE);
//        } else {
//            holder.progressPagination.setVisibility(View.GONE);
//        }

    }

    public void removePosition(int position) {
        mDataset.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mDataset.size());
    }

    public void renamePosition(int position, String name) {
        mDataset.get(position).setName(name);
        notifyItemChanged(position);
        notifyItemRangeChanged(position, mDataset.size());
    }

    public void showProgress(boolean isShow) {
        if (isShow) {
            mDataset.add(new Row());
            notifyItemInserted(mDataset.size() - 1);
        } else {

            if (mDataset.size() != 0) {
                int removePosition = mDataset.size() - 1;
                mDataset.remove(removePosition);
                notifyItemRemoved(removePosition);
            }

        }
    }

    public void addData(List<Row> newData, boolean isNew) {

        if (isNew) {
            mDataset.clear();
        }

        mDataset.addAll(newData);
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe_main;
    }

    public interface ItemSwipeClickListener {
        void clickItem(int idProgect, String name, int size, int position);

        void clickTrash(int idProgect, String name, int position);

        void clickRefresh(int idProgect, String name);

        void clickRename(int idProgect, String name, int position);

        void clickRecovery(int idProgect, String name, int position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public SwipeLayout swipeLayout;
        public TextView text, refreshDate, percent;
        public View divider;
        public LinearLayout trash;
        public LinearLayout rename;
        public LinearLayout refresh;
        public LinearLayout recovery;
        public RelativeLayout content;
        public ProgressBar progress, progressPagination;

        public ViewHolder(View v) {
            super(v);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe_main);

            text = (TextView) itemView.findViewById(R.id.tv_itemMain);
            percent = (TextView) itemView.findViewById(R.id.tv_itemMain_percent);
            refreshDate = (TextView) itemView.findViewById(R.id.tv_item_mainRefershDate);
            divider = itemView.findViewById(R.id.v_itemMain);
            trash = (LinearLayout) itemView.findViewById(R.id.ll_itemMain_trash);
            rename = (LinearLayout) itemView.findViewById(R.id.ll_itemMain_rename);
            refresh = (LinearLayout) itemView.findViewById(R.id.ll_itemMain_refresh);
            recovery = (LinearLayout) itemView.findViewById(R.id.ll_itemMain_recovery);
            content = (RelativeLayout) itemView.findViewById(R.id.rl_itemMain);
            progress = (ProgressBar) itemView.findViewById(R.id.ProgressBar_itemMain);
            progressPagination = (ProgressBar) itemView.findViewById(R.id.tv_itemMain_progress);
        }
    }
}
