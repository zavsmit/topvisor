package ru.handh.topvisor.ui.project.selectGroup;

import java.util.List;

import ru.handh.topvisor.data.network.model.currentProject.Group;

/**
 * Created by sergey on 11.03.16.
 */
public interface ViewGroups {
    void initAdapter(List<Group> groups, AdapterGroups.ItemGroupClickListener listener);

    void selectGroup();
}
