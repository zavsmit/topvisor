package ru.handh.topvisor.ui.project.selectDate;

import android.content.Context;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import ru.handh.topvisor.R;

/**
 * Created by sergey on 29.03.16.
 * экран оплаты
 */
public class AdapterSelectDate extends RecyclerSwipeAdapter<RecyclerView.ViewHolder> {
    public final static int TYPE_LAST_7_DAYS = 0;
    public final static int TYPE_CURRENT_WEEK = 1;
    public final static int TYPE_CURRENT_MONTH = 2;
    public final static int TYPE_LAST_MONTH = 3;
    public final static int TYPE_ONE_DATES = 4;
    public final static int TYPE_TWO_DATES = 5;
    private static final int TYPE_DATE = 0;
    private static final int TYPE_TITLE = 1;
    private static final int TYPE_CHECKED = 2;
    private static final int TYPE_DIVIDER = 3;
    private List<ItemData> dataList;
    private List<String> allDates;
    private List<Integer> listSelected;
    private ItemSearcherClickListener mListener;
    private boolean isLastTwoDate = true;
    private Context context;

    public AdapterSelectDate(List<String> allDates, ItemSearcherClickListener listener, List<Integer> listSelected,
                             Context context) {
        mListener = listener;
        this.allDates = allDates;
        this.listSelected = listSelected;
        this.context = context;
        dataList = new ArrayList<>();

        if (listSelected.get(1) != -1) {
            initDataList(true);
        } else {
            initDataList(false);
        }

    }


    private void initDataList(boolean isTwoDay) {
        isLastTwoDate = isTwoDay;
        dataList.clear();

        if (isTwoDay) {

            for (int i = 0; i < listSelected.size(); i++) {
                String title = "";
                if (i == 0) {
                    title = context.getString(R.string.from);
                } else if (i == 1) {
                    title = context.getString(R.string.before);
                }

                Date date = new Date();
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                DateFormat dfNew = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH);

                try {
                    date = format.parse(allDates.get(listSelected.get(i)));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                String dateS = dfNew.format(date);

                dataList.add(new ItemData(TYPE_DATE, title, dateS, listSelected.get(i)));
            }

            dataList.add(new ItemData(TYPE_DIVIDER));

            dataList.add(new ItemData(TYPE_TITLE, context.getString(R.string.interval)));

            dataList.add(new ItemData(TYPE_CHECKED, context.getString(R.string.last7Days), false, TYPE_LAST_7_DAYS));
            dataList.add(new ItemData(TYPE_CHECKED, context.getString(R.string.currentWeek), false, TYPE_CURRENT_WEEK));
            dataList.add(new ItemData(TYPE_CHECKED, context.getString(R.string.currentMonth), false, TYPE_CURRENT_MONTH));
            dataList.add(new ItemData(TYPE_CHECKED, context.getString(R.string.lastMonth), false, TYPE_LAST_MONTH));

            dataList.add(new ItemData(TYPE_DIVIDER));

            dataList.add(new ItemData(TYPE_TITLE, context.getString(R.string.show)));

            dataList.add(new ItemData(TYPE_CHECKED, context.getString(R.string.oneDate), false, TYPE_ONE_DATES));
            dataList.add(new ItemData(TYPE_CHECKED, context.getString(R.string.twoDate), true, TYPE_TWO_DATES));

//            dataList.add(new ItemData(TYPE_DIVIDER));

        } else {
            dataList.add(new ItemData(TYPE_DATE, context.getString(R.string.selectedDate), allDates.get(listSelected.get(0)), 0));

            dataList.add(new ItemData(TYPE_DIVIDER));

            dataList.add(new ItemData(TYPE_TITLE, context.getString(R.string.show)));

            dataList.add(new ItemData(TYPE_CHECKED, context.getString(R.string.oneDate), true, TYPE_ONE_DATES));
            if (allDates.size() > 1)
                dataList.add(new ItemData(TYPE_CHECKED, context.getString(R.string.twoDate), false, TYPE_TWO_DATES));

//            dataList.add(new ItemData(TYPE_DIVIDER));
        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case TYPE_DATE:
                return new ViewHolderDate(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_date_interval, parent, false));
            case TYPE_TITLE:
                return new ViewHolderTitle(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_title_interval, parent, false));
            case TYPE_CHECKED:
                return new ViewHolderChecked(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_main_settings, parent, false));
            case TYPE_DIVIDER:
                return new ViewHolderDivider(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_divider, parent, false));
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        final ItemData data = dataList.get(position);

        switch (holder.getItemViewType()) {

            case TYPE_DATE:


                final ViewHolderDate mHolderDate = ((ViewHolderDate) holder);
                mHolderDate.title.setText(data.title);
                mHolderDate.date.setText(data.description);

                mHolderDate.ll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mListener.clickDate(data.positionInListSelect, position);
                    }
                });


                break;

            case TYPE_TITLE:

                final ViewHolderTitle mHolderTitle = ((ViewHolderTitle) holder);
                mHolderTitle.title.setText(data.title);

                break;

            case TYPE_CHECKED:

                final ViewHolderChecked mHolderChecked = ((ViewHolderChecked) holder);
                mHolderChecked.text.setText(data.title);
                mHolderChecked.radioButton.setChecked(data.isChecked);

                mHolderChecked.ll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mHolderChecked.radioButton.setChecked(true);
                        int type = data.typeChecked;

                        mListener.clickType(type);

                        if (type == TYPE_ONE_DATES) {
                            clickOneDate(position, type);

                        } else if (type == TYPE_TWO_DATES) {
                            clickTwoDate(position, type);
                        }


                    }
                });

                break;
        }
    }


    private void clickOneDate(int position, int type) {
        mListener.clickDays(type);
        if (!dataList.get(position).isChecked) {
            dataList.get(position).isChecked = true;
            dataList.get(position + 1).isChecked = false;
        }
        initDataList(false);
        notifyDataSetChanged();
    }

    private void clickTwoDate(int position, int type) {
        mListener.clickDays(type);
        if (!dataList.get(position).isChecked) {
            dataList.get(position).isChecked = true;
            dataList.get(position - 1).isChecked = false;
        }

        initDataList(true);
        notifyDataSetChanged();
    }

    public void changeDate() {
        initDataList(isLastTwoDate);
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return dataList.size();
    }


    @Override
    public int getItemViewType(int position) {

        switch (dataList.get(position).type) {
            case TYPE_DATE:
                return TYPE_DATE;
            case TYPE_TITLE:
                return TYPE_TITLE;
            case TYPE_CHECKED:
                return TYPE_CHECKED;
            case TYPE_DIVIDER:
                return TYPE_DIVIDER;
            default:
                return TYPE_DIVIDER;
        }
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe_searcher;
    }


    public interface ItemSearcherClickListener {
        void clickType(int typeSelect);

        void clickDate(int positionInListSelect, int typeSelect);

        void clickDays(int typeSelect);
    }

    public class ViewHolderDate extends RecyclerView.ViewHolder {
        public TextView title, date;
        public LinearLayout ll;

        public ViewHolderDate(View v) {
            super(v);
            title = (TextView) itemView.findViewById(R.id.tv_itemDateInterval_title);
            date = (TextView) itemView.findViewById(R.id.tv_itemDateInterval_date);
            ll = (LinearLayout) itemView.findViewById(R.id.ll_itemDateInterval);
        }
    }

    public class ViewHolderTitle extends RecyclerView.ViewHolder {
        public TextView title;

        public ViewHolderTitle(View v) {
            super(v);
            title = (TextView) itemView.findViewById(R.id.tv_intervalTitle);
        }
    }

    public class ViewHolderChecked extends RecyclerView.ViewHolder {
        public AppCompatRadioButton radioButton;
        public TextView text;
        public LinearLayout ll;

        public ViewHolderChecked(View v) {
            super(v);
            text = (TextView) itemView.findViewById(R.id.tv_item_mainSettings);
            radioButton = (AppCompatRadioButton) itemView.findViewById(R.id.rb_item_mainSettings);
            ll = (LinearLayout) itemView.findViewById(R.id.ll_item_mainSettings);

            text.setTextSize(18);
        }
    }

    public class ViewHolderDivider extends RecyclerView.ViewHolder {
        public ViewHolderDivider(View v) {
            super(v);
        }
    }

    private class ItemData {
        int type, positionInListSelect, typeChecked;
        String title, description;
        boolean isChecked;


        ItemData(int type) {
            this.type = type;
        }

        ItemData(int type, String title) {
            this.type = type;
            this.title = title;
        }

        ItemData(int type, String title, boolean isChecked, int typeChecked) {
            this.type = type;
            this.title = title;
            this.isChecked = isChecked;
            this.typeChecked = typeChecked;
        }

        ItemData(int type, String title, String description, int selectPosition) {
            this.type = type;
            this.description = description;
            this.title = title;
            positionInListSelect = selectPosition;
        }
    }
}
