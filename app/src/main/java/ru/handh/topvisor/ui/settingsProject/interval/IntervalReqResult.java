package ru.handh.topvisor.ui.settingsProject.interval;

import ru.handh.topvisor.data.network.model.ModelResult;
import ru.handh.topvisor.data.network.model.ReturnResponce;

/**
 * Created by sergey on 28.03.16.
 */
public interface IntervalReqResult {
    void changeIntervalResponce(ReturnResponce<ModelResult> result);
}
