package ru.handh.topvisor.ui.bank;

/**
 * Created by sergey on 08.04.16.
 * получение даты из диалога
 */
public interface DateSet {
    void setDate(int year, int monthOfYear, int dayOfMonth, int type);
}
