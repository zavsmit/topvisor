package ru.handh.topvisor.ui.settingsProject.searchSystems.searchRegion;

import java.util.List;

import ru.handh.topvisor.data.network.model.ModelSearchRegion;

/**
 * Created by sergey on 25.03.16.
 */
public interface ViewSearchRegion {

    void changeList(List<ModelSearchRegion> list, String s);

    AdapterSearchRegion initAdapter(AdapterSearchRegion.ItemSearchRegionClickListener listener);

    void showProgress(boolean isShow);

    void showDialog();

    void showResult(int number);

    void showToast(String text);

    void showToast(int textId);

    void end();

    void showEmpty(boolean isEmpty);
}
