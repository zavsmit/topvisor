package ru.handh.topvisor.ui.settingsProject.interval.intervalYandex;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;

import java.util.List;

import ru.handh.topvisor.R;

/**
 * Created by sergey on 28.03.16.
 */
public class AdapterYandexInsert extends RecyclerSwipeAdapter<AdapterYandexInsert.ViewHolder> {
    int selected;
    private List<String> mDataset;
    private ItemGroupClickListener mListener;

    public AdapterYandexInsert(List<String> list, ItemGroupClickListener listener, int selected) {
        mDataset = list;
        mListener = listener;
        this.selected = selected;

    }

    @Override
    public AdapterYandexInsert.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main_settings, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {


        holder.text.setText(mDataset.get(position));

        if (selected == position) {
            holder.radioButton.setChecked(true);
        } else {
            holder.radioButton.setChecked(false);
        }

        holder.ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redraveList(position);
            }
        });

        holder.radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                redraveList(position);
            }
        });


    }

    private void redraveList(int position) {

        mListener.clickItem(position);

        selected = position;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe_project_request;
    }

    public interface ItemGroupClickListener {
        void clickItem(int idSelected);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView text;
        public RadioButton radioButton;
        public LinearLayout ll;

        public ViewHolder(View v) {
            super(v);
            text = (TextView) itemView.findViewById(R.id.tv_item_mainSettings);
            radioButton = (RadioButton) itemView.findViewById(R.id.rb_item_mainSettings);
            ll = (LinearLayout) itemView.findViewById(R.id.ll_item_mainSettings);
        }
    }
}
