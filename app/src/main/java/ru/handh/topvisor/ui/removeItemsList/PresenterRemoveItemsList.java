package ru.handh.topvisor.ui.removeItemsList;

/**
 * Created by sergey on 21.03.16.
 */
public interface PresenterRemoveItemsList {
    void clickAdd();
    void onResume();
}
