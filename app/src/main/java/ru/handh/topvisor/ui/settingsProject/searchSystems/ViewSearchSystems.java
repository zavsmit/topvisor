package ru.handh.topvisor.ui.settingsProject.searchSystems;

import android.view.ActionMode;

import java.util.ArrayList;

import io.realm.RealmList;
import ru.handh.topvisor.data.database.project.SearchData;
import ru.handh.topvisor.data.model.SearcherItemDialog;

/**
 * Created by sergey on 17.03.16.
 */
public interface ViewSearchSystems {
    AdapterSearchSystems initAdapter(RealmList<SearchData> listSearch, AdapterSearchSystems.ItemSearcherClickListener listener);
//    void selectRegion(String idRegion, SearchData searcher);
    void selectRegion(String idRegion, String idProject, int position);
    void showHide(boolean isShow);
    ActionMode startMyActionMode(ActionMode.Callback callback);
    void showToast(String text);
    void showAddDialog(ArrayList<SearcherItemDialog> list);
    void hideAddPSDialog();
    void openAddRegion(String idSearcher, String typeSearcher);
    void showEmpty(boolean isShow);
    void showData();
    void showError();
    void showProgress();
}
