package ru.handh.topvisor.ui.settingsProject.searchSystems.typeSystems;

import android.content.Context;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.DataManager;
import ru.handh.topvisor.data.database.project.RegionData;
import ru.handh.topvisor.data.database.project.SearchData;
import ru.handh.topvisor.data.model.DateItem;
import ru.handh.topvisor.ui.settingsProject.searchSystems.AdapterSearchSystems;

/**
 * Created by sergey on 17.03.16.
 */
public class PresenterTypeSystemImpl implements PresenterTypeSystem {

    Map<String, LinkedHashMap<String, String>> mapLangs;
    private ViewTypeSystem mView;
    private SearchData searcher;
    private RegionData region;
    private String projectId;
    private int positionSearch;
    private Context context;


    public PresenterTypeSystemImpl(ViewTypeSystem view, String idRegion, String lang,
                                   String projectId, int positionSearch, Context context) {
        mView = view;
        searcher = DataManager.getDB().getProject(projectId).getSearchers().get(positionSearch);

        region = new RegionData();

        for (int i = 0; i < searcher.getRegions().size(); i++) {
            if (idRegion.equals(searcher.getRegions().get(i).getId())) {
                region = searcher.getRegions().get(i);
                break;
            }
        }

        this.projectId = projectId;
        this.context = context;
        this.positionSearch = positionSearch;


        List<String> stringList = new ArrayList<>();

        while (lang.length() > 0) {
            stringList.add(lang.substring(0, lang.indexOf("</select>") + 9));

            lang = lang.substring(lang.indexOf("</select>") + 9, lang.length());
        }


        mapLangs = new HashMap<>();

        String langCurrent = "";
        for (int i = 0; i < stringList.size(); i++) {
            String parseString = stringList.get(i);

            LinkedHashMap<String, String> map = new LinkedHashMap<>();


            while (parseString.indexOf("</option>") != -1) {
                int startPosition = parseString.indexOf("value=\"") + 7;

                String key = parseString.substring(startPosition, parseString.indexOf("\">", startPosition));
                String value = parseString.substring(parseString.indexOf("\">", startPosition) + 2,
                        parseString.indexOf("</option>", startPosition));


                if (region.getLang().equals(key)) {
                    langCurrent = value;
                }

                map.put(key, value);

                parseString = parseString.substring(parseString.indexOf("</option>", startPosition) + 9,
                        parseString.length());


            }

            String idSearcher = "0";


            switch (i) {
                case 0:
                    idSearcher = AdapterSearchSystems.SEARCHER_GOOGLE;
                    break;
                case 1:
                    idSearcher = AdapterSearchSystems.SEARCHER_YOUTUBE;
                    break;
                case 2:
                    idSearcher = AdapterSearchSystems.SEARCHER_BING;
                    break;
                case 3:
                    idSearcher = AdapterSearchSystems.SEARCHER_YAHOO;
                    break;
            }

            mapLangs.put(String.valueOf(idSearcher), map);
        }


        mView.changeLandDepthDevice(langCurrent, region.getDepth(), region.getDevice());


        switch (searcher.getSearcher()) {
            case AdapterSearchSystems.SEARCHER_YANDEX:
                mView.showItem(ActivityTypeSystem.RESQUEST_CODE_DEPTH);
                break;
            case AdapterSearchSystems.SEARCHER_GOOGLE:
                mView.showItem(ActivityTypeSystem.RESQUEST_CODE_DEPTH);
                mView.showItem(ActivityTypeSystem.RESQUEST_CODE_LANG);
                mView.showItem(ActivityTypeSystem.RESQUEST_CODE_DEVICE);
                break;
            case AdapterSearchSystems.SEARCHER_GO_MAIL:
                break;
            case AdapterSearchSystems.SEARCHER_SPUTNIK:
                break;
            case AdapterSearchSystems.SEARCHER_YOUTUBE:
                break;
            case AdapterSearchSystems.SEARCHER_BING:
                mView.showItem(ActivityTypeSystem.RESQUEST_CODE_LANG);
                break;
            case AdapterSearchSystems.SEARCHER_YAHOO:
                mView.showItem(ActivityTypeSystem.RESQUEST_CODE_LANG);
                break;
            case AdapterSearchSystems.SEARCHER_YANDEX_COM:
                mView.showItem(ActivityTypeSystem.RESQUEST_CODE_DEPTH);
                break;
            case AdapterSearchSystems.SEARCHER_YANDEX_COM_TR:
                mView.showItem(ActivityTypeSystem.RESQUEST_CODE_DEPTH);
                break;
        }


    }

    @Override
    public void clickStartActivity(int typeClick) {
        ArrayList<DateItem> dateItemList = new ArrayList<>();
        boolean isSelect = false;

        switch (typeClick) {
            case ActivityTypeSystem.RESQUEST_CODE_LANG:
                dateItemList.clear();


                LinkedHashMap<String, String> mapL = mapLangs.get(searcher.getSearcher());

                if (mapL == null) {
                    return;
                }

                for (Map.Entry<String, String> entry : mapL.entrySet()) {

                    isSelect = region.getLang().equals(entry.getKey());

                    dateItemList.add(new DateItem(entry.getValue(), isSelect, entry.getKey()));
                }

                mView.startMyActivity(dateItemList, context.getString(R.string.langInterface), typeClick, searcher.getId(), region.getId(),
                        projectId, positionSearch);
                break;
            case ActivityTypeSystem.RESQUEST_CODE_DEPTH:
                dateItemList.clear();

                int depth = 5;

                if (searcher.getSearcher().equals(AdapterSearchSystems.SEARCHER_GOOGLE)) {
                    depth = 10;
                }

                for (int i = 0; i < depth; i++) {


                    isSelect = Integer.valueOf(region.getDepth()) - 1 == i;

                    dateItemList.add(new DateItem(String.valueOf((i + 1) * 100), isSelect));

                }

                mView.startMyActivity(dateItemList, context.getString(R.string.depth), typeClick, searcher.getId(), region.getId(), projectId, positionSearch);
                break;
            case ActivityTypeSystem.RESQUEST_CODE_DEVICE:

                dateItemList.clear();

                for (int i = 0; i < 3; i++) {
                    int type = Integer.valueOf(region.getDevice());

                    isSelect = type == i;

                    String typeDevice = "";
                    switch (i) {
                        case 0:
                            typeDevice = context.getString(R.string.pc);
                            break;
                        case 1:
                            typeDevice = context.getString(R.string.tablet);
                            break;
                        case 2:
                            typeDevice =context.getString(R.string.mobile);
                            break;
                    }

                    dateItemList.add(new DateItem(typeDevice, isSelect));
                }

                mView.startMyActivity(dateItemList, context.getString(R.string.devices), typeClick, searcher.getId(), region.getId(),
                        projectId, positionSearch);
                break;
        }
    }

}
