package ru.handh.topvisor.ui.project.selectRegion;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import io.realm.RealmList;
import ru.handh.topvisor.R;
import ru.handh.topvisor.data.database.project.SearchData;
import ru.handh.topvisor.ui.base.ParentBackActivity;

/**
 * Created by sergey on 25.02.16.
 */
public class ActivitySelectRegion extends ParentBackActivity implements ViewRegions {

    private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_list);


        initToolbar(R.id.toolbarGroups, getString(R.string.regions1));

        mRecyclerView = (RecyclerView) findViewById(R.id.recicleView_Groups);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        String idProject = "";

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            idProject = extras.getString("idProject");
        }
        PresentRegions presenter = new PresentRegionsImpl(this, idProject);
    }

    @Override
    public void initAdapter(RealmList<SearchData> regions, AdapterRegions.ItemRegionClickListener listener) {
        RecyclerView.Adapter mAdapter = new AdapterRegions(regions, listener);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void selectRegion() {
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
    }
}


