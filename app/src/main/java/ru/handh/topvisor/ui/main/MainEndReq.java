package ru.handh.topvisor.ui.main;

import java.util.ArrayList;

import ru.handh.topvisor.data.network.model.ModelPriceForProgects;
import ru.handh.topvisor.data.network.model.ModelResult;
import ru.handh.topvisor.data.network.model.ReturnResponce;
import ru.handh.topvisor.data.network.model.currentProject.Row;

/**
 * Created by sergey on 16.02.16.
 * интерфейс для передачи данных из модели в презетер на экране проектов
 */
public interface MainEndReq {
    void addProjectsResponce(ReturnResponce<ModelResult> result);

    void getProjectsResponce(ReturnResponce<ArrayList<Row>> result);

    void getPriceProgect(ReturnResponce<ModelPriceForProgects> result);

    void getInspectProjectResult(ReturnResponce<ModelResult> result);
}
