package ru.handh.topvisor.ui.menu;

import ru.handh.topvisor.data.network.model.ModelUserData;

/**
 * Created by sergey on 09.04.16.
 */
public interface ViewMenu {

    void showRefreshMenu(boolean isShow);

    void showToast(int idString);

    void showToast(String text);

    void initCountNewMessage(int newMessage);

    void initMenu(ModelUserData modelUserData);

    void closeDtaver();

    void logout();
}
