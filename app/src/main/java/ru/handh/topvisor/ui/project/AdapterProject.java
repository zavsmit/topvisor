package ru.handh.topvisor.ui.project;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;

import java.util.HashMap;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.network.model.projecPhrases.ModelProjectPhrases;
import ru.handh.topvisor.data.network.model.projecPhrases.Phrase;
import ru.handh.topvisor.utils.Constants;
import ru.handh.topvisor.utils.Utils;

/**
 * Created by sergey on 24.02.16.
 * адаптер фрагмента со списком позиций проекта
 */
public class AdapterProject extends RecyclerSwipeAdapter<AdapterProject.ViewHolderProject> {
    private final static int COLUMN_FIRST = 0;
    private final static int COLUMN_SECOND = 1;
    private Context context;
    private int showColumns;
    private HashMap<String, Phrase> mDataset;
    private ProjectSwipeClickListener mListener;
    private int pagePosition;

    public AdapterProject(ModelProjectPhrases myDataset, Context context, ProjectSwipeClickListener listener, int
            showColumns) {
        mDataset = myDataset.getRows().getPhrases();
        pagePosition = (myDataset.getPage() - 1) * Integer.valueOf(Constants.COUNT_IN_PAGE);
        this.context = context;
        this.showColumns = showColumns;
        mListener = listener;

    }

    @Override
    public AdapterProject.ViewHolderProject onCreateViewHolder(ViewGroup parent,
                                                               int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_project_request, parent, false);
        return new ViewHolderProject(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolderProject holder, final int positionInt) {


        final String positionMap = String.valueOf(pagePosition + positionInt);

        holder.disciption.setText(mDataset.get(positionMap).getPhrase());

        changeColor(holder, positionMap, COLUMN_SECOND);
        changeColor(holder, positionMap, COLUMN_FIRST);


        holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);

        holder.trash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                holder.swipeLayout.close();


                if (mDataset.get(positionMap).getId() != null) {
                    mListener.clickTrash(Integer.valueOf(mDataset.get(positionMap).getId()), positionInt);
                }
            }
        });


        mItemManger.bindView(holder.itemView, positionInt);
    }

    public void removePosition(int position) {
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe_project_request;
    }

    private void changeColor(ViewHolderProject holder, String positionMap, int idColumn) {

        RelativeLayout item = holder.rlProjectFirst;
        TextView text = holder.ratingFirst;

        switch (idColumn) {
            case 0:
                text = holder.ratingSecond;
                item = holder.rlProjectSecond;
                break;
            case 1:

                break;
        }

        if (mDataset.size() == 0)
            return;

        if (mDataset.get(positionMap).getDates().size() == 0) {
            holder.rlProjectFirst.setVisibility(View.GONE);
            return;
        }


        String ratingPosition = mDataset.get(positionMap).getDates().get(idColumn).getPosition();


        if (idColumn == 1 && ratingPosition.trim().equals("")) {
            holder.rlProjectFirst.setVisibility(View.GONE);
        }


        if(showColumns == 1){
            holder.rlProjectFirst.setVisibility(View.GONE);
        } else {
            holder.rlProjectFirst.setVisibility(View.VISIBLE);
        }

        text.setText(ratingPosition);

        int colorInt = R.color.positionNull;

        switch (ratingPosition) {
            case "0":
                text.setText("--");
                colorInt = R.color.positionNull;
                break;
            case "1":
                colorInt = R.color.position1;
                break;
            case "2":
                colorInt = R.color.position2;
                break;
            case "3":
                colorInt = R.color.position3;
                break;
            case "4":
                colorInt = R.color.position4;
                break;
            case "5":
                colorInt = R.color.position5;
                break;
            case "6":
                colorInt = R.color.position6;
                break;
            case "7":
                colorInt = R.color.position7;
                break;
            case "8":
                colorInt = R.color.position8;
                break;
            case "9":
                colorInt = R.color.position9;
                break;
        }

        item.setBackgroundColor(Utils.getCustomColor(context, colorInt));

        if (idColumn == COLUMN_FIRST) {


            if (mDataset.get(positionMap).getDates().get(COLUMN_FIRST).getPosition().trim().isEmpty()) {
                mDataset.get(positionMap).getDates().get(COLUMN_FIRST).setPosition("0");
            }
            if (mDataset.get(positionMap).getDates().get(COLUMN_SECOND).getPosition().trim().isEmpty()) {
                mDataset.get(positionMap).getDates().get(COLUMN_SECOND).setPosition("0");
            }

            int ratFirst = Integer.valueOf(mDataset.get(positionMap).getDates().get(COLUMN_FIRST).getPosition());
            int ratSecond = Integer.valueOf(mDataset.get(positionMap).getDates().get(COLUMN_SECOND).getPosition());

            int different = ratFirst - ratSecond;

            holder.changeFirst.setText(String.valueOf(different));


            holder.imageFirst.setVisibility(View.VISIBLE);
            holder.changeFirst.setVisibility(View.VISIBLE);

            if (ratFirst == 0 || different == 0) {
                holder.imageFirst.setVisibility(View.GONE);
                holder.changeFirst.setVisibility(View.GONE);
                holder.ratingDivider.setVisibility(View.GONE);
            } else {
                holder.imageFirst.setVisibility(View.VISIBLE);
                holder.changeFirst.setVisibility(View.VISIBLE);
                holder.ratingDivider.setVisibility(View.VISIBLE);
            }

            if (different > 0) {
                holder.imageFirst.setImageResource(R.drawable.triangle_top_vector);
                holder.changeFirst.setTextColor(Utils.getCustomColor(context, R.color.textGreen));
            } else if (different < 0) {
                holder.imageFirst.setImageResource(R.drawable.triangle_buttom_vector);
                holder.changeFirst.setTextColor(Utils.getCustomColor(context, R.color.textRed));

            }
        }

    }

    public interface ProjectSwipeClickListener {

        void clickTrash(int idProgect, int position);
    }

    public static class ViewHolderProject extends RecyclerView.ViewHolder {
        public SwipeLayout swipeLayout;
        public TextView disciption, ratingFirst, ratingSecond, changeFirst, changeSecond;
        public ImageView imageFirst, imageSecond;
        public View divider, ratingDivider;
        public LinearLayout trash;
        public RelativeLayout rlProjectFirst, rlProjectSecond;

        public ViewHolderProject(View v) {
            super(v);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe_project_request);

            divider = itemView.findViewById(R.id.view_item_projectDivider);
            trash = (LinearLayout) itemView.findViewById(R.id.rl_item_project_trash);
            disciption = (TextView) itemView.findViewById(R.id.tv_item_projectDescription);
            ratingFirst = (TextView) itemView.findViewById(R.id.tv_item_projectFirstRating);
            ratingSecond = (TextView) itemView.findViewById(R.id.tv_item_projectSecondRating);
            changeFirst = (TextView) itemView.findViewById(R.id.tv_item_projectFirstRatingChange);
            changeSecond = (TextView) itemView.findViewById(R.id.tv_item_projectSecondRatingChange);

            imageFirst = (ImageView) itemView.findViewById(R.id.iv_item_projectFirstRating);
            imageSecond = (ImageView) itemView.findViewById(R.id.iv_item_projectSecondRating);
            ratingDivider = itemView.findViewById(R.id.v_item_projectFirst);

            rlProjectFirst = (RelativeLayout) itemView.findViewById(R.id.rl_item_projectFirst);
            rlProjectSecond = (RelativeLayout) itemView.findViewById(R.id.rl_item_projectSecond);
        }
    }
}