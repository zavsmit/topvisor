package ru.handh.topvisor.ui.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import ru.handh.topvisor.R;
import ru.handh.topvisor.ui.main.ActivityMainView;

/**
 * Created by sergey on 17.02.16.
 * переименование проекта
 */
public class RenameDialog extends DialogFragment {

    public EditText name;
    public int mPosition;
    protected DialogResult mListener;

    public RenameDialog() {
    }

    public static RenameDialog newInstance(String name, int position) {
        RenameDialog f = new RenameDialog();

        Bundle args = new Bundle();
        args.putString("name", name);
        args.putInt("position", position);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (DialogResult) ((ActivityMainView) activity).presenter;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_new_ticket, null);

        name = (EditText) view.findViewById(R.id.et_newDialog_ticket);

        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
        adb.setView(view);

        adb.setTitle(getContext().getString(R.string.rename))
                .setPositiveButton(getContext().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        if (name.getText().toString().isEmpty()) {
                            Toast.makeText(getContext(), R.string.nameNotEmpty, Toast.LENGTH_SHORT).show();
                            return;
                        }

                        mListener.onDialogPositiveClick(RenameDialog.this);
                    }
                })
                .setNegativeButton(getActivity().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mListener.onDialogNegativeClick(RenameDialog.this);
                    }
                });

        name.setText(getArguments().getString("name"));
        mPosition = getArguments().getInt("position");

        return adb.create();
    }

}

