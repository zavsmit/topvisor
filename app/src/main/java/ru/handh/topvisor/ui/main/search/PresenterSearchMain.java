package ru.handh.topvisor.ui.main.search;

import java.util.ArrayList;

import ru.handh.topvisor.data.network.model.currentProject.Row;

/**
 * Created by sergey on 23.02.16.
 */
public interface PresenterSearchMain {

    ArrayList<Row> getDataEnd();

    boolean isAddListener();

    void changedEndData(String s);
}
