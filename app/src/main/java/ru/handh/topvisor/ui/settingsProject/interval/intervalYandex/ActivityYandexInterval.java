package ru.handh.topvisor.ui.settingsProject.interval.intervalYandex;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ViewFlipper;

import java.util.ArrayList;
import java.util.List;

import ru.handh.topvisor.R;
import ru.handh.topvisor.ui.base.ParentBackActivity;

/**
 * Created by sergey on 28.03.16.
 * экран с выбором опдейтов яндекс
 */
public class ActivityYandexInterval extends ParentBackActivity implements ViewIntervalYandex {

    private RecyclerView mRecyclerView;
    private PresenterIntervalYandex presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_yandex_inspect);

        viewFlipperMain = (ViewFlipper) findViewById(R.id.viewFlipper);
        showData();

        mRecyclerView = (RecyclerView) findViewById(R.id.recicleView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabSearchSystem);
        fab.setVisibility(View.GONE);

        ArrayList<String> list = new ArrayList<>();
        int selected = 0;
        String title = "";
        String idProject = "";

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            list = extras.getStringArrayList("list");
            selected = extras.getInt("selected");
            title = extras.getString("name");
            idProject = extras.getString("idProject");
        }

        initToolbar(R.id.toolbar, title);
        presenter = new PresenterIntervalYandexImpl(this, list, selected, idProject);
    }

    @Override
    public void initAdapter(List<String> groups, AdapterYandexInsert.ItemGroupClickListener listener, int select) {
        RecyclerView.Adapter mAdapter = new AdapterYandexInsert(groups, listener, select);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void selectGroup(int idSelected) {
        Intent intent = new Intent();
        intent.putExtra("idSelected", idSelected);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        presenter.close();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            presenter.close();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
