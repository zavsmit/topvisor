package ru.handh.topvisor.ui.settingsProject.searchSystems;

import java.util.ArrayList;

import ru.handh.topvisor.data.network.model.ModelResult;
import ru.handh.topvisor.data.network.model.ModelResultPS;
import ru.handh.topvisor.data.network.model.ReturnResponce;

/**
 * Created by sergey on 23.03.16.
 */
public interface SearcherResult {
    void addPSResponce(ReturnResponce<ModelResultPS> result);

    void checkedPSResponce(ReturnResponce<ModelResultPS> result, int position);

    void removePSResponce(ReturnResponce<ModelResult> result, int position, String type);

    void changeItemsPSResponce(ReturnResponce<ModelResultPS> result, ArrayList<Integer> positions);
}
