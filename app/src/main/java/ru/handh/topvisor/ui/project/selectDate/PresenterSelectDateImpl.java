package ru.handh.topvisor.ui.project.selectDate;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import ru.handh.topvisor.utils.Log;

/**
 * Created by sergey on 15.03.16.
 * логика выбора дат
 */
public class PresenterSelectDateImpl implements PresenterSelectDate, AdapterSelectDate.ItemSearcherClickListener {

    private final static int DATE_FROM = 0;
    private final static int DATE_BEFORE = 1;
    private ViewSelectDate mView;
    private List<String> allDates;
    private List<Integer> selectedList;
    private List<Calendar> calendarList;

    public PresenterSelectDateImpl(ViewSelectDate view, List<String> dates, List<Integer> selectedList) {
        mView = view;
        allDates = dates;
        this.selectedList = selectedList;

        calendarList = new ArrayList<>();


        for (int i = 0; i < allDates.size(); i++) {
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

            Log.deb(i + " " + allDates.get(i));
            Calendar cal = Calendar.getInstance();
            try {
                cal.setTime(format.parse(allDates.get(i)));
                calendarList.add(cal);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }


        mView.initAdapter(allDates, this, selectedList);
    }


    @Override
    public void clickType(int typeSelect) {

        Calendar cal = calendarList.get(0);
        int weekFirst = cal.get(Calendar.WEEK_OF_YEAR);
        int monthFirst = cal.get(Calendar.MONTH);


        cal.add(Calendar.DATE, -7);
        int secondPosition;

        switch (typeSelect) {

            case AdapterSelectDate.TYPE_LAST_7_DAYS:

                selectedList.set(DATE_BEFORE, 0);

                for (int i = 1; i < calendarList.size(); i++) {
                    if (cal.after(calendarList.get(i))) {
                        secondPosition = i - 1;
                        if (i == 1) {
                            secondPosition = 1;
                        }

                        selectedList.set(DATE_FROM, secondPosition);
                        break;
                    }
                }
                break;

            case AdapterSelectDate.TYPE_CURRENT_WEEK:

                selectedList.set(DATE_BEFORE, 0);

                for (int i = 1; i < calendarList.size(); i++) {
                    if (weekFirst != calendarList.get(i).get(Calendar.WEEK_OF_YEAR)) {
                        secondPosition = i - 1;
                        if (i == 1) {
                            secondPosition = 1;
                        }
                        selectedList.set(DATE_FROM, secondPosition);
                        break;
                    }
                }
                break;

            case AdapterSelectDate.TYPE_CURRENT_MONTH:

                selectedList.set(DATE_BEFORE, 0);

                for (int i = 1; i < calendarList.size(); i++) {
                    if (monthFirst != calendarList.get(i).get(Calendar.MONTH)) {
                        secondPosition = i - 1;
                        if (i == 1) {
                            secondPosition = 1;
                        }
                        selectedList.set(DATE_FROM, secondPosition);
                        break;
                    }
                }
                break;

            case AdapterSelectDate.TYPE_LAST_MONTH:

                Calendar firstDayLastMonth;
                int lastMonth = 0;
                int startIdLastMonth = 0;

                for (int i = 1; i < calendarList.size(); i++) {
                    if (monthFirst != calendarList.get(i).get(Calendar.MONTH)) {
                        firstDayLastMonth = calendarList.get(i);
                        startIdLastMonth = i;
                        lastMonth = firstDayLastMonth.get(Calendar.MONTH);


                        selectedList.set(DATE_BEFORE, i);
                        break;
                    }
                }

                for (int i = startIdLastMonth; i < calendarList.size(); i++) {
                    if (lastMonth != calendarList.get(i).get(Calendar.MONTH)) {
                        secondPosition = i - 1;
                        if (i == 1) {
                            secondPosition = 1;
                        }

                        if (secondPosition == startIdLastMonth) {
                            if (calendarList.size() - 1 > secondPosition) {
                                ++secondPosition;
                            } else if (startIdLastMonth != 0) {
                                --startIdLastMonth;
                                selectedList.set(DATE_BEFORE, startIdLastMonth);
                            }


                        }
                        selectedList.set(DATE_FROM, secondPosition);
                        break;
                    }
                }

                break;
        }

        switch (typeSelect) {
            case AdapterSelectDate.TYPE_ONE_DATES:
                selectedList.set(DATE_FROM, 0);
                selectedList.set(DATE_BEFORE, -1);
                break;
            case AdapterSelectDate.TYPE_TWO_DATES:
                selectedList.set(DATE_FROM, 1);
                selectedList.set(DATE_BEFORE, 0);
                break;
        }


//        if (typeSelect != AdapterSelectDate.TYPE_ONE_DATES && typeSelect != AdapterSelectDate.TYPE_TWO_DATES) {
            mView.close((ArrayList<Integer>) selectedList);
//        }

    }

    @Override
    public void clickDate(int positionInListSelect, int typeSelect) {
        mView.openListDate(positionInListSelect, (ArrayList<String>) allDates, typeSelect);
    }

    @Override
    public void clickDays(int typeSelect) {

    }


    @Override
    public void returnSelectItem(int positionSelected, int typeSelect) {


        if (typeSelect == DATE_FROM) {
            int x = selectedList.get(typeSelect + 1);

            if (positionSelected <= x) {
                if (positionSelected == 0) {
                    positionSelected = 1;
                } else {
                    selectedList.set(DATE_BEFORE, positionSelected - 1);
                }
            }
        }

        if (typeSelect == DATE_BEFORE) {

            int x = selectedList.get(typeSelect - 1);

            if (positionSelected >= x) {
                selectedList.set(DATE_FROM, positionSelected + 1);
            }
        }


        selectedList.set(typeSelect, positionSelected);
        mView.notificationAdapter();
    }

    @Override
    public void close() {
        mView.close((ArrayList<Integer>) selectedList);
    }
}
