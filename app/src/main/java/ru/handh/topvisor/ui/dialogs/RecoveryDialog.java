package ru.handh.topvisor.ui.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import ru.handh.topvisor.R;
import ru.handh.topvisor.ui.main.ActivityMainView;

/**
 * Created by sergey on 18.02.16.
 * восстановление прокта из архива
 */
public class RecoveryDialog extends DialogFragment {

    public int mPosition;
    private DialogResult mListener;

    public RecoveryDialog() {
    }

    public static RecoveryDialog newInstance(String name, int position) {
        RecoveryDialog f = new RecoveryDialog();

        Bundle args = new Bundle();
        args.putString("name", name);
        args.putInt("position", position);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (DialogResult) ((ActivityMainView) activity).presenter;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mPosition = getArguments().getInt("position");
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
                .setTitle(getArguments().getString("name"))
                .setPositiveButton(getContext().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mListener.onDialogPositiveClick(RecoveryDialog.this);
                    }
                })
                .setNegativeButton(getContext().getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mListener.onDialogNegativeClick(RecoveryDialog.this);
                    }
                })
                .setMessage(getContext().getString(R.string.recoveryProject));

        return adb.create();
    }
}
