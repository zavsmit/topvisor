package ru.handh.topvisor.ui.project;

import java.util.ArrayList;

/**
 * Created by sergey on 19.02.16.
 */
public interface PresenterProject {
    void loadPhrases(int currentNumberPage, int page);

    void openGroups();

    void openRegions();

    void openDate();

    void clickSettings();

    void changeHeader();

    void changeData();

    void changeDates(ArrayList<Integer> listInt);

    void showDialogNew(int idDialog);

    void initAll();

    void onStop();
}
