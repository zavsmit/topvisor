package ru.handh.topvisor.ui.support;

import java.util.List;

import ru.handh.topvisor.data.network.model.ModelTicket;

/**
 * Created by sergey on 31.03.16.
 */
public interface ViewDialogsList {
    void selectItem(String name);

    void initAdapter(List<ModelTicket> groups, AdapterDialogsList.ItemClickListener listener);

    void showData();

    void showProgress();

    void showError();

    void showEmptyText(boolean isShow);

    void showInfoToast(String text);
}
