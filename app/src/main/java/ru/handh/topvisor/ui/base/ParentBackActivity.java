package ru.handh.topvisor.ui.base;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;
import android.widget.ViewFlipper;

import ru.handh.topvisor.R;
import ru.handh.topvisor.utils.Constants;

/**
 * Created by sergey on 28.02.16.
 * родительское активити для экранов с кнопкой назад
 */
public class ParentBackActivity extends AppCompatActivity {

    protected Toolbar toolbar;
    protected ViewFlipper viewFlipperMain;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void initToolbar(int idLayout) {
        initToolbar(idLayout, "");
    }

    protected void initToolbar(int idLayout, String name) {
        toolbar = (Toolbar) findViewById(idLayout);

        if (!name.isEmpty())
            toolbar.setTitle(name);

        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.back_vector);
    }

    public void showData() {
        if (viewFlipperMain.getDisplayedChild() != Constants.VIEW_CONTENT)
            viewFlipperMain.setDisplayedChild(Constants.VIEW_CONTENT);
    }

    public void showProgress() {
        if (viewFlipperMain.getDisplayedChild() != Constants.VIEW_PROGRESS)
            viewFlipperMain.setDisplayedChild(Constants.VIEW_PROGRESS);
    }

    public void showError() {
        if (viewFlipperMain.getDisplayedChild() != Constants.VIEW_ERROR)
            viewFlipperMain.setDisplayedChild(Constants.VIEW_ERROR);
    }

    public void showInfoToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }
}
