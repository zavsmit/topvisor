package ru.handh.topvisor.ui.login;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.DataManager;
import ru.handh.topvisor.utils.Log;
import ru.ulogin.sdk.ProviderArrayAdapter;

/**
 * Created by sergey on 25.04.16.
 * переделанный класс из либы ulogin
 */
public class ActivityUlogin extends Activity {
    public static final String PROVIDERS = "com.ulogin.sdk.providers";
    public static final String FIELDS = "com.ulogin.sdk.fields";
    public static final String OPTIONAL = "com.ulogin.sdk.optional";
    public static final String USERDATA = "com.ulogin.sdk.userdata";
    public static final String APPLICATION_ID = "com.ulogin.sdk.app_id";
    public static final String SECRET_KEY = "com.ulogin.sdk.secret_key";
    public static final String CLEAN_COOKIES_AFTER_AUTH = "com.ulogin.sdk.clean_cookies";
    private static int minPanelWidth;
    private static int shadowWidth;
    private static int browserWidthPixels;
    private static boolean browserOpenned;
    private int screenWidth = 0;
    private String fields = "";
    private String optional_fields = "";
    private String applicationId = null;
    private String secretKey = null;
    private Integer animationTime;
    private boolean cleanCookiesAfterFinishing;
    private boolean singleProvider;
    private ArrayList<String> providers;
    private String deviceId = null;

    public ActivityUlogin() {
    }

    @SuppressLint({"SetJavaScriptEnabled", "AddJavascriptInterface"})
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(1);
        this.setContentView(R.layout.activity_ulogin);

        WebView web1 = (WebView) this.findViewById(ru.ulogin.sdk.R.id.webView);
        web1.setWebViewClient(new ActivityUlogin.MyBrowser());
        web1.getSettings().setLoadsImagesAutomatically(true);
        web1.getSettings().setJavaScriptEnabled(true);
        web1.getSettings().setSupportMultipleWindows(false);
        web1.requestFocus(130);
        web1.getSettings().setBuiltInZoomControls(true);
        Intent callingIntent = this.getIntent();
        this.providers = callingIntent.getStringArrayListExtra(PROVIDERS);
        if (this.providers == null) {
            this.providers = new ArrayList(Arrays.asList(this.getResources().getStringArray(ru.ulogin.sdk.R.array.ulogin_providers)));
        }

        singleProvider = providers.size() == 1;
        minPanelWidth = singleProvider ? 0 : (int) this.getResources().getDimension(ru.ulogin.sdk.R.dimen.ulogin_panel_min_width);
        shadowWidth = singleProvider ? 0 : (int) this.getResources().getDimension(ru.ulogin.sdk.R.dimen.ulogin_shadow_width);
        this.animationTime = Integer.valueOf(singleProvider ? 0 : this.getResources().getInteger(ru.ulogin.sdk.R.integer.ulogin_animation_time));
        ArrayList allowed_array_list = new ArrayList(Arrays.asList(this.getResources()
                .getStringArray(ru.ulogin.sdk.R.array.ulogin_fields)));
        ArrayList fields_array_parameter = callingIntent.getStringArrayListExtra(FIELDS);
        if (fields_array_parameter == null) {
            fields_array_parameter = new ArrayList(Arrays.asList(this.getResources()
                    .getStringArray(ru.ulogin.sdk.R.array.ulogin_fields_default)));
        }

        ArrayList optional_fields_array_parameter = callingIntent.getStringArrayListExtra(OPTIONAL);
        Iterator providerListView;
        String adapt;
        if (optional_fields_array_parameter != null) {
            providerListView = optional_fields_array_parameter.iterator();

            while (providerListView.hasNext()) {
                adapt = (String) providerListView.next();
                if (!allowed_array_list.contains(adapt)) {
                    Log.deb("ulogin.sdk.init", "Unknown optional data field \"" + adapt + "\". Ignoring");
                } else {
                    if (fields_array_parameter.contains(adapt)) {
                        Log.deb("ulogin.sdk.init", "Data field \"" + adapt + "\" is already marked as mandatory. Now " +
                                "marking it as optional");
                        fields_array_parameter.remove(adapt);
                    }

                    this.optional_fields = this.optional_fields + (this.optional_fields.length() > 0 ? "," : "") + adapt;
                }
            }
        }

        this.fields = "";
        providerListView = fields_array_parameter.iterator();

        while (providerListView.hasNext()) {
            adapt = (String) providerListView.next();
            if (!allowed_array_list.contains(adapt)) {
                Log.deb("ulogin.sdk.init", "Unknown mandatory data field \"" + adapt + "\". Ignoring");
            } else {
                this.fields = this.fields + (this.fields.length() > 0 ? "," : "") + adapt;
            }
        }

        this.applicationId = callingIntent.getStringExtra(APPLICATION_ID);
        if (this.applicationId == null || this.applicationId.length() == 0) {
            this.applicationId = "";
        }

        this.secretKey = callingIntent.getStringExtra(SECRET_KEY);
        if (this.secretKey == null || this.secretKey.length() == 0) {
            this.secretKey = "";
        }

        cleanCookiesAfterFinishing = callingIntent.getBooleanExtra(CLEAN_COOKIES_AFTER_AUTH, false);
        ListView providerListView1 = (ListView) this.findViewById(ru.ulogin.sdk.R.id.providerListView);
        if (singleProvider) {
            providerListView1.setVisibility(View.GONE);
            ViewGroup adapt1 = (ViewGroup) this.findViewById(ru.ulogin.sdk.R.id.webLayer);
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) adapt1.getLayoutParams();
            lp.width = -1;
            adapt1.setLayoutParams(lp);
            this.selectProvider(null);
        } else {
            ProviderArrayAdapter adapt2 = new ProviderArrayAdapter(this, this.providers);
            providerListView1.setAdapter(adapt2);
            providerListView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id) {
                    ActivityUlogin.this.selectProvider((ViewGroup) itemClicked);
                }
            });
        }

        this.resizeLayout();
    }

    private void resizeLayout() {
        WindowManager manager = (WindowManager) this.getSystemService(WINDOW_SERVICE);
        DisplayMetrics metrics = new DisplayMetrics();
        manager.getDefaultDisplay().getMetrics(metrics);
        if (!singleProvider) {
            browserWidthPixels = (int) ((float) (metrics.widthPixels - minPanelWidth) / metrics.density);
            this.screenWidth = metrics.widthPixels;
            ViewGroup webLayer = (ViewGroup) this.findViewById(ru.ulogin.sdk.R.id.webLayer);
            ListView providerListView = (ListView) this.findViewById(ru.ulogin.sdk.R.id.providerListView);
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) webLayer.getLayoutParams();
            lp.width = this.screenWidth - minPanelWidth;
            webLayer.setLayoutParams(lp);
            LinearLayout.LayoutParams layoutParams1 = new LinearLayout.LayoutParams(browserOpenned ? minPanelWidth : this.screenWidth + shadowWidth, -1);
            providerListView.setLayoutParams(layoutParams1);
        }

    }

    protected void selectProvider(ViewGroup item) {
        ListView providerListView = (ListView) this.findViewById(ru.ulogin.sdk.R.id.providerListView);
        ProviderArrayAdapter adapter = (ProviderArrayAdapter) providerListView.getAdapter();
        Resources resources = this.getResources();
        if (item == null && !singleProvider) {
            ((TextView) this.findViewById(ru.ulogin.sdk.R.id.header)).setText(resources.getString(ru.ulogin.sdk.R.string.ulogin_select_provider));
            providerListView.setVerticalScrollBarEnabled(true);
            ((ProviderArrayAdapter) providerListView.getAdapter()).setSelectedId("");
            if (browserOpenned) {
                browserOpenned = false;
                ActivityUlogin.ExpandAnimation a2 = new ActivityUlogin.ExpandAnimation((float) (this.screenWidth + shadowWidth), providerListView);
                a2.setDuration((long) this.animationTime.intValue());
                ViewGroup webLayer2 = (ViewGroup) this.findViewById(ru.ulogin.sdk.R.id.webLayer);
                webLayer2.startAnimation(a2);
            }
        } else {
            ((TextView) this.findViewById(ru.ulogin.sdk.R.id.header)).setText(resources.getString(ru.ulogin.sdk.R.string.ulogin_authorize));
            providerListView.setVerticalScrollBarEnabled(false);
            String a;
            if (singleProvider) {
                a = providers.get(0);
            } else {
                a = item.findViewById(ru.ulogin.sdk.R.id.label).getTag().toString();
                ((ProviderArrayAdapter) providerListView.getAdapter()).setSelectedId(a);
            }

            if (this.deviceId == null) {
                TelephonyManager url = (TelephonyManager) this.getSystemService(TELEPHONY_SERVICE);
                this.deviceId = url.getDeviceId() + "_" + Settings.Secure.getString(this.getContentResolver(),
                        "android_id");
            }

            String serverType = DataManager.getPref().getServerShort();

            String url1 = "https://ulogin.ru/auth.php?name="
                    + a
                    + "&lang=" + this.getResources().getConfiguration().locale.getLanguage()
                    + "&fields=" + this.fields
                    + "&optional=" + this.optional_fields
                    + "&redirect_uri=https%3A%2F%2Ftopvisor." + serverType + "/login/&" +
                    "&q=https%3A%2F%2Ftopvisor." + serverType + "/login/&window=3";


            Log.deb("url = " + url1);
            WebView web = (WebView) this.findViewById(ru.ulogin.sdk.R.id.webView);
            web.loadUrl(url1);
            if (!browserOpenned && !singleProvider) {
                browserOpenned = true;
                ViewGroup webLayer1 = (ViewGroup) this.findViewById(ru.ulogin.sdk.R.id.webLayer);
                ActivityUlogin.ExpandAnimation a1 = new ActivityUlogin.ExpandAnimation(
                        (float) minPanelWidth, providerListView);
                a1.setDuration((long) this.animationTime.intValue());
                a1.start();
                webLayer1.startAnimation(a1);
            }
        }

        if (!singleProvider) {
            adapter.notifyDataSetChanged();
        }

    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        this.resizeLayout();
    }

    private void goBack() {
        if (browserOpenned && !singleProvider) {
            this.closeAuthScreen();
        } else {
            this.sendResult(null);
        }

    }

    public void closeAuthScreen() {
        if (browserOpenned && !singleProvider) {
            this.selectProvider(null);
        }

    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            this.goBack();
            return true;
        } else {
            return super.onKeyUp(keyCode, event);
        }
    }

    protected void sendResult(HashMap<String, String> result) {
        if (result != null && cleanCookiesAfterFinishing) {
            CookieManager res = CookieManager.getInstance();
            res.removeAllCookie();
        }

        browserOpenned = false;
        int res1 = result != null && result.get("error") == null ? -1 : 0;
        if (result == null) {
            result = new HashMap();
            result.put("error", "canceled");
        }

        Intent intent = new Intent();
        intent.putExtra(USERDATA, result);
        this.setResult(res1, intent);
        this.finish();
    }

    public class ExpandAnimation extends Animation {
        private final float aPanelStartWidth;
        private final float aPanelEndWidth;
        private final float aDeltaWidth;
        private final View aPanel;
        private final ProviderArrayAdapter aAdapter;
        private final float a_ulogin_text_delta_margin;
        private float a_start_ulogin_text_margin;

        ExpandAnimation(float endWidth, View panel) {
            this.aPanel = panel;
            this.aPanelStartWidth = (float) panel.getWidth();
            this.aPanelEndWidth = endWidth;
            this.aDeltaWidth = this.aPanelEndWidth - this.aPanelStartWidth;
            this.aAdapter = (ProviderArrayAdapter) ((ListView) panel).getAdapter();
            Resources res = panel.getContext().getResources();
            this.a_start_ulogin_text_margin = this.aAdapter.getTextOffset();
            float endOffset = this.aDeltaWidth > 0.0F ? res.getDimension(ru.ulogin.sdk.R.dimen.ulogin_text_left_margin_wide_list) : res.getDimension(ru.ulogin.sdk.R.dimen.ulogin_text_left_margin_panel);
            this.a_ulogin_text_delta_margin = endOffset - this.a_start_ulogin_text_margin;
        }

        protected void applyTransformation(float interpolatedTime, Transformation t) {
            LinearLayout.LayoutParams layoutParams1 = new LinearLayout.LayoutParams((int)
                    (this.aPanelStartWidth + this.aDeltaWidth * interpolatedTime), -1);
            this.aPanel.setLayoutParams(layoutParams1);
            this.aAdapter.setTextOffset((float) ((int) (this.a_start_ulogin_text_margin
                    + this.a_ulogin_text_delta_margin * interpolatedTime)));
        }

        public boolean willChangeBounds() {
            return true;
        }
    }

    private class MyBrowser extends WebViewClient {
        private boolean loadingFinished;
        private boolean redirect;

        private MyBrowser() {
            loadingFinished = true;
            redirect = false;
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (!loadingFinished) {
                redirect = true;
            }

            loadingFinished = false;
            if (url.startsWith("https://ulogin.ru/")) {
                view.getSettings().setLoadWithOverviewMode(true);
                view.getSettings().setUseWideViewPort(true);
            } else {
                view.getSettings().setLoadWithOverviewMode(false);
                view.getSettings().setUseWideViewPort(false);
            }

            view.loadUrl(url);
            return true;
        }

        public void onPageStarted(WebView view, String url, Bitmap facIcon) {
            loadingFinished = false;
            ActivityUlogin.this.findViewById(ru.ulogin.sdk.R.id.loadingPanel).setVisibility(View.VISIBLE);
        }

        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            Toast.makeText(ActivityUlogin.this.getApplicationContext(), description + " " +
                    ActivityUlogin.this.getResources().getString(ru.ulogin.sdk.R.string.ulogin_no_connection) + ".", Toast.LENGTH_LONG).show();
            ActivityUlogin.this.closeAuthScreen();
        }

        public void onPageFinished(WebView view, String url) {

            if (!redirect) {
                loadingFinished = true;
                ActivityUlogin.this.findViewById(ru.ulogin.sdk.R.id.loadingPanel).setVisibility(View.GONE);
            }

            super.onPageFinished(view, url);
            if (loadingFinished && !redirect) {


                String cookies = CookieManager.getInstance().getCookie(url);
                Log.deb("All the cookies in a string:" + cookies);

                int startPosition = cookies.indexOf("auth=");

                if (startPosition != -1) {
                    String auth = cookies.substring(cookies.indexOf("auth=") + 5);
                    Log.deb("auth: " + auth);
                    Intent output = new Intent();
                    output.putExtra("auth", auth);
                    setResult(RESULT_OK, output);

                    CookieManager res = CookieManager.getInstance();
                    res.removeAllCookie();

                    finish();
                }

            } else {
                redirect = false;
            }

        }
    }
}
