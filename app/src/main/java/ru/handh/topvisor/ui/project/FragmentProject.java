package ru.handh.topvisor.ui.project;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.daimajia.swipe.util.Attributes;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.DataManager;
import ru.handh.topvisor.data.network.model.projecPhrases.Date_;
import ru.handh.topvisor.data.network.model.projecPhrases.ModelProjectPhrases;
import ru.handh.topvisor.data.network.model.projecPhrases.Rows;
import ru.handh.topvisor.utils.Constants;

/**
 * Created by sergey on 20.02.16.
 * фрагмент одного списка вопросов
 */
public class FragmentProject extends Fragment {

    private TextView countReq, dateSecond, dateFirst, groupName, regionName, calendarStart, calendarEnd;
    private RecyclerView mRecyclerView;
    private LinearLayout llRegionGroup;
    private int currentNumberPage;
    private RecyclerView.Adapter mAdapter;
    private ActivityViewProject activity;
    private List<String> spinnerList;
    private Spinner spinner;
    private int countClickSpinner;
    private ViewFlipper viewFlipperMain;
    private TextView tvEmpty;
    private View vDividerDesc;
    private Date currentDate;

    public FragmentProject() {
    }

    public static Fragment newInstance(int position) {
        FragmentProject fragment = new FragmentProject();
        Bundle args = new Bundle();
        args.putInt("page_position", position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        currentNumberPage = getArguments().getInt("page_position");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        currentDate = new Date();

        activity = (ActivityViewProject) getActivity();
        View rootView = inflater.inflate(R.layout.fragment_project, container, false);


        viewFlipperMain = (ViewFlipper) rootView.findViewById(R.id.viewFlipperProject);

        llRegionGroup = (LinearLayout) rootView.findViewById(R.id.ll_project_RegionGroup);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recicleView_project);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));


        tvEmpty = (TextView) rootView.findViewById(R.id.tv_emptyTextProject);
        vDividerDesc = rootView.findViewById(R.id.v_item_projectDescriptionDivider);

        countReq = (TextView) rootView.findViewById(R.id.tv_item_projectDescription);
        dateFirst = (TextView) rootView.findViewById(R.id.tv_item_projectDateFirst);
        dateSecond = (TextView) rootView.findViewById(R.id.tv_item_projectDateSecond);
        regionName = (TextView) rootView.findViewById(R.id.tv_projectRegionName);
        groupName = (TextView) rootView.findViewById(R.id.tv_projectGroupName);

        calendarStart = (TextView) rootView.findViewById(R.id.tv_project_calendarStart);
        calendarEnd = (TextView) rootView.findViewById(R.id.tv_project_calendarEnd);

        ImageButton refresh = (ImageButton) rootView.findViewById(R.id.ib_project_refresh);
        ImageButton addNew = (ImageButton) rootView.findViewById(R.id.ib_project_add);
        LinearLayout tv_projectRegion = (LinearLayout) rootView.findViewById(R.id.ll_projectRegion);
        LinearLayout tv_projectGroup = (LinearLayout) rootView.findViewById(R.id.ll_projectGroup);
        RelativeLayout rl_project_calendar = (RelativeLayout) rootView.findViewById(R.id.rl_project_calendar);


        tv_projectRegion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.presenter.openRegions();
            }
        });


        tv_projectGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.presenter.openGroups();
            }
        });

        rl_project_calendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.presenter.openDate();
            }
        });


        addNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.presenter.showDialogNew(ActivityViewProject.DIALOG_ADD);
            }
        });


        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.presenter.showDialogNew(ActivityViewProject.DIALOG_REFRESH_ALL);
            }
        });


        hideRegionGroup(DataManager.getPref().getVisibleHeaderProject());


        spinner = (Spinner) rootView.findViewById(R.id.spinner_project_list);
        spinnerList = new ArrayList<>();

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View itemSelected, int selectedItemPosition, long selectedId) {

                ++countClickSpinner;
                if (countClickSpinner > 1) {
                    activity.presenter.loadPhrases(currentNumberPage, selectedItemPosition + 1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        showData();

        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
        activity.presenter.loadPhrases(currentNumberPage, 1);
    }

    public void hideRegionGroup(boolean isVisible) {
        if (isVisible) {
            llRegionGroup.setVisibility(View.VISIBLE);
        } else {
            llRegionGroup.setVisibility(View.GONE);
        }
    }

    public void initAdapter(ModelProjectPhrases rows, List<String> dates, PresenterProjectImpl presenterImpl) {


        double total = 0;

        if (!rows.getRows().getTotal().isEmpty()) {
            total = Double.valueOf(rows.getRows().getTotal());
            total = total / 50;
        }

        double pages = Math.ceil(total);

        spinnerList.clear();

        for (int i = 0; i < pages; i++) {
            spinnerList.add(String.valueOf(i + 1));
        }
        countClickSpinner = 0;
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_project, spinnerList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);

        spinner.setSelection(rows.getPage() - 1);

        if (countReq == null) {
            return;
        }
        int showColumns = changeCountReqDates(dates, rows.getRows());

        mAdapter = new AdapterProject(rows, getActivity(), presenterImpl, showColumns);
        ((AdapterProject) mAdapter).setMode(Attributes.Mode.Single);
        mRecyclerView.setAdapter(mAdapter);

        if (rows.getRows().getPhrases().size() == 0) {
            mRecyclerView.setVisibility(View.GONE);
            tvEmpty.setVisibility(View.VISIBLE);
        }



    }


    public int changeCountReqDates(List<String> dates, Rows rows) {
        countReq.setText(activity.getString(R.string.reqNumber) + rows.getTotal() + ")");

        int showColumns = 2;

        List<Date_> datePercent = rows.getScheme().getDates();

        if (dates.get(0).isEmpty() || dates.get(0).equals(dates.get(1))) {
            dateFirst.setVisibility(View.GONE);
            vDividerDesc.setVisibility(View.GONE);
            showColumns = 1;
        } else {
            dateFirst.setVisibility(View.VISIBLE);
            vDividerDesc.setVisibility(View.VISIBLE);
            dateFirst.setText(dates.get(0) + "\n" + datePercent.get(0).getTopPercent() + "%");
        }

        if (dates.get(1).isEmpty()) {
            dateSecond.setVisibility(View.GONE);
            showColumns = 1;
        } else {
            dateSecond.setVisibility(View.VISIBLE);
            dateSecond.setText(dates.get(1) + "\n" + datePercent.get(1).getTopPercent() + "%");
        }

        return showColumns;
    }

    public void removeItem(int position) {
        ((AdapterProject) mAdapter).removePosition(position);
    }

    public void changeHeader(String nameRegion, String nameGroup) {
        if (regionName != null) {
            regionName.setText(nameRegion);
            groupName.setText(nameGroup);
        }

    }

    public void changeCalendarFooter(String start, String end) {

        if (calendarStart == null) {
            return;
        }
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        DateFormat dfNew = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH);

        try {
            Date date = format.parse(start);
            start = dfNew.format(date);

            date = format.parse(end);
            end = dfNew.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        calendarStart.setText(start);
        calendarEnd.setText(end);

        if (end.isEmpty()) {
            calendarStart.setVisibility(View.GONE);
            calendarEnd.setText(dfNew.format(currentDate));
        } else {
            calendarStart.setVisibility(View.VISIBLE);
        }


    }


    public void showData() {
        if (viewFlipperMain.getDisplayedChild() != Constants.VIEW_CONTENT)
            viewFlipperMain.setDisplayedChild(Constants.VIEW_CONTENT);
    }

    public void showProgress() {
        if (viewFlipperMain.getDisplayedChild() != Constants.VIEW_PROGRESS)
            viewFlipperMain.setDisplayedChild(Constants.VIEW_PROGRESS);
    }

    public void showError() {
        if (viewFlipperMain.getDisplayedChild() != Constants.VIEW_ERROR)
            viewFlipperMain.setDisplayedChild(Constants.VIEW_ERROR);
    }
}
