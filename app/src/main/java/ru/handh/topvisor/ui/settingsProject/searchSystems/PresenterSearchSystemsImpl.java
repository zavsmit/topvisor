package ru.handh.topvisor.ui.settingsProject.searchSystems;

import android.support.v4.app.DialogFragment;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.realm.RealmList;
import ru.handh.topvisor.R;
import ru.handh.topvisor.data.DataManager;
import ru.handh.topvisor.data.database.project.ProjectData;
import ru.handh.topvisor.data.database.project.SearchData;
import ru.handh.topvisor.data.model.SearcherItemDialog;
import ru.handh.topvisor.data.network.model.ModelPriseInspectFilter;
import ru.handh.topvisor.data.network.model.ModelResult;
import ru.handh.topvisor.data.network.model.ModelResultPS;
import ru.handh.topvisor.data.network.model.ReturnResponce;
import ru.handh.topvisor.data.network.model.currentProject.Row;
import ru.handh.topvisor.data.network.model.projecDates.Rows;
import ru.handh.topvisor.data.network.model.projecPhrases.ModelProjectPhrases;
import ru.handh.topvisor.ui.dialogs.AddListPSDialog;
import ru.handh.topvisor.ui.dialogs.DialogResult;
import ru.handh.topvisor.ui.project.ProjectEndReq;

/**
 * Created by sergey on 17.03.16.
 * презентер экрана наскройки поисковых систем
 */
public class PresenterSearchSystemsImpl implements
        PresenterSearchSystems,
        AdapterSearchSystems.ItemSearcherClickListener,
        ActionMode.Callback,
        ProjectEndReq,
        SearcherResult, DialogResult {

    private ViewSearchSystems mView;
    private ActionMode actionMode;
    private AdapterSearchSystems mAdapter;
    private RealmList<SearchData> searchDataList;
    private String idProject;
    private ArrayList<SearcherItemDialog> listNewSearcher, initList;
    private int selectDialogPosition;

    public PresenterSearchSystemsImpl(ViewSearchSystems view, String idProject) {
        mView = view;
        this.idProject = idProject;
        loadProject(idProject, true);
    }

    @Override
    public void clickItem(String idRegion, int position, String idSearcher) {

        if (actionMode != null) {
            myToggleSelection(position);
            return;
        }

        for (int i = 0; i < searchDataList.size(); i++) {
            if (idSearcher.equals(searchDataList.get(i).getId())) {

                mView.selectRegion(idRegion, idProject, i);
                return;
            }
        }

    }

    @Override
    public void longClick(int position) {
        if (actionMode != null) {
            return;
        }
        actionMode = mView.startMyActionMode(this);
        myToggleSelection(position);
    }

    @Override
    public void removeItem(ArrayList<Integer> positions, String idSearcher, ArrayList<String> listRegions) {

        if (listRegions.size() == 0) {
            listRegions.add("\""); // если удаляется последний элемент - то надо посылать хоть что-то, иначе вернется ошибка
        }
        DataManager.getSettingsRest().reqChangeItemsPS(idSearcher, this, listRegions, positions);
    }


    @Override
    public void removeSP(int position, String idSearcher, String type) {
        DataManager.getSettingsRest().reqRemovePS(idSearcher, this, position, type);
    }

    @Override
    public void clickAddRegion(String idSearcher, String searcher) {
        mView.openAddRegion(idSearcher, searcher);
    }

    @Override
    public void changeSwitch(boolean isEnable, String idSearcher, int position) {

        String isCheck = "0";
        if (isEnable) {
            isCheck = "1";
        }

        for (int i = 0; i < searchDataList.size(); i++) {
            if (searchDataList.get(i).getId().equals(idSearcher)) {
                DataManager.getDB().checkSearchSystem(searchDataList.get(i), isCheck);
                break;
            }
        }

        DataManager.getSettingsRest().reqCheckedPS(idSearcher, isEnable, this, position);
    }


    private void myToggleSelection(int idx) {
        mAdapter.toggleSelection(idx);
        actionMode.setTitle(String.valueOf(mAdapter.getSelectedItemCount()));
    }


    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(R.menu.menu_remove, menu);
        mView.showHide(false);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_delete:
                Map<String, ArrayList<Integer>> selectedItemPositions = mAdapter.getSelectedItems();
                Map<String, ArrayList<String>> allRegionsForChange = mAdapter.getAllRegionsChanged();


                for (Map.Entry<String, ArrayList<String>> entry : allRegionsForChange.entrySet()) {
                    removeItem(selectedItemPositions.get(entry.getKey()), entry.getKey(), entry.getValue());
                }
                actionMode.finish();
                return true;
            default:
                return false;
        }
    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {
        this.actionMode = null;
        mAdapter.clearSelections();
        mView.showHide(true);
    }

    @Override
    public void addPSResponce(ReturnResponce<ModelResultPS> result) {
        if (result.isFailure()) {
            mView.showToast(result.getErrorMessage());
        } else {
            mAdapter.addPsItem(result.getResult().getResult());
            listNewSearcher.remove(selectDialogPosition);
            mView.showEmpty(false);
        }
    }

    @Override
    public void checkedPSResponce(ReturnResponce<ModelResultPS> result, int position) {

        if (!result.isFailure()) {

            boolean isCheck = false;

            if (result.getResult().getResult().getEnabled().equals("1"))
                isCheck = true;

            mAdapter.checkRegion(isCheck, position);
        } else {
            mView.showToast(result.getErrorMessage());
        }
    }

    @Override
    public void removePSResponce(ReturnResponce<ModelResult> result, int position, String type) {
        if (result.isFailure()) {
            mView.showToast(result.getErrorMessage());
        } else {
            mAdapter.removePS(position);
            if (searchDataList.size() == 0) {
                mView.showEmpty(true);
            }

            for (int i = 0; i < initList.size(); i++) {
                if (type.equals(initList.get(i).getIdSearcher())) {
                    listNewSearcher.add(initList.get(i));
                    break;
                }
            }

        }
    }

    @Override
    public void changeItemsPSResponce(ReturnResponce<ModelResultPS> result, ArrayList<Integer> positions) {
        if (!result.isFailure()) {
            for (int i = 0; i < positions.size(); i++) {
                mAdapter.removeItemInterface(positions.get(i));
            }

            mAdapter.notifyDataSetChanged();
        } else {
            mView.showToast(result.getErrorMessage());
        }

    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        if (dialog instanceof AddListPSDialog) {

            selectDialogPosition = ((AddListPSDialog) dialog).mPosition;

            DataManager.getSettingsRest().reqAddPS(idProject,
                    listNewSearcher.get(selectDialogPosition).getIdSearcher(),
                    this);

            mView.hideAddPSDialog();
        }
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
    }

    @Override
    public void addPs() {
        for (int i = 0; i < searchDataList.size(); i++) {

            String idSearcherRow = searchDataList.get(i).getSearcher();

            for (int j = 0; j < listNewSearcher.size(); j++) {

                String idSearcherAll = listNewSearcher.get(j).getIdSearcher();
                if (idSearcherRow.equals(idSearcherAll)) {
                    listNewSearcher.remove(j);
                    break;
                }
            }
        }

        mView.showAddDialog(listNewSearcher);
    }


    private void loadProject(String idProject, boolean isShowProgress) {
        if (isShowProgress)
            mView.showProgress();

        String jsonSearch = "{\"id\": \"" + idProject + "\"}";
        DataManager.getProjectRest().reqItemProject(jsonSearch, this);
    }

    @Override
    public void itemProjectResponce(ReturnResponce<Row> result) {
        if(!result.isFailure()){
            ProjectData projectData = DataManager.getDB().rowTpProgect(result.getResult());
            DataManager.getDB().projectRowWriteDatabase(projectData);
            if (projectData.getSearchers().size() != 0 && projectData.getSearchers().get(0).getRegions().size() != 0) {
                projectData.getSearchers().get(0).getRegions().get(0).setSelected(true);
            }

            searchDataList = projectData.getSearchers();

            mAdapter = mView.initAdapter(searchDataList, this);

            if (searchDataList.size() == 0) {
                mView.showEmpty(true);
            }

            initList = new ArrayList<>();
            initList.add(new SearcherItemDialog(AdapterSearchSystems.SEARCHER_YANDEX, "YANDEX"));
            initList.add(new SearcherItemDialog(AdapterSearchSystems.SEARCHER_GOOGLE, "GOOGLE"));
            initList.add(new SearcherItemDialog(AdapterSearchSystems.SEARCHER_GO_MAIL, "MAIL"));
            initList.add(new SearcherItemDialog(AdapterSearchSystems.SEARCHER_SPUTNIK, "SPUTNIK"));
            initList.add(new SearcherItemDialog(AdapterSearchSystems.SEARCHER_YOUTUBE, "YOUTUBE"));
            initList.add(new SearcherItemDialog(AdapterSearchSystems.SEARCHER_BING, "BING"));
            initList.add(new SearcherItemDialog(AdapterSearchSystems.SEARCHER_YAHOO, "YAHOO"));
            initList.add(new SearcherItemDialog(AdapterSearchSystems.SEARCHER_YANDEX_COM, "YANDEX.COM"));
            initList.add(new SearcherItemDialog(AdapterSearchSystems.SEARCHER_YANDEX_COM_TR, "YANDEX.COM.TR"));

            listNewSearcher = new ArrayList<>();
            listNewSearcher.addAll(initList);

            mView.showData();
        } else {
            mView.showError();
        }
    }

    @Override
    public void datesProjectResponce(ReturnResponce<Rows> result, String idProject, String regionKey) {
//
    }

    @Override
    public void phrasesProjectResponce(ReturnResponce<ModelProjectPhrases> result, int currentNumberPage, List<String> dates) {
//
    }

    @Override
    public void removePharseResult(ReturnResponce<ModelResult> result) {
//
    }

    @Override
    public void addPharseResult(ReturnResponce<ModelResult> result) {
//
    }

    @Override
    public void priceOnFilter(ReturnResponce<ModelPriseInspectFilter> result) {
//
    }

    @Override
    public void inspectOnFilter(ReturnResponce<ModelResult> result) {
//
    }
}
