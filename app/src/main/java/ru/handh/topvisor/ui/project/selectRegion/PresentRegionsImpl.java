package ru.handh.topvisor.ui.project.selectRegion;

import io.realm.RealmList;
import ru.handh.topvisor.data.DataManager;
import ru.handh.topvisor.data.database.project.SearchData;

/**
 * Created by sergey on 11.03.16.
 */
public class PresentRegionsImpl implements PresentRegions, AdapterRegions.ItemRegionClickListener {

    private ViewRegions mView;
    private RealmList<SearchData> regions;

    public PresentRegionsImpl(ViewRegions view, String idProject) {
        mView = view;
        regions = DataManager.getDB().getProject(idProject).getSearchers();
        mView.initAdapter(regions, this);
    }

    @Override
    public void clickItem(String idRegion) {

        DataManager.getDB().selectItemRegion(regions, idRegion);

        mView.selectRegion();
    }
}
