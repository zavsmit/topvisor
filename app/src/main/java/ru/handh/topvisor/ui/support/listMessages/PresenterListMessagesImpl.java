package ru.handh.topvisor.ui.support.listMessages;

import android.app.NotificationManager;
import android.content.Context;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import ru.handh.topvisor.data.DataManager;
import ru.handh.topvisor.data.gcm.MyGcmListenerService;
import ru.handh.topvisor.data.network.model.ModelItemMessage;
import ru.handh.topvisor.data.network.model.ModelResult;
import ru.handh.topvisor.data.network.model.ModelTicket;
import ru.handh.topvisor.data.network.model.ReturnResponce;
import ru.handh.topvisor.ui.support.SupportEndReq;

/**
 * Created by sergey on 01.04.16.
 * презентер списка сообщений
 */
public class PresenterListMessagesImpl implements PresenterListMessages, SupportEndReq, AdapterPhoto.ItemClickListener {

    private ViewListMessages mView;
    private int idTicket;
    private boolean isNewTicket;
    private List<String> photoList;

    public PresenterListMessagesImpl(ViewListMessages view, int idTicket) {
        mView = view;
        this.idTicket = idTicket;

        DataManager.getSupportRest().getImagesList(this);


        if (idTicket != 0) {
            DataManager.getSupportRest().getMessages(idTicket, this);
        } else {
            mView.showData();
            isNewTicket = true;
            mView.initAdapter(new ArrayList<ModelItemMessage>());
        }


        photoList = new ArrayList<>();
        mView.initPhotoAdapter(photoList, this);
    }

    @Override
    public void sendMessage(String text) {

        if (!text.isEmpty()) {
            mView.addMessage(text, photoList);
            if (isNewTicket) {
                DataManager.getSupportRest().sendNewDialogMessage(text, this);
            } else {
                DataManager.getSupportRest().sendMessage(text, idTicket, this);
            }
            removeAllImage();
        }
    }

    private void removeAllImage() {
        photoList.clear();
        mView.showPhotoToAdapter(false);
    }

    @Override
    public void addPhoto() {
        mView.openIntentPhoto();
    }

    @Override
    public void addSupportMessage(String text, Context context) {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) context.getApplicationContext().getSystemService(ns);
        nMgr.cancel(MyGcmListenerService.TICKET_NOTIFICATION);
        mView.addSupportMessage(text);

    }

    @Override
    public void resultIntent(String originalFilePath, String thumbnailSmallFilePath) {
        mView.addPhotoToAdapter(originalFilePath);

        File file = new File(originalFilePath);
        DataManager.getSupportRest().sendPhoto(file, this);
    }

    @Override
    public void errorRefresh() {
        if (idTicket != 0) {
            DataManager.getSupportRest().getMessages(idTicket, this);
        }
    }


    @Override
    public void listDialogsResponce(ReturnResponce<List<ModelTicket>> result) {
        if (!result.isFailure()) {
            idTicket = Integer.valueOf(result.getResult().get(0).getId());
            mView.initToolbar(result.getResult().get(0).getId());
            DataManager.getSupportRest().getMessages(idTicket, this);
        }
    }

    @Override
    public void listMessagesResponce(ReturnResponce<List<ModelItemMessage>> result) {
        if (!result.isFailure()) {
            mView.showData();
            mView.initAdapter(result.getResult());
        } else {
            mView.showError();
            mView.showInfoToast(result.getErrorMessage());
        }
    }

    @Override
    public void removiImageResponce(ReturnResponce<ModelResult> result) {
//--
    }

    @Override
    public void getImagesResponce(ReturnResponce<List<String>> result) {

        if(!result.isFailure()){
            List<String> list = result.getResult();

            for (int i = 0; i < list.size(); i++) {
                DataManager.getSupportRest().removeImage(list.get(i), this);
            }
        }

    }

    @Override
    public void sendMessageResponce(ReturnResponce<ModelResult> result) {
        if (!result.isFailure()) {
//            mView.addMessage(text);
        } else {
            mView.showInfoToast(result.getErrorMessage());
        }
    }

    @Override
    public void sendNewDialogMessageResponce(ReturnResponce<ModelResult> result) {
        if (!result.isFailure()) {
            DataManager.getSupportRest().reqDialogList(1, 10, this);
        } else {
            mView.showInfoToast(result.getErrorMessage());
        }
    }

    @Override
    public void sendPhotoResponce(ReturnResponce<ModelResult> result) {
//--
    }


    @Override
    public void removeItem(String removedString) {
        DataManager.getSupportRest().removeImage(removedString, this);
        if (photoList.size() == 0) {
            mView.showPhotoToAdapter(false);
        }
    }

}
