package ru.handh.topvisor.ui.dialogs;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;

import java.util.List;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.model.SearcherItemDialog;

/**
 * Created by sergey on 24.03.16.
 */
public class AdapterList extends RecyclerSwipeAdapter<AdapterList.ViewHolder> {
    private List<SearcherItemDialog> mDataset;
    private ItemDialogClickListener mListener;

    public AdapterList(List<SearcherItemDialog> myDataset, ItemDialogClickListener listener) {
        mDataset = myDataset;
        mListener = listener;
    }

    @Override
    public AdapterList.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_dialog, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.text.setText(mDataset.get(position).getNameSearcher());

        holder.ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.clickItem(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe_project_request;
    }

    public interface ItemDialogClickListener {
        void clickItem(int idSelected);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView text;
        public LinearLayout ll;

        public ViewHolder(View v) {
            super(v);
            text = (TextView) itemView.findViewById(R.id.tv_itemListDialog);
            ll = (LinearLayout) itemView.findViewById(R.id.ll_itemListDialog);
        }
    }
}
