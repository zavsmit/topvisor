package ru.handh.topvisor.ui.settingsProject.searchSystems.searchRegion;

import android.support.v4.app.DialogFragment;

import java.util.ArrayList;
import java.util.List;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.DataManager;
import ru.handh.topvisor.data.network.model.ModelResultType;
import ru.handh.topvisor.data.network.model.ModelSearchRegion;
import ru.handh.topvisor.data.network.model.ReturnResponce;
import ru.handh.topvisor.ui.dialogs.DialogResult;

/**
 * Created by sergey on 25.03.16.
 */
public class PresenterSearchRegionImpl implements PresenterSearchRegion,
        SearchRegionResult,
        AdapterSearchRegion.ItemSearchRegionClickListener,
        DialogResult {

    private ViewSearchRegion mView;
    private String idSearcher, typeSearcher;
    private List<ModelSearchRegion> addRegions;

    public PresenterSearchRegionImpl(ViewSearchRegion view, String idSearcher, String typeSearcher) {
        mView = view;
        this.idSearcher = idSearcher;
        this.typeSearcher = typeSearcher;
        addRegions = new ArrayList<>();
        mView.initAdapter(this);
    }


    @Override
    public void loadListRegion(String term) {
        mView.showProgress(true);
        DataManager.getSettingsRest().resSearchRegions(typeSearcher, term, this);
    }

    @Override
    public void clickEnter() {
        List<String> regionsId = new ArrayList<>();

        for (int i = 0; i < addRegions.size(); i++) {
            regionsId.add(addRegions.get(i).getId());
        }

        if (regionsId.size() != 0) {
            DataManager.getSettingsRest().reqAddRegions(idSearcher, regionsId, this);
            mView.showDialog();
        } else {
            mView.showToast(R.string.selectMin1Region);
        }

    }


    @Override
    public void searchRegionResponce(ReturnResponce<List<ModelSearchRegion>> result, String term) {
        mView.showProgress(false);
        if (!result.isFailure()) {
            mView.changeList(result.getResult(), term);
            mView.showEmpty(result.getResult().size() == 0);
        }
    }

    @Override
    public void addRegionsResponce(ReturnResponce<ModelResultType> result) {
        if (!result.isFailure()) {


            List<String> list = result.getResult().getResult();

            int size = 0;

            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).equals("1")) {
                    ++size;
                }
            }

            mView.showResult(size);
        } else {
            mView.showToast(result.getErrorMessage());
        }
    }

    @Override
    public void clickItem(ModelSearchRegion region, boolean isAdd) {
        if (isAdd) {
            addRegions.add(region);
        } else {
            for (int i = 0; i < addRegions.size(); i++) {
                if (addRegions.get(i).getId().equals(region.getId())) {
                    addRegions.remove(i);
                    break;
                }
            }
        }
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        mView.end();
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {

    }
}
