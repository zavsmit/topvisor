package ru.handh.topvisor.ui.project.selectRegion;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import ru.handh.topvisor.R;
import ru.handh.topvisor.data.database.project.RegionData;
import ru.handh.topvisor.data.database.project.SearchData;

/**
 * Created by sergey on 11.03.16.
 * адаптер списка регионов
 */
public class AdapterRegions extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_TITLE = 1;
    private static final int TYPE_REGION = 2;
    private static final int TYPE_DIVIDER = 3;
    private List<RegionsData> regionsList;
    private ItemRegionClickListener mListener;


    public AdapterRegions(RealmList<SearchData> dataset, ItemRegionClickListener listener) {
        mListener = listener;
        regionsList = new ArrayList<>();

        for (int i = 0; i < dataset.size(); i++) {
            regionsList.add(new RegionsData(TYPE_TITLE, dataset.get(i).getName()));


            RealmList<RegionData> regions = dataset.get(i).getRegions();

            for (int j = 0; j < regions.size(); j++) {
                regionsList.add(new RegionsData(TYPE_REGION, regions.get(j), regions.get(j).isSelected()));
            }

            regionsList.add(new RegionsData(TYPE_DIVIDER));
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case TYPE_TITLE:
                return new ViewHolderTitle(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_region_title, parent, false));
            case TYPE_REGION:
                return new ViewHolderRegion(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_region_selected, parent, false));
            case TYPE_DIVIDER:
                return new ViewHolderDivider(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_divider, parent, false));
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        switch (holder.getItemViewType()) {

            case TYPE_TITLE:

                ((ViewHolderTitle) holder).text.setText(regionsList.get(position).title);

                break;

            case TYPE_REGION:

                final ViewHolderRegion mHolderRegion = ((ViewHolderRegion) holder);

                RegionData region = regionsList.get(position).region;

                mHolderRegion.name.setText(region.getName());


                String code;
                if (region.getDomain().isEmpty() || region.getDomain().equals("false")) {
                    code = "0";
                } else {
                    code = region.getDomain();
                }
                code = code + " [" + region.getLang() + "]";
                mHolderRegion.code.setText(code);


                if (region.getCountryCode().isEmpty()) {
                    mHolderRegion.image.setVisibility(View.INVISIBLE);
                } else {
                    mHolderRegion.image.setVisibility(View.VISIBLE);
                    String urlImage = "https://topvisor.ru/images/common/flags/" + region.getCountryCode() + ".png";
                    Picasso.with(mHolderRegion.image.getContext()).load(urlImage).into(mHolderRegion.image);
                }


                mHolderRegion.rl.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        redravList(position);
                    }
                });


                if (regionsList.get(position).isChecked) {
                    mHolderRegion.radioButton.setChecked(true);
                } else {
                    mHolderRegion.radioButton.setChecked(false);
                }

                break;
        }
    }

    private void redravList(int position) {

        mListener.clickItem(regionsList.get(position).region.getId());

        for (int i = 0; i < regionsList.size(); i++) {
            regionsList.get(i).isChecked = position == i;
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return regionsList.size();
    }


    @Override
    public int getItemViewType(int position) {

        switch (regionsList.get(position).type) {
            case TYPE_TITLE:
                return TYPE_TITLE;
            case TYPE_REGION:
                return TYPE_REGION;
            case TYPE_DIVIDER:
                return TYPE_DIVIDER;
            default:
                return TYPE_DIVIDER;
        }
    }


    public interface ItemRegionClickListener {
        void clickItem(String idRegion);
    }

    public class ViewHolderTitle extends RecyclerView.ViewHolder {
        public TextView text;

        public ViewHolderTitle(View v) {
            super(v);
            text = (TextView) itemView.findViewById(R.id.tv_itemRegion_title);
        }
    }

    public class ViewHolderRegion extends RecyclerView.ViewHolder {
        public TextView name, code;
        public ImageView image;
        public RadioButton radioButton;
        public RelativeLayout rl;

        public ViewHolderRegion(View v) {
            super(v);
            name = (TextView) itemView.findViewById(R.id.tv_regionItem_name);
            code = (TextView) itemView.findViewById(R.id.tv_regionItem_code);
            image = (ImageView) itemView.findViewById(R.id.iv_regionItem_icon);
            radioButton = (RadioButton) itemView.findViewById(R.id.rb_regionItem);
            rl = (RelativeLayout) itemView.findViewById(R.id.rl_regionItem);

        }
    }

    public class ViewHolderDivider extends RecyclerView.ViewHolder {
        public ViewHolderDivider(View v) {
            super(v);
        }
    }

    private class RegionsData {
        int type;
        String title;
        RegionData region;
        boolean isChecked;

        RegionsData(int type) {
            this.type = type;
            title = "";
        }

        RegionsData(int type, String title) {
            this.type = type;
            this.title = title;
        }

        RegionsData(int type, RegionData region, boolean isChecked) {
            this.type = type;
            this.region = region;
            this.isChecked = isChecked;
        }
    }
}