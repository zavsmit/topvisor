package ru.handh.topvisor.ui.support.listMessages;

import android.content.Context;

/**
 * Created by sergey on 01.04.16.
 */
public interface PresenterListMessages {
    void sendMessage(String text);
    void addPhoto();
    void addSupportMessage(String text, Context context);
    void resultIntent(String originalFilePath, String thumbnailSmallFilePath);
    void errorRefresh();
}
