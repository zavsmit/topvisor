package ru.handh.topvisor.ui.settingsProject;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import ru.handh.topvisor.R;
import ru.handh.topvisor.ui.base.ParentBackActivity;
import ru.handh.topvisor.ui.settingsProject.competitor.ActivityCompetitors;
import ru.handh.topvisor.ui.settingsProject.group.ActivityGroups;
import ru.handh.topvisor.ui.settingsProject.interval.ActivityIntervalInspect;
import ru.handh.topvisor.ui.settingsProject.searchSystems.ActivitySearchSystems;

/**
 * Created by sergey on 16.03.16.
 */
public class ActivitySettings extends ParentBackActivity implements ViewSettings {

    public final static int ACTIVITY_INTERVAL = 0;
    public final static int ACTIVITY_SEARCHER = 1;
    public final static int ACTIVITY_GROUPS = 2;
    public final static int ACTIVITY_COMPETITORS = 3;
    private PresenterSettings presenter;
    private TextView tvIntervalName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_settings);

        initToolbar(R.id.toolbar, getString(R.string.settings));

        tvIntervalName = (TextView) findViewById(R.id.tv_settings_intervalName);

        String idProject = "";

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            idProject = extras.getString("idProject");
        }


        presenter = new PresenterSettingsImpl(this, idProject, this);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.changeDemand();
    }

    public void clickItem(View view) {
        switch (view.getId()) {
            case R.id.ll_settings_interval:
                presenter.clickOpenActvity(ACTIVITY_INTERVAL);
                break;

            case R.id.tv_settings_searcher:
                presenter.clickOpenActvity(ACTIVITY_SEARCHER);
                break;

            case R.id.tv_settings_groups:
                presenter.clickOpenActvity(ACTIVITY_GROUPS);
                break;

            case R.id.tv_settings_сompetitors:
                presenter.clickOpenActvity(ACTIVITY_COMPETITORS);
                break;
        }
    }


    @Override
    public void openInterval(String idProject) {
        Intent intent = new Intent(this, ActivityIntervalInspect.class);
        intent.putExtra("idProject", idProject);
        startActivityForResult(intent, ACTIVITY_INTERVAL);
    }

    @Override
    public void openSearcher(String idProject) {
        Intent intent = new Intent(this, ActivitySearchSystems.class);
        intent.putExtra("idProject", idProject);
        startActivity(intent);
    }

    @Override
    public void openGroups(String idProject) {
        Intent intent = new Intent(this, ActivityGroups.class);
        intent.putExtra("name", getString(R.string.groups));
        intent.putExtra("idProject", idProject);
        startActivity(intent);
    }

    @Override
    public void openCompetitors(String idProject) {
        Intent intent = new Intent(this, ActivityCompetitors.class);
        intent.putExtra("name", getString(R.string.competitors));
        intent.putExtra("idProject", idProject);
        startActivity(intent);
    }

    @Override
    public void setDiscription(String text) {
        tvIntervalName.setText(text);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            presenter.close();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        presenter.close();
    }

    @Override
    public void close() {
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
    }


}
