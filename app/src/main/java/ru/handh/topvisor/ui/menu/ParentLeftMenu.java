package ru.handh.topvisor.ui.menu;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.DataManager;
import ru.handh.topvisor.data.network.model.ModelUserData;
import ru.handh.topvisor.ui.bank.ActivityBank;
import ru.handh.topvisor.ui.licence.ActivityLicence;
import ru.handh.topvisor.ui.main.ActivityMainView;
import ru.handh.topvisor.ui.pay.ActivityRefill;
import ru.handh.topvisor.ui.support.ActivityDialogsList;
import ru.handh.topvisor.ui.support.RoundedImageView;
import ru.handh.topvisor.utils.Utils;

/**
 * Created by sergey on 09.04.16.
 * родительское активити для меню
 */
public abstract class ParentLeftMenu extends AppCompatActivity implements
        SwipeRefreshLayout.OnRefreshListener, ViewMenu {

    public PresenterMenu presenter;
    protected Toolbar toolbar;
    protected String title = "";
    protected DrawerLayout drawer;
    protected boolean isMainScreen = true;
    private TextView limit, balance, name, url, badge;
    private RoundedImageView photo;
    private SwipeRefreshLayout srlMenu;
    private int idClickSelect = -1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent_menu);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        initView();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                // Do whatever you want here

                if (idClickSelect != -1)
                    clickId(idClickSelect);
                idClickSelect = -1;
            }
        };
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        toolbar.setNavigationIcon(R.drawable.menu_vector);


        srlMenu = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        srlMenu.setOnRefreshListener(this);

        limit = (TextView) findViewById(R.id.tv_menu_limit);
        balance = (TextView) findViewById(R.id.tv_menu_balance);
        name = (TextView) findViewById(R.id.tv_menu_name);
        url = (TextView) findViewById(R.id.tv_menu_url);
        photo = (RoundedImageView) findViewById(R.id.iv_menu_photo);
        badge = (TextView) findViewById(R.id.tv_menu_badge);


        presenter = new PresenterMenuImpl(this, isMainScreen);
    }

    protected void setTitle(String title) {
        toolbar.setTitle(title);
    }

    private void clickId(int id) {
        switch (id) {

            case R.id.rl_menu_wallet:
                clickBank();
                break;

            case R.id.rl_menu_case:
                clickProjects();
                break;

            case R.id.rl_menu_support:
                clickSupport();
                break;

            case R.id.rl_menu_agreement:
                clickAgreement();
                break;

            case R.id.rl_menu_exit:
                logout();
                break;

            case R.id.tv_menu_add:
                startActivity(new Intent(this, ActivityRefill.class));
                break;
        }
    }

    public void clickMenu(View view) {
        idClickSelect = view.getId();
        closeDtaver();
    }

    protected void clickBank() {
        startActivity(new Intent(this, ActivityBank.class));
    }

    protected void clickProjects() {
        startActivity(new Intent(this, ActivityMainView.class));
    }

    protected void clickSupport() {
        startActivity(new Intent(this, ActivityDialogsList.class));
    }

    protected void clickAgreement() {
        startActivity(new Intent(this, ActivityLicence.class));
    }

    public void closeDtaver() {
        drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public void logout() {
        DataManager.getPref().logoutApp(ParentLeftMenu.this);
    }

    @Override
    public void onBackPressed() {

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            closeDtaver();
        } else {

            if (!isMainScreen) {
                NavUtils.navigateUpFromSameTask(this);
            } else {
                super.onBackPressed();

            }
        }


    }

    @Override
    public void onRefresh() {
        presenter.loadUserdata();
    }

    @Override
    public void showRefreshMenu(boolean isShow) {
        srlMenu.setRefreshing(isShow);
    }

    @Override
    public void showToast(int idString) {
        Toast.makeText(this, getString(idString), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void initMenu(ModelUserData modelUserData) {

        if (modelUserData.getName() == null) {
            return;
        }

        name.setText(modelUserData.getName());
        url.setText(modelUserData.getEmail());

        SpannableString stringChange;

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            double balance = Utils.round(modelUserData.getBalance() + modelUserData.getTariff().getBalance_t(), 2);
            stringChange = new SpannableString(getString(R.string.balance) + " " + balance + " " + getString(R.string.valuteSimbol));
        } else {
            stringChange = new SpannableString(getString(R.string.balance) + " " + modelUserData.getBalance() + " " + getString(R.string.valuteString));
        }

        stringChange.setSpan(new ForegroundColorSpan(Utils.getCustomColor(this, R.color.textWhite)), 0, 7, 0);

        balance.setText(stringChange);

        SpannableString stringChange1 = new SpannableString(getString(R.string.xmlLimits) + " " + modelUserData.getYandex_xml_balance());
        stringChange1.setSpan(new ForegroundColorSpan(Utils.getCustomColor(this, R.color.textWhite)), 0, 11, 0);

        limit.setText(stringChange1);

        if (!modelUserData.getAvatar().isEmpty()){
            String url = DataManager.getPref().getServer();


            url = url.substring(0, url.length() - 1);
            url = url + modelUserData.getAvatar();

            Picasso.with(ParentLeftMenu.this).load(url).into(photo);
        }




    }

    @Override
    public void initCountNewMessage(int newMessage) {
        if (newMessage == 0) {
            toolbar.setNavigationIcon(R.drawable.menu_vector);
            badge.setVisibility(View.GONE);
        } else {
            toolbar.setNavigationIcon(R.drawable.menu_new_vector);
            badge.setVisibility(View.VISIBLE);
            badge.setText(String.valueOf(newMessage));
        }
    }

    protected abstract void initView();

}
