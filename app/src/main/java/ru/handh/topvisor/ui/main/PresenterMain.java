package ru.handh.topvisor.ui.main;

/**
 * Created by sergey on 10.02.16.
 * интерфейс презентера экран проектов
 */
public interface PresenterMain {

    void loadList();

    void onStop();

    void initFiltersHead();

    void refreshAll();

    void openSearchActivity();

    void onPause(int visiblePosition);
}
