package ru.handh.topvisor.ui.dialogs;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;
import java.util.Date;

import ru.handh.topvisor.ui.bank.ActivityBank;
import ru.handh.topvisor.ui.bank.DateSet;

/**
 * Created by sergey on 07.04.16.
 */
public class DateDialog extends DialogFragment implements DatePickerDialog.OnDateSetListener{

    private DateSet mListener;

    public DateDialog() {
    }

    public static DateDialog newInstance(Date date, int type) {
        DateDialog f = new DateDialog();

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        Bundle args = new Bundle();
        args.putInt("DAY_OF_MONTH", cal.get(Calendar.DAY_OF_MONTH));
        args.putInt("MONTH", cal.get(Calendar.MONTH));
        args.putInt("YEAR", cal.get(Calendar.YEAR));
        args.putInt("type", type);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (DateSet) ((ActivityBank) activity).presenter;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new DatePickerDialog(getActivity(), this,
                getArguments().getInt("YEAR"), getArguments().getInt("MONTH"), getArguments().getInt("DAY_OF_MONTH"));
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        mListener.setDate(year, monthOfYear, dayOfMonth, getArguments().getInt("type"));
    }
}
