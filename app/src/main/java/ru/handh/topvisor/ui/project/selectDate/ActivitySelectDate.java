package ru.handh.topvisor.ui.project.selectDate;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ViewFlipper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import ru.handh.topvisor.R;
import ru.handh.topvisor.ui.base.ParentBackActivity;
import ru.handh.topvisor.ui.project.selectDate.listDates.ActivityListDates;

/**
 * Created by sergey on 15.03.16.
 * адаптер списка дат
 */
public class ActivitySelectDate extends ParentBackActivity implements ViewSelectDate {

    public PresenterSelectDate presenter;
    private RecyclerView mRecyclerView;
    private AdapterSelectDate mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_yandex_inspect);
        initToolbar(R.id.toolbar, getString(R.string.selectTime));
        viewFlipperMain = (ViewFlipper) findViewById(R.id.viewFlipper);
        showData();
        mRecyclerView = (RecyclerView) findViewById(R.id.recicleView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabSearchSystem);
        fab.setVisibility(View.GONE);


        List<String> allDates = new ArrayList<>();
        List<Integer> selectedList = new ArrayList<>();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            allDates = extras.getStringArrayList("allDates");
            selectedList = extras.getIntegerArrayList("selectedList");
        }


        if (allDates.size() == 0) {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            Calendar cal = Calendar.getInstance();
            allDates.add(dateFormat.format(cal.getTime()));
        }

        presenter = new PresenterSelectDateImpl(this, allDates, selectedList);
    }

    @Override
    public AdapterSelectDate initAdapter(List<String> allDates, AdapterSelectDate.ItemSearcherClickListener listener, List<Integer> integerList) {

        mAdapter = new AdapterSelectDate(allDates, listener, integerList, this);
        mRecyclerView.setAdapter(mAdapter);
        return mAdapter;
    }

    @Override
    public void openListDate(int selectPosition, ArrayList<String> list, int typeSelect) {
        Intent intent = new Intent(this, ActivityListDates.class);
        intent.putExtra("selectPosition", selectPosition);
        intent.putExtra("typeSelect", typeSelect);
        intent.putStringArrayListExtra("listDates", list);
        startActivityForResult(intent, 100);
    }

    @Override
    public void close(ArrayList<Integer> selectedPositions) {
        Intent intent = new Intent();
        intent.putIntegerArrayListExtra("selectedPositions", selectedPositions);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        presenter.close();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            presenter.close();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void notificationAdapter() {
        mAdapter.changeDate();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            presenter.returnSelectItem(data.getIntExtra("idSelected", 0), data.getIntExtra("typeSelect", 0));
        }
    }


}
