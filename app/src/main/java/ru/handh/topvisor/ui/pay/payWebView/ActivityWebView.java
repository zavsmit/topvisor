package ru.handh.topvisor.ui.pay.payWebView;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.DataManager;
import ru.handh.topvisor.ui.base.ParentBackActivity;

/**
 * Created by sergey on 11.04.16.
 */
public class ActivityWebView extends ParentBackActivity {

    public final static int PAY_OK = 1;
    public final static int PAY_FALSE = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String url = "";

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            url = extras.getString("url");
        }

        setContentView(R.layout.activity_pay_web);
        initToolbar(R.id.toolbar, getString(R.string.refill1));

        WebView webView = (WebView) findViewById(R.id.webviewPay);

        assert webView != null;
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);


        CookieManager cookieManager = CookieManager.getInstance();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            CookieSyncManager.createInstance(this);
        }
        cookieManager.setAcceptCookie(true);


        cookieManager.setCookie(url, "auth=" + DataManager.getPref().getTokenApp());
        CookieSyncManager.getInstance().sync();

        webView.getSettings().setAppCacheEnabled(true);

        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                Log.deb("shouldOverrideUrlLoading url = " + url);


                if (url.contains("https://topvisor.ru/account/bank/")) {

                    Intent intent = new Intent();

                    if (url.contains("https://topvisor.ru/account/bank/ok")) {
                        intent.putExtra("status", PAY_OK);
                    }
                    if (url.contains("https://topvisor.ru/account/bank/nok")) {

                        intent.putExtra("status", PAY_FALSE);

                    }

                    setResult(RESULT_OK, intent);
                    finish();
                }


                view.loadUrl(url);
                return false;
            }

        });

        webView.loadUrl(url);

    }


}
