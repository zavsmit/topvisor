package ru.handh.topvisor.ui.dialogs;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

/**
 * Created by sergey on 15.03.16.
 */
public class LoginProgressDialog extends DialogFragment {


    public LoginProgressDialog() {
    }

    public static LoginProgressDialog newInstance(String name) {
        LoginProgressDialog f = new LoginProgressDialog();

        Bundle args = new Bundle();
        args.putString("name", name);
        f.setArguments(args);

        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        ProgressDialog pd = new ProgressDialog(getActivity());
        pd.setTitle(getArguments().getString("name"));

        return pd;
    }

}