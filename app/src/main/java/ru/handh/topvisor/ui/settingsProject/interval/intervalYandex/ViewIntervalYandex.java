package ru.handh.topvisor.ui.settingsProject.interval.intervalYandex;

import java.util.List;

/**
 * Created by sergey on 28.03.16.
 */
public interface ViewIntervalYandex {

    void selectGroup(int idSelected);
    void initAdapter(List<String> groups, AdapterYandexInsert.ItemGroupClickListener listener, int select);
}
