package ru.handh.topvisor.ui.removeItemsList;

import android.view.ActionMode;

import io.realm.RealmList;
import ru.handh.topvisor.data.database.project.CompetitorData;
import ru.handh.topvisor.data.database.project.GroupData;

/**
 * Created by sergey on 21.03.16.
 */
public interface ViewRemoveItemsList {
    AdapterRemoveItemsList initAdapter(RealmList<GroupData> groups, RealmList<CompetitorData> competitors, AdapterRemoveItemsList.ItemSearcherClickListener listener);
    void selectRegion(int position);
    void showHide(boolean isShow);
    ActionMode startMyActionMode(ActionMode.Callback callback);
    void showDialogList(int dialogType, String name,String hint, int position);
    void showToast(String text);

    void showEmptyText(boolean isShow);
}
