package ru.handh.topvisor.ui.main;

import android.app.Activity;
import android.support.v4.app.DialogFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.DataManager;
import ru.handh.topvisor.data.network.model.ModelPriceForProgects;
import ru.handh.topvisor.data.network.model.ModelResult;
import ru.handh.topvisor.data.network.model.ReturnResponce;
import ru.handh.topvisor.data.network.model.currentProject.Row;
import ru.handh.topvisor.ui.dialogs.DialogResult;
import ru.handh.topvisor.ui.dialogs.NewTicketDialog;
import ru.handh.topvisor.ui.dialogs.PriceDialog;
import ru.handh.topvisor.ui.dialogs.RecoveryDialog;
import ru.handh.topvisor.ui.dialogs.RemoveDialog;
import ru.handh.topvisor.ui.dialogs.RenameDialog;
import ru.handh.topvisor.utils.Log;
import ru.handh.topvisor.utils.Utils;

/**
 * Created by sergey on 10.02.16.
 * логика экрана проектов
 */
public class PresenterMainImpl implements PresenterMain, MainEndReq,
        AdapterMain.ItemSwipeClickListener, DialogResult {

    private static final int ALL_INSPECT_PROJECT = -1;
    private static final int NOT_INSPECT_PROJECT = 0;
    private Activity context;
    private int countReqInspect = 0;
    private MainView view;
    private int currentId;
    private ArrayList<Row> myDataset;
    private Timer timer;
    private int lastVisiblePosition;

    public PresenterMainImpl(MainView view, Activity context) {
        this.view = view;
        this.context = context;
    }

    @Override
    public void loadList() {
        view.showRefresh(true);
        DataManager.getMainRest().reqGetProjects(this);
    }

    @Override
    public void onStop() {
        if (timer != null)
            timer.cancel();

        DataManager.getDB().closeDB();
    }

    @Override
    public void initFiltersHead() {
        DataManager.getDB().initDB();
        DataManager.getMainRest().initHeader();
        view.setDataToFilter(DataManager.getMainRest().getHeaderText(),
                DataManager.getMainRest().getIdResLabel());
        loadList();
        startUpdatesForTimer();
    }

    @Override
    public void refreshAll() {
        view.showDialogMain(ActivityMainView.DIALOG_PRICE_PROJECT, context.getString(R.string.allProjects));

        List<Integer> listIds = new ArrayList<>();

        for (int i = 0; i < myDataset.size(); i++) {
            listIds.add(Integer.valueOf(myDataset.get(i).getId()));
        }

        DataManager.getMainRest().reqPriceProjects(this, listIds);
        currentId = ALL_INSPECT_PROJECT;
    }

    @Override
    public void openSearchActivity() {
        view.openSearchActivity(myDataset);
    }

    @Override
    public void onPause(int visiblePosition) {
        lastVisiblePosition = visiblePosition;
        if (timer != null)
            timer.cancel();
    }

    @Override
    public void addProjectsResponce(ReturnResponce<ModelResult> result) {
        if (!result.isFailure()) {
            DataManager.getMainRest().reqGetProjects(this);
        } else {
            view.showToast(result.getErrorMessage());
        }
    }

    @Override
    public void getProjectsResponce(ReturnResponce<ArrayList<Row>> result) {
        if (!result.isFailure()) {


            ArrayList<Row> list = result.getResult();

            if (list != null && list.size() != 0) {
                myDataset = result.getResult();

                DataManager.getDB().projectWriteDatabase(list);
                view.setTextToHeader(list.size());
                view.initAdapter(list, this);
                view.scrollToPosition(lastVisiblePosition);
                view.showData();
                view.showEmpty(false);
            } else {
                view.showToast(result.getErrorMessage());
                view.setTextToHeader(0);
                view.showEmpty(true);
            }


            view.showRefresh(false);
            view.showData();


        } else {
            view.showToast(context.getString(R.string.settingNet));
            view.showRefresh(false);
            view.showEmpty(true);
            view.showError();
            view.setTextToHeader(0);
        }

    }

    @Override
    public void getPriceProgect(ReturnResponce<ModelPriceForProgects> result) {
        if (!result.isFailure()) {

            if (result.getResult() != null) {

                double price = Utils.round(result.getResult().getPrice(), 2);
                double xml = Utils.round(result.getResult().getXml_for_use(), 2);

                view.showDescriptionInDialog(price, xml);

            } else {
                view.showToast(result.getErrorMessage());
            }

        } else {
            view.showToast(context.getString(R.string.settingNet));
        }
    }

    @Override
    public void getInspectProjectResult(ReturnResponce<ModelResult> result) {

        if (currentId == ALL_INSPECT_PROJECT && myDataset.size() - 1 > countReqInspect) {
            ++countReqInspect;
            Log.deb("end = " + countReqInspect);
        } else {
            Log.deb("normLoad = " + countReqInspect);
            if (result.getResult().getResult().equals("1")) {
                DataManager.getMainRest().reqGetProjects(this);
            } else {
                view.showToast(context.getString(R.string.settingNet));
            }
            view.showRefresh(false);
            countReqInspect = NOT_INSPECT_PROJECT;
            currentId = NOT_INSPECT_PROJECT;
        }

    }

    @Override
    public void clickItem(int idProgect, String name, int size, int position) {
        view.openActivity(myDataset.get(position));
    }

    @Override
    public void clickTrash(int idProgect, String name, int position) {
        currentId = idProgect;
        view.showDialogMain(ActivityMainView.DIALOG_REMOVE_PROJECT, name, position);
    }

    @Override
    public void clickRefresh(int idProgect, String name) {

        view.showDialogMain(ActivityMainView.DIALOG_PRICE_PROJECT, name);

        if (idProgect != 0) {
            List<Integer> listIds = new ArrayList<>();
            listIds.add(idProgect);
            DataManager.getMainRest().reqPriceProjects(this, listIds);
        }
        currentId = idProgect;
    }

    @Override
    public void clickRename(int idProgect, String name, int position) {
        currentId = idProgect;
        view.showDialogMain(ActivityMainView.DIALOG_RENAME_PROJECT, name, position);
    }

    @Override
    public void clickRecovery(int idProgect, String name, int position) {
        currentId = idProgect;
        view.showDialogMain(ActivityMainView.DIALOG_RECOVERY_PROJECT, name, position);
    }


    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        if (dialog instanceof NewTicketDialog) {
            String name = ((NewTicketDialog) dialog).name.getText().toString().trim();
            DataManager.getMainRest().reqAddProjects(this, name);
            return;
        }
        if (dialog instanceof PriceDialog && currentId != NOT_INSPECT_PROJECT) {
            if (currentId == ALL_INSPECT_PROJECT) {
                inspectAllProjects();
            } else {
                DataManager.getMainRest().reqInspectionProjects(this, currentId);
            }
            return;
        }
        if (dialog instanceof RemoveDialog && currentId != NOT_INSPECT_PROJECT) {
            view.removeItemList(((RemoveDialog) dialog).mPosition);
            DataManager.getMainRest().reqRemoveProjects(this, currentId);
            return;
        }
        if (dialog instanceof RenameDialog && currentId != NOT_INSPECT_PROJECT) {
            view.renameItemList(((RenameDialog) dialog).mPosition,
                    ((RenameDialog) dialog).name.getText().toString().trim());
            DataManager.getMainRest().reqRenameProjects(this, currentId, ((RenameDialog) dialog).name.getText().toString().trim());
            return;
        }
        if (dialog instanceof RecoveryDialog && currentId != NOT_INSPECT_PROJECT) {
            view.removeItemList(((RecoveryDialog) dialog).mPosition);
            DataManager.getMainRest().reqRecoveryProjects(this, currentId);
        }
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        if (dialog instanceof PriceDialog) {
            currentId = NOT_INSPECT_PROJECT;
        }
    }

    private void inspectAllProjects() {
        view.showRefresh(true);
        for (int i = 0; i < myDataset.size(); i++) {
            DataManager.getMainRest().reqInspectionProjects(this, Integer.valueOf(myDataset.get(i).getId()));
        }
    }

    private void startUpdatesForTimer() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }

        timer = new Timer();
        timer.scheduleAtFixedRate(new UpdateTimeTask(), 30000, 30000);
    }

    private class UpdateTimeTask extends TimerTask {
        public void run() {
            context.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadList();
                }
            });
        }
    }
}
