package ru.handh.topvisor.ui.settingsProject.searchSystems;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ActionMode;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import java.util.ArrayList;

import io.realm.RealmList;
import ru.handh.topvisor.R;
import ru.handh.topvisor.data.database.project.SearchData;
import ru.handh.topvisor.data.model.SearcherItemDialog;
import ru.handh.topvisor.ui.base.ParentBackActivity;
import ru.handh.topvisor.ui.dialogs.AddListPSDialog;
import ru.handh.topvisor.ui.settingsProject.searchSystems.searchRegion.ActivitySearchRegion;
import ru.handh.topvisor.ui.settingsProject.searchSystems.typeSystems.ActivityTypeSystem;


/**
 * Created by sergey on 17.03.16.
 */
public class ActivitySearchSystems extends ParentBackActivity implements ViewSearchSystems {
    public PresenterSearchSystems presenter;
    private RecyclerView mRecyclerView;
    private TextView tv_empty;
    private FloatingActionButton fab;
    private DialogFragment dialog;
    private String idProject = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_yandex_inspect);

        initToolbar(R.id.toolbar, getString(R.string.searchSystem));

        viewFlipperMain = (ViewFlipper) findViewById(R.id.viewFlipper);
        tv_empty = (TextView) findViewById(R.id.tv_empty);
        tv_empty.setText(R.string.addSearchSystem);
        mRecyclerView = (RecyclerView) findViewById(R.id.recicleView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        fab = (FloatingActionButton) findViewById(R.id.fabSearchSystem);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.addPs();
            }
        });


        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            idProject = extras.getString("idProject");
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        presenter = new PresenterSearchSystemsImpl(this, idProject);
    }

    public void clickButtons(View view) {

        switch (view.getId()) {
            case R.id.button_reqError:
                presenter = new PresenterSearchSystemsImpl(this, idProject);
                break;
        }
    }

    public void showEmpty(boolean isShow) {

        if (isShow) {
            viewFlipperMain.setVisibility(View.GONE);
            tv_empty.setVisibility(View.VISIBLE);
        } else {
            viewFlipperMain.setVisibility(View.VISIBLE);
            tv_empty.setVisibility(View.GONE);
        }

    }

    @Override
    public AdapterSearchSystems initAdapter(RealmList<SearchData> listSearch, AdapterSearchSystems.ItemSearcherClickListener listener) {
        AdapterSearchSystems mAdapter = new AdapterSearchSystems(listSearch, listener, this);
        mRecyclerView.setAdapter(mAdapter);
        return mAdapter;
    }

    @Override
    public void selectRegion(String idRegion, String idProject, int position) {
        Intent intent = new Intent(this, ActivityTypeSystem.class);
        intent.putExtra("position", position);
        intent.putExtra("idProject", idProject);
        intent.putExtra("idRegion", idRegion);
        startActivity(intent);
    }

    @Override
    public void showHide(boolean isShow) {
        if (isShow) {
            toolbar.setVisibility(View.VISIBLE);
            fab.setVisibility(View.VISIBLE);
        } else {
            toolbar.setVisibility(View.GONE);
            fab.setVisibility(View.GONE);
        }
    }

    @Override
    public ActionMode startMyActionMode(ActionMode.Callback callback) {
        return startActionMode(callback);
    }

    @Override
    public void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showAddDialog(ArrayList<SearcherItemDialog> list) {
        dialog = AddListPSDialog.newInstance(list);
        String dialogTag = "AddListPSDialog";
        dialog.show(getSupportFragmentManager(), dialogTag);
    }

    @Override
    public void hideAddPSDialog() {
        dialog.dismiss();
    }

    @Override
    public void openAddRegion(String idSearcher, String typeSearcher) {
        Intent intent = new Intent(this, ActivitySearchRegion.class);
        intent.putExtra("idSearcher", idSearcher);
        intent.putExtra("typeSearcher", typeSearcher);
        startActivity(intent);
    }
}
