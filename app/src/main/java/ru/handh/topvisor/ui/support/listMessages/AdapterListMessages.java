package ru.handh.topvisor.ui.support.listMessages;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.squareup.picasso.Picasso;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.DataManager;
import ru.handh.topvisor.data.network.model.ModelItemMessage;
import ru.handh.topvisor.ui.support.RoundedImageView;

/**
 * Created by sergey on 01.04.16.
 * адаптер
 */
public class AdapterListMessages extends RecyclerSwipeAdapter<AdapterListMessages.ViewHolder> {
    private List<ModelItemMessage> mDataset;
    private ModelItemMessage startModel;
    private Calendar cal = Calendar.getInstance();
    private int dayToday, monthToday, yearToday;

    public AdapterListMessages(List<ModelItemMessage> list) {
        mDataset = list;



        if (list.size() != 0) {
            startModel = mDataset.get(0);
            mDataset.remove(0);

            Collections.reverse(mDataset);
        }

        dayToday = cal.get(Calendar.DAY_OF_MONTH);
        monthToday = cal.get(Calendar.MONTH);
        yearToday = cal.get(Calendar.YEAR);
    }

    @Override
    public AdapterListMessages.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        cal.setTime(mDataset.get(position).getTime());

        String time;
        if (cal.get(Calendar.DAY_OF_MONTH) == dayToday && cal.get(Calendar.MONTH) == monthToday && cal.get(Calendar.YEAR) == yearToday) {
            time = String.format(Locale.US, "%02d:%02d", cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE));
        } else if (cal.get(Calendar.YEAR) == yearToday) {
            time = String.format(Locale.US, "%2d.%02d", cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.MONTH) + 1);
        } else {
            time = String.format(Locale.US, "%2d.%02d", cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.MONTH)) + 1 + "." + cal.get(Calendar.YEAR);
        }


        if (mDataset.get(position).getUser().equals(startModel.getUser())) {
            holder.rlMy.setVisibility(View.VISIBLE);
            holder.rlOperator.setVisibility(View.GONE);
            holder.text.setText(Html.fromHtml(mDataset.get(position).getText()));
            holder.timeMy.setText(time);


            if (mDataset.get(position).getFiles().size() != 0) {
                holder.recyclerView.setHasFixedSize(true);
                holder.recyclerView.setLayoutManager(new StaggeredGridLayoutManager(4, 1));
                holder.recyclerView.setAdapter(new AdapterPhotoInMessage(mDataset.get(position).getFiles()));
            }

        } else {
            holder.rlMy.setVisibility(View.GONE);
            holder.rlOperator.setVisibility(View.VISIBLE);
            holder.textOperator.setText(Html.fromHtml(mDataset.get(position).getText()));
            holder.textOperator.setMovementMethod(LinkMovementMethod.getInstance());

            String urlStart = DataManager.getPref().getServer();
            urlStart = urlStart.substring(0, urlStart.length() - 1);
            // последнего емента
            String urlImage = urlStart + mDataset.get(position)
                    .getAvatar();
            Picasso.with(holder.ivOperator.getContext()).load(urlImage).into(holder.ivOperator);
            holder.timeOperator.setText(time);
        }

    }

    public void addMessage(String message, List<String> photos) {

        ModelItemMessage itemMessage = new ModelItemMessage();

        if (startModel == null) {
            startModel = new ModelItemMessage();
            startModel.setUser("0");
        }
        itemMessage.setUser(startModel.getUser());

        itemMessage.setText(message);
        itemMessage.setFiles(photos);

        Calendar cal = Calendar.getInstance();
        new Date(cal.getTimeInMillis());
        itemMessage.setTime(new Date(cal.getTimeInMillis()));

        mDataset.add(itemMessage);
        notifyItemInserted(mDataset.size() - 1);
    }

    public void addSupportMessage(String message) {

        ModelItemMessage itemMessage = new ModelItemMessage();

        if (startModel == null) {
            startModel = new ModelItemMessage();
            startModel.setUser("0");
        }
        itemMessage.setUser(startModel.getUser() + 5);

        itemMessage.setText(message);


        Calendar cal = Calendar.getInstance();
        new Date(cal.getTimeInMillis());
        itemMessage.setTime(new Date(cal.getTimeInMillis()));

        mDataset.add(itemMessage);
        notifyItemInserted(mDataset.size() - 1);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe_project_request;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textOperator, text, timeMy, timeOperator;
        public RelativeLayout rlOperator, rlMy;
        public RoundedImageView ivOperator;
        public RecyclerView recyclerView;

        public ViewHolder(View v) {
            super(v);
            textOperator = (TextView) itemView.findViewById(R.id.tv_itemMessageOperator);
            timeMy = (TextView) itemView.findViewById(R.id.tv_itemMessage_time);
            timeOperator = (TextView) itemView.findViewById(R.id.tv_itemMessageOperator_time);
            text = (TextView) itemView.findViewById(R.id.tv_itemMessage);
            ivOperator = (RoundedImageView) itemView.findViewById(R.id.iv_itemMessageOperator);
            rlOperator = (RelativeLayout) itemView.findViewById(R.id.rl_operator);
            rlMy = (RelativeLayout) itemView.findViewById(R.id.rl_my);
            recyclerView = (RecyclerView) itemView.findViewById(R.id.recicleViewMessage);
        }
    }
}
