package ru.handh.topvisor.ui.pay;

/**
 * Created by sergey on 11.04.16.
 */
public interface ViewRefill {
    void openWebView(String url);
    void showToast(String text);
    void showToast(int textId);
    void showResponceDialog(int type);
    void changeDialogStatus(int type);
}
