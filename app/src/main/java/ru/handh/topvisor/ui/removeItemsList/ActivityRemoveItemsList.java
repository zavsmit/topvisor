package ru.handh.topvisor.ui.removeItemsList;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ActionMode;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import ru.handh.topvisor.R;
import ru.handh.topvisor.ui.base.ParentBackActivity;
import ru.handh.topvisor.ui.dialogs.AddDialog;
import ru.handh.topvisor.ui.dialogs.RemoveGroupDialog;
import ru.handh.topvisor.ui.dialogs.RenameGroupDialog;
import ru.handh.topvisor.ui.settingsProject.searchSystems.typeSystems.ActivityTypeSystem;

/**
 * Created by sergey on 21.03.16.
 */
public abstract class ActivityRemoveItemsList extends ParentBackActivity implements ViewRemoveItemsList {
    public final static int DIALOG_ADD = 0;
    public final static int DIALOG_REMOVE = 1;
    public final static int DIALOG_RENAME = 2;
    public PresenterRemoveItemsList presenter;
    protected RecyclerView mRecyclerView;
    protected String emptyText;
    private FloatingActionButton fab;
    private TextView emptyTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_yandex_inspect);

        viewFlipperMain = (ViewFlipper) findViewById(R.id.viewFlipper);

        mRecyclerView = (RecyclerView) findViewById(R.id.recicleView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        fab = (FloatingActionButton) findViewById(R.id.fabSearchSystem);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.clickAdd();
            }
        });

        emptyTv = (TextView) findViewById(R.id.tv_empty);

    }

    protected void initToolbar(String name) {
        initToolbar(R.id.toolbar, name);
        emptyTv.setText(emptyText);
    }

    @Override
    public void selectRegion(int position) {
        Intent intent = new Intent(this, ActivityTypeSystem.class);
        startActivity(intent);
        setResult(RESULT_OK, intent);
    }

    @Override
    public void showHide(boolean isShow) {
        if (isShow) {
            toolbar.setVisibility(View.VISIBLE);
            fab.setVisibility(View.VISIBLE);
        } else {
            toolbar.setVisibility(View.GONE);
            fab.setVisibility(View.GONE);
        }
    }

    @Override
    public ActionMode startMyActionMode(ActionMode.Callback callback) {
        return startActionMode(callback);
    }

    @Override
    public void showDialogList(int dialogType, String name, String hint, int position) {
        DialogFragment dialog = null;
        String dialogTag = "";

        switch (dialogType) {
            case DIALOG_ADD:
                dialog = AddDialog.newInstance(name, hint);
                dialogTag = "AddDialog";
                break;
            case DIALOG_REMOVE:
                dialog = RemoveGroupDialog.newInstance(position);
                dialogTag = "RemoveGroupDialog";
                break;
            case DIALOG_RENAME:
                dialog = RenameGroupDialog.newInstance(name, position);
                dialogTag = "RenameGroupDialog";
                break;
        }

        if (dialog != null)
            dialog.show(getSupportFragmentManager(), dialogTag);
    }

    @Override
    public void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showEmptyText(boolean isShow) {
        if (isShow) {
            emptyTv.setVisibility(View.VISIBLE);
            viewFlipperMain.setVisibility(View.GONE);
        } else {
            emptyTv.setVisibility(View.GONE);
            viewFlipperMain.setVisibility(View.VISIBLE);
        }
    }

}
