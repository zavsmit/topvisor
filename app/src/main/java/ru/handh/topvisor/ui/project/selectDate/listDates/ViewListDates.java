package ru.handh.topvisor.ui.project.selectDate.listDates;

import java.util.List;

/**
 * Created by sergey on 30.03.16.
 */
public interface ViewListDates {
    void selectGroup(int idSelected, int typeSelect);
    void initAdapter(List<String> groups, AdapterListDates.ItemGroupClickListener listener, int select);

}
