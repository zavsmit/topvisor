package ru.handh.topvisor.ui.dialogs;

import android.support.v4.app.DialogFragment;

/**
 * Created by sergey on 09.02.16.
 * интерфейс результатов диалогов
 */
public interface DialogResult {
    void onDialogPositiveClick(DialogFragment dialog);

    void onDialogNegativeClick(DialogFragment dialog);
}
