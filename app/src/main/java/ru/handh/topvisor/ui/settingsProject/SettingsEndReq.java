package ru.handh.topvisor.ui.settingsProject;

import ru.handh.topvisor.data.network.model.ModelResult;
import ru.handh.topvisor.data.network.model.ResultCompetitors;
import ru.handh.topvisor.data.network.model.ReturnResponce;

/**
 * Created by sergey on 22.03.16.
 */
public interface SettingsEndReq {
    void addCompetitorsResponce(ReturnResponce<ResultCompetitors> result);

    void removeCompetitorsResponce(ReturnResponce<ModelResult> result);

    void removeGroupResponce(ReturnResponce<ModelResult> result, int positionw);

    void addGroupResponce(ReturnResponce<ModelResult> result);

    void renameGroupResponce(ReturnResponce<ModelResult> result);

    void checkGroupResponce(ReturnResponce<ModelResult> result);
}

