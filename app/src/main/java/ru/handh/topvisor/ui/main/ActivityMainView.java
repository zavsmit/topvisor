package ru.handh.topvisor.ui.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.daimajia.swipe.util.Attributes;

import java.util.ArrayList;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.network.model.currentProject.Row;
import ru.handh.topvisor.ui.dialogs.NewTicketDialog;
import ru.handh.topvisor.ui.dialogs.PriceDialog;
import ru.handh.topvisor.ui.dialogs.RecoveryDialog;
import ru.handh.topvisor.ui.dialogs.RemoveDialog;
import ru.handh.topvisor.ui.dialogs.RenameDialog;
import ru.handh.topvisor.ui.main.mainSettings.SettingsMainActivity;
import ru.handh.topvisor.ui.main.search.SearchViewActivity;
import ru.handh.topvisor.ui.menu.ParentLeftMenu;
import ru.handh.topvisor.ui.project.ActivityViewProject;
import ru.handh.topvisor.utils.Constants;

/**
 * экран проектов, стартовый экран после регистрации
 */
public class ActivityMainView extends ParentLeftMenu implements SwipeRefreshLayout.OnRefreshListener, MainView {

    public final static int DIALOG_NEW_TICKET = 0;
    public final static int DIALOG_PRICE_PROJECT = 2;
    public final static int DIALOG_REMOVE_PROJECT = 3;
    public final static int DIALOG_RENAME_PROJECT = 4;
    public final static int DIALOG_RECOVERY_PROJECT = 5;
    public PresenterMain presenter;
    private TextView headerText, emptyText;
    private ImageView iconHeader;
    private SwipeRefreshLayout srlMain;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private SendDataToDialog sendDataToDialog;
    private ViewFlipper viewFlipperMain;
    private LinearLayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewFlipperMain = (ViewFlipper) findViewById(R.id.viewFlipperMain);

        headerText = (TextView) findViewById(R.id.tv_main_Settings);
        iconHeader = (ImageView) findViewById(R.id.iv_main_Settings);

        srlMain = (SwipeRefreshLayout) findViewById(R.id.swipe_container_main);
        srlMain.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.loadList();
            }
        });

        emptyText = (TextView) findViewById(R.id.tv_main_empty);
        mRecyclerView = (RecyclerView) findViewById(R.id.recicleView_main);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);

        mRecyclerView.setLayoutManager(mLayoutManager);

        presenter = new PresenterMainImpl(this, this);
    }

    public void clickButtons(View view) {

        switch (view.getId()) {
            case R.id.ll_main_settings:

                Intent intent = new Intent(ActivityMainView.this, SettingsMainActivity.class);
                startActivity(intent);
                break;

            case R.id.fabMainAdd:
                showDialogMain(DIALOG_NEW_TICKET);
                break;

            case R.id.button_reqError:
                presenter.loadList();
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.initFiltersHead();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (presenter != null) {
            presenter.onStop();
        }

    }

    @Override
    protected void onPause() {
        presenter.onPause(mLayoutManager.findFirstVisibleItemPosition());
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_search) {
            presenter.openSearchActivity();
            return true;
        }

        if (id == R.id.action_refresh) {
            presenter.refreshAll();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void showDialogMain(int dialogType) {
        showDialogMain(dialogType, "", 0);
    }

    @Override
    public void showDialogMain(int dialogType, String name) {
        showDialogMain(dialogType, name, 0);
    }

    @Override
    public void showDialogMain(int dialogType, String name, int position) {
        DialogFragment dialog = null;
        String dialogTag = "";

        switch (dialogType) {
            case DIALOG_NEW_TICKET:
                dialog = NewTicketDialog.newInstance(NewTicketDialog.NEW_DOMEN, "");
                dialogTag = "NewTicketDialog";
                break;
            case DIALOG_PRICE_PROJECT:
                dialog = PriceDialog.newInstance(name);
                dialogTag = "PriceDialog";
                break;
            case DIALOG_REMOVE_PROJECT:
                dialog = RemoveDialog.newInstance(name, position);
                dialogTag = "RemoveDialog";
                break;
            case DIALOG_RENAME_PROJECT:
                dialog = RenameDialog.newInstance(name, position);
                dialogTag = "RenameDialog";
                break;
            case DIALOG_RECOVERY_PROJECT:
                dialog = RecoveryDialog.newInstance(name, position);
                dialogTag = "RecoveryDialog";
                break;
        }

        if (dialog != null)
            dialog.show(getSupportFragmentManager(), dialogTag);

        if (dialog instanceof PriceDialog)
            sendDataToDialog = ((PriceDialog) dialog);
    }

    @Override
    public void showRefresh(boolean isShow) {
        srlMain.setRefreshing(isShow);

    }

    @Override
    public void showData() {
        if (viewFlipperMain.getDisplayedChild() != Constants.VIEW_CONTENT)
            viewFlipperMain.setDisplayedChild(Constants.VIEW_CONTENT);
    }

    @Override
    public void showProgress() {
        if (viewFlipperMain.getDisplayedChild() != Constants.VIEW_PROGRESS)
            viewFlipperMain.setDisplayedChild(Constants.VIEW_PROGRESS);
    }

    @Override
    public void showError() {
        if (viewFlipperMain.getDisplayedChild() != Constants.VIEW_ERROR)
            viewFlipperMain.setDisplayedChild(Constants.VIEW_ERROR);
    }

    @Override
    public void showEmpty(boolean isShow) {
        if (isShow) {
            emptyText.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        } else {
            emptyText.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void initView() {
        LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        CoordinatorLayout v = (CoordinatorLayout) vi.inflate(R.layout.app_bar_main, null);
        drawer.addView(v, 0, new ViewGroup.LayoutParams(DrawerLayout.LayoutParams.MATCH_PARENT, DrawerLayout.LayoutParams.MATCH_PARENT));
    }

    @Override
    public void initAdapter(ArrayList<Row> myDataset, PresenterMainImpl presenterImpl) {
        mAdapter = new AdapterMain(myDataset, presenterImpl);
        ((AdapterMain) mAdapter).setMode(Attributes.Mode.Single);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void setTextToHeader(int count) {
        toolbar.setTitle(getString(R.string.projects) + "(" + count + ")");
    }

    @Override
    public void setDataToFilter(String name, int idRes) {
        headerText.setText(name);
        iconHeader.setBackgroundResource(idRes);
    }

    @Override
    public void showDescriptionInDialog(double price, double xmlPrice) {
        sendDataToDialog.showPrice(price, xmlPrice);
    }

    @Override
    public void removeItemList(int position) {
        ((AdapterMain) mAdapter).removePosition(position);
    }

    @Override
    public void renameItemList(int position, String name) {
        ((AdapterMain) mAdapter).renamePosition(position, name);
    }

    @Override
    public void openActivity(Row row) {
        Intent intent = new Intent(this, ActivityViewProject.class);
        intent.putExtra("row", row);
        startActivity(intent);
    }

    @Override
    public void openSearchActivity(ArrayList<Row> myDataset) {
        Intent intent = new Intent(ActivityMainView.this, SearchViewActivity.class);
        intent.putParcelableArrayListExtra("EXTRA_DATA_MAIN", myDataset);
        startActivity(intent);
    }

    @Override
    public void scrollToPosition(int visiblePosition) {
        mRecyclerView.scrollToPosition(visiblePosition);
    }


    public interface SendDataToDialog {
        void showPrice(double price, double xmlPrice);
    }

}
