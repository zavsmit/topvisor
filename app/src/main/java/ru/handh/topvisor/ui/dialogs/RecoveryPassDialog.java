package ru.handh.topvisor.ui.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import ru.handh.topvisor.R;

/**
 * Created by sergey on 02.02.16.
 * диалог восстановления пароля
 */
public class RecoveryPassDialog extends DialogFragment {

    public EditText email;
    private DialogResult mListener;

    public RecoveryPassDialog() {
    }

    public static RecoveryPassDialog newInstance(String email) {
        RecoveryPassDialog f = new RecoveryPassDialog();

        Bundle args = new Bundle();
        args.putString("email", email);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (DialogResult) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_recovery_pass, null);


        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
        adb.setView(view);

        adb.setTitle(getActivity().getString(R.string.recoveryPass))
                .setPositiveButton(getActivity().getString(R.string.send), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        if (email.getText().toString().isEmpty()) {
                            Toast.makeText(getActivity(), getActivity().getString(R.string.writeLogin), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        Toast.makeText(getActivity(), email.getText(), Toast.LENGTH_SHORT).show();

                        mListener.onDialogPositiveClick(RecoveryPassDialog.this);
                    }
                })
                .setNegativeButton(getActivity().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mListener.onDialogNegativeClick(RecoveryPassDialog.this);
                    }
                });

        email = (EditText) view.findViewById(R.id.et_loginDialog_email);
        email.setText(getArguments().getString("email"));

        return adb.create();
    }

}
