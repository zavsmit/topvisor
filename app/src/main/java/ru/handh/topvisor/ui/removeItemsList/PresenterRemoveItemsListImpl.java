package ru.handh.topvisor.ui.removeItemsList;

import android.support.v4.app.DialogFragment;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import java.util.List;

import io.realm.RealmList;
import ru.handh.topvisor.R;
import ru.handh.topvisor.data.database.project.CompetitorData;
import ru.handh.topvisor.data.database.project.GroupData;
import ru.handh.topvisor.ui.dialogs.DialogResult;

/**
 * Created by sergey on 21.03.16.
 * родительский адаптер
 */
public abstract class PresenterRemoveItemsListImpl implements PresenterRemoveItemsList,
        AdapterRemoveItemsList.ItemSearcherClickListener,
        ActionMode.Callback,
        DialogResult {

    protected boolean isGroup;
    protected AdapterRemoveItemsList mAdapter;
    protected ViewRemoveItemsList mView;
    protected List<Integer> selectedItemPositions;
    private ActionMode actionMode;
    protected RealmList<CompetitorData> listCompetitiors;
    protected RealmList<GroupData> listGroups;

    public PresenterRemoveItemsListImpl(ViewRemoveItemsList view, RealmList<GroupData> groups, RealmList<CompetitorData> competitors) {
        mView = view;

        listGroups = groups;
        listCompetitiors = competitors;
        mAdapter = mView.initAdapter(listGroups, listCompetitiors, this);


        if (listGroups != null && listGroups.size() == 0) {
            mView.showEmptyText(true);
        }

        if (listCompetitiors != null && listCompetitiors.size() == 0) {
            mView.showEmptyText(true);
        }

    }

    @Override
    public void clickItem(int position) {

        if (actionMode != null) {
            myToggleSelection(position);
            return;
        }
    }

    @Override
    public void longClick(int position) {
        if (actionMode != null) {
            return;
        }
        actionMode = mView.startMyActionMode(this);
        myToggleSelection(position);
    }

    private void myToggleSelection(int idx) {
        mAdapter.toggleSelection(idx);
        actionMode.setTitle(String.valueOf(mAdapter.getSelectedItemCount()));
    }


    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(R.menu.menu_remove, menu);
        mView.showHide(false);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_delete:
                selectedItemPositions = mAdapter.getSelectedItems();


                if (isGroup) {
                    removeAction(-1);
                } else {
                    int currPos;
                    for (int i = selectedItemPositions.size() - 1; i >= 0; i--) {
                        currPos = selectedItemPositions.get(i);
                        removeAction(currPos);
                    }
                }


                actionMode.finish();
                return true;
            default:
                return false;
        }
    }

    protected abstract void removeAction(int positon);


    @Override
    public void onDestroyActionMode(ActionMode mode) {
        this.actionMode = null;
        mAdapter.clearSelections();
        mView.showHide(true);
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
    }


}
