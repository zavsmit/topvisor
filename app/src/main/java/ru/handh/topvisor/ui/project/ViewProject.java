package ru.handh.topvisor.ui.project;

import java.util.ArrayList;
import java.util.List;

import ru.handh.topvisor.data.database.project.ProjectData;
import ru.handh.topvisor.data.model.InspectDialogData;
import ru.handh.topvisor.data.network.model.projecPhrases.ModelProjectPhrases;

/**
 * Created by sergey on 19.02.16.
 */
public interface ViewProject {
    void initViewPager(ProjectData currentRow);

    void initAdapterFragment(int fragmentNumber, ModelProjectPhrases rows,
                             List<String> dates, PresenterProjectImpl presenterImpl);

    int getCurrentNumberPage();

    void removeItemInFragment(int position);

    void changeHeaderInFragment(String nameRegion, String nameGroup, int pageNumber);

    void changeFooterInFragment(String start, String end, int numberPage);

    void openGroups(String idProject);

    void openRegions(String idProject);

    void openDate(ArrayList<String> allDates, ArrayList<Integer> selectedList);

    void openSettings(String idProject);

    void showNewDialog(int idDialog, String groupId, InspectDialogData data);

    void addFilterPrice(double price);

    void addAllPrice(double price);

    void showToast(String text);
}
