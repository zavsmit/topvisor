package ru.handh.topvisor.ui.support;

import java.util.List;

import ru.handh.topvisor.data.network.model.ModelItemMessage;
import ru.handh.topvisor.data.network.model.ModelResult;
import ru.handh.topvisor.data.network.model.ModelTicket;
import ru.handh.topvisor.data.network.model.ReturnResponce;

/**
 * Created by sergey on 31.03.16.
 */
public interface SupportEndReq {
    void listDialogsResponce(ReturnResponce<List<ModelTicket>> result);

    void listMessagesResponce(ReturnResponce<List<ModelItemMessage>> result);

    void removiImageResponce(ReturnResponce<ModelResult> result);

    void getImagesResponce(ReturnResponce<List<String>> result);

    void sendMessageResponce(ReturnResponce<ModelResult> result);

    void sendNewDialogMessageResponce(ReturnResponce<ModelResult> result);

    void sendPhotoResponce(ReturnResponce<ModelResult> result);
}
