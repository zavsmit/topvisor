package ru.handh.topvisor.ui.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import ru.handh.topvisor.R;
import ru.handh.topvisor.ui.project.ActivityViewProject;

/**
 * Created by sergey on 25.02.16.
 */
public class EmptyRegionProjectDialog extends DialogFragment {

    public int mPosition;
    private DialogResult mListener;

    public EmptyRegionProjectDialog() {
    }

    public static EmptyRegionProjectDialog newInstance(String name) {
        EmptyRegionProjectDialog f = new EmptyRegionProjectDialog();

        Bundle args = new Bundle();
        args.putString("name", name);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (DialogResult) ((ActivityViewProject) activity).presenter;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
                .setTitle(getContext().getString(R.string.attention))
                .setPositiveButton(getActivity().getString(R.string.next), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mListener.onDialogPositiveClick(EmptyRegionProjectDialog.this);
                    }
                })
                .setMessage(getActivity().getString(R.string.inThisPsNotRegions) + getArguments().getString("name"));

        return adb.create();
    }
}
