package ru.handh.topvisor.ui.project.selectDate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sergey on 15.03.16.
 */
public interface ViewSelectDate {
    AdapterSelectDate initAdapter(List<String> allDates,
                                  AdapterSelectDate.ItemSearcherClickListener listener,
                                  List<Integer> integerList);

    void close(ArrayList<Integer> selectedPositions);

    void openListDate(int selectPosition, ArrayList<String> list, int typeSelect);

    void notificationAdapter();
}
