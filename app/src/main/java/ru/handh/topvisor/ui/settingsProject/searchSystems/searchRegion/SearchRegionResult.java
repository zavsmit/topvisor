package ru.handh.topvisor.ui.settingsProject.searchSystems.searchRegion;

import java.util.List;

import ru.handh.topvisor.data.network.model.ModelResultType;
import ru.handh.topvisor.data.network.model.ModelSearchRegion;
import ru.handh.topvisor.data.network.model.ReturnResponce;

/**
 * Created by sergey on 27.03.16.
 */
public interface SearchRegionResult {
    void searchRegionResponce(ReturnResponce<List<ModelSearchRegion>> result, String term);
    void addRegionsResponce(ReturnResponce<ModelResultType> result);
}
