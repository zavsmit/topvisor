package ru.handh.topvisor.ui.project;

import java.util.List;

import ru.handh.topvisor.data.network.model.ModelPriseInspectFilter;
import ru.handh.topvisor.data.network.model.ModelResult;
import ru.handh.topvisor.data.network.model.ReturnResponce;
import ru.handh.topvisor.data.network.model.currentProject.Row;
import ru.handh.topvisor.data.network.model.projecDates.Rows;
import ru.handh.topvisor.data.network.model.projecPhrases.ModelProjectPhrases;

/**
 * Created by sergey on 29.02.16.
 */
public interface ProjectEndReq {

    void itemProjectResponce(ReturnResponce<Row> result);

    void datesProjectResponce(ReturnResponce<Rows> result, String idProject, String regionKey);

    void phrasesProjectResponce(ReturnResponce<ModelProjectPhrases> result,
                                int currentNumberPage, List<String> dates);

    void removePharseResult(ReturnResponce<ModelResult> result);

    void addPharseResult(ReturnResponce<ModelResult> result);

    void priceOnFilter(ReturnResponce<ModelPriseInspectFilter> result);

    void inspectOnFilter(ReturnResponce<ModelResult> result);


}
