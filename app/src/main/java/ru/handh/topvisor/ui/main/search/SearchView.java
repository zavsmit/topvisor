package ru.handh.topvisor.ui.main.search;

import java.util.ArrayList;

import ru.handh.topvisor.data.network.model.currentProject.Row;

/**
 * Created by sergey on 23.02.16.
 * интерфейс экрана поиска
 */
public interface SearchView {

    void openNewActivity(Row item);

    void changeList(ArrayList<Row> myDataset, String s);

    void showEmpty(boolean isEmpty);
}
