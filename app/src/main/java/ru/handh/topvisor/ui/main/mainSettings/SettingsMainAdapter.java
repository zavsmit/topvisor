package ru.handh.topvisor.ui.main.mainSettings;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.List;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.DataManager;
import ru.handh.topvisor.data.network.model.Labels;
import ru.handh.topvisor.data.network.model.Projects;

/**
 * Created by sergey on 11.02.16.
 * адаптер фильтра проетов
 */
public class SettingsMainAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_PROGECT = 1;
    private static final int TYPE_LABEL = 2;
    private static final int TYPE_DIVIDER = 3;
    private List<Projects> mDataset;
    private List<Labels> mDatasetLaber;


    public SettingsMainAdapter(List<Projects> dataset, List<Labels> datasetLaber) {
        mDataset = dataset;
        mDatasetLaber = datasetLaber;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case TYPE_PROGECT:
                return new ViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_main_settings, parent, false));
            case TYPE_LABEL:
                return new ViewHolderLabel(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_main_setting_label, parent, false));
            case TYPE_DIVIDER:
                return new ViewHolderDivider(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_divider, parent, false));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        switch (holder.getItemViewType()) {

            case TYPE_PROGECT:

                ViewHolder mHolder = ((ViewHolder) holder);

                mHolder.text.setText(mDataset.get(position).name);

                if (mDataset.get(position).isChecked) {
                    mHolder.radioButton.setChecked(true);
                } else {
                    mHolder.radioButton.setChecked(false);
                }

                mHolder.ll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        redraveList(position);
                    }
                });

                mHolder.radioButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        redraveList(position);
                    }
                });

                break;

            case TYPE_LABEL:

                final ViewHolderLabel mHolderLabel = ((ViewHolderLabel) holder);

                final int currentPosition = position - mDataset.size() - 1;

                mHolderLabel.text.setText(mDatasetLaber.get(currentPosition).name);
                mHolderLabel.label.setBackgroundResource(mDatasetLaber.get(currentPosition).icon);


                mHolderLabel.ll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        redraveListLabel(currentPosition);
                    }
                });

                if (mDatasetLaber.get(currentPosition).isChecked) {
                    mHolderLabel.radioButton.setChecked(true);
                } else {
                    mHolderLabel.radioButton.setChecked(false);
                }

                mHolderLabel.radioButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        redraveListLabel(currentPosition);
                    }
                });

                break;
        }

    }

    private void redraveList(int position) {

        DataManager.getPref().setMainSettingsFolder(mDataset.get(position));

        for (int i = 0; i < mDataset.size(); i++) {
            mDataset.get(i).isChecked = position == i;
        }
        notifyDataSetChanged();
    }

    private void redraveListLabel(int currentPosition) {

        DataManager.getPref().setMainSettingsTab(mDatasetLaber.get(currentPosition));

        for (int i = 0; i < mDatasetLaber.size(); i++) {
            mDatasetLaber.get(i).isChecked = currentPosition == i;
        }
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return mDataset.size() + mDatasetLaber.size() + 1;
    }


    @Override
    public int getItemViewType(int position) {

        if (position < mDataset.size()) {
            return TYPE_PROGECT;
        } else if (position == mDataset.size()) {
            return TYPE_DIVIDER;
        } else {
            return TYPE_LABEL;
        }
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView text;
        public RadioButton radioButton;
        public LinearLayout ll;

        public ViewHolder(View v) {
            super(v);
            text = (TextView) itemView.findViewById(R.id.tv_item_mainSettings);
            radioButton = (RadioButton) itemView.findViewById(R.id.rb_item_mainSettings);
            ll = (LinearLayout) itemView.findViewById(R.id.ll_item_mainSettings);
        }
    }

    public class ViewHolderLabel extends RecyclerView.ViewHolder {
        public TextView text;
        public ImageView label;
        public RadioButton radioButton;
        public LinearLayout ll;

        public ViewHolderLabel(View v) {
            super(v);
            label = (ImageView) itemView.findViewById(R.id.iv_item_mainSettingsLabel);
            text = (TextView) itemView.findViewById(R.id.tv_item_mainSettingsLabel);
            radioButton = (RadioButton) itemView.findViewById(R.id.rb_item_mainSettingsLabel);
            ll = (LinearLayout) itemView.findViewById(R.id.rl_item_mainSettingsLabel);

        }
    }

    public class ViewHolderDivider extends RecyclerView.ViewHolder {
        public ViewHolderDivider(View v) {
            super(v);
        }
    }
}