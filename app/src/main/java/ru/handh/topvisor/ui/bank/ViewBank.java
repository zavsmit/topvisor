package ru.handh.topvisor.ui.bank;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ru.handh.topvisor.data.model.SearcherItemDialog;
import ru.handh.topvisor.data.network.model.ModelItemBank;

/**
 * Created by sergey on 07.04.16.
 * методы вьюхи банка
 */
public interface ViewBank {
    void initAdapter(List<ModelItemBank> list, AdapterBank.ItemClickListener listener);

    void addDataToAdapter(List<ModelItemBank> list, boolean isNew);

    void showData();

    void showProgress();

    void showError();

    void showEmpty(boolean isShow);

    void showDialogDiscription(String text);

    void initFooter(double inc, double dec);

    void initHeader(Date startDate, Date endDate);

    void showDateDialog(Date date, int type);

    void showRegionsDialog(ArrayList<SearcherItemDialog> list);

    void hideAddPSDialog();

    void setProjectname(String name);

    void openPay();
}
