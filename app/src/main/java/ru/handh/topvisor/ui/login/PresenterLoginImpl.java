package ru.handh.topvisor.ui.login;

import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;

import java.util.Locale;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.DataManager;
import ru.handh.topvisor.data.network.model.ReturnResponce;
import ru.handh.topvisor.ui.dialogs.LicenseDialog;
import ru.handh.topvisor.ui.dialogs.RecoveryPassDialog;
import ru.handh.topvisor.utils.Constants;
import ru.handh.topvisor.utils.Log;

/**
 * Created by sergey on 08.02.16.
 * реализация презентера для экрана логина / авторизации
 */
public class PresenterLoginImpl implements PresenterLogin, LoginEndLoad {

    private boolean isRegistration;

    private LoginView view;

    public PresenterLoginImpl(LoginView view) {
        this.view = view;
        if (Locale.getDefault().getDisplayLanguage().equals(Constants.US_LANGUAGE)) {
            view.hideRuSocial();
            view.addEnLogo();
        }

        view.showData();
    }

    @Override
    public void onEnterButtonClick(String login, String password) {

        if (login.isEmpty()) {
            view.showEmptyLogin();
            return;
        }

        if (!isValidEmail(login)) {
            view.showCorrectLogin();
            return;
        }

        if (isRegistration) {

            view.showDialogLogin(LoginViewActivity.DIALOG_REGISTRATION);

        } else {

            if (password.isEmpty()) {
                view.showEmptyPass();
                return;
            }

            view.showProgress(R.string.auth);
            DataManager.getLoginRest().reqServerTypeAuth(login.trim(), password.trim(), this);
        }
    }

    @Override
    public void openMainAndSaveAuth(ReturnResponce<String> result) {
        if (!result.isFailure()) {

            if (result.getResult() != null && !result.getResult().isEmpty()) {
                DataManager.getMainRest().sendGCMToken();
                view.startMainActivity();
            } else {
                view.showToast(result.getErrorMessage());
                view.showData();
            }

        } else {
//            view.showToast(R.string.falseAuthFromNetwork);
            view.showToast(R.string.falseNetwork);
        }
    }

    @Override
    public void sendReqAndShowDialog(ReturnResponce<Boolean> result, int numberDialog) {
        if (!result.isFailure()) {

            if (result.getResult()) {
                view.showDialogLogin(numberDialog);
            } else {
                view.showToast(result.getErrorMessage());
            }
            view.showData();

        } else {
            view.showToast(R.string.falseAuthFromNetwork);
        }
    }


    @Override
    public void onForgetPassButtonClick() {
        view.showDialogLogin(LoginViewActivity.DIALOG_RECOVERY_PASS);
    }

    @Override
    public void onReqLoginButtonClick() {
        if (isRegistration) {
            view.showRegistrationView();
        } else {
            view.showLoginView();
        }

        isRegistration = !isRegistration;
    }

    @Override
    public void openFromLink(String intentParem) {
        if (intentParem != null) {

            String emailAuth = intentParem.substring(intentParem.indexOf("email=") + 6, intentParem.indexOf("&code"));
            String code = intentParem.substring(intentParem.indexOf("&code=") + 6, intentParem.indexOf("&params"));

            String serverType = "";
            if (intentParem.contains("domain=")) {
                serverType = intentParem.substring(intentParem.indexOf("domain=") + 7, intentParem.length() - 1);
            }

            int type = 1;
            if (!serverType.isEmpty()) {
                type = Integer.valueOf(serverType);
            }

            view.showProgress(R.string.auth);
            DataManager.getLoginRest().reqAuthorizationOnToken(emailAuth, code, "", type, this);
        }
    }

    @Override
    public void activityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == LoginViewActivity.REQUEST_ULOGIN) {

            switch (resultCode) {
                case LoginViewActivity.RESULT_OK:
                    String auth = data.getStringExtra("auth");
                    if (!auth.isEmpty()) {
                        DataManager.getPref().setTokenApp(auth);
                        DataManager.getMainRest().sendGCMToken();
                        view.startMainActivity();
                    }
                    break;
                case LoginViewActivity.RESULT_CANCELED:
                    Log.deb("Unknown result");
                    break;
                default:
                    Log.deb("Unknown result");
            }
        }
    }

    @Override
    public void onStop() {
    }

    @Override
    public void positivDialog(DialogFragment dialog, String email) {

        if (dialog instanceof RecoveryPassDialog) {
            DataManager.getLoginRest().reqServerTypeForget(((RecoveryPassDialog) dialog).email.getText().toString().trim(), this);
        }

        if (dialog instanceof LicenseDialog) {
            DataManager.getLoginRest().reqRegistration(email, this);
        }
    }

    @Override
    public void negativDialog(DialogFragment dialog) {

    }

    @Override
    public void clickSocial(int idsocial) {
        String social = "vkontakte";
        switch (idsocial) {
            case R.id.iv_login_vk:
                social = "vkontakte";
                break;
            case R.id.iv_login_gplus:
                social = "googleplus";
                break;
            case R.id.iv_login_facebook:
                social = "facebook";
                break;
            case R.id.iv_login_twitter:
                social = "twitter";
                break;
            case R.id.iv_login_yandex:
                social = "yandex";
                break;
        }

        view.runUlogin(social);
    }

    private boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

}
