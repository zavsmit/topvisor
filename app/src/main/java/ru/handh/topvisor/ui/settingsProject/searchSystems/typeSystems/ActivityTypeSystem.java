package ru.handh.topvisor.ui.settingsProject.searchSystems.typeSystems;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.model.DateItem;
import ru.handh.topvisor.ui.base.ParentBackActivity;
import ru.handh.topvisor.ui.listResultSelected.ActivityListResult;

/**
 * Created by sergey on 17.03.16.
 */
public class ActivityTypeSystem extends ParentBackActivity implements ViewTypeSystem {

    public final static int RESQUEST_CODE_LANG = 2000;
    public final static int RESQUEST_CODE_DEPTH = 2001;
    public final static int RESQUEST_CODE_DEVICE = 2002;
    public PresenterTypeSystem presenter;

    private TextView lang, device, depth;
    private LinearLayout llLang, llDevice, llDepth;

    private Map<String, String> depthMap = new HashMap<>();
    private Map<String, String> deviceMap = new HashMap<>();

    private String strLang;
    private String projectId;
    private int positionSearch;
    private String idRegion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_type_system);
        initToolbar(R.id.toolbar, getString(R.string.settings));


        deviceMap.put("0", getString(R.string.pc));
        deviceMap.put("1", getString(R.string.tablet));
        deviceMap.put("2", getString(R.string.mobile));

        for (int i = 1; i < 11; i++) {

            int x = i;
            String key = String.valueOf(x);

            x = x * 100;
            String value = String.valueOf(x);

            depthMap.put(key, value);
        }

        lang = (TextView) findViewById(R.id.tv_typeSearch_langDesctiption);
        device = (TextView) findViewById(R.id.tv_typeSearch_typeDesctiption);
        depth = (TextView) findViewById(R.id.tv_typeSearch_depthDesctiption);

        llLang = (LinearLayout) findViewById(R.id.ll_typeSearch_lang);
        llDevice = (LinearLayout) findViewById(R.id.ll_typeSearch_type);
        llDepth = (LinearLayout) findViewById(R.id.ll_typeSearch_depth);


        projectId = "";
        positionSearch = 0;
        idRegion = "";

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            projectId = extras.getString("idProject");
            idRegion = extras.getString("idRegion");
            positionSearch = extras.getInt("position");
        }

        strLang = getStringFromAssetFile(this);
    }


    public void clickItem(View view) {
        switch (view.getId()) {
            case R.id.ll_typeSearch_lang:
                presenter.clickStartActivity(RESQUEST_CODE_LANG);
                break;

            case R.id.ll_typeSearch_depth:
                presenter.clickStartActivity(RESQUEST_CODE_DEPTH);
                break;

            case R.id.ll_typeSearch_type:
                presenter.clickStartActivity(RESQUEST_CODE_DEVICE);
                break;
        }
    }


    @Override
    public void startMyActivity(ArrayList<DateItem> dateItemListType, String name,
                                int type, String idSearcher, String idCurrentRegion,
                                String idProject, int iSearcher) {

        Intent intent = new Intent(this, ActivityListResult.class);
        intent.putParcelableArrayListExtra("dateItemArrayList", dateItemListType);
        intent.putExtra("name", name);
        intent.putExtra("idSearcher", idSearcher);
        intent.putExtra("idProject", idProject);
        intent.putExtra("iSearcher", iSearcher);
        intent.putExtra("idCurrentRegion", idCurrentRegion);
        intent.putExtra("type", type);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter = new PresenterTypeSystemImpl(this, idRegion, strLang, projectId, positionSearch, this);
    }

    public void changeLandDepthDevice(String langS, String depthS, String deviceS) {
        lang.setText(langS);

        String deviceType="";

        switch (deviceS){
            case "0":
                deviceType = getString(R.string.pc);
                break;
            case "1":
                deviceType = getString(R.string.tablet);
                break;
            case "2":
                deviceType = getString(R.string.mobile);
                break;
        }

        device.setText(deviceType);



        int depthI= Integer.valueOf(depthS) * 100;

        depth.setText(String.valueOf(depthI));
    }

    @Override
    public void showItem(int typeItem) {

        switch (typeItem) {
            case RESQUEST_CODE_LANG:
                llLang.setVisibility(View.VISIBLE);
                break;
            case RESQUEST_CODE_DEPTH:
                llDepth.setVisibility(View.VISIBLE);
                break;
            case RESQUEST_CODE_DEVICE:
                llDevice.setVisibility(View.VISIBLE);
                break;
        }
    }


    private String getStringFromAssetFile(Activity activity) {
        String text = "langs.txt";
        byte[] buffer = null;
        InputStream is;
        try {
            is = activity.getAssets().open(text);
            int size = is.available();
            buffer = new byte[size];
            is.read(buffer);
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new String(buffer);
    }
}
