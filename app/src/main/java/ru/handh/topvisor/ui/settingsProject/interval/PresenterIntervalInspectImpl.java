package ru.handh.topvisor.ui.settingsProject.interval;

import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;
import java.util.List;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.DataManager;
import ru.handh.topvisor.data.database.project.ProjectData;
import ru.handh.topvisor.data.network.model.ModelResult;
import ru.handh.topvisor.data.network.model.ReturnResponce;

/**
 * Created by sergey on 16.03.16.
 * логика выбора интервалов проверки
 */
public class PresenterIntervalInspectImpl implements PresenterIntervalInspect, IntervalReqResult {

    private ViewIntervalInspect mView;
    private String idProject;
    private String hour, autoCord;
    private int yandexSelect;
    private Context context;
    private ArrayList<String> listYandex = new ArrayList<>();
    private ArrayList<String> listDate = new ArrayList<>();

    public PresenterIntervalInspectImpl(ViewIntervalInspect view, String idProject, Context context) {

        listYandex.add(context.getString(R.string.atOnce));
        listYandex.add(context.getString(R.string.after1));
        listYandex.add(context.getString(R.string.after2));
        listYandex.add(context.getString(R.string.after3));
        listYandex.add(context.getString(R.string.after4));
        listYandex.add(context.getString(R.string.after6));
        listYandex.add(context.getString(R.string.after12));

        this.idProject = idProject;
        this.context = context;
        mView = view;
    }

    public void changeDescription() {
        ProjectData projectData = DataManager.getDB().getProject(idProject);
        String timeForUpdate = projectData.getTimeForUpdate();
        listDate.clear();
        if (timeForUpdate.length() != 0) {
            while (timeForUpdate.indexOf("|:") != 0) {
                int startPosition = timeForUpdate.indexOf("|") + 1;
                int endPosition = timeForUpdate.indexOf("|", startPosition);

                if (endPosition == -1) {
                    break;
                }
                listDate.add(timeForUpdate.substring(startPosition, endPosition));

                timeForUpdate = timeForUpdate.substring(endPosition, timeForUpdate.length());
            }

            hour = timeForUpdate.substring(2, timeForUpdate.length());
        }


        mView.setChecked(Integer.valueOf(projectData.getOn()));

        this.yandexSelect = Integer.valueOf(projectData.getWaitAfterUpdates());
        this.autoCord = projectData.getAutoCond();

        mView.changeYandexDiscription(listYandex.get(yandexSelect));
        mView.changeDateDiscription(makeDiscription(hour, listDate));
    }

    private String makeDiscription(String hour, List<String> listDate) {
        String descriptionStr = "";

        if (listDate.size() == 0) {
            return "";
        }

        for (int i = 0; i < listDate.size(); i++) {
            switch (listDate.get(i)) {
                case "1":
                    descriptionStr = descriptionStr + context.getString(R.string.mon);
                    break;
                case "2":
                    descriptionStr = descriptionStr + " " + context.getString(R.string.tu);
                    break;
                case "3":
                    descriptionStr = descriptionStr + " " + context.getString(R.string.wen);
                    break;
                case "4":
                    descriptionStr = descriptionStr + " " + context.getString(R.string.thu);
                    break;
                case "5":
                    descriptionStr = descriptionStr + " " + context.getString(R.string.fri);
                    break;
                case "6":
                    descriptionStr = descriptionStr + " " + context.getString(R.string.sat);
                    break;
                case "7":
                    descriptionStr = descriptionStr + " " + context.getString(R.string.sun);
                    break;
            }
        }

        return descriptionStr + " " + context.getString(R.string.in) + " " + hour + " " + context.getString(R.string.hour);
    }

    @Override
    public void changeIntervalResponce(ReturnResponce<ModelResult> result) {
    }

    @Override
    public void clickItem(int position) {
        switch (position) {
            case R.id.rl_inspectSchedule:
                mView.openChangeDate(hour, listDate, idProject, autoCord);
                break;
            case R.id.rl_inspectYandex:
                mView.openChangeYandex(yandexSelect, listYandex, idProject);
                break;
            case R.id.rl_inspectOnDemand:
                DataManager.getSettingsRest().reqChangeInterval(ActivityIntervalInspect.DEMAND, idProject, "", "", this);
                mView.setChecked(ActivityIntervalInspect.DEMAND);
                DataManager.getDB().checkIntervalInspection(DataManager.getDB().getProject(idProject),
                        String.valueOf(ActivityIntervalInspect.DEMAND));
                break;
        }
    }

    @Override
    public void returnData(Intent data, int requestCode) {

        switch (requestCode) {
            case ActivityIntervalInspect.RESULT_YANDEX:
                mView.setChecked(ActivityIntervalInspect.YANDEX);
                DataManager.getDB().checkIntervalInspection(DataManager.getDB().getProject(idProject),
                        String.valueOf(ActivityIntervalInspect.YANDEX));
                mView.changeYandexDiscription(listYandex.get(data.getIntExtra("idSelected", 0)));
                break;
            case ActivityIntervalInspect.RESULT_DATE:
                mView.setChecked(ActivityIntervalInspect.DATE);
                DataManager.getDB().checkIntervalInspection(DataManager.getDB().getProject(idProject),
                        String.valueOf(ActivityIntervalInspect.DATE));
                mView.changeDateDiscription(makeDiscription(
                        data.getStringExtra("hour"),
                        data.getStringArrayListExtra("listDate")));
                break;
        }


    }
}
