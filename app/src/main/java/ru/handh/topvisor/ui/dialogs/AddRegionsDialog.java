package ru.handh.topvisor.ui.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import ru.handh.topvisor.R;
import ru.handh.topvisor.ui.settingsProject.searchSystems.searchRegion.ActivitySearchRegion;

/**
 * Created by sergey on 27.03.16.
 */
public class AddRegionsDialog extends DialogFragment implements ActivitySearchRegion.SendNumberToDialog {

    public TextView description;
    public ProgressBar progressBar;
    private DialogResult mListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (DialogResult) ((ActivitySearchRegion) activity).presenter;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_desription_progress, null);

        description = (TextView) view.findViewById(R.id.tv_dialog_description);
        progressBar = (ProgressBar) view.findViewById(R.id.progress_dialog_description);


        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
        adb.setView(view);

        adb.setPositiveButton(getContext().getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                mListener.onDialogPositiveClick(AddRegionsDialog.this);
            }
        });

        return adb.create();
    }


    @Override
    public void showResult(int number) {
        progressBar.setVisibility(View.GONE);
        description.setVisibility(View.VISIBLE);

        String quantityString = getResources().getQuantityString(R.plurals.addRegions,
                number, number);

        description.setText(quantityString);
    }
}

