package ru.handh.topvisor.ui.settingsProject.interval;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.AppCompatRadioButton;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

import ru.handh.topvisor.R;
import ru.handh.topvisor.ui.base.ParentBackActivity;
import ru.handh.topvisor.ui.settingsProject.interval.intervalDate.ActivitySettingDate;
import ru.handh.topvisor.ui.settingsProject.interval.intervalYandex.ActivityYandexInterval;

/**
 * Created by sergey on 16.03.16.
 * экран выбора интревалов проверки
 */
public class ActivityIntervalInspect extends ParentBackActivity implements ViewIntervalInspect {


    public final static int RESULT_DATE = 3000;
    public final static int RESULT_YANDEX = 3001;
    public final static int DEMAND = 0;
    public final static int DATE = 1;
    public final static int YANDEX = 2;

    public PresenterIntervalInspect presenter;
    private AppCompatRadioButton rb_Schedule, rb_Yandex, rb_demand;
    private TextView tvDate, tvYandex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_inspect);

        initToolbar(R.id.toolbar, getString(R.string.inspection));

        rb_Schedule = (AppCompatRadioButton) findViewById(R.id.rb_inspectSchedule);
        rb_Yandex = (AppCompatRadioButton) findViewById(R.id.rb_inspectYandex);
        rb_demand = (AppCompatRadioButton) findViewById(R.id.rb_inspectOnDemand);
        tvDate = (TextView) findViewById(R.id.tv_inspectScheduleDescription);
        tvYandex = (TextView) findViewById(R.id.trl_inspectYandexDescription);


        String idProject = "";
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            idProject = extras.getString("idProject");
        }

        presenter = new PresenterIntervalInspectImpl(this, idProject, this);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.changeDescription();
    }

    @Override
    public void setChecked(int position) {
        switch (position) {
            case DATE:
                rb_Schedule.setChecked(true);
                rb_Yandex.setChecked(false);
                rb_demand.setChecked(false);
                break;
            case YANDEX:
                rb_Schedule.setChecked(false);
                rb_Yandex.setChecked(true);
                rb_demand.setChecked(false);
                break;
            case DEMAND:
                rb_Schedule.setChecked(false);
                rb_Yandex.setChecked(false);
                rb_demand.setChecked(true);
                break;
        }
    }

    @Override
    public void changeYandexDiscription(String text) {
        tvYandex.setText(text);
    }

    @Override
    public void changeDateDiscription(String text) {
        tvDate.setText(text);
    }


    public void clickInspect(View view) {
        switch (view.getId()) {
            case R.id.rl_inspectSchedule:
                presenter.clickItem(R.id.rl_inspectSchedule);
                break;

            case R.id.rl_inspectYandex:
                presenter.clickItem(R.id.rl_inspectYandex);
                break;

            case R.id.rl_inspectOnDemand:
                presenter.clickItem(R.id.rl_inspectOnDemand);
                break;
        }
    }

    @Override
    public void openChangeDate(String hour, ArrayList<String> listDate, String idProject, String autoCond) {
        Intent intent = new Intent(this, ActivitySettingDate.class);
        intent.putExtra("hour", hour);
        intent.putExtra("idProject", idProject);
        intent.putExtra("autoCond", autoCond);
        intent.putStringArrayListExtra("listDate", listDate);
        startActivityForResult(intent, RESULT_DATE);
    }

    @Override
    public void openChangeYandex(int selectPosition, ArrayList<String> list, String idProject) {

        Intent intent = new Intent(this, ActivityYandexInterval.class);
        intent.putStringArrayListExtra("list", list);
        intent.putExtra("name", getString(R.string.theFrequencyOfInspection));
        intent.putExtra("selected", selectPosition);
        intent.putExtra("idProject", idProject);
        startActivityForResult(intent, RESULT_YANDEX);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            presenter.returnData(data, requestCode);
        }
    }
}
