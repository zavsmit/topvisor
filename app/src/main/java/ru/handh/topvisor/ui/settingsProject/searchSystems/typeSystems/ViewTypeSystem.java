package ru.handh.topvisor.ui.settingsProject.searchSystems.typeSystems;

import java.util.ArrayList;

import ru.handh.topvisor.data.model.DateItem;

/**
 * Created by sergey on 17.03.16.
 */
public interface ViewTypeSystem {
    void showItem(int typeItem);
    void startMyActivity(ArrayList<DateItem> dateItemListType, String name,
                         int type, String idSearcher, String idCurrentRegion,
                         String idProject, int iSearcher);
    void changeLandDepthDevice(String langS, String depthS, String deviceS);
}
