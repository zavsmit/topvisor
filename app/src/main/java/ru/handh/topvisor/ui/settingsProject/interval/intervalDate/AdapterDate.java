package ru.handh.topvisor.ui.settingsProject.interval.intervalDate;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;

import java.util.List;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.model.DateItem;

/**
 * Created by sergey on 15.03.16.
 */
public class AdapterDate extends RecyclerSwipeAdapter<RecyclerView.ViewHolder> {

    private static final int TYPE_HEADER = 1;
    private static final int TYPE_WEEK = 2;
    private static final int TYPE_FOOTER = 3;

    private ItemDateClickListener mListener;
    private String hour;
    private boolean isSelect;

    private List<DateItem> dateItems;

    public AdapterDate(List<String> listSelected, ItemDateClickListener listener, String hour, boolean isSelect, List<DateItem> dateItems) {

        mListener = listener;
        this.hour = hour;
        this.isSelect = isSelect;
        this.dateItems = dateItems;

//        dateItems.add(new DateItem(.getString(R.string.monday), false));
//        dateItems.add(new DateItem("Вторник", false));
//        dateItems.add(new DateItem("Среда", false));
//        dateItems.add(new DateItem("Четверг", false));
//        dateItems.add(new DateItem("Пятница", false));
//        dateItems.add(new DateItem("Суббота", false));
//        dateItems.add(new DateItem("Воскресенье", false));

        for (int i = 0; i < listSelected.size(); i++) {
            dateItems.get(Integer.valueOf(listSelected.get(i)) - 1).setIsEnable(true);
        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        switch (viewType) {
            case TYPE_HEADER:
                return new ViewHolderHeader(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_date_header, parent, false));
            case TYPE_WEEK:
                return new ViewHolderWeek(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_date, parent, false));
            case TYPE_FOOTER:
                return new ViewHolderFooter(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_date_footer, parent, false));
        }

        return null;

    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {


        switch (holder.getItemViewType()) {

            case TYPE_HEADER:

                if (hour == null) {
                    ((ViewHolderHeader) holder).time.setText("--/--");
                } else {
                    ((ViewHolderHeader) holder).time.setText(hour + ":00");
                }

                ((ViewHolderHeader) holder).ll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mListener.clickTime(hour);
                    }
                });

                break;

            case TYPE_WEEK:

                final ViewHolderWeek mHolderRegion = ((ViewHolderWeek) holder);

                mHolderRegion.text.setText(dateItems.get(position - 1).getName());

                if (dateItems.get(position - 1).isEnable()) {
                    mHolderRegion.checkBox.setChecked(true);
                } else {
                    mHolderRegion.checkBox.setChecked(false);
                }

                mHolderRegion.rl.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mHolderRegion.checkBox.setChecked(!mHolderRegion.checkBox.isChecked());
                    }
                });


                mHolderRegion.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        mListener.clickWeekItem(position - 1, isChecked);
                    }
                });

                break;
            case TYPE_FOOTER:

                ((ViewHolderFooter) holder).switchDate.setChecked(isSelect);

                ((ViewHolderFooter) holder).ll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((ViewHolderFooter) holder).switchDate.setChecked(!((ViewHolderFooter) holder).switchDate.isChecked());
                    }
                });

                ((ViewHolderFooter) holder).switchDate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        mListener.clickSwitch(isChecked);
                    }
                });

                break;
        }
    }

    public void changeHour(String hour) {
        this.hour = hour;
        notifyItemChanged(0);
    }

    @Override
    public int getItemViewType(int position) {


        if (position == 0) {
            return TYPE_HEADER;
        }

        if (position == dateItems.size() + 1) {
            return TYPE_FOOTER;
        } else {
            return TYPE_WEEK;
        }

    }

    @Override
    public int getItemCount() {
        return dateItems.size() + 2;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe_project_request;
    }

    public interface ItemDateClickListener {
        void clickWeekItem(int idSelected, boolean isAdd);

        void clickTime(String hour);

        void clickSwitch(boolean isSelected);
    }

    public static class ViewHolderWeek extends RecyclerView.ViewHolder {
        public TextView text;
        public CheckBox checkBox;
        public RelativeLayout rl;

        public ViewHolderWeek(View v) {
            super(v);
            text = (TextView) itemView.findViewById(R.id.tv_itemDate);
            checkBox = (CheckBox) itemView.findViewById(R.id.cb_itemDate);
            rl = (RelativeLayout) itemView.findViewById(R.id.rl_itemDate);
        }
    }

    public static class ViewHolderFooter extends RecyclerView.ViewHolder {
        public LinearLayout ll;
        public SwitchCompat switchDate;

        public ViewHolderFooter(View v) {
            super(v);
            ll = (LinearLayout) itemView.findViewById(R.id.ll_date_switch);
            switchDate = (SwitchCompat) itemView.findViewById(R.id.switch_date_footer);
        }
    }

    public static class ViewHolderHeader extends RecyclerView.ViewHolder {
        public TextView time;
        public LinearLayout ll;

        public ViewHolderHeader(View v) {
            super(v);
            time = (TextView) itemView.findViewById(R.id.tv_date_time);
            ll = (LinearLayout) itemView.findViewById(R.id.ll_itemDate_time);
        }
    }

}
