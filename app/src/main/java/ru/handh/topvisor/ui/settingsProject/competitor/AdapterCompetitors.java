package ru.handh.topvisor.ui.settingsProject.competitor;

import android.view.View;

import io.realm.RealmList;
import ru.handh.topvisor.data.database.project.CompetitorData;
import ru.handh.topvisor.data.network.model.ResultCompetitors;
import ru.handh.topvisor.ui.removeItemsList.AdapterRemoveItemsList;

/**
 * Created by sergey on 22.03.16.
 * адаптрер настройки конкурентов
 */
public class AdapterCompetitors extends AdapterRemoveItemsList {
    public AdapterCompetitors(RealmList<CompetitorData> competitors, ItemSearcherClickListener listener) {
        super();

        mListener = listener;
        this.competitors = competitors;

        for (int i = 0; i < competitors.size(); i++) {
            mData.add(new DataItem(competitors.get(i).getName()));
        }
    }


    @Override
    public void onBindViewHolder(final ViewHolderTitle holder, int position) {
        holder.llTrash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.removeItem(holder.getAdapterPosition(), competitors.get(holder.getAdapterPosition()).getId());
            }
        });
        onBindClicks(holder, position);
    }

    public void removeData(int position) {
        mData.remove(position);
        notifyItemRemoved(position);
    }

    public void addItem(ResultCompetitors item) {
        mData.add(new DataItem(item.getName()));
        notifyItemInserted(mData.size() - 1);
    }
}
