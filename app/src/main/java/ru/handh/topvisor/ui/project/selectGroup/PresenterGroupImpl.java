package ru.handh.topvisor.ui.project.selectGroup;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import ru.handh.topvisor.data.DataManager;
import ru.handh.topvisor.data.database.project.GroupData;
import ru.handh.topvisor.data.network.model.currentProject.Group;

/**
 * Created by sergey on 11.03.16.
 */
public class PresenterGroupImpl implements PresenterGroup, AdapterGroups.ItemGroupClickListener {

    private ViewGroups mView;
    private RealmList<GroupData> groupsData;

    public PresenterGroupImpl(ViewGroups view, String idProject) {
        mView = view;
        groupsData = DataManager.getDB().getProject(idProject).getGroups();


        List<Group> groupList = new ArrayList<>();

        for (int i = 0; i < groupsData.size(); i++) {

            if (!groupsData.get(i).getOn().equals("0")) {
                Group group = new Group();

                group.setId(groupsData.get(i).getId());
                group.setIsEnable(groupsData.get(i).isEnable());
                group.setName(groupsData.get(i).getName());
                group.setOn(groupsData.get(i).getOn());
                group.setStatus(groupsData.get(i).getStatus());


                groupList.add(group);
            }
        }

        mView.initAdapter(groupList, this);
    }

    @Override
    public void clickItem(String idGroup) {

        DataManager.getDB().selectItemGroup(groupsData, idGroup);

        mView.selectGroup();
    }
}
