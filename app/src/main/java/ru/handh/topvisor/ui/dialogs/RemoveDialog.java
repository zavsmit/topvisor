package ru.handh.topvisor.ui.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import ru.handh.topvisor.R;
import ru.handh.topvisor.ui.main.ActivityMainView;

/**
 * Created by sergey on 17.02.16.
 * удаление проекта
 */
public class RemoveDialog extends DialogFragment {

    public int mPosition;
    private DialogResult mListener;

    public RemoveDialog() {
    }

    public static RemoveDialog newInstance(String name, int position) {
        RemoveDialog f = new RemoveDialog();

        Bundle args = new Bundle();
        args.putString("name", name);
        args.putInt("position", position);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (DialogResult) ((ActivityMainView) activity).presenter;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mPosition = getArguments().getInt("position");
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
                .setTitle(getContext().getString(R.string.attention))
                .setPositiveButton(getContext().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mListener.onDialogPositiveClick(RemoveDialog.this);
                    }
                })
                .setNegativeButton(getContext().getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mListener.onDialogNegativeClick(RemoveDialog.this);
                    }
                })
                .setMessage(getContext().getString(R.string.removeProgectDesript) + getArguments().getString("name") + "\"?");

        return adb.create();
    }
}