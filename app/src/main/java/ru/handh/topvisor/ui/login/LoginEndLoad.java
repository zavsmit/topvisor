package ru.handh.topvisor.ui.login;


import ru.handh.topvisor.data.network.model.ReturnResponce;

/**
 * Created by sergey on 09.02.16.
 * интерфейс для передачи данных из модели в презетер на экране авторизации
 */
public interface LoginEndLoad {

    void openMainAndSaveAuth(ReturnResponce<String> result);

    void sendReqAndShowDialog(ReturnResponce<Boolean> result, int numberDialog);
}
