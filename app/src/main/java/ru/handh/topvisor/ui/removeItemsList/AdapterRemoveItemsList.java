package ru.handh.topvisor.ui.removeItemsList;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import ru.handh.topvisor.R;
import ru.handh.topvisor.data.database.project.CompetitorData;
import ru.handh.topvisor.data.database.project.GroupData;
import ru.handh.topvisor.data.network.model.ResultCompetitors;

/**
 * Created by sergey on 21.03.16.
 */
public abstract class AdapterRemoveItemsList extends RecyclerSwipeAdapter<AdapterRemoveItemsList.ViewHolderTitle> {
    public RealmList<GroupData> groups;
    public RealmList<CompetitorData> competitors;
    protected List<DataItem> mData;
    protected ItemSearcherClickListener mListener;
    protected SparseBooleanArray selectedItems;

    public AdapterRemoveItemsList() {
        selectedItems = new SparseBooleanArray();
        mData = new ArrayList<>();
    }

    @Override
    public ViewHolderTitle onCreateViewHolder(ViewGroup parent, int viewType) {

        return new ViewHolderTitle(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_name_switch, parent, false));

    }
    protected void onBindClicks(ViewHolderTitle mHolder, final int position) {
        mHolder.title.setText(mData.get(position).title);

        mHolder.rl.setSelected(selectedItems.get(position));

        mHolder.rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.clickItem(position);
            }
        });

        mHolder.rl.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mListener.longClick(position);
                return true;
            }
        });
    }


    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe_searcher;
    }

    public void toggleSelection(int pos) {
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
        } else {
            selectedItems.put(pos, true);
        }
        notifyItemChanged(pos);
    }

    public void clearSelections() {
        selectedItems.clear();
        notifyDataSetChanged();
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items = new ArrayList<>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }

    public abstract void removeData(int position);

    public void removeFirst(){
        mData.remove(0);
        notifyItemRemoved(0);
    }

    public void renameData(int position, String name){
        mData.get(position).title = name;
        notifyItemChanged(position);
    }

    public abstract void addItem(ResultCompetitors item);


    public interface ItemSearcherClickListener {
        void clickItem(int position);

        void longClick(int position);

        void changeSwitch(boolean isCheck, String idGroup, int checkPosition);

        void removeItem(int position, String id);

        void renameItem(int position, String name, String idGroup);
    }

    public class ViewHolderTitle extends RecyclerView.ViewHolder {
        public TextView title;
        public SwitchCompat switchCompat;
        public View view;
        public RelativeLayout rl;
        public SwipeLayout swipe_searcher;
        public LinearLayout llTrash, llRename, ll_itemNameSwitch;

        public ViewHolderTitle(View v) {
            super(v);
            title = (TextView) itemView.findViewById(R.id.tv_itemNameSwitch);
            switchCompat = (SwitchCompat) itemView.findViewById(R.id.switch_itemNameSwitch);
            view = itemView.findViewById(R.id.v_itemNameSwitch);
            rl = (RelativeLayout) itemView.findViewById(R.id.rl_itemNameSwitch);
            llTrash = (LinearLayout) itemView.findViewById(R.id.ll_itemNameSwitch_trash);
            ll_itemNameSwitch = (LinearLayout) itemView.findViewById(R.id.ll_itemNameSwitch);
            llRename = (LinearLayout) itemView.findViewById(R.id.ll_itemNameSwitch_rename);
            swipe_searcher = (SwipeLayout) itemView.findViewById(R.id.swipe_searcher);
        }
    }

    public class DataItem {
        public String title;
        boolean isChecked;

        public DataItem(String title) {
            this.title = title;
        }
    }
}
