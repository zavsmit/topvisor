package ru.handh.topvisor.ui.settingsProject;

/**
 * Created by sergey on 16.03.16.
 */
public interface ViewSettings {
    void openInterval(String idProject);
    void openSearcher(String idProjec);
    void openGroups(String idProject);
    void openCompetitors(String idProject);
    void setDiscription(String text);
    void close();
}
