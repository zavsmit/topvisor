package ru.handh.topvisor.ui.settingsProject;

import android.content.Context;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.DataManager;
import ru.handh.topvisor.ui.settingsProject.interval.ActivityIntervalInspect;

/**
 * Created by sergey on 16.03.16.
 * презентер главного экрана настроек
 */
public class PresenterSettingsImpl implements PresenterSettings {

    private ViewSettings mView;
    private String idProject;
    private  Context context;

    public PresenterSettingsImpl(ViewSettings view, String idProject, Context context) {
        this.idProject = idProject;
        this.context = context;
        mView = view;
    }

    public void changeDemand() {
        String description = "";
        switch (Integer.valueOf(DataManager.getDB().getProject(idProject).getOn())) {
            case ActivityIntervalInspect.DEMAND:
                description = context.getString(R.string.byDemand);
                break;
            case ActivityIntervalInspect.DATE:
                description = context.getString(R.string.bySchedule);

                break;
            case ActivityIntervalInspect.YANDEX:
                description = context.getString(R.string.afterYandex);
                break;
        }

        mView.setDiscription(description);
    }

    @Override
    public void close() {
        mView.close();
    }


    @Override
    public void clickOpenActvity(int idActivity) {
        switch (idActivity) {
            case ActivitySettings.ACTIVITY_INTERVAL:
                mView.openInterval(idProject);
                break;
            case ActivitySettings.ACTIVITY_SEARCHER:
                mView.openSearcher(idProject);
                break;
            case ActivitySettings.ACTIVITY_GROUPS:
                mView.openGroups(idProject);
                break;
            case ActivitySettings.ACTIVITY_COMPETITORS:
                mView.openCompetitors(idProject);
                break;
        }
    }
}
