package ru.handh.topvisor.ui.bank;

import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.DataManager;
import ru.handh.topvisor.data.network.model.ModelItemBank;
import ru.handh.topvisor.utils.Constants;
import ru.handh.topvisor.utils.Utils;

/**
 * Created by sergey on 07.04.16.
 * адаптер списка транакций
 */
public class AdapterBank extends RecyclerSwipeAdapter<AdapterBank.ViewHolder> {
    private List<ModelItemBank> mDataset;
    private ItemClickListener mListener;

    public AdapterBank(List<ModelItemBank> list, ItemClickListener listener) {
        mDataset = list;
        mListener = listener;
    }

    @Override
    public AdapterBank.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bank, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        if (mDataset.get(holder.getAdapterPosition()).getId() == null) {
            holder.progressBar.setVisibility(View.VISIBLE);
        } else {
            holder.progressBar.setVisibility(View.GONE);

            holder.text.setText(mDataset.get(holder.getAdapterPosition()).getInfo());

            setPrice(holder.getAdapterPosition(), holder);
            setDate(holder.getAdapterPosition(), holder);

            holder.rl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.clickItem(mDataset.get(holder.getAdapterPosition()).getInfo());
                }
            });
        }

    }

    private String getMonthForInt(int num) {
        String month = "wrong";
        DateFormatSymbols dfs = new DateFormatSymbols();
        String[] months = dfs.getMonths();
        if (num >= 0 && num <= 11) {
            month = months[num];
        }
        return month;
    }

    public void addData(List<ModelItemBank> newData, boolean isNew) {

        if (isNew) {
            mDataset.clear();
        }

        mDataset.addAll(newData);
        notifyDataSetChanged();
    }

    public void showProgress(boolean isShow) {
        if (isShow) {
            mDataset.add(new ModelItemBank());
            notifyItemInserted(mDataset.size() - 1);
        } else {

            if (mDataset.size() != 0) {
                int removePosition = mDataset.size() - 1;
                mDataset.remove(removePosition);
                notifyItemRemoved(removePosition);
            }

        }
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe_project_request;
    }

    private void setPrice(int position, ViewHolder holder) {
        double sum = mDataset.get(position).getSum();

        if (sum < 0) {
            holder.price.setTextColor(Utils.getCustomColor(holder.price.getContext(), R.color.textRed));
        } else {
            holder.price.setTextColor(Utils.getCustomColor(holder.price.getContext(), R.color.textGreen));
        }


        String symbol;

        if (DataManager.getPref().getServerShort().equals(Constants.RU)) {

            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                symbol = holder.price.getContext().getString(R.string.valuteSimbol) + " ";
            } else {
                symbol = holder.price.getContext().getString(R.string.valuteString) + " ";
            }

        } else {

            symbol = R.string.simbolUS + " ";
        }

        holder.price.setText(symbol + String.valueOf(sum));
    }

    private void setDate(int position, ViewHolder holder) {
        Calendar cal = Calendar.getInstance();
        int dayToday = cal.get(Calendar.DAY_OF_MONTH);
        int monthToday = cal.get(Calendar.MONTH);
        int yearToday = cal.get(Calendar.YEAR);

        cal.setTime(mDataset.get(position).getDate());

        String date;
        if (dayToday == cal.get(Calendar.DAY_OF_MONTH) && monthToday == cal.get(Calendar.MONTH) && yearToday == cal.get(Calendar.YEAR)) {
            date = String.format(Locale.US, "%02d:%02d", cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE));
        } else if (cal.get(Calendar.YEAR) == yearToday) {
            date = cal.get(Calendar.DAY_OF_MONTH) + " " + getMonthForInt(cal.get(Calendar.MONTH));
        } else {
            int month = cal.get(Calendar.MONTH) + 1;
            date = String.format(Locale.US, "%2d.%02d", cal.get(Calendar.DAY_OF_MONTH), month) + "." + cal.get(Calendar.YEAR);
        }


        holder.date.setText(date);
    }

    public interface ItemClickListener {
        void clickItem(String text);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView date, price, text;
        public RelativeLayout rl;
        public ProgressBar progressBar;

        public ViewHolder(View v) {
            super(v);
            date = (TextView) itemView.findViewById(R.id.tv_itemBank_date);
            price = (TextView) itemView.findViewById(R.id.tv_itemBank_price);
            text = (TextView) itemView.findViewById(R.id.tv_itemBank_text);
            rl = (RelativeLayout) itemView.findViewById(R.id.rl_itemBank);
            progressBar = (ProgressBar) itemView.findViewById(R.id.tv_itemBank_progress);
        }
    }
}