package ru.handh.topvisor.ui.support.listMessages;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import ru.handh.topvisor.R;

/**
 * Created by sergey on 05.04.16.
 * адаптер блока добавляемых фото
 */
public class AdapterPhoto extends RecyclerView.Adapter<AdapterPhoto.ViewHolder> {

    private List<String> itemList;
    private ItemClickListener mListener;

    public AdapterPhoto(List<String> itemList, ItemClickListener listener) {
        this.itemList = itemList;
        mListener = listener;
    }

    @Override
    public AdapterPhoto.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_add_photo, parent, false);
        return new ViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(AdapterPhoto.ViewHolder holder, final int position) {

        Picasso.with(holder.photo.getContext())
                .load(Uri.fromFile(new File(itemList.get(position))))
                .fit()
                .centerInside()
                .into(holder.photo);

        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeItem(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public void appPhoto(String image) {
        itemList.add(image);
        notifyItemInserted(itemList.size() - 1);
    }

    public void removeItem(int position) {


        String removedString = itemList.get(position);
        itemList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, getItemCount());
        mListener.removeItem(removedString);

    }


    public interface ItemClickListener {
        void removeItem(String removedString);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView photo, remove;

        public ViewHolder(View v) {
            super(v);
            photo = (ImageView) itemView.findViewById(R.id.iv_addPhoto);
            remove = (ImageView) itemView.findViewById(R.id.iv_addPhoto_remove);
        }
    }
}

