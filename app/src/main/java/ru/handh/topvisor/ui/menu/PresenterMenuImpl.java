package ru.handh.topvisor.ui.menu;

import java.util.List;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.DataManager;
import ru.handh.topvisor.data.database.TicketData;
import ru.handh.topvisor.data.database.UserData;
import ru.handh.topvisor.data.network.model.ModelList;
import ru.handh.topvisor.data.network.model.ModelTicket;
import ru.handh.topvisor.data.network.model.ModelUserData;
import ru.handh.topvisor.data.network.model.ReturnResponce;

/**
 * Created by sergey on 09.04.16.
 */
public class PresenterMenuImpl implements PresenterMenu, MenuEndReq {

    private ViewMenu view;

    public PresenterMenuImpl(ViewMenu view, boolean isFirstLoad) {
        this.view = view;

        if (isFirstLoad) {
            loadUserdata();
            loadTicketsMenu();
        } else {
            loadFromDB();
        }

    }


    @Override
    public void loadTicketsMenu() {
        DataManager.getMainRest().reqTikets(this);
    }

    @Override
    public void loadUserdata() {
        view.showRefreshMenu(true);
        DataManager.getMainRest().reqUserData(this);
    }

    @Override
    public void getTicketsResponce(ReturnResponce<ModelList<ModelTicket>> result) {
        if (!result.isFailure()) {

            if (result.getResult() != null) {

                List<ModelTicket> rows = result.getResult().getRows();
                DataManager.getDB().ticketWriteDatabase(rows);
                int count = 0;

                if (rows != null) {
                    for (int i = 0; i < rows.size(); i++) {
                        if (String.valueOf(rows.get(i).getOwner_readed()).equals("0")) {
                            ++count;
                        }
                    }
                }

                view.initCountNewMessage(count);

            } else {
                view.showToast(result.getErrorMessage());
            }
        } else {
//            view.showToast(R.string.falseNetwork);
        }
    }

    @Override
    public void getUserDataResponce(ReturnResponce<ModelUserData> result) {
        if (!result.isFailure()) {

            ModelUserData userData = result.getResult();

            if (userData != null) {

                view.initMenu(userData);
                DataManager.getDB().userDataWriteDatabase(userData);
            } else {
                view.showToast(result.getErrorMessage());
            }
            view.showRefreshMenu(false);

        } else {
            view.showRefreshMenu(false);
            view.showToast(R.string.settingNet);
        }
    }

    private void loadFromDB() {


        ModelUserData mud = new ModelUserData();

        UserData ud = DataManager.getDB().getUser();

        mud.setAuth(ud.getAuth());
        mud.setAvatar(ud.getAvatar());
        mud.setBalance(ud.getBalance());
        mud.setDate_reg(ud.getDate_reg());
        mud.setDomain(ud.getDomain());
        mud.setEmail(ud.getEmail());
        mud.setId(ud.getId());
        mud.setLastactive(ud.getLastactive());
        mud.setName(ud.getName());
        mud.setRef(ud.getRef());
        mud.setWarning_message(ud.getWarning_message());
        mud.setYandex_xml_balance(ud.getYandex_xml_balance());

        ModelUserData.Tarif udt = mud.new Tarif();
        udt.setBalance_t(ud.getTariff().getBalance_t());
        udt.setDiscount(ud.getTariff().getDiscount());
        udt.setTariff(ud.getTariff().getTariff());

        mud.setTariff(udt);

        view.initMenu(mud);




        List<TicketData> td = DataManager.getDB().getTickets();


        int count = 0;

        if (td != null) {
            for (int i = 0; i < td.size(); i++) {
                if (String.valueOf(td.get(i).getOwner_readed()).equals("0")) {
                    ++count;
                }
            }
        }

        view.initCountNewMessage(count);
    }


}
