package ru.handh.topvisor.ui.settingsProject.interval.intervalDate;

import android.app.TimePickerDialog;
import android.widget.TimePicker;

import java.util.ArrayList;
import java.util.List;

import ru.handh.topvisor.data.DataManager;
import ru.handh.topvisor.data.network.model.ModelResult;
import ru.handh.topvisor.data.network.model.ReturnResponce;
import ru.handh.topvisor.ui.settingsProject.interval.ActivityIntervalInspect;
import ru.handh.topvisor.ui.settingsProject.interval.IntervalReqResult;

/**
 * Created by sergey on 14.03.16.
 */
public class PresenterDateImpl implements
        PresenterDate,
        AdapterDate.ItemDateClickListener,
        TimePickerDialog.OnTimeSetListener,
        IntervalReqResult {

    private ViewDate mView;
    private List<String> listDate;
    private String hour, idProject, autoCond;

    public PresenterDateImpl(ViewDate view, String hour, List<String> listDate, String idProject, String autoCond) {
        mView = view;
        this.listDate = listDate;
        this.idProject = idProject;
        this.autoCond = autoCond;
        this.hour = hour;

        boolean isSelect = autoCond.equals("1");

        mView.initAdapter(listDate, this, hour, isSelect);
    }

    @Override
    public void clickWeekItem(int idSelected, boolean isAdd) {

        String changeItem = String.valueOf(idSelected + 1);

        if (isAdd) {
            listDate.add(changeItem);
        } else {
            for (int i = 0; i < listDate.size(); i++) {
                if (listDate.get(i).equals(changeItem)) {
                    listDate.remove(i);
                }
            }
        }
    }

    @Override
    public void clickTime(String house) {
        mView.showTimeDialog(house);
    }

    @Override
    public void clickSwitch(boolean isSelected) {

        if (isSelected) {
            autoCond = "1";
        } else {
            autoCond = "0";
        }
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        hour = String.valueOf(hourOfDay);
        mView.changeAdapteHour(hour);
    }

    @Override
    public void close() {

        String sendList = "";


        for (int i = 0; i < listDate.size(); i++) {
            sendList = sendList + "|" + listDate.get(i);
        }
        sendList = sendList + "|:" + hour;

        DataManager.getDB().changeDateIntervalInspection(idProject, sendList, autoCond);

        DataManager.getSettingsRest().reqChangeInterval(ActivityIntervalInspect.DATE, idProject, sendList, "", this);
        DataManager.getSettingsRest().reqAutoCond(autoCond, idProject, this);

        mView.customStop(hour, (ArrayList<String>) listDate);
    }

    @Override
    public void changeIntervalResponce(ReturnResponce<ModelResult> result) {

    }
}

