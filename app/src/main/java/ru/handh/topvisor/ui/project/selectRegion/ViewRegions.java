package ru.handh.topvisor.ui.project.selectRegion;

import io.realm.RealmList;
import ru.handh.topvisor.data.database.project.SearchData;

/**
 * Created by sergey on 11.03.16.
 */
public interface ViewRegions {
    void initAdapter(RealmList<SearchData> regions, AdapterRegions.ItemRegionClickListener listener);
    void selectRegion();
}
