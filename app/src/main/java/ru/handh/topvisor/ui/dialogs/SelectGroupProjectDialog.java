package ru.handh.topvisor.ui.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import ru.handh.topvisor.R;
import ru.handh.topvisor.ui.project.ActivityViewProject;

/**
 * Created by sergey on 25.02.16.
 */
public class SelectGroupProjectDialog extends DialogFragment {

    private DialogResult mListener;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (DialogResult) ((ActivityViewProject) activity).presenter;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
//                .setTitle(getActivity().getString(R.string.actionNotBe))
//                .setPositiveButton(getActivity().getString(R.string.select), new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        mListener.onDialogPositiveClick(SelectGroupProjectDialog.this);
//                    }
//                })
//                .setNegativeButton(getContext().getString(R.string.cancel), new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        mListener.onDialogNegativeClick(SelectGroupProjectDialog.this);
//                    }
//                })
//                .setMessage(getActivity().getString(R.string.selectGroupFor));



        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_desription_progress, null);

        TextView description = (TextView) view.findViewById(R.id.tv_dialog_description);
        ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progress_dialog_description);
        progressBar.setVisibility(View.GONE);
        description.setVisibility(View.VISIBLE);

        description.setText((getActivity().getString(R.string.selectGroupFor)));

        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
        adb.setView(view);

        adb.setTitle(getActivity().getString(R.string.actionNotBe)).
        setPositiveButton(getActivity().getString(R.string.select), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                mListener.onDialogPositiveClick(SelectGroupProjectDialog.this);
            }
        })
                .setNegativeButton(getContext().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mListener.onDialogNegativeClick(SelectGroupProjectDialog.this);
                    }
                });


        return adb.create();

    }
}
