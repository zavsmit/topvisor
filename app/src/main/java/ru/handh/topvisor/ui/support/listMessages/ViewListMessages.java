package ru.handh.topvisor.ui.support.listMessages;

import java.util.List;

import ru.handh.topvisor.data.network.model.ModelItemMessage;

/**
 * Created by sergey on 01.04.16.
 */
public interface ViewListMessages {
    void initAdapter(List<ModelItemMessage> groups);

    void initPhotoAdapter(List<String> list, AdapterPhoto.ItemClickListener listener);

    void addMessage(String text, List<String> photos);

    void addSupportMessage(String text);

    void showInfoToast(String text);

    void showData();

    void showProgress();

    void showError();

    void initToolbar(String name);

    void openIntentPhoto();

    void addPhotoToAdapter(String bitmap);

    void showPhotoToAdapter(boolean isShow);
}
