package ru.handh.topvisor.ui.support;

import java.util.ArrayList;
import java.util.List;

import ru.handh.topvisor.data.DataManager;
import ru.handh.topvisor.data.database.TicketData;
import ru.handh.topvisor.data.network.model.ModelItemMessage;
import ru.handh.topvisor.data.network.model.ModelResult;
import ru.handh.topvisor.data.network.model.ModelTicket;
import ru.handh.topvisor.data.network.model.ReturnResponce;

/**
 * Created by sergey on 31.03.16.
 */
public class PresenterDialogsListImpl implements PresenterDialogsList, SupportEndReq, AdapterDialogsList.ItemClickListener {


    private ViewDialogsList mView;
    private List<ModelTicket> listDialogs;

    public PresenterDialogsListImpl(ViewDialogsList view) {
        mView = view;
        listDialogs = new ArrayList<>();
        loadFromDB();
    }

    private void loadFromDB() {
        listDialogs.clear();
        List<TicketData> tikets = DataManager.getDB().getTickets();
        for (int i = 0; i < tikets.size(); i++) {
            TicketData mt = tikets.get(i);


            ModelTicket td = new ModelTicket();

            td.setId(mt.getId());
            td.setUser(mt.getUser());
            td.setSocial_user_id(mt.getSocial_user_id());
            td.setEmail(mt.getEmail());
            td.setType(mt.getType());
            td.setText(mt.getText());
            td.setUser_data(mt.getUser_data());
            td.setStatus(mt.getStatus());
            td.setOwner_readed(mt.getOwner_readed());
            td.setAnswerer_readed(mt.getAnswerer_readed());
            td.setTime(mt.getTime());
            td.setAdmin_writing(mt.getAdmin_writing());
            td.setTask_id(mt.getTask_id());
            td.setLast_message(mt.getLast_message());
            td.setUserName(mt.getUserName());

            listDialogs.add(td);
        }
        mView.initAdapter(listDialogs, this);
        if (listDialogs.size() == 0) {
            mView.showEmptyText(true);
        }
    }


    @Override
    public void clickItem(int idSelected) {
        mView.selectItem(listDialogs.get(idSelected).getId());
    }


    @Override
    public void listDialogsResponce(ReturnResponce<List<ModelTicket>> result) {
        if (!result.isFailure()) {
            listDialogs.clear();
            listDialogs.addAll(result.getResult());

            mView.initAdapter(listDialogs, this);
            DataManager.getDB().ticketWriteDatabase(listDialogs);
            if (listDialogs.size() == 0) {
                mView.showEmptyText(true);
            }
            mView.showData();
        } else {
            mView.showError();
            mView.showInfoToast(result.getErrorMessage());
        }

    }

    @Override
    public void listMessagesResponce(ReturnResponce<List<ModelItemMessage>> result) {

    }

    @Override
    public void removiImageResponce(ReturnResponce<ModelResult> result) {

    }

    @Override
    public void getImagesResponce(ReturnResponce<List<String>> result) {

    }

    @Override
    public void sendMessageResponce(ReturnResponce<ModelResult> result) {

    }

    @Override
    public void sendNewDialogMessageResponce(ReturnResponce<ModelResult> result) {

    }

    @Override
    public void sendPhotoResponce(ReturnResponce<ModelResult> result) {

    }

    @Override
    public void init() {
        DataManager.getSupportRest().reqDialogList(1, 100, this);
    }

    @Override
    public void clickAdd() {
        mView.selectItem("");
    }
}
