package ru.handh.topvisor.ui.support.listMessages;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.DataManager;

/**
 * Created by sergey on 06.04.16.
 */
public class AdapterPhotoInMessage extends RecyclerView.Adapter<AdapterPhotoInMessage.ViewHolder> {

    private List<String> itemList;

    public AdapterPhotoInMessage(List<String> itemList) {
        this.itemList = itemList;
    }

    @Override
    public AdapterPhotoInMessage.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mesage_photo, parent, false);
        return new ViewHolder(layoutView);
    }

    @Override
    public void onBindViewHolder(AdapterPhotoInMessage.ViewHolder holder, final int position) {


        if (itemList.get(position).contains("bichooser")) {
            Picasso.with(holder.photo.getContext())
                    .load(Uri.fromFile(new File(itemList.get(position))))
                    .fit()
                    .centerInside()
                    .into(holder.photo);
        } else {
            String urlStart = DataManager.getPref().getServer();
            urlStart = urlStart.substring(0, urlStart.length() - 1);
            // последнего емента
            String urlImage = urlStart + itemList.get(position);
            Picasso.with(holder.photo.getContext()).load(urlImage).into(holder.photo);
        }

    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView photo;

        public ViewHolder(View v) {
            super(v);
            photo = (ImageView) itemView.findViewById(R.id.iv_itemMessagePhoto);
        }
    }
}

