package ru.handh.topvisor.ui.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import ru.handh.topvisor.R;

/**
 * Created by sergey on 07.04.16.
 */
public class TextDialog extends DialogFragment {

    public EditText name;

    public TextDialog() {
    }

    public static TextDialog newInstance(String text) {
        TextDialog f = new TextDialog();
        Bundle args = new Bundle();
        args.putString("text", text);
        f.setArguments(args);
        return f;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_text, null);

        TextView name = (TextView) view.findViewById(R.id.tv_textDialog);
        name.setText(getArguments().getString("text"));

        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
        adb.setView(view);
        adb.setTitle(getActivity().getString(R.string.detail))
                .setPositiveButton(getContext().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

        return adb.create();
    }

}