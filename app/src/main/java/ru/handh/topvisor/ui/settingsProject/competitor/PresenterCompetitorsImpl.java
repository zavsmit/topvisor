package ru.handh.topvisor.ui.settingsProject.competitor;

import android.content.Context;
import android.support.v4.app.DialogFragment;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.DataManager;
import ru.handh.topvisor.data.database.project.CompetitorData;
import ru.handh.topvisor.data.network.model.ModelResult;
import ru.handh.topvisor.data.network.model.ResultCompetitors;
import ru.handh.topvisor.data.network.model.ReturnResponce;
import ru.handh.topvisor.ui.dialogs.AddDialog;
import ru.handh.topvisor.ui.removeItemsList.ActivityRemoveItemsList;
import ru.handh.topvisor.ui.removeItemsList.PresenterRemoveItemsListImpl;
import ru.handh.topvisor.ui.removeItemsList.ViewRemoveItemsList;
import ru.handh.topvisor.ui.settingsProject.SettingsEndReq;

/**
 * Created by sergey on 22.03.16.
 * логика работы списка конкурентов
 */
public class PresenterCompetitorsImpl extends PresenterRemoveItemsListImpl implements SettingsEndReq {

    private String isProject;
    private int removePosition;
    private Context context;

    public PresenterCompetitorsImpl(ViewRemoveItemsList view,
                                    String idProject, Context context) {

        super(view, null, DataManager.getDB().getProject(idProject).getCompetitors());
        this.isProject = idProject;
        this.context = context;
    }

    @Override
    public void clickAdd() {

        mView.showDialogList(ActivityRemoveItemsList.DIALOG_ADD, context.getString(R.string.addSiteCompetitors), context.getString(R.string.siteCompetitors), 0);
    }

    @Override
    public void onResume() {

    }

    @Override
    public void changeSwitch(boolean isCheck, String idGroup, int checkPosition) {

    }

    @Override
    public void removeItem(int position, String idCompetitors) {
        removePosition = position;
        DataManager.getSettingsRest().reqRemoveCompetitors(idCompetitors, this);
    }

    @Override
    public void renameItem(int position, String name, String idGroup) {

    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        if (dialog instanceof AddDialog) {
            String name = ((AddDialog) dialog).name.getText().toString().trim();
            DataManager.getSettingsRest().reqAddCompetitors(name, isProject, this);
            return;
        }
    }

    @Override
    public void addCompetitorsResponce(ReturnResponce<ResultCompetitors> result) {
        if (!result.isFailure()) {

            ResultCompetitors item = result.getResult();

            CompetitorData competitor = new CompetitorData();
            competitor.setId(item.getId());
            competitor.setName(item.getName());
            competitor.setSite(item.getSite());
            DataManager.getDB().addCompetitorToProject(listCompetitiors, competitor);

            mAdapter.addItem(item);
            mView.showEmptyText(false);
        } else {
            mView.showToast(result.getErrorMessage());
        }
    }

    @Override
    public void addGroupResponce(ReturnResponce<ModelResult> result) {

    }

    @Override
    public void renameGroupResponce(ReturnResponce<ModelResult> result) {

    }

    @Override
    public void checkGroupResponce(ReturnResponce<ModelResult> result) {
    }

    @Override
    public void removeCompetitorsResponce(ReturnResponce<ModelResult> result) {
        if (!result.isFailure()) {
            DataManager.getDB().removeCompetitorFromProject(listCompetitiors, removePosition);
            mAdapter.removeData(removePosition);
            if (listCompetitiors.size() == 0) {
                mView.showEmptyText(true);
            }
        } else {
            mView.showToast(result.getErrorMessage());
        }
    }

    @Override
    public void removeGroupResponce(ReturnResponce<ModelResult> result, int positionw) {

    }

    @Override
    protected void removeAction(int positon) {
        removeItem(positon, mAdapter.competitors.get(positon).getId());
    }
}
