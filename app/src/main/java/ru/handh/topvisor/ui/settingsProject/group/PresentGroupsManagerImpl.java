package ru.handh.topvisor.ui.settingsProject.group;

import android.content.Context;
import android.support.v4.app.DialogFragment;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.DataManager;
import ru.handh.topvisor.data.database.project.GroupData;
import ru.handh.topvisor.data.network.model.ModelResult;
import ru.handh.topvisor.data.network.model.ResultCompetitors;
import ru.handh.topvisor.data.network.model.ReturnResponce;
import ru.handh.topvisor.ui.dialogs.AddDialog;
import ru.handh.topvisor.ui.dialogs.RemoveGroupDialog;
import ru.handh.topvisor.ui.dialogs.RenameGroupDialog;
import ru.handh.topvisor.ui.removeItemsList.ActivityRemoveItemsList;
import ru.handh.topvisor.ui.removeItemsList.PresenterRemoveItemsListImpl;
import ru.handh.topvisor.ui.removeItemsList.ViewRemoveItemsList;
import ru.handh.topvisor.ui.settingsProject.SettingsEndReq;

/**
 * Created by sergey on 22.03.16.
 */
public class PresentGroupsManagerImpl extends PresenterRemoveItemsListImpl implements SettingsEndReq {

    private int position, checkPosition;
    private String idProject, idGroup, on;
    private String lastAddName, lastRenameName;
    private Context context;

    public PresentGroupsManagerImpl(ViewRemoveItemsList view, String idProject, Context context) {
        super(view, DataManager.getDB().getProject(idProject).getGroups(), null);

        this.idProject = idProject;
        this.context = context;
        isGroup = true;

    }

    @Override
    public void clickAdd() {
        mView.showDialogList(ActivityRemoveItemsList.DIALOG_ADD, context.getString(R.string.addGroup), context.getString(R.string.groupName), 0);
    }

    @Override
    public void onResume() {
        if (listGroups.size() != 0 && listGroups.get(0).getId().equals("-1")) {
//            mAdapter.removeFirst();
        }
    }

    @Override
    public void changeSwitch(boolean isCheck, String idGroup, int checkPosition) {
        on = "0";
        this.checkPosition = checkPosition;
        if (isCheck)
            on = "1";

        DataManager.getSettingsRest().reqCheckGroup(idProject, on, idGroup, this);
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        if (dialog instanceof RemoveGroupDialog) {

            if (selectedItemPositions != null && selectedItemPositions.size() != 0) {
                for (int i = selectedItemPositions.size() - 1; i > -1; i--) {
                    DataManager.getSettingsRest().reqRemoveGroup(mAdapter.groups
                            .get(selectedItemPositions.get(i)).getId(), selectedItemPositions.get(i), this);

                    selectedItemPositions.remove(i);
                }
            } else {
                position = ((RemoveGroupDialog) dialog).position;
                DataManager.getSettingsRest().reqRemoveGroup(idGroup, position, this);
            }


            return;
        }
        if (dialog instanceof AddDialog) {
            lastAddName = ((AddDialog) dialog).name.getText().toString().trim();
            DataManager.getSettingsRest().reqAddGroup(idProject, lastAddName, this);
            return;
        }
        if (dialog instanceof RenameGroupDialog) {
            lastRenameName = ((RenameGroupDialog) dialog).name.getText().toString().trim();
            position = ((RenameGroupDialog) dialog).mPosition;
            DataManager.getSettingsRest().reqRenameGroup(idProject, lastRenameName, idGroup, this);
            return;
        }
    }

    @Override
    public void removeItem(int position, String idGroup) {
        this.idGroup = idGroup;
        mView.showDialogList(ActivityRemoveItemsList.DIALOG_REMOVE, "", "", position);
    }

    @Override
    public void renameItem(int position, String name, String idGroup) {
        this.idGroup = idGroup;
        mView.showDialogList(ActivityRemoveItemsList.DIALOG_RENAME, name, "", position);
    }

    @Override
    public void addCompetitorsResponce(ReturnResponce<ResultCompetitors> result) {
//--
    }

    @Override
    public void removeCompetitorsResponce(ReturnResponce<ModelResult> result) {
//--
    }

    @Override
    public void removeGroupResponce(ReturnResponce<ModelResult> result, int position) {
        DataManager.getDB().removeGroupFromProject(listGroups, position);
        mAdapter.removeData(position);
        if (listGroups.size() == 0) {
            mView.showEmptyText(true);
        }
    }

    @Override
    public void addGroupResponce(ReturnResponce<ModelResult> result) {

        if (!result.isFailure()) {
            ResultCompetitors item = new ResultCompetitors();
            item.setId(String.valueOf(result.getResult()));
            item.setName(lastAddName);


            GroupData group = new GroupData();
            group.setId(item.getId());
            group.setName(item.getName());
            group.setOn("1");
            DataManager.getDB().addGroupToProject(listGroups, group);

            mAdapter.addItem(item);
            mView.showEmptyText(false);
        } else {
            mView.showToast(result.getErrorMessage());
        }

    }

    @Override
    public void renameGroupResponce(ReturnResponce<ModelResult> result) {
        if (!result.isFailure()) {
            DataManager.getDB().renameGroupProject(listGroups, position, lastRenameName);
            mAdapter.renameData(position, lastRenameName);
        } else {
            mView.showToast(result.getErrorMessage());
        }
    }

    @Override
    public void checkGroupResponce(ReturnResponce<ModelResult> result) {
        if (!result.isFailure()) {
            DataManager.getDB().checkGroupProject(listGroups, checkPosition, on);
        } else {
            mView.showToast(result.getErrorMessage());
        }
    }

    @Override
    protected void removeAction(int positon) {
        mView.showDialogList(ActivityRemoveItemsList.DIALOG_REMOVE, "", "", position);
    }
}