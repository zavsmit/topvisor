package ru.handh.topvisor.ui.settingsProject.competitor;

import android.os.Bundle;
import android.widget.ViewFlipper;

import io.realm.RealmList;
import ru.handh.topvisor.R;
import ru.handh.topvisor.data.database.project.CompetitorData;
import ru.handh.topvisor.data.database.project.GroupData;
import ru.handh.topvisor.ui.removeItemsList.ActivityRemoveItemsList;
import ru.handh.topvisor.ui.removeItemsList.AdapterRemoveItemsList;

/**
 * Created by sergey on 22.03.16.
 * экран конкурентов
 */
public class ActivityCompetitors extends ActivityRemoveItemsList {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String name = "";
        String isProject = "";
        viewFlipperMain = (ViewFlipper) findViewById(R.id.viewFlipper);
        showData();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            name = extras.getString("name");
            isProject = extras.getString("idProject");
        }

        emptyText = getString(R.string.notMakeCompetitores);
        initToolbar(name);
        presenter = new PresenterCompetitorsImpl(this, isProject, this);
    }

    @Override
    public AdapterRemoveItemsList initAdapter(RealmList<GroupData> groups,
                                              RealmList<CompetitorData> competitors,
                                              AdapterRemoveItemsList.ItemSearcherClickListener listener) {
        AdapterRemoveItemsList mAdapter = new AdapterCompetitors(competitors, listener);
        mRecyclerView.setAdapter(mAdapter);
        return mAdapter;
    }

}
