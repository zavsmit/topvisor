package ru.handh.topvisor.ui.pay;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import ru.handh.topvisor.R;
import ru.handh.topvisor.ui.base.ParentBackActivity;
import ru.handh.topvisor.ui.dialogs.DialogText;
import ru.handh.topvisor.ui.pay.payWebView.ActivityWebView;

/**
 * Created by sergey on 11.04.16.
 */
public class ActivityRefill extends ParentBackActivity implements ViewRefill {
    public PresenterRefill presenter;
    private EditText price;
    private int dialogStatus = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_refill);
        initToolbar(R.id.toolbar, getString(R.string.refillBalance));

        price = (EditText) findViewById(R.id.et_refill);

        presenter = new PresenterRefillImpl(this);
    }

    @Override
    protected void onResume() {
        super.onResume();


        if (dialogStatus != -1) {
            showResponceDialog(dialogStatus);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        dialogStatus = -1;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_button, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_button) {
            presenter.clickNext(price.getText().toString().trim());
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void openWebView(String url) {
        Intent intent = new Intent(this, ActivityWebView.class);
        intent.putExtra("url", url);
        startActivityForResult(intent, 100);
    }

    @Override
    public void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showToast(int textId) {
        Toast.makeText(this, getString(textId), Toast.LENGTH_SHORT).show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {

            presenter.listenerResult(data.getIntExtra("status", 0));

        }
    }

    @Override
    public void showResponceDialog(int type) {
        String title = "";
        String text = "";

        switch (type) {
            case ActivityWebView.PAY_OK:
                title = getString(R.string.paySuccess);
                text = "";
                price.setText("");
                break;
            case ActivityWebView.PAY_FALSE:
                title = getString(R.string.payUnsessful);
                text = getString(R.string.repeatLater);
                break;
        }

        DialogText dialogText = DialogText.newInstance(title, text);
        dialogText.show(getSupportFragmentManager(), "DialogText");
    }

    @Override
    public void changeDialogStatus(int type) {
        dialogStatus = type;
    }
}
