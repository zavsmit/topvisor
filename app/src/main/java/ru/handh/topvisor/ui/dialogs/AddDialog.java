package ru.handh.topvisor.ui.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import ru.handh.topvisor.R;
import ru.handh.topvisor.ui.removeItemsList.ActivityRemoveItemsList;

/**
 * Created by sergey on 22.03.16.
 */
public class AddDialog extends DialogFragment {

    public EditText name;
    private DialogResult mListener;

    public AddDialog() {
    }

    public static AddDialog newInstance(String title, String hint) {
        AddDialog f = new AddDialog();

        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("hint", hint);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (DialogResult) ((ActivityRemoveItemsList) activity).presenter;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_new_ticket, null);

        name = (EditText) view.findViewById(R.id.et_newDialog_ticket);

        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
        adb.setView(view);

        adb.setTitle(getArguments().getString("title"))
                .setPositiveButton(getContext().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        if (name.getText().toString().isEmpty()) {
                            Toast.makeText(getContext(), R.string.nameNotEmpty, Toast.LENGTH_SHORT).show();
                            return;
                        }

                        mListener.onDialogPositiveClick(AddDialog.this);
                    }
                })
                .setNegativeButton(getActivity().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mListener.onDialogNegativeClick(AddDialog.this);
                    }
                });

        name.setHint(getArguments().getString("hint"));

        return adb.create();
    }

}