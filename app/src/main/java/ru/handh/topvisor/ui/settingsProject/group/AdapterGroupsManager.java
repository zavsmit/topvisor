package ru.handh.topvisor.ui.settingsProject.group;

import android.view.View;
import android.widget.CompoundButton;

import io.realm.RealmList;
import ru.handh.topvisor.data.database.project.GroupData;
import ru.handh.topvisor.data.network.model.ResultCompetitors;
import ru.handh.topvisor.ui.removeItemsList.AdapterRemoveItemsList;

/**
 * Created by sergey on 22.03.16.
 * адаптер групп
 */
public class AdapterGroupsManager extends AdapterRemoveItemsList {

    public AdapterGroupsManager(RealmList<GroupData> groups, ItemSearcherClickListener listener) {
        super();

        mListener = listener;
        this.groups = groups;

        for (int i = 0; i < groups.size(); i++) {
            mData.add(new DataItem(groups.get(i).getName()));
        }
    }

    @Override
    public void onBindViewHolder(final ViewHolderTitle holder, final int position) {

        holder.switchCompat.setVisibility(View.VISIBLE);
        holder.llRename.setVisibility(View.VISIBLE);

        int ap = holder.getAdapterPosition();

        if (groups.get(ap).getOn().equals("0")) {
            holder.switchCompat.setChecked(false);
        } else {
            holder.switchCompat.setChecked(true);
        }

        onBindClicks(holder, position);
        holder.llTrash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.removeItem(holder.getAdapterPosition(), groups.get(holder.getAdapterPosition()).getId());
            }
        });
        holder.llRename.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.renameItem(holder.getAdapterPosition(), groups.get(holder.getAdapterPosition()).getName(),
                        groups.get(holder.getAdapterPosition()).getId());
            }
        });


        holder.switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mListener.changeSwitch(isChecked, groups.get(holder.getAdapterPosition()).getId(), holder.getAdapterPosition());
            }
        });
    }

    public void removeData(int position) {
        selectedItems.delete(position);
        mData.remove(position);
        notifyItemRemoved(position);
    }

    public void addItem(ResultCompetitors item) {
        mData.add(new DataItem(item.getName()));
        notifyItemInserted(mData.size() - 1);
    }
}
