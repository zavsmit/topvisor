package ru.handh.topvisor.ui.login;

/**
 * Created by sergey on 08.02.16.
 * интерфейс экрана авторизации
 */
public interface LoginView {

    void showProgress(String text);

    void showProgress(int idText);

    void showData();

    void showError();

    void showDialogLogin(int dialogType);

    void showEmptyLogin();

    void showCorrectLogin();

    void showEmptyPass();

    void showRegistrationView();

    void showLoginView();

    void startMainActivity();

    void showToast(String text);

    void showToast(int textId);

    void hideRuSocial();

    void addEnLogo();

    void runUlogin(String provider);

}
