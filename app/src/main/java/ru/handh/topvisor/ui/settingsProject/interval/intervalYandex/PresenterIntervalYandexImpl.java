package ru.handh.topvisor.ui.settingsProject.interval.intervalYandex;

import java.util.List;

import ru.handh.topvisor.data.DataManager;
import ru.handh.topvisor.data.network.model.ModelResult;
import ru.handh.topvisor.data.network.model.ReturnResponce;
import ru.handh.topvisor.ui.settingsProject.interval.ActivityIntervalInspect;
import ru.handh.topvisor.ui.settingsProject.interval.IntervalReqResult;

/**
 * Created by sergey on 28.03.16.
 * экрана модлеи яндекс
 */
public class PresenterIntervalYandexImpl implements PresenterIntervalYandex,
        AdapterYandexInsert.ItemGroupClickListener, IntervalReqResult {

    private ViewIntervalYandex mView;
    private String idProject;
    private int selected;

    public PresenterIntervalYandexImpl(ViewIntervalYandex view,
                                       List<String> list,
                                       int selected, String idProject) {
        mView = view;
        this.idProject = idProject;
        this.selected = selected;
        mView.initAdapter(list, this, selected);

    }


    @Override
    public void clickItem(int idSelected) {
        DataManager.getSettingsRest().reqChangeInterval(ActivityIntervalInspect.YANDEX, idProject, "", String.valueOf(idSelected), this);
        DataManager.getDB().changeYandexIntervalInspection(idProject, String.valueOf(idSelected));
        mView.selectGroup(idSelected);
    }

    @Override
    public void changeIntervalResponce(ReturnResponce<ModelResult> result) {
    }

    @Override
    public void close() {
        mView.selectGroup(selected);
    }
}