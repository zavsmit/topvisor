package ru.handh.topvisor.ui.project.selectGroup;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.network.model.currentProject.Group;
import ru.handh.topvisor.ui.base.ParentBackActivity;

/**
 * Created by sergey on 25.02.16.
 * выбор групп запросов
 */
public class ActivitySelectGroup extends ParentBackActivity implements ViewGroups {

    private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_group_list);

        initToolbar(R.id.toolbarGroups, "Группы");

        mRecyclerView = (RecyclerView) findViewById(R.id.recicleView_Groups);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        String idProject = "";

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            idProject = extras.getString("idProject");
        }
        PresenterGroup presenter = new PresenterGroupImpl(this, idProject);// надо
    }

    @Override
    public void initAdapter(List<Group> groups, AdapterGroups.ItemGroupClickListener listener) {
        RecyclerView.Adapter mAdapter = new AdapterGroups(groups, listener);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void selectGroup() {
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
    }
}

