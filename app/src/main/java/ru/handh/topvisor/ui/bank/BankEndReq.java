package ru.handh.topvisor.ui.bank;

import java.util.List;

import ru.handh.topvisor.data.network.model.ModelBankPrice;
import ru.handh.topvisor.data.network.model.ModelItemBank;
import ru.handh.topvisor.data.network.model.ModelList;
import ru.handh.topvisor.data.network.model.ReturnResponce;

/**
 * Created by sergey on 07.04.16.
 * интерфейсы результата ответа на экрана банка
 */
public interface BankEndReq {
    void listBanksResponce(ReturnResponce<ModelList<ModelItemBank>> result);

    void priceBanksResponce(ReturnResponce<List<ModelBankPrice>> result);

}
