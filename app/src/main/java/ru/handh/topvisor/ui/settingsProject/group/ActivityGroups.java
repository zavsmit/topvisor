package ru.handh.topvisor.ui.settingsProject.group;

import android.os.Bundle;
import android.widget.ViewFlipper;

import io.realm.RealmList;
import ru.handh.topvisor.R;
import ru.handh.topvisor.data.database.project.CompetitorData;
import ru.handh.topvisor.data.database.project.GroupData;
import ru.handh.topvisor.ui.removeItemsList.ActivityRemoveItemsList;
import ru.handh.topvisor.ui.removeItemsList.AdapterRemoveItemsList;

/**
 * Created by sergey on 22.03.16.
 */
public class ActivityGroups extends ActivityRemoveItemsList {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String name = "";
        String idProject = "";
        viewFlipperMain = (ViewFlipper) findViewById(R.id.viewFlipper);
        showData();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            name = extras.getString("name");
            idProject = extras.getString("idProject");
        }

        emptyText = getString(R.string.notMakeGroups);
        initToolbar(name);
        presenter = new PresentGroupsManagerImpl(this, idProject, this);
    }

    @Override
    public void onResume(){
        super.onResume();
        presenter.onResume();
    }

    @Override
    public AdapterRemoveItemsList initAdapter(RealmList<GroupData> groups,
                                              RealmList<CompetitorData> competitors,
                                              AdapterRemoveItemsList.ItemSearcherClickListener listener) {
        AdapterRemoveItemsList mAdapter = new AdapterGroupsManager(groups, listener);
        mRecyclerView.setAdapter(mAdapter);
        return mAdapter;
    }
}
