package ru.handh.topvisor.ui.settingsProject.searchSystems;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.realm.RealmList;
import ru.handh.topvisor.R;
import ru.handh.topvisor.data.database.project.RegionData;
import ru.handh.topvisor.data.database.project.SearchData;
import ru.handh.topvisor.data.network.model.ModelResultPS;

/**
 * Created by sergey on 17.03.16.
 * адаптер экрана настройки поиковых систем
 */
public class AdapterSearchSystems extends RecyclerSwipeAdapter<RecyclerView.ViewHolder> {
    public static final String SEARCHER_YANDEX = "0";
    public static final String SEARCHER_GOOGLE = "1";
    public static final String SEARCHER_GO_MAIL = "2";
    public static final String SEARCHER_SPUTNIK = "3";
    public static final String SEARCHER_YOUTUBE = "4";
    public static final String SEARCHER_BING = "5";
    public static final String SEARCHER_YAHOO = "6";
    public static final String SEARCHER_YANDEX_COM = "20";
    public static final String SEARCHER_YANDEX_COM_TR = "21";
    private static final int TYPE_ADD = 0;
    private static final int TYPE_TITLE = 1;
    private static final int TYPE_REGION = 2;
    private static final int TYPE_DIVIDER = 3;
    private List<RegionsData> regionsList;
    private ItemSearcherClickListener mListener;
    private SparseBooleanArray selectedItems;

    private Map<String, String> depthMap = new HashMap<>();
    private Map<String, String> device = new HashMap<>();

    private Map<String, ArrayList<String>> allRegionsForChange = new HashMap<>();
    private Map<String, ArrayList<Integer>> mapPositions = new HashMap<>();
    private Map<String, ArrayList<String>> regionsBeforeChange = new HashMap<>();

    public AdapterSearchSystems(RealmList<SearchData> searchers, ItemSearcherClickListener listener, Context context) {
        mListener = listener;
        regionsList = new ArrayList<>();

        for (int i = 1; i < 11; i++) {

            int x = i;
            String key = String.valueOf(x);

            x = x * 100;
            String value = String.valueOf(x);

            depthMap.put(key, value);
        }

        device.put("0", context.getString(R.string.pc));
        device.put("1", context.getString(R.string.tablet));
        device.put("2", context.getString(R.string.mobile));

        selectedItems = new SparseBooleanArray();
        for (int i = 0; i < searchers.size(); i++) {

            boolean isSelected = false;
            if (searchers.get(i).getEnabled().equals("1")) {
                isSelected = true;
            }

            regionsList.add(new RegionsData(TYPE_TITLE,
                    searchers.get(i).getName(),
                    isSelected,
                    searchers.get(i).getId(),
                    searchers.get(i).getSearcher()));

            regionsList.add(new RegionsData(TYPE_ADD, searchers.get(i).getSearcher(), searchers.get(i).getId()));

            RealmList<RegionData> regions = searchers.get(i).getRegions();


            ArrayList<String> kldRegionList = new ArrayList<>();
            for (int j = 0; j < regions.size(); j++) {

                String kld = regions.get(j).getKey() + ":" + regions.get(j).getDepth() + ":"
                        + regions.get(j).getLang() + ":" + regions.get(j).getDevice();

                regionsList.add(new RegionsData(TYPE_REGION,
                        regions.get(j),
                        searchers.get(i).getSearcher(),
                        searchers.get(i).getId(),
                        kld));

                kldRegionList.add(kld);

            }

            allRegionsForChange.put(searchers.get(i).getId(), kldRegionList);


            if (i != searchers.size() - 1) {
                regionsList.add(new RegionsData(TYPE_DIVIDER));
            }


        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case TYPE_TITLE:
                return new ViewHolderTitle(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_searcher_title, parent, false));
            case TYPE_ADD:
                return new ViewHolderAdd(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_searcher_add, parent, false));
            case TYPE_REGION:
                return new ViewHolderRegion(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_searcher_region, parent, false));
            case TYPE_DIVIDER:
                return new ViewHolderDivider(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_divider, parent, false));
        }

        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int positions) {

        switch (holder.getItemViewType()) {

            case TYPE_TITLE:

                final ViewHolderTitle mHoldertitle = ((ViewHolderTitle) holder);

                mHoldertitle.title.setText(regionsList.get(holder.getAdapterPosition()).title);


                mHoldertitle.switchCompat.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mHoldertitle.switchCompat.setChecked(mHoldertitle.switchCompat.isChecked());
                        mListener.changeSwitch(mHoldertitle.switchCompat.isChecked(), regionsList.get(holder.getAdapterPosition()).idSearcher, holder.getAdapterPosition());
                    }
                });

                mHoldertitle.switchCompat.setChecked(regionsList.get(holder.getAdapterPosition()).isChecked);

                int imageRes = R.drawable.yandex_search_vector;

                switch (regionsList.get(holder.getAdapterPosition()).searcherType) {
                    case SEARCHER_YANDEX:
                        imageRes = R.drawable.yandex_search_vector;
                        break;
                    case SEARCHER_GOOGLE:
                        imageRes = R.drawable.google_search_vector;
                        break;
                    case SEARCHER_GO_MAIL:
                        imageRes = R.drawable.mail_search_vector;
                        break;
                    case SEARCHER_SPUTNIK:
                        imageRes = R.drawable.sputnik_search_vector;
                        break;
                    case SEARCHER_YOUTUBE:
                        imageRes = R.drawable.youtube_search_vector;
                        break;
                    case SEARCHER_BING:
                        imageRes = R.drawable.bing_search_vector;
                        break;
                    case SEARCHER_YAHOO:
                        imageRes = R.drawable.yahoo_search_vector;
                        break;
                    case SEARCHER_YANDEX_COM:
                        imageRes = R.drawable.yandex_search_vector;
                        break;
                    case SEARCHER_YANDEX_COM_TR:
                        imageRes = R.drawable.yandex_search_vector;
                        break;
                }

                mHoldertitle.imageView.setImageResource(imageRes);

                mHoldertitle.llTrash.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mListener.removeSP(holder.getAdapterPosition(),
                                regionsList.get(holder.getAdapterPosition()).idSearcher,
                                regionsList.get(holder.getAdapterPosition()).searcherType);
                    }
                });

                break;

            case TYPE_ADD:

                ((ViewHolderAdd) holder).rl.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mListener.clickAddRegion(regionsList.get(holder.getAdapterPosition()).idSearcher, regionsList.get(holder.getAdapterPosition()).searcherType);
                    }
                });

                break;

            case TYPE_REGION:

                final ViewHolderRegion mHolderRegion = ((ViewHolderRegion) holder);

                RegionData region = regionsList.get(holder.getAdapterPosition()).region;

                mHolderRegion.name.setText(region.getName());

                String lang = region.getLang();
                String depth = depthMap.get(region.getDepth());
                String deviceString = device.get(region.getDevice());

                switch (regionsList.get(holder.getAdapterPosition()).searcherType) {
                    case SEARCHER_YANDEX:
                        lang = "";
                        deviceString = "";
                        break;
                    case SEARCHER_GOOGLE:
                        break;
                    case SEARCHER_GO_MAIL:
                        lang = "";
                        depth = "";
                        deviceString = "";
                        break;
                    case SEARCHER_SPUTNIK:
                        lang = "";
                        depth = "";
                        deviceString = "";
                        break;
                    case SEARCHER_YOUTUBE:
                        lang = "";
                        depth = "";
                        deviceString = "";
                        break;
                    case SEARCHER_BING:
                        depth = "";
                        deviceString = "";
                        break;
                    case SEARCHER_YAHOO:
                        depth = "";
                        deviceString = "";
                        break;
                    case SEARCHER_YANDEX_COM:
                        lang = "";
                        deviceString = "";
                        break;
                    case SEARCHER_YANDEX_COM_TR:
                        lang = "";
                        deviceString = "";
                        break;
                }


                String discription = lang + " " + depth + " " + deviceString;

                mHolderRegion.description.setText(discription.trim());

                if (region.getCountryCode().isEmpty()) {
                    mHolderRegion.image.setVisibility(View.INVISIBLE);
                } else {
                    mHolderRegion.image.setVisibility(View.VISIBLE);
                    String urlImage = "https://topvisor.ru/images/common/flags/" + region.getCountryCode() + ".png";
                    Picasso.with(mHolderRegion.image.getContext()).load(urlImage).into(mHolderRegion.image);
                }

                mHolderRegion.rl.setSelected(selectedItems.get(holder.getAdapterPosition()));


                mHolderRegion.rl.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mListener.clickItem(regionsList.get(holder.getAdapterPosition()).region.getId(), holder.getAdapterPosition(), regionsList.get(holder.getAdapterPosition()).idSearcher);
                    }
                });

                mHolderRegion.rl.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        mListener.longClick(holder.getAdapterPosition());
                        return true;
                    }
                });

                mHolderRegion.llRemove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        mHolderRegion.swipeLayout.close();
                        ArrayList<Integer> positions = new ArrayList<>();

                        positions.add(holder.getAdapterPosition());

                        makeSendItemList(positions);

                        for (Map.Entry<String, ArrayList<String>> entry : regionsBeforeChange.entrySet()) {
                            mListener.removeItem(positions, entry.getKey(), entry.getValue());
                        }

                    }
                });
                break;
        }
    }


    @Override
    public int getItemCount() {
        return regionsList.size();
    }


    @Override
    public int getItemViewType(int position) {

        switch (regionsList.get(position).type) {
            case TYPE_TITLE:
                return TYPE_TITLE;
            case TYPE_ADD:
                return TYPE_ADD;
            case TYPE_REGION:
                return TYPE_REGION;
            case TYPE_DIVIDER:
                return TYPE_DIVIDER;
            default:
                return TYPE_DIVIDER;
        }
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe_searcher;
    }

    public void toggleSelection(int pos) {
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
        } else {
            selectedItems.put(pos, true);
        }
        notifyItemChanged(pos);
    }

    public void clearSelections() {
        selectedItems.clear();
        notifyDataSetChanged();
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public Map<String, ArrayList<Integer>> getSelectedItems() {
        ArrayList<Integer> removePositions = new ArrayList<>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            removePositions.add(selectedItems.keyAt(i));
        }


        Collections.reverse(removePositions);

        makeSendItemList(removePositions);


        return mapPositions;
    }

    private void makeSendItemList(List<Integer> removePosition) {

        regionsBeforeChange.clear();
        mapPositions.clear();

        ArrayList<Integer> listPositions = new ArrayList<>();

        for (int i = 0; i < removePosition.size(); i++) {

            listPositions.add(removePosition.get(i));


            String idSearcher = regionsList.get(removePosition.get(i)).idSearcher;
            String kld = regionsList.get(removePosition.get(i)).kld;

            ArrayList<String> listRegions = allRegionsForChange.get(idSearcher);


            for (int f = 0; f < listRegions.size(); f++) {

                if (kld.equals(listRegions.get(f))) {
                    listRegions.remove(f);
                    allRegionsForChange.put(idSearcher, listRegions);
                    regionsBeforeChange.put(idSearcher, listRegions);
                    break;
                }
            }


            if (listPositions.size() != 1 && !regionsList.get(listPositions.get(listPositions.size() - 2)).idSearcher.equals(idSearcher)) {
                listPositions.remove(listPositions.size() - 1);
                listPositions = new ArrayList<>();
                listPositions.add(removePosition.get(i));
            }

            mapPositions.put(idSearcher, listPositions);

        }
    }

    public Map<String, ArrayList<String>> getAllRegionsChanged() {
        return regionsBeforeChange;
    }

    public void removeItemInterface(int position) {
        selectedItems.delete(position);
        regionsList.remove(position);
        notifyItemRemoved(position);
    }

    public void removePS(int position) {

        regionsList.remove(position);
        notifyItemRemoved(position);

        List<Integer> list = new ArrayList<>();

        for (int i = position; i < regionsList.size(); i++) {
            if (regionsList.get(i).type == TYPE_DIVIDER) {
                break;
            }
            list.add(i);
        }

        for (int i = 0; i < list.size(); i++) {
            regionsList.remove(position);
            notifyItemRemoved(position);
        }

        regionsList.remove(position - 1);
        notifyItemRemoved(position - 1);

        notifyDataSetChanged();
    }

    public void checkRegion(boolean isCheck, int position) {
        regionsList.get(position).isChecked = isCheck;
        notifyItemChanged(position);
    }

    public void addPsItem(ModelResultPS.ResultPS resultPS) {


        boolean isSelected = false;
        if (resultPS.getEnabled().equals("1")) {
            isSelected = true;
        }

        regionsList.add(new RegionsData(TYPE_DIVIDER));

        regionsList.add(new RegionsData(TYPE_TITLE,
                resultPS.getName(),
                isSelected,
                resultPS.getId(),
                resultPS.getSearcher()));

        if (!resultPS.getSearcher().equals(SEARCHER_YAHOO)) {
            regionsList.add(new RegionsData(TYPE_ADD, resultPS.getSearcher(), resultPS.getId()));
        }


        notifyDataSetChanged();
    }

    public interface ItemSearcherClickListener {
        void clickItem(String idRegion, int position, String idSearcher);

        void clickAddRegion(String idSearcher, String searcher);

        void changeSwitch(boolean isEnable, String idSearcher, int position);

        void longClick(int idRegion);

        void removeItem(ArrayList<Integer> positions, String idSearcher, ArrayList<String> listRegions);

        void removeSP(int position, String idSearcher, String SearcherType);
    }

    public class ViewHolderTitle extends RecyclerView.ViewHolder {
        public TextView title;
        public SwitchCompat switchCompat;
        public ImageView imageView;
        public LinearLayout llTrash;

        public ViewHolderTitle(View v) {
            super(v);
            title = (TextView) itemView.findViewById(R.id.tv_itemSearcherTitle);
            switchCompat = (SwitchCompat) itemView.findViewById(R.id.switch_itemSearcherTitle);
            imageView = (ImageView) itemView.findViewById(R.id.iv_itemSearcherTitle);
            llTrash = (LinearLayout) itemView.findViewById(R.id.ll_itemTitleSearcher_trash);
        }
    }

    public class ViewHolderAdd extends RecyclerView.ViewHolder {
        public RelativeLayout rl;

        public ViewHolderAdd(View v) {
            super(v);
            rl = (RelativeLayout) itemView.findViewById(R.id.rl_searcher_add);
        }
    }

    public class ViewHolderRegion extends RecyclerView.ViewHolder {
        public SwipeLayout swipeLayout;
        public TextView name, description;
        public ImageView image;
        public RelativeLayout rl;
        public LinearLayout llRemove;

        public ViewHolderRegion(View v) {
            super(v);
            name = (TextView) itemView.findViewById(R.id.tv_searcherRegion_name);
            description = (TextView) itemView.findViewById(R.id.tv_searcherRegion_description);
            image = (ImageView) itemView.findViewById(R.id.iv_searcherRegion);
            rl = (RelativeLayout) itemView.findViewById(R.id.ll_searcherRegion);
            llRemove = (LinearLayout) itemView.findViewById(R.id.ll_itemSearcher_trash);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe_searcher);
        }
    }

    public class ViewHolderDivider extends RecyclerView.ViewHolder {
        public ViewHolderDivider(View v) {
            super(v);
        }
    }

    private class RegionsData {
        int type;
        String title;
        RegionData region;
        boolean isChecked;
        String searcherType, idSearcher;

        String kld;

        RegionsData(int type) {
            this.type = type;
            title = "";
        }

        RegionsData(int type, String searcherType, String idSearcher) {
            this.type = type;
            this.idSearcher = idSearcher;
            this.searcherType = searcherType;
            title = "";
        }

        RegionsData(int type, String title, boolean isChecked, String idSearcher, String searcherType) {
            this.type = type;
            this.title = title;
            this.isChecked = isChecked;
            this.idSearcher = idSearcher;
            this.searcherType = searcherType;

        }

        RegionsData(int type, RegionData region, String searcherType, String idSearcher, String kld) {
            this.type = type;
            this.region = region;
            this.idSearcher = idSearcher;
            this.searcherType = searcherType;
            this.kld = kld;
        }
    }
}
