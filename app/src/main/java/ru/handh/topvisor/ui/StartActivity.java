package ru.handh.topvisor.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import ru.handh.topvisor.data.DataManager;
import ru.handh.topvisor.ui.login.LoginViewActivity;
import ru.handh.topvisor.ui.main.ActivityMainView;

/**
 * Created by sergey on 30.01.16.
 * стартовое активитит с выбором запускаемого экрана
 */
public class StartActivity extends AppCompatActivity {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        openStartActivity();
        finish();
    }

    public void openStartActivity() {
        if (DataManager.getPref().getTokenApp().isEmpty()) {
            Intent intent = new Intent(this, LoginViewActivity.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, ActivityMainView.class);
            startActivity(intent);
        }
    }

}
