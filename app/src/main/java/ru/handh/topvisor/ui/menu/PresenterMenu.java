package ru.handh.topvisor.ui.menu;

/**
 * Created by sergey on 09.04.16.
 */
public interface PresenterMenu {

    void loadTicketsMenu();

    void loadUserdata();

}
