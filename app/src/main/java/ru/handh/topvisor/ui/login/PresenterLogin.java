package ru.handh.topvisor.ui.login;

import android.content.Intent;
import android.support.v4.app.DialogFragment;

/**
 * Created by sergey on 08.02.16.
 * интерфейс презентера экрана авторизации
 */
public interface PresenterLogin {

    void onEnterButtonClick(String login, String password);

    void onForgetPassButtonClick();

    void onReqLoginButtonClick();

    void openFromLink(String intentParem);

    void activityResult(int requestCode, int resultCode, Intent data);

    void onStop();

    void positivDialog(DialogFragment dialog, String email);

    void negativDialog(DialogFragment dialog);

    void clickSocial(int idsocial);
}
