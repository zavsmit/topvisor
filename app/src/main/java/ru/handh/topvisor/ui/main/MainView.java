package ru.handh.topvisor.ui.main;

import java.util.ArrayList;

import ru.handh.topvisor.data.network.model.currentProject.Row;

/**
 * Created by sergey on 10.02.16.
 * интерфейс для экрана проектов
 */
public interface MainView {
    void showDialogMain(int dialogType);

    void showDialogMain(int dialogType, String name);

    void showDialogMain(int dialogType, String name, int position);

    void showRefresh(boolean isShow);

    void showData();

    void showProgress();

    void showError();

    void showEmpty(boolean isShow);

    void showToast(int idString);

    void showToast(String text);

    void initAdapter(ArrayList<Row> myDataset, PresenterMainImpl presenterImpl);

//    void addDataToAdapter(List<Row> list, boolean isNew);

    void setTextToHeader(int count);

    void setDataToFilter(String name, int idRes);

    void showDescriptionInDialog(double price, double xmlPrice);

    void removeItemList(int position);

    void renameItemList(int position, String name);

    void openActivity(Row row);

    void openSearchActivity(ArrayList<Row> myDataset);

    void scrollToPosition(int visiblePosition);
}
