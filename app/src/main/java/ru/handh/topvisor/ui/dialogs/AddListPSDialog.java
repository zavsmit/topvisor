package ru.handh.topvisor.ui.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.model.SearcherItemDialog;
import ru.handh.topvisor.ui.bank.ActivityBank;
import ru.handh.topvisor.ui.settingsProject.searchSystems.ActivitySearchSystems;

/**
 * Created by sergey on 24.03.16.
 */
public class AddListPSDialog extends DialogFragment implements AdapterList.ItemDialogClickListener {

    public int mPosition;
    private DialogResult mListener;

    public AddListPSDialog() {
    }

    public static AddListPSDialog newInstance(ArrayList<SearcherItemDialog> list) {
        AddListPSDialog f = new AddListPSDialog();

        Bundle args = new Bundle();
        args.putParcelableArrayList("list", list);
        f.setArguments(args);

        return f;
    }

    public static AddListPSDialog newInstance(ArrayList<SearcherItemDialog> list, String title) {
        AddListPSDialog f = new AddListPSDialog();

        Bundle args = new Bundle();
        args.putParcelableArrayList("list", list);
        args.putString("title", title);
        args.putInt("type", 1);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {

            if(getArguments().getInt("type") == 1){
                mListener = (DialogResult) ((ActivityBank) activity).presenter;
            } else {
                mListener = (DialogResult) ((ActivitySearchSystems) activity).presenter;
            }

        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_list, null);


        List<SearcherItemDialog> list = getArguments().getParcelableArrayList("list");


        RecyclerView mRecyclerView = (RecyclerView) view.findViewById(R.id.recicleView_Dialog);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));


        AdapterList mAdapter = new AdapterList(list, this);
        mRecyclerView.setAdapter(mAdapter);


        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
        adb.setView(view);


        String title = getActivity().getString(R.string.selectSearchSystem);
        if (list.size() == 0) {
            title = getActivity().getString(R.string.notNewSearchSystem);
        }

        if (getArguments().getString("title") != null) {
            title = getArguments().getString("title");
        }


        adb.setTitle(title)
                .setNegativeButton(getContext().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mListener.onDialogNegativeClick(AddListPSDialog.this);
                    }
                });

        return adb.create();
    }

    @Override
    public void clickItem(int idSelected) {
        mPosition = idSelected;
        mListener.onDialogPositiveClick(AddListPSDialog.this);
    }
}

