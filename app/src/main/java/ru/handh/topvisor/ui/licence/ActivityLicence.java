package ru.handh.topvisor.ui.licence;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import ru.handh.topvisor.R;
import ru.handh.topvisor.ui.menu.ParentLeftMenu;

/**
 * Created by sergey on 23.03.16.
 * экран лицензионного соглашения
 */
public class ActivityLicence extends ParentLeftMenu {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        TextView text = (TextView) findViewById(R.id.tv_licence);

        text.setText(Html.fromHtml(getString(R.string.liceseText)));
    }

    @Override
    protected void initView() {
        isMainScreen = false;
        title = getString(R.string.agreement);
        LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout v = (LinearLayout) vi.inflate(R.layout.activity_licence, null);
        drawer.addView(v, 0, new ViewGroup.LayoutParams(DrawerLayout.LayoutParams.MATCH_PARENT, DrawerLayout.LayoutParams.MATCH_PARENT));

    }

}
