package ru.handh.topvisor.ui.pay;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.DataManager;
import ru.handh.topvisor.utils.Constants;

/**
 * Created by sergey on 11.04.16.
 * логика экрана оплаты
 */
public class PresenterRefillImpl implements PresenterRefill {

    private ViewRefill mView;

    public PresenterRefillImpl(ViewRefill view) {
        mView = view;
    }

    @Override
    public void clickNext(String price) {

        if (price.isEmpty()) {
            mView.showToast(R.string.writeSum);
            return;
        }

        double priceD = Double.valueOf(price);


        String serger = DataManager.getPref().getServerShort();

        String url;

        if (serger.equals(Constants.RU)) {
            if (priceD < 100) {
                mView.showToast("Сумма пополенния болжна быть более 100 рублей");
                return;
            }
            url = "https://topvisor.ru/account/bank/payment/?system=card&sum=" + price + "&valuta=rub&phone=7&application=1";
        } else {
            if (priceD < 5) {
                mView.showToast("summ more 5$");
                return;
            }
            url = "https://topvisor.com/account/bank/payment/?system=co&sum=" + price + "valuta=USD&phone=undefined";
        }

        mView.openWebView(url);
    }

    public void listenerResult(int status) {
        mView.changeDialogStatus(status);
    }
}
