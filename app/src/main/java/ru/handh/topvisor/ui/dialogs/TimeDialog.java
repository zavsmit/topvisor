package ru.handh.topvisor.ui.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;

import ru.handh.topvisor.ui.settingsProject.interval.intervalDate.ActivitySettingDate;

/**
 * Created by sergey on 15.03.16.
 */
public class TimeDialog extends DialogFragment {

    private TimePickerDialog.OnTimeSetListener mListener;

    public TimeDialog() {
    }

    public static TimeDialog newInstance(String house) {
        TimeDialog f = new TimeDialog();

        Bundle args = new Bundle();
        args.putString("hour", house);
        f.setArguments(args);

        return f;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (TimePickerDialog.OnTimeSetListener) ((ActivitySettingDate) activity).presenter;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        int hour = 0;
        if (getArguments().getString("hour") != null && !getArguments().getString("hour").isEmpty()) {
            hour = Integer.valueOf(getArguments().getString("hour"));
        }
        int minute = 0;

        // Create a new instance of TimePickerDialog and return it
        return new TimePickerDialog(getActivity(), mListener, hour, minute,
                DateFormat.is24HourFormat(getActivity()));
    }
}