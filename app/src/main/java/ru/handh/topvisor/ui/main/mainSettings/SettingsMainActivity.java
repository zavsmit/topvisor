package ru.handh.topvisor.ui.main.mainSettings;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.DataManager;
import ru.handh.topvisor.data.network.model.Labels;
import ru.handh.topvisor.data.network.model.Projects;
import ru.handh.topvisor.ui.base.ParentBackActivity;

/**
 * Created by sergey on 11.02.16.
 * активити фильтров проектов
 */
public class SettingsMainActivity extends ParentBackActivity {

    public static List<Projects> listFolder;
    public static List<Labels> listLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_main);

        initToolbar(R.id.toolbar);

        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recicleView_mainSettings);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));


        listFolder = new ArrayList<>();
        listFolder.add(new Projects(0, "my", getString(R.string.myProject)));
        listFolder.add(new Projects(1, "auto", getString(R.string.autoInspect)));
        listFolder.add(new Projects(2, "manual", getString(R.string.inspectOnDemand)));
        listFolder.add(new Projects(3, "guest", getString(R.string.guestProject)));
        listFolder.add(new Projects(4, "all", getString(R.string.myAndGuestProject)));
        listFolder.add(new Projects(5, "archive", getString(R.string.archiveProjects)));

        listFolder.get(DataManager.getPref().getMainSettingsFolder().id).isChecked = true;



        listLabel = new ArrayList<>();
        listLabel.add(new Labels(0, getString(R.string.allMarks), R.drawable.label_all_vector));
        listLabel.add(new Labels(1, getString(R.string.withoutMarks), R.drawable.label_without_vector));
        listLabel.add(new Labels(2, getString(R.string.red), R.drawable.label_red_vector));
        listLabel.add(new Labels(3, getString(R.string.orange), R.drawable.label_orange_vector));
        listLabel.add(new Labels(4, getString(R.string.yellow), R.drawable.label_yellow_vector));
        listLabel.add(new Labels(5, getString(R.string.blue), R.drawable.label_blue_vector));
        listLabel.add(new Labels(6, getString(R.string.purpure), R.drawable.label_purple_vector));
        listLabel.add(new Labels(7, getString(R.string.green), R.drawable.label_green_vector));
        listLabel.add(new Labels(8, getString(R.string.mangeta), R.drawable.label_magenta_vector));
        listLabel.add(new Labels(9, getString(R.string.blueDarke), R.drawable.label_dark_blue_vector));
        listLabel.add(new Labels(10, getString(R.string.greenDark), R.drawable.label_dark_green_vector));

        listLabel.get(DataManager.getPref().getMainSettingsTab().id).isChecked = true;

        RecyclerView.Adapter mAdapter = new SettingsMainAdapter(listFolder, listLabel);
        mRecyclerView.setAdapter(mAdapter);
    }

}
