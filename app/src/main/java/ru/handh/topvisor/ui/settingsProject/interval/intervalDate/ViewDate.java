package ru.handh.topvisor.ui.settingsProject.interval.intervalDate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sergey on 14.03.16.
 */
public interface ViewDate {
    void initAdapter(List<String> listSelected, AdapterDate.ItemDateClickListener listener, String hour, boolean isSelect);
    void showTimeDialog(String house);
    void changeAdapteHour(String hour);
    void customStop(String hour, ArrayList<String> listDate);
    void finishActivtiy();
}
