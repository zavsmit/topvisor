package ru.handh.topvisor.ui.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.model.InspectDialogData;
import ru.handh.topvisor.ui.project.ActivityViewProject;
import ru.handh.topvisor.utils.Utils;

/**
 * Created by sergey on 25.02.16.
 * диалог проверки позиции проекта
 */
public class InspectPositionProjectDialog extends DialogFragment implements ActivityViewProject.SendDataToDialog {

    private DialogResult mListener;

    private TextView filterPrice;
    private TextView allPrice;
    private RadioButton filterRadio, allRadio;
    private ProgressBar progressAll, progressFilter;
    public boolean isFilter = true;

    public InspectPositionProjectDialog() {
    }

    public static InspectPositionProjectDialog newInstance(InspectDialogData data) {
        InspectPositionProjectDialog f = new InspectPositionProjectDialog();

        Bundle args = new Bundle();
        args.putParcelable("data", data);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (DialogResult) ((ActivityViewProject) activity).presenter;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_inspect_position, null);


        InspectDialogData data = getArguments().getParcelable("data");


        TextView ps = (TextView) view.findViewById(R.id.tv_dialogInspect_ps);
        TextView region = (TextView) view.findViewById(R.id.tv_dialogInspect_region);
        TextView group = (TextView) view.findViewById(R.id.tv_dialogInspect_newGroup);
        filterPrice = (TextView) view.findViewById(R.id.tv_dialogInspect_filterPrice);
        allPrice = (TextView) view.findViewById(R.id.tv_dialogInspect_allPrice);
        RelativeLayout filter = (RelativeLayout) view.findViewById(R.id.rl_dialogInspect_filter);
        RelativeLayout all = (RelativeLayout) view.findViewById(R.id.rl_dialogInspect_all);
        filterRadio = (RadioButton) view.findViewById(R.id.rb_dialogInspect_filter);
        allRadio = (RadioButton) view.findViewById(R.id.rb_dialogInspect_all);
        progressAll = (ProgressBar) view.findViewById(R.id.progress_dialogInspect_allPrice);
        progressFilter = (ProgressBar) view.findViewById(R.id.progress_dialogInspect_filterPrice);


        progressAll.setVisibility(View.VISIBLE);
        progressFilter.setVisibility(View.VISIBLE);
        allPrice.setVisibility(View.GONE);
        filterPrice.setVisibility(View.GONE);


        SpannableStringBuilder spb = new SpannableStringBuilder(getActivity().getString(R.string.ps) + " " + data
                .getPs());
        final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
        spb.setSpan(bss, spb.length() - 1 - data.getPs().length(), spb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        ps.setText(spb);

        SpannableStringBuilder regionb = new SpannableStringBuilder(getActivity().getString(R.string.regions) + " " +
                data
                .getRegion());
        regionb.setSpan(bss, regionb.length() - 1 - data.getRegion().length(), regionb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        region.setText(regionb);

        SpannableStringBuilder groupb = new SpannableStringBuilder(getActivity().getString(R.string.group) + " " + data
                .getGroup());
        groupb.setSpan(bss, groupb.length() - 1 - data.getGroup().length(), groupb.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        group.setText(groupb);


        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterRadio.setChecked(true);
                filterRadio.setTextColor(Utils.getCustomColor(getActivity(), R.color.colorButton));
                filterPrice.setTextColor(Utils.getCustomColor(getActivity(), R.color.colorButton));

                allRadio.setChecked(false);
                allRadio.setTextColor(Utils.getCustomColor(getActivity(), R.color.textGray));
                allPrice.setTextColor(Utils.getCustomColor(getActivity(), R.color.textGray));

                isFilter = true;
            }
        });

        all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterRadio.setChecked(false);
                filterRadio.setTextColor(Utils.getCustomColor(getActivity(), R.color.textGray));
                filterPrice.setTextColor(Utils.getCustomColor(getActivity(), R.color.textGray));

                allRadio.setChecked(true);
                allRadio.setTextColor(Utils.getCustomColor(getActivity(), R.color.colorButton));
                allPrice.setTextColor(Utils.getCustomColor(getActivity(), R.color.colorButton));
                isFilter = false;
            }
        });

        filterRadio.setChecked(true);
        filterRadio.setTextColor(Utils.getCustomColor(getActivity(), R.color.colorButton));
        filterPrice.setTextColor(Utils.getCustomColor(getActivity(), R.color.colorButton));

        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
        adb.setView(view);


        adb.setTitle(getActivity().getString(R.string.inspectPositionProject) + " \"" + data.getName() + "\"")
                .setPositiveButton(getActivity().getString(R.string.inspect1), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mListener.onDialogPositiveClick(InspectPositionProjectDialog.this);
                    }
                })
                .setNegativeButton(getContext().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mListener.onDialogNegativeClick(InspectPositionProjectDialog.this);
                    }
                });

        return adb.create();
    }

    @Override
    public void showFilterPrice(double price) {
        progressFilter.setVisibility(View.GONE);
        filterPrice.setVisibility(View.VISIBLE);


        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            filterPrice.setText(price + " " + getString(R.string.valuteSimbol));
        } else {
            filterPrice.setText(price + " " + getString(R.string.valuteString));
        }
    }

    @Override
    public void showAllPrice(double price) {
        progressAll.setVisibility(View.GONE);
        allPrice.setVisibility(View.VISIBLE);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            allPrice.setText(price + " " + getString(R.string.valuteSimbol));
        } else {
            allPrice.setText(price + " " + getString(R.string.valuteString));
        }
    }


}
