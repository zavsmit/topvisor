package ru.handh.topvisor.ui.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import ru.handh.topvisor.R;

/**
 * Created by sergey on 02.02.16.
 * диалог с переходом в почту
 */
public class SendLoginDialog extends DialogFragment {

    public static final int SEND_REGISTRATION = 0;
    public static final int SEND_FORGET = 1;

    private String title, message;
    private DialogResult mListener;

    public SendLoginDialog() {
    }

    public static SendLoginDialog newInstance(int typeSend) {
        SendLoginDialog f = new SendLoginDialog();

        Bundle args = new Bundle();
        args.putInt("typeSend", typeSend);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (DialogResult) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        switch (getArguments().getInt("typeSend")) {
            case SEND_REGISTRATION:
                title = getActivity().getString(R.string.youDadeSend);
                message = getActivity().getString(R.string.endRegistration);
                break;
            case SEND_FORGET:
                title = getActivity().getString(R.string.attention);
                message = getActivity().getString(R.string.endRecovery);
                break;
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
                .setTitle(title).setPositiveButton(getActivity().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mListener.onDialogPositiveClick(SendLoginDialog.this);
                    }
                })
                .setNegativeButton(getActivity().getString(R.string.inEmail), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mListener.onDialogNegativeClick(SendLoginDialog.this);
                    }
                })
                .setMessage(message);
        return adb.create();
    }
}