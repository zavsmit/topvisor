package ru.handh.topvisor.ui.login;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.gcm.QuickstartPreferences;
import ru.handh.topvisor.data.gcm.RegistrationIntentService;
import ru.handh.topvisor.ui.dialogs.DialogResult;
import ru.handh.topvisor.ui.dialogs.LicenseDialog;
import ru.handh.topvisor.ui.dialogs.LoginProgressDialog;
import ru.handh.topvisor.ui.dialogs.RecoveryPassDialog;
import ru.handh.topvisor.ui.dialogs.SendLoginDialog;
import ru.handh.topvisor.ui.main.ActivityMainView;
import ru.handh.topvisor.utils.Constants;

/**
 * Created by sergey on 30.01.16.
 * экран авторизации / регистрации
 */
public class LoginViewActivity extends AppCompatActivity implements LoginView, DialogResult {

    public static final String SOCIAL_NETWORK_TAG = "SocialIntegrationMain.SOCIAL_NETWORK_TAG";
    public final static int DIALOG_REGISTRATION = 0;
    public final static int DIALOG_RECOVERY_PASS = 1;
    public final static int DIALOG_SEND_FORGET = 2;
    public final static int DIALOG_SEND_REGISTRATION = 3;

    public final static int REQUEST_ULOGIN = 101;
    public final static int REQUEST_CODE_ASK_STORAGE_PERMISSIONS = 11111;
    public final static int REQUEST_CODE_ASK_PHONE_PERMISSIONS = 11112;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private EditText email, password;
    private Button changeScreen, buttonEnter;
    private LinearLayout socialBlock, passwordBlock;
    private ViewFlipper viewFlipper;
    private DialogFragment progressDialog;

    private PresenterLogin presenter;

    private int clickSocialId;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private boolean isReceiverRegistered;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_login);
        viewFlipper = (ViewFlipper) findViewById(R.id.viewFlipperLogin);

        email = (EditText) findViewById(R.id.et_login_email);
        password = (EditText) findViewById(R.id.et_login_password);

        buttonEnter = (Button) findViewById(R.id.b_login_enter);
        changeScreen = (Button) findViewById(R.id.b_login_regLogin);
        socialBlock = (LinearLayout) findViewById(R.id.ll_login_social);
        passwordBlock = (LinearLayout) findViewById(R.id.ll_login_password);

        presenter = new PresenterLoginImpl(this);
        presenter.openFromLink(getIntent().getDataString());

        getPermissionsStorage();


        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                SharedPreferences sharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences
                        .getBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false);
                if (sentToken) {
//                    mInformationTextView.setText(getString(R.string.gcm_send_message));
                } else {
//                    mInformationTextView.setText(getString(R.string.token_error_message));
                }
            }
        };

        registerReceiver();

        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }

    }

    private void registerReceiver() {
        if (!isReceiverRegistered) {
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));
            isReceiverRegistered = true;
        }
    }


    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver();
    }

    @Override
    protected void onStop() {
        super.onStop();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        isReceiverRegistered = false;
        if (presenter != null) {
            presenter.onStop();
        }
    }

    public void clickButtons(View view) {

        switch (view.getId()) {
            case R.id.b_login_enter:

                presenter.onEnterButtonClick(email.getText().toString(), password.getText().toString());

                break;

            case R.id.b_login_forgetPass:

                presenter.onForgetPassButtonClick();

                break;

            case R.id.b_login_regLogin:

                presenter.onReqLoginButtonClick();

                break;
        }
    }

    //через соцсети
    public void clickSocialButton(View view) {
        clickSocialId = view.getId();
        getPermissionsPhone();
    }

    public void runUlogin(String provider) {
        Intent intent = new Intent(getApplicationContext(),
                ActivityUlogin.class);

        String[] providers = {provider};
        String[] mandatory_fields = new String[]{"first_name", "last_name"};
        String[] optional_fields = new String[]{"nickname", "email", "photo", "photo_big"};

        intent.putExtra(
                ActivityUlogin.PROVIDERS,
                new ArrayList<>(Arrays.asList(providers))
        );
        intent.putExtra(
                ActivityUlogin.FIELDS,
                new ArrayList<>(Arrays.asList(mandatory_fields))
        );
        intent.putExtra(
                ActivityUlogin.OPTIONAL,
                new ArrayList<>(Arrays.asList(optional_fields))
        );

        intent.putExtra(ActivityUlogin.CLEAN_COOKIES_AFTER_AUTH, true);

        startActivityForResult(intent, REQUEST_ULOGIN);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.activityResult(requestCode, resultCode, data);
    }


    @Override
    public void showData() {

        if (progressDialog != null)
            progressDialog.dismiss();

        if (viewFlipper.getDisplayedChild() != Constants.VIEW_CONTENT)
            viewFlipper.setDisplayedChild(Constants.VIEW_CONTENT);
    }

    @Override
    public void showProgress(String text) {
        progressDialog = LoginProgressDialog.newInstance(text);
        String tag = "LoginProgressDialog";
        progressDialog.show(getSupportFragmentManager(), tag);
    }

    @Override
    public void showProgress(int idText) {
        showProgress(getString(idText));
    }

    @Override
    public void showError() {
        if (viewFlipper.getDisplayedChild() != Constants.VIEW_ERROR)
            viewFlipper.setDisplayedChild(Constants.VIEW_ERROR);
    }

    @Override
    public void showEmptyLogin() {
        email.setError(getString(R.string.writeLogin));
    }

    @Override
    public void showCorrectLogin() {
        email.setError(getString(R.string.writeCorrectEmail));
    }

    @Override
    public void showEmptyPass() {
        password.setError(getString(R.string.WritePass));
    }

    @Override
    public void showRegistrationView() {
        socialBlock.setVisibility(View.VISIBLE);
        passwordBlock.setVisibility(View.VISIBLE);
        changeScreen.setText(getString(R.string.registration));
        buttonEnter.setText(getString(R.string.enter));
    }

    @Override
    public void showLoginView() {
        socialBlock.setVisibility(View.GONE);
        passwordBlock.setVisibility(View.GONE);
        changeScreen.setText(getString(R.string.enter));
        buttonEnter.setText(getString(R.string.registration));
    }

    @Override
    public void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showToast(int textId) {
        Toast.makeText(this, getString(textId), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void hideRuSocial() {
        findViewById(R.id.iv_login_yandex).setVisibility(View.GONE);
        findViewById(R.id.iv_login_vk).setVisibility(View.GONE);
    }

    @Override
    public void addEnLogo() {
//        imageIcon.setImageResource(R.drawable.topvisor_en_vector);
    }

    @Override
    public void showDialogLogin(int dialogType) {

        DialogFragment dialog = null;
        String dialogTag = "";

        switch (dialogType) {
            case DIALOG_REGISTRATION:
                dialog = new LicenseDialog();
                dialogTag = "dialogRegistration";
                break;
            case DIALOG_RECOVERY_PASS:
                dialog = RecoveryPassDialog.newInstance(email.getText().toString().trim());
                dialogTag = "RecoveryPassDialog";
                break;
            case DIALOG_SEND_FORGET:
                dialog = SendLoginDialog.newInstance(SendLoginDialog.SEND_FORGET);
                dialogTag = "SEND_FORGET";
                break;
            case DIALOG_SEND_REGISTRATION:
                dialog = SendLoginDialog.newInstance(SendLoginDialog.SEND_REGISTRATION);
                dialogTag = "SendLoginDialog";
                break;
        }

        if (dialog != null)
            dialog.show(getSupportFragmentManager(), dialogTag);
    }

    @Override
    public void startMainActivity() {
        Intent intent = new Intent(LoginViewActivity.this, ActivityMainView.class);
        startActivity(intent);
        finish();
    }


    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        presenter.positivDialog(dialog, email.getText().toString().trim());
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        if (dialog instanceof SendLoginDialog) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_APP_EMAIL);
            startActivity(intent);
        }
    }


    public void getPermissionsStorage() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            final List<String> permissionsList = new ArrayList<>();

            addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE);

            if (permissionsList.size() > 0) {
                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_STORAGE_PERMISSIONS);
            }
        }
    }


    public void getPermissionsPhone() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            final List<String> permissionsList = new ArrayList<>();

            addPermission(permissionsList, Manifest.permission.READ_PHONE_STATE);

            if (permissionsList.size() > 0) {
                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_PHONE_PERMISSIONS);
            } else {
                clickSocial();
            }
        } else {
            clickSocial();
        }

    }

    private void clickSocial() {
        presenter.clickSocial(clickSocialId);
        clickSocialId = 0;
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean addPermission(List<String> permissionsList, String permission) {
        if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            // Check for Rationale Option
            if (!shouldShowRequestPermissionRationale(permission))
                return false;
        }
        return true;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_STORAGE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<>();

                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with results
                for (int i = 0; i < permissions.length; i++) {
                    perms.put(permissions[i], grantResults[i]);
                }

                // Check for ACCESS_FINE_LOCATION
                if (perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                } else {
                    Toast.makeText(LoginViewActivity.this, R.string.not_permissions, Toast.LENGTH_SHORT)
                            .show();
                    finish();
                }
                break;
            }
            case REQUEST_CODE_ASK_PHONE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<>();

                perms.put(Manifest.permission.READ_PHONE_STATE, PackageManager.PERMISSION_GRANTED);
                // Fill with results
                for (int i = 0; i < permissions.length; i++) {
                    perms.put(permissions[i], grantResults[i]);
                }

                // Check for ACCESS_FINE_LOCATION
                if (perms.get(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                    clickSocial();
                } else {
                    Toast.makeText(LoginViewActivity.this, R.string.not_permissions, Toast.LENGTH_SHORT)
                            .show();

                }
                break;
            }
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

}
