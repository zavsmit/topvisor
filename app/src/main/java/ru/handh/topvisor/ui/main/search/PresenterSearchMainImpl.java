package ru.handh.topvisor.ui.main.search;

import java.util.ArrayList;

import ru.handh.topvisor.data.network.model.currentProject.Row;

/**
 * Created by sergey on 23.02.16.
 * логика экрана поиска
 */
public class PresenterSearchMainImpl implements PresenterSearchMain, AdapterSearch.ItemSearchClickListener {

    private SearchView view;
    private ArrayList<Row> myDatasetEnd;
    private ArrayList<Row> myDataset;

    public PresenterSearchMainImpl(SearchView view, ArrayList<Row> myDataset) {
        this.view = view;
        myDatasetEnd = new ArrayList<>();
        this.myDataset = myDataset;
    }

    @Override
    public ArrayList<Row> getDataEnd() {
        return myDatasetEnd;
    }

    public void initDataEnd(ArrayList<Row> date) {
        myDatasetEnd.addAll(date);
    }

    @Override
    public boolean isAddListener() {
        if (myDataset.size() == 0) {
            return false;
        } else {
            initDataEnd(myDataset);
            return true;
        }
    }

    @Override
    public void changedEndData(String s) {
        myDatasetEnd.clear();

        for (int i = 0; i < myDataset.size(); i++) {

            String searchStr = s.toLowerCase();
            String startStr = myDataset.get(i).getName().toLowerCase();

            if (startStr.contains(searchStr)) {
                myDatasetEnd.add(myDataset.get(i));
            }
        }

        view.changeList(myDatasetEnd, s);

        view.showEmpty(myDatasetEnd.size() == 0);
    }

    @Override
    public void clickItem(String id) {

        for (int i = 0; i < myDatasetEnd.size(); i++) {
            if (myDatasetEnd.get(i).getId().equals(id)) {
                view.openNewActivity(myDatasetEnd.get(i));
                break;
            }
        }
    }
}
