package ru.handh.topvisor.ui.listResultSelected;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import ru.handh.topvisor.data.DataManager;
import ru.handh.topvisor.data.database.project.RegionData;
import ru.handh.topvisor.data.model.DateItem;
import ru.handh.topvisor.data.network.model.ModelResult;
import ru.handh.topvisor.data.network.model.ModelResultPS;
import ru.handh.topvisor.data.network.model.ReturnResponce;
import ru.handh.topvisor.ui.settingsProject.searchSystems.SearcherResult;
import ru.handh.topvisor.ui.settingsProject.searchSystems.typeSystems.ActivityTypeSystem;

/**
 * Created by sergey on 17.03.16.
 */
public class PresenterListResultImpl implements PresenterListResult, AdapterListResult.ItemGroupClickListener, SearcherResult {

    private ViewListResult mView;
    private List<DateItem> groups;
    private RealmList<RegionData> regions;
    private String isSearcher;
    private int type;
    private ArrayList<String> kldRegionList;


    private int positionCurrentRegion;

    public PresenterListResultImpl(ViewListResult view,
                                   List<DateItem> groups,
                                   String isSearcher,
                                   String idProject,
                                   int iSearcher,
                                   String idCurrentRegion,
                                   int type) {
        mView = view;
        this.groups = groups;
        this.type = type;
        this.regions = DataManager.getDB().getProject(idProject).getSearchers().get(iSearcher).getRegions();
        this.isSearcher = isSearcher;

        mView.initAdapter(groups, this);

        kldRegionList = new ArrayList<>();
        if (regions != null)
            for (int j = 0; j < regions.size(); j++) {

                if (idCurrentRegion.equals(regions.get(j).getId())) {
                    positionCurrentRegion = j;
                }

                String kld = regions.get(j).getKey() + ":"
                        + regions.get(j).getDepth() + ":"
                        + regions.get(j).getLang() + ":"
                        + regions.get(j).getDevice();

                kldRegionList.add(kld);

            }
    }

    private ArrayList<String> makeRegionList(int selectPosition) {

        String key = regions.get(positionCurrentRegion).getKey();
        String depth = regions.get(positionCurrentRegion).getDepth();
        String lang = regions.get(positionCurrentRegion).getLang();
        String device = regions.get(positionCurrentRegion).getDevice();


        switch (type) {
            case ActivityTypeSystem.RESQUEST_CODE_LANG:
                lang = groups.get(selectPosition).getShot();
                break;
            case ActivityTypeSystem.RESQUEST_CODE_DEPTH:
                int x = Integer.valueOf(groups.get(selectPosition).getName());
//                x = x / 100 - 1;
                x = x / 100;
                depth = String.valueOf(x);
                break;
            case ActivityTypeSystem.RESQUEST_CODE_DEVICE:
                device = String.valueOf(selectPosition);
                break;
        }

        DataManager.getDB().changeLangDepthDevice(regions, positionCurrentRegion, lang, depth, device);

        String kdld = key + ":"
                + depth + ":"
                + lang + ":"
                + device;


        kldRegionList.set(positionCurrentRegion, kdld);

        return kldRegionList;
    }


    @Override
    public void clickItem(int idSelected) {
        DataManager.getSettingsRest().reqChangeItemsPS(isSearcher, this, makeRegionList(idSelected), new ArrayList<Integer>());
        mView.selectGroup(idSelected);
    }

    @Override
    public void addPSResponce(ReturnResponce<ModelResultPS> result) {
        //--
    }

    @Override
    public void checkedPSResponce(ReturnResponce<ModelResultPS> result, int position) {
        //--
    }

    @Override
    public void removePSResponce(ReturnResponce<ModelResult> result, int position, String type) {
        //--
    }

    @Override
    public void changeItemsPSResponce(ReturnResponce<ModelResultPS> result, ArrayList<Integer> positions) {

    }
}