package ru.handh.topvisor.ui.settingsProject.interval.intervalDate;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.model.DateItem;
import ru.handh.topvisor.ui.base.ParentBackActivity;
import ru.handh.topvisor.ui.dialogs.TimeDialog;

/**
 * Created by sergey on 14.03.16.
 */
public class ActivitySettingDate extends ParentBackActivity implements ViewDate {

    public PresenterDate presenter;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_project_date);

        initToolbar(R.id.toolbarDateProject, getString(R.string.shedule));

        mRecyclerView = (RecyclerView) findViewById(R.id.recicleView_date);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        String hour = "";
        String autoCond = "";
        String idProject = "";
        List<String> listDate = new ArrayList<>();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            hour = extras.getString("hour");
            listDate = extras.getStringArrayList("listDate");
            idProject = extras.getString("idProject");
            autoCond = extras.getString("autoCond");
        }

        if(hour == null){
            hour = "";
        }

        presenter = new PresenterDateImpl(this, hour, listDate, idProject, autoCond);
    }

    @Override
    public void initAdapter(List<String> listSelected, AdapterDate.ItemDateClickListener listener, String hour, boolean isSelect) {

        List<DateItem> dateItems = new ArrayList<>();
        dateItems.add(new DateItem(getString(R.string.monday), false));
        dateItems.add(new DateItem(getString(R.string.tuesday), false));
        dateItems.add(new DateItem(getString(R.string.wednesday), false));
        dateItems.add(new DateItem(getString(R.string.thursday), false));
        dateItems.add(new DateItem(getString(R.string.friday), false));
        dateItems.add(new DateItem(getString(R.string.saturday), false));
        dateItems.add(new DateItem(getString(R.string.sunday), false));


        mAdapter = new AdapterDate(listSelected, listener, hour, isSelect, dateItems);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void showTimeDialog(String house) {
        TimeDialog timeDialog = TimeDialog.newInstance(house);
        timeDialog.show(getSupportFragmentManager(), "timePicker");
    }

    @Override
    public void changeAdapteHour(String hour) {
        ((AdapterDate) mAdapter).changeHour(hour);
    }

    @Override
    public void customStop(String hour, ArrayList<String> listDate) {
        Intent intent = new Intent();
        intent.putExtra("hour", hour);
        intent.putStringArrayListExtra("listDate", listDate);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void finishActivtiy() {
        finish();
    }

    @Override
    public void  onBackPressed(){
        presenter.close();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            presenter.close();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
