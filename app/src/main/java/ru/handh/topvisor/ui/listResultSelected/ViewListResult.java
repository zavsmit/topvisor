package ru.handh.topvisor.ui.listResultSelected;

import java.util.List;

import ru.handh.topvisor.data.model.DateItem;

/**
 * Created by sergey on 17.03.16.
 */
public interface ViewListResult {
    void selectGroup(int idSelected);
    void initAdapter(List<DateItem> groups, AdapterListResult.ItemGroupClickListener listener);
}
