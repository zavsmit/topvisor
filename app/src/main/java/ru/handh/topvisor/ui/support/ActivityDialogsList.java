package ru.handh.topvisor.ui.support;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import java.util.List;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.network.model.ModelTicket;
import ru.handh.topvisor.ui.menu.ParentLeftMenu;
import ru.handh.topvisor.ui.support.listMessages.ActivityListMessages;
import ru.handh.topvisor.utils.Constants;

/**
 * Created by sergey on 31.03.16.
 * экран поддержки
 */
public class ActivityDialogsList extends ParentLeftMenu implements ViewDialogsList {

    protected ViewFlipper viewFlipperMain;
    private RecyclerView mRecyclerView;
    private PresenterDialogsList presenter;
    private TextView tvEmpty;
//    private Button bReqError;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewFlipperMain = (ViewFlipper) findViewById(R.id.viewFlipper);
        showData();
        mRecyclerView = (RecyclerView) findViewById(R.id.recicleView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));


        tvEmpty = (TextView) findViewById(R.id.tv_empty);
//        bReqError = (Button) findViewById(R.id.button_reqError);
        assert tvEmpty != null;
        tvEmpty.setText(R.string.addQest);
        presenter = new PresenterDialogsListImpl(this);
    }

    @Override
    protected void initView() {
        isMainScreen = false;
        title = getString(R.string.support);
        LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        CoordinatorLayout v = (CoordinatorLayout) vi.inflate(R.layout.activity_yandex_inspect, null);
        drawer.addView(v, 0, new ViewGroup.LayoutParams(DrawerLayout.LayoutParams.MATCH_PARENT, DrawerLayout.LayoutParams.MATCH_PARENT));
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.init();
    }

    @Override
    public void selectItem(String nameId) {
        Intent intent = new Intent(this, ActivityListMessages.class);
        intent.putExtra("nameId", nameId);
        startActivity(intent);
    }

    public void clickButtons(View view) {

        switch (view.getId()) {
            case R.id.fabSearchSystem:
                presenter.clickAdd();
                break;

            case R.id.button_reqError:
                presenter.init();
                break;
        }
    }

    @Override
    public void initAdapter(List<ModelTicket> groups, AdapterDialogsList.ItemClickListener listener) {
        RecyclerView.Adapter mAdapter = new AdapterDialogsList(groups, listener, this);
        mRecyclerView.setAdapter(mAdapter);
    }


    @Override
    public void showEmptyText(boolean isShow) {
        if (isShow) {
            viewFlipperMain.setVisibility(View.GONE);
            tvEmpty.setVisibility(View.VISIBLE);
        } else {
            viewFlipperMain.setVisibility(View.VISIBLE);
            tvEmpty.setVisibility(View.GONE);
        }
    }

    public void showData() {
        if (viewFlipperMain.getDisplayedChild() != Constants.VIEW_CONTENT)
            viewFlipperMain.setDisplayedChild(Constants.VIEW_CONTENT);
    }

    public void showProgress() {
        if (viewFlipperMain.getDisplayedChild() != Constants.VIEW_PROGRESS)
            viewFlipperMain.setDisplayedChild(Constants.VIEW_PROGRESS);
    }

    public void showError() {
        if (viewFlipperMain.getDisplayedChild() != Constants.VIEW_ERROR)
            viewFlipperMain.setDisplayedChild(Constants.VIEW_ERROR);
    }

    public void showInfoToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

}

