package ru.handh.topvisor.ui.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import ru.handh.topvisor.R;

/**
 * Created by sergey on 12.04.16.
 */
public class DialogText extends DialogFragment {

    public DialogText() {
    }

    public static DialogText newInstance(String title, String text) {
        DialogText f = new DialogText();

        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("text", text);
        f.setArguments(args);

        return f;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());

        adb.setTitle(getArguments().getString("title"))
                .setPositiveButton(getContext().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        adb.setMessage(getArguments().getString("text"));

        return adb.create();
    }


}
