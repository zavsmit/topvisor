package ru.handh.topvisor.ui.settingsProject.searchSystems.searchRegion;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.network.model.ModelSearchRegion;
import ru.handh.topvisor.ui.base.ParentBackActivity;
import ru.handh.topvisor.ui.dialogs.AddRegionsDialog;

/**
 * Created by sergey on 25.03.16.
 * адаптер результата поиска
 */
public class ActivitySearchRegion extends ParentBackActivity implements ViewSearchRegion {


    public PresenterSearchRegion presenter;
    private AdapterSearchRegion mAdapter;
    private RecyclerView mRecyclerView;
    private ProgressBar progressBar;
    private TextView tvEmpty;
    private SendNumberToDialog sendNumberToDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        String idSearcher = "";
        String typeSearcher = "";
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            idSearcher = extras.getString("idSearcher");
            typeSearcher = extras.getString("typeSearcher");
        }


        initToolbar(R.id.toolbar_search);

        EditText search = (EditText) findViewById(R.id.et_search);
        tvEmpty = (TextView) findViewById(R.id.tv_empty_search);
        progressBar = (ProgressBar) findViewById(R.id.progress_search);
        Button bEnter = (Button) findViewById(R.id.ib_search);
        bEnter.setVisibility(View.VISIBLE);

        bEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.clickEnter();
            }
        });

        mRecyclerView = (RecyclerView) findViewById(R.id.recicleView_search);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        if (search != null) {
            search.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    presenter.loadListRegion(s.toString());
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }

        presenter = new PresenterSearchRegionImpl(this, idSearcher, typeSearcher);

    }

    @Override
    public AdapterSearchRegion initAdapter(AdapterSearchRegion.ItemSearchRegionClickListener listener) {
        mAdapter = new AdapterSearchRegion(listener);
        mRecyclerView.setAdapter(mAdapter);
        return mAdapter;
    }

    @Override
    public void showProgress(boolean isShow) {
        if (isShow) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void changeList(List<ModelSearchRegion> myDataset, String s) {
        mAdapter.notifyAllData(myDataset, s);
    }

    @Override
    public void showDialog() {
        DialogFragment dialog = new AddRegionsDialog();
        String dialogTag = "AddRegionsDialog";
        dialog.show(getSupportFragmentManager(), dialogTag);
        sendNumberToDialog = ((AddRegionsDialog) dialog);
    }

    @Override
    public void showEmpty(boolean isEmpty) {
        if (isEmpty) {
            tvEmpty.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        } else {
            tvEmpty.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showResult(int number) {
        sendNumberToDialog.showResult(number);
    }

    @Override
    public void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showToast(int textId) {
        Toast.makeText(this, getString(textId), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void end() {
        finish();
    }

    public interface SendNumberToDialog {
        void showResult(int number);
    }

}
