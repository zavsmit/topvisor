package ru.handh.topvisor.ui.project;

import android.content.Context;
import android.support.v4.app.DialogFragment;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.realm.RealmList;
import ru.handh.topvisor.R;
import ru.handh.topvisor.data.DataManager;
import ru.handh.topvisor.data.database.project.GroupData;
import ru.handh.topvisor.data.database.project.ProjectData;
import ru.handh.topvisor.data.database.project.RegionData;
import ru.handh.topvisor.data.database.project.SearchData;
import ru.handh.topvisor.data.model.InspectDialogData;
import ru.handh.topvisor.data.network.model.ModelPriceForProgects;
import ru.handh.topvisor.data.network.model.ModelPriseInspectFilter;
import ru.handh.topvisor.data.network.model.ModelResult;
import ru.handh.topvisor.data.network.model.ReturnResponce;
import ru.handh.topvisor.data.network.model.currentProject.Row;
import ru.handh.topvisor.data.network.model.projecDates.Rows;
import ru.handh.topvisor.data.network.model.projecPhrases.ModelProjectPhrases;
import ru.handh.topvisor.ui.dialogs.DialogResult;
import ru.handh.topvisor.ui.dialogs.InspectPositionProjectDialog;
import ru.handh.topvisor.ui.dialogs.NewTicketDialog;
import ru.handh.topvisor.ui.dialogs.SelectGroupProjectDialog;
import ru.handh.topvisor.ui.main.MainEndReq;

/**
 * Created by sergey on 19.02.16.
 * логика экрана с табами запросов
 */
public class PresenterProjectImpl implements PresenterProject,
        DialogResult,
        ProjectEndReq,
        MainEndReq,
        AdapterProject.ProjectSwipeClickListener {

    private ViewProject view;
    private String idProject;
    private ProjectData projectData;
    private List<String> datesString;
    private ArrayList<String> allDates;
    private int lastPhrase;
    private String jsonSearch;
    private boolean isFirst = true;
    private Context context;
    private ArrayList<Integer> selectedDateList = new ArrayList<>();

    public PresenterProjectImpl(ViewProject view, String idProject, Context context) {
        this.view = view;
        this.idProject = idProject;
        this.context = context;
        datesString = new ArrayList<>();

        jsonSearch = "{\"id\": \"" + idProject + "\"}";
        DataManager.getProjectRest().reqItemProject(jsonSearch, this);
    }

    private void loadDates() {
        DataManager.getProjectRest().loadDatesProject(
                idProject,
                getCompetitorId(0),
                getSelectedIdRegion(),
                getSelectedIdSearcher(),
                getSelectedIdRegion(),
                this);
    }

    @Override
    public void loadPhrases(int currentNumberPage, int page) {
        lastPhrase = currentNumberPage;

        DataManager.getProjectRest().loadPhrasesProject(
                getCompetitorId(currentNumberPage),
                idProject,
                getSelectedIdRegion(),
                getDateString(),
                this,
                currentNumberPage,
                getSelectedGroupId(),
                getSelectedIdSearcher(),
                String.valueOf(page));

        changeHeaderFragment(currentNumberPage);

        String start = datesString.get(0);
        String end = "";

        if (datesString.size() > 1) {
            end = datesString.get(1);
        }


        int currentPage = view.getCurrentNumberPage();
        view.changeFooterInFragment(start, end, currentPage);

        if (currentPage != 0) {
            view.changeFooterInFragment(start, end, currentPage - 1);
        }

        if (projectData.getCompetitors().size() > currentPage) {
            view.changeFooterInFragment(start, end, currentPage + 1);
        }
    }

    @Override
    public void openGroups() {
        if (projectData.getGroups().size() == 0) {
            view.openSettings(idProject);
        } else {
            view.openGroups(idProject);
        }
    }

    @Override
    public void openRegions() {
        if (projectData.getSearchers().size() == 0) {
            view.openSettings(idProject);
        } else {
            view.openRegions(idProject);
        }
    }

    @Override
    public void openDate() {
        view.openDate(allDates, selectedDateList);
    }

    @Override
    public void clickSettings() {
        view.openSettings(idProject);
    }


    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        if (dialog instanceof NewTicketDialog) {

            DataManager.getProjectRest().reqAddPhrase(((NewTicketDialog) dialog).name.getText().toString().trim(),
                    idProject,
                    ((NewTicketDialog) dialog).groupId,
                    this);
        }

        if (dialog instanceof SelectGroupProjectDialog) {
            openGroups();
        }

        if (dialog instanceof InspectPositionProjectDialog) {

            if (((InspectPositionProjectDialog) dialog).isFilter) {

                DataManager.getProjectRest().reqInspectOnFilter(idProject,
                        getSelectedIdSearcher(), getSelectedDomain(),
                        getSelectedIdRegion(), getSelectedRegionLang(), getSelectedGroupId(), this);
            } else {
                DataManager.getMainRest().reqInspectionProjects(this, Integer.valueOf(idProject));
            }
        }

    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {

    }

    @Override
    public void itemProjectResponce(ReturnResponce<Row> result) {

        if (!result.isFailure()) {
            projectData = DataManager.getDB().rowTpProgect(result.getResult());
            DataManager.getDB().projectRowWriteDatabase(projectData);
            loadDates();
        }
    }

    @Override
    public void datesProjectResponce(ReturnResponce<Rows> result, String idProject, String regionKey) {
        if (!result.isFailure()) {

            allDates = (ArrayList<String>) result.getResult().getAllDates();
            Collections.reverse(allDates);


            selectedDateList.clear();// TODO: 28.04.2016 адаптировать под планшеты

            if (allDates.size() > 1) {
                selectedDateList.add(1);
                selectedDateList.add(0);
            } else {
                selectedDateList.add(0);
            }


            getDateString();
            DataManager.getDB().projectDatesWriteDatabase(allDates, idProject, regionKey);

            if (isFirst) {
                view.initViewPager(projectData);
                isFirst = false;
            } else {


                int currentPage = view.getCurrentNumberPage();
                loadPhrases(currentPage, 1);

                if (currentPage != 0) {
                    loadPhrases(currentPage - 1, 1);
                }

                if (projectData.getCompetitors().size() - 1 > currentPage) {
                    loadPhrases(currentPage + 1, 1);
                }

            }

        } else {
            view.showToast(result.getErrorMessage());
        }
    }

    @Override
    public void phrasesProjectResponce(ReturnResponce<ModelProjectPhrases> result,
                                       int currentNumberPage, List<String> dates) {

        if (!result.isFailure()) {
            List<String> formatDates = new ArrayList<>();
            Date date = new Date();
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            DateFormat dfNew = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH);
            for (int i = 0; i < dates.size(); i++) {

                try {
                    date = format.parse(dates.get(i));
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                formatDates.add(dfNew.format(date));
            }

            view.initAdapterFragment(currentNumberPage, result.getResult(), formatDates, this);
        }

    }

    @Override
    public void changeHeader() {
        projectData = DataManager.getDB().getProject(idProject);

        int currentPage = view.getCurrentNumberPage();

        changeHeaderFragment(currentPage);

        if (currentPage != 0) {
            changeHeaderFragment(currentPage - 1);
        }

        if (projectData.getCompetitors().size() - 1 > currentPage) {
            changeHeaderFragment(currentPage + 1);
        }

        loadDates();
    }


    private void changeHeaderFragment(int page) {

        if (projectData.getSearchers().size() == 0) {
            view.changeHeaderInFragment(null, getSelectedGroupName(), page);
        } else {
            view.changeHeaderInFragment(getSelectedPsName() + " - " + getSelectedRegionName(), getSelectedGroupName(), page);
        }
    }

    @Override
    public void changeData() {
        projectData = DataManager.getDB().getProject(idProject);
    }

    @Override
    public void changeDates(ArrayList<Integer> listInt) {
        selectedDateList.clear();
        selectedDateList.addAll(listInt);
    }

    @Override
    public void showDialogNew(int idDialog) {

        InspectDialogData data = new InspectDialogData();
        data.setName(projectData.getName());
        data.setGroup(getSelectedGroupName());
        data.setPs(getSelectedPsName());
        data.setRegion(getSelectedRegionName());


        if (idDialog == ActivityViewProject.DIALOG_ADD && getSelectedGroupId().equals("-1")) {
            view.showNewDialog(ActivityViewProject.DIALOG_SELECT_GROUP, getSelectedGroupId(), data);
        } else if (idDialog == ActivityViewProject.DIALOG_REFRESH_ALL) {

            view.showNewDialog(idDialog, getSelectedGroupId(), data);

            DataManager.getProjectRest().loadPriceOnFilter(jsonSearch,
                    getSelectedIdSearcher(),
                    getSelectedDomain(),
                    getSelectedIdRegion(),
                    getSelectedRegionLang(),
                    getSelectedGroupId(),
                    this);


            List<Integer> listIdProject = new ArrayList<>();
            listIdProject.add(Integer.valueOf(idProject));
            DataManager.getMainRest().reqPriceProjects(this,
                    listIdProject);

        } else {
            view.showNewDialog(idDialog, getSelectedGroupId(), data);
        }

    }

    @Override
    public void initAll() {
    }

    @Override
    public void onStop() {

    }

    private String getSelectedGroupId() {
        for (int i = 0; i < projectData.getGroups().size(); i++) {
            if (projectData.getGroups().get(i).isEnable()) {
                return projectData.getGroups().get(i).getId();
            }
        }
        return "-1";
    }

    private String getCompetitorId(int numberPage) {
        String competitorid;

        if (numberPage == 0) {
            competitorid = idProject;
        } else {
            competitorid = projectData.getCompetitors().get(numberPage - 1).getId();
        }

        return competitorid;
    }

    private String getSelectedIdRegion() {

        if (projectData.getSearchers() == null) {
            return "-1";
        }
        for (int i = 0; i < projectData.getSearchers().size(); i++) {

            SearchData searcher = projectData.getSearchers().get(i);

            for (int j = 0; j < searcher.getRegions().size(); j++) {
                RegionData region = searcher.getRegions().get(j);
                if (region.isSelected()) {

                    return region.getKey();
                }
            }
        }

        return "-1";
    }

    private String getSelectedRegionLang() {

        if (projectData.getSearchers() == null) {
            return "-1";
        }
        for (int i = 0; i < projectData.getSearchers().size(); i++) {

            SearchData searcher = projectData.getSearchers().get(i);

            for (int j = 0; j < searcher.getRegions().size(); j++) {
                RegionData region = searcher.getRegions().get(j);
                if (region.isSelected()) {

                    return region.getLang();
                }
            }
        }

        return "-1";
    }

    private String getSelectedIdSearcher() {

        for (int i = 0; i < projectData.getSearchers().size(); i++) {

            SearchData searcher = projectData.getSearchers().get(i);

            for (int j = 0; j < searcher.getRegions().size(); j++) {
                RegionData region = searcher.getRegions().get(j);
                if (region.isSelected()) {

                    return searcher.getSearcher();
                }
            }
        }

        return "0";
    }

    private String getSelectedDomain() {

        for (int i = 0; i < projectData.getSearchers().size(); i++) {

            SearchData searcher = projectData.getSearchers().get(i);

            for (int j = 0; j < searcher.getRegions().size(); j++) {
                RegionData region = searcher.getRegions().get(j);
                if (region.isSelected()) {

                    return region.getDomain();
                }
            }
        }

        return "";
    }

    private String getSelectedGroupName() {
        RealmList<GroupData> listGD = projectData.getGroups();

        for (int i = 0; i < listGD.size(); i++) {
            GroupData gd = listGD.get(i);

            if (gd.isEnable()) {
                return gd.getName();
            }
        }
        return context.getString(R.string.selectGroup);
    }

    private String getSelectedRegionName() {

        for (int i = 0; i < projectData.getSearchers().size(); i++) {

            SearchData searcher = projectData.getSearchers().get(i);

            for (int j = 0; j < searcher.getRegions().size(); j++) {
                RegionData region = searcher.getRegions().get(j);
                if (region.isSelected()) {
                    return region.getName();
                }
            }
        }

        return "//";
    }

    private String getSelectedPsName() {

        for (int i = 0; i < projectData.getSearchers().size(); i++) {

            SearchData searcher = projectData.getSearchers().get(i);

            for (int j = 0; j < searcher.getRegions().size(); j++) {
                RegionData region = searcher.getRegions().get(j);
                if (region.isSelected()) {
                    return searcher.getName();
                }
            }
        }

        return "//";
    }


    private List<String> getDateString() {
        datesString.clear();


        if (allDates.size() != 0) {
            for (int i = 0; i < selectedDateList.size(); i++) {
                if (selectedDateList.get(i) != -1)
                    datesString.add(allDates.get(selectedDateList.get(i)));
            }
        }

        if (datesString.size() == 1) {
            datesString.add("");
        }

        if (datesString.size() == 0) {

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            Date currentDate = new Date();

            datesString.add(dateFormat.format(currentDate));
            datesString.add("");
        }


        return datesString;
    }


    @Override
    public void removePharseResult(ReturnResponce<ModelResult> result) {

    }

    @Override
    public void addPharseResult(ReturnResponce<ModelResult> result) {
        if (!result.isFailure()) {
            loadPhrases(lastPhrase, 1);
        }
    }

    @Override
    public void priceOnFilter(ReturnResponce<ModelPriseInspectFilter> result) {
        if (!result.isFailure()) {
            view.addFilterPrice(result.getResult().getPrise().getPrice());
        }
    }

    @Override
    public void inspectOnFilter(ReturnResponce<ModelResult> result) {
        if(!result.isFailure()){
            if (result.getResult().isError()) {
                view.showToast(result.getResult().getMessage());
            }
        }
    }

    @Override
    public void clickTrash(int idPharse, int position) {
        view.removeItemInFragment(position);
        DataManager.getProjectRest().reqRemovePhrare(idProject, String.valueOf(idPharse), this);
    }

    @Override
    public void addProjectsResponce(ReturnResponce<ModelResult> result) {
        //--
    }

    @Override
    public void getProjectsResponce(ReturnResponce<ArrayList<Row>> result) {
        //--
    }

    @Override
    public void getPriceProgect(ReturnResponce<ModelPriceForProgects> result) {
        if (!result.isFailure()) {
            view.addAllPrice(result.getResult().getPrice());
        }
    }

    @Override
    public void getInspectProjectResult(ReturnResponce<ModelResult> result) {
        if (result.getResult().isError()) {
            view.showToast(result.getResult().getMessage());
        }
    }
}