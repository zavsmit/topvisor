package ru.handh.topvisor.ui.project;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.DataManager;
import ru.handh.topvisor.data.database.project.ProjectData;
import ru.handh.topvisor.data.model.InspectDialogData;
import ru.handh.topvisor.data.network.model.currentProject.Row;
import ru.handh.topvisor.data.network.model.projecPhrases.ModelProjectPhrases;
import ru.handh.topvisor.ui.base.ParentBackActivity;
import ru.handh.topvisor.ui.dialogs.InspectPositionProjectDialog;
import ru.handh.topvisor.ui.dialogs.NewTicketDialog;
import ru.handh.topvisor.ui.dialogs.SelectGroupProjectDialog;
import ru.handh.topvisor.ui.project.selectDate.ActivitySelectDate;
import ru.handh.topvisor.ui.project.selectGroup.ActivitySelectGroup;
import ru.handh.topvisor.ui.project.selectRegion.ActivitySelectRegion;
import ru.handh.topvisor.ui.settingsProject.ActivitySettings;

/**
 * Created by sergey on 19.02.16.
 * экран с табами запросов
 */
public class ActivityViewProject extends ParentBackActivity implements ViewProject {

    public final static int RESQUEST_CODE_GROUPS = 1000;
    public final static int RESQUEST_CODE_DATES = 1002;
    public final static int RESQUEST_CODE_SETTINGS = 1003;
    public final static int RESQUEST_CODE_REGIONS = 1001;

    public final static int DIALOG_ADD = 2001;
    public final static int DIALOG_REFRESH_ALL = 2000;
    public final static int DIALOG_SELECT_GROUP = 2002;

    public PresenterProject presenter;
    public ViewPager viewPager;
    private String name;
    private TabLayout tabLayout;
    private SendDataToDialog sendDataToDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_project);

        Row data = getIntent().getExtras().getParcelable("row");
        name = data.getName();
        String projectId = data.getId();


        initToolbar(R.id.toolbarProject, name);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);

        presenter = new PresenterProjectImpl(this, projectId, this);

    }

    @Override
    public void initViewPager(ProjectData currentRow) {
        setupViewPager(viewPager, currentRow);
        tabLayout.setupWithViewPager(viewPager);

        if (currentRow.getCompetitors().size() == 0) {
            tabLayout.setVisibility(View.GONE);
        }

    }


    private void setupViewPager(ViewPager viewPager, ProjectData currentRow) {// TODO: 10.03.16 в presenter
        AdapterProjectPager adapter = new AdapterProjectPager(getSupportFragmentManager());
        adapter.addFragment(FragmentProject.newInstance(0), name);// 0 - добавляем свой сайт

        for (int i = 0; i < currentRow.getCompetitors().size(); i++) {// добавляем сайты конкурентов
            adapter.addFragment(FragmentProject.newInstance(i + 1), currentRow.getCompetitors().get(i).getName());
        }

        viewPager.setAdapter(adapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.project, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        if (id == R.id.action_region) {

            boolean isVisible = !DataManager.getPref().getVisibleHeaderProject();

            int currentPosition = viewPager.getCurrentItem();
            AdapterProjectPager adapter = ((AdapterProjectPager) viewPager.getAdapter());
            getFragmentByNumber(currentPosition).hideRegionGroup(isVisible);

            // отображение, скрытие у соседних тобов регионов
            if (adapter.getCount() != 1)
                if (currentPosition == 0) {
                    getFragmentByNumber(currentPosition + 1).hideRegionGroup(isVisible);
                } else if (currentPosition == adapter.getCount() - 1) {
                    getFragmentByNumber(currentPosition - 1).hideRegionGroup(isVisible);
                } else {
                    getFragmentByNumber(currentPosition + 1).hideRegionGroup(isVisible);
                    getFragmentByNumber(currentPosition - 1).hideRegionGroup(isVisible);
                }

            DataManager.getPref().setVisibleHeaderProject(isVisible);

            return true;
        }

        if (id == R.id.action_settings) {
            presenter.clickSettings();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void initAdapterFragment(int fragmentNumber, ModelProjectPhrases rows,
                                    List<String> dates, PresenterProjectImpl presenterImpl) {

        getFragmentByNumber(fragmentNumber).initAdapter(rows, dates, presenterImpl);
    }

    @Override
    public int getCurrentNumberPage() {
        return viewPager.getCurrentItem();
    }

    private FragmentProject getFragmentByNumber(int number) {
        AdapterProjectPager adapter = ((AdapterProjectPager) viewPager.getAdapter());
        return ((FragmentProject) adapter.getItem(number));
    }

    @Override
    public void removeItemInFragment(int position) {
        getFragmentByNumber(getCurrentNumberPage()).removeItem(position);
    }

    @Override
    public void changeHeaderInFragment(String nameRegion, String nameGroup, int pageNumber) {

        if (nameRegion == null) {
            nameRegion = getString(R.string.addPsAndRegion);
        }

        getFragmentByNumber(pageNumber).changeHeader(nameRegion, nameGroup);
    }

    @Override
    public void changeFooterInFragment(String start, String end, int numberPage) {
        getFragmentByNumber(numberPage).changeCalendarFooter(start, end);
    }

    @Override
    public void openGroups(String idProject) {
        Intent intent = new Intent(this, ActivitySelectGroup.class);
        intent.putExtra("idProject", idProject);
        startActivityForResult(intent, RESQUEST_CODE_GROUPS);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {

            switch (requestCode) {
                case RESQUEST_CODE_GROUPS:

                    presenter.changeHeader();

                    break;
                case RESQUEST_CODE_REGIONS:

                    presenter.changeHeader();

                    break;

                case RESQUEST_CODE_DATES:

                    presenter.changeDates(data.getIntegerArrayListExtra("selectedPositions"));

                    break;

                case RESQUEST_CODE_SETTINGS:

                    presenter.changeData();

                    break;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.initAll();
    }


    @Override
    protected void onStop() {
        super.onStop();
        if (presenter != null) {
            presenter.onStop();
        }
    }

    @Override
    public void openRegions(String idProject) {
        Intent intent = new Intent(this, ActivitySelectRegion.class);
        intent.putExtra("idProject", idProject);
        startActivityForResult(intent, RESQUEST_CODE_REGIONS);
    }

    @Override
    public void openDate(ArrayList<String> allDates, ArrayList<Integer> selectedList) {
        Intent intent = new Intent(this, ActivitySelectDate.class);
        intent.putStringArrayListExtra("allDates", allDates);
        intent.putIntegerArrayListExtra("selectedList", selectedList);
        startActivityForResult(intent, RESQUEST_CODE_DATES);
    }

    @Override
    public void openSettings(String idProject) {
        Intent intent = new Intent(this, ActivitySettings.class);
        intent.putExtra("idProject", idProject);
        startActivityForResult(intent, RESQUEST_CODE_SETTINGS);
    }

    @Override
    public void showNewDialog(int idDialog, String groupId, InspectDialogData data) {
        DialogFragment dialog = null;
        String dialogTag = "";
        switch (idDialog) {
            case DIALOG_ADD:
                dialog = NewTicketDialog.newInstance(NewTicketDialog.NEW_REQUEST, groupId);
                dialogTag = "NewTicketDialog";
                break;
            case DIALOG_REFRESH_ALL:
                dialog = InspectPositionProjectDialog.newInstance(data);
                dialogTag = "EmptyRegionProjectDialog";
                break;
            case DIALOG_SELECT_GROUP:
                dialog = new SelectGroupProjectDialog();
                dialogTag = "SelectGroupProjectDialog";
                break;
        }

        if (dialog != null)
            dialog.show(getSupportFragmentManager(), dialogTag);

        if (dialog instanceof InspectPositionProjectDialog)
            sendDataToDialog = ((InspectPositionProjectDialog) dialog);
    }

    @Override
    public void addFilterPrice(double price) {
        sendDataToDialog.showFilterPrice(price);
    }

    @Override
    public void addAllPrice(double price) {
        sendDataToDialog.showAllPrice(price);
    }

    @Override
    public void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }

    public interface SendDataToDialog {
        void showFilterPrice(double price);

        void showAllPrice(double price);
    }


}

