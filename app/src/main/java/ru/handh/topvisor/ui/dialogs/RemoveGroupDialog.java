package ru.handh.topvisor.ui.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import ru.handh.topvisor.R;
import ru.handh.topvisor.ui.removeItemsList.ActivityRemoveItemsList;

/**
 * Created by sergey on 22.03.16.
 */
public class RemoveGroupDialog extends DialogFragment {

    private DialogResult mListener;
    public int position;

    public RemoveGroupDialog() {
    }

    public static RemoveGroupDialog newInstance(int position) {
        RemoveGroupDialog f = new RemoveGroupDialog();

        Bundle args = new Bundle();
        args.putInt("position", position);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (DialogResult) ((ActivityRemoveItemsList) activity).presenter;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
                .setTitle(getActivity().getString(R.string.removeGroup))
                .setPositiveButton(getActivity().getString(R.string.remove), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mListener.onDialogPositiveClick(RemoveGroupDialog.this);
                    }
                })
                .setNegativeButton(getActivity().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mListener.onDialogNegativeClick(RemoveGroupDialog.this);
                    }
                })
                .setMessage(getActivity().getString(R.string.removeGroupAlert));

        position = getArguments().getInt("position");

        return adb.create();
    }
}
