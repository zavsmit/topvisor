package ru.handh.topvisor.ui.settingsProject.interval;

import android.content.Intent;

/**
 * Created by sergey on 16.03.16.
 */
public interface PresenterIntervalInspect {
    void clickItem(int position);
    void returnData(Intent data, int requestCode);
    void changeDescription();
}
