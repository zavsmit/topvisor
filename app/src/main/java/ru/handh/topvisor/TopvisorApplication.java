package ru.handh.topvisor;

import android.app.Application;

import com.bettervectordrawable.Convention;
import com.bettervectordrawable.VectorDrawableCompat;

import ru.handh.topvisor.data.DataManager;

/**
 * Created by sergey on 04.02.16.
 * applicaion приложения
 */
public class TopvisorApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        int[] ids = VectorDrawableCompat.findVectorResourceIdsByConvention(getResources(),
                R.drawable.class, Convention.RESOURCE_NAME_HAS_VECTOR_SUFFIX);
        VectorDrawableCompat.enableResourceInterceptionFor(getResources(), ids);

        DataManager.initInstance(this);
    }
}
