package ru.handh.topvisor.data;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.handh.topvisor.BuildConfig;
import ru.handh.topvisor.data.network.BankImpl;
import ru.handh.topvisor.data.network.BankModel;
import ru.handh.topvisor.data.network.LoginImpl;
import ru.handh.topvisor.data.network.LoginModel;
import ru.handh.topvisor.data.network.MainImpl;
import ru.handh.topvisor.data.network.MainModel;
import ru.handh.topvisor.data.network.ProjectImpl;
import ru.handh.topvisor.data.network.ProjectModel;
import ru.handh.topvisor.data.network.RestApi;
import ru.handh.topvisor.data.network.SettingsImpl;
import ru.handh.topvisor.data.network.SettingsModel;
import ru.handh.topvisor.data.network.SupportImpl;
import ru.handh.topvisor.data.network.SupportModel;
import ru.handh.topvisor.utils.Log;

/**
 * Created by sergey on 28.02.16.
 * для сети с разбивкой по экранам
 */
public class NetworkHelper {
    private final String[] DATE_FORMATS = new String[]{
            "yyyy-MM-dd HH:mm:ss",
            "yyyy-MM-dd"
    };
    private MainModel mainNetwork;
    private LoginModel loginNetwork;
    private ProjectModel projectNetwork;
    private SettingsModel settingNetwork;
    private SupportModel supportNetwork;
    private BankModel bankNetwork;

    private Context context;
    private OkHttpClient client;
    private Gson gson;
    private RestApi restApi;


    public NetworkHelper(Context context, String baseUrl) {
        this.context = context;
        initNetworkApi(baseUrl);
    }

    private void initNetworkApi(String baseUrl) {
        gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .registerTypeAdapter(Date.class, new DateDeserializer())
                .create();


        PackageManager manager = context.getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        String versionName = "";

        if (info != null) {
            versionName = info.versionName;
        }


        final String version = "Topvisor_Android " + versionName + " " + System.getProperty("http.agent");

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        if (BuildConfig.TVDEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);
            httpClient.addInterceptor(interceptor);
        }

        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .header("Cookie", "auth=" + DataManager.getPref().getTokenApp())
                        .header("User-Agent", version)
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            }
        });


        client = httpClient.build();

        firstInitRest(baseUrl);

        loginNetwork = new LoginImpl(context, restApi);
        mainNetwork = new MainImpl(restApi, context);
        projectNetwork = new ProjectImpl(restApi, context);
        settingNetwork = new SettingsImpl(restApi, context);
        supportNetwork = new SupportImpl(restApi, context);
        bankNetwork = new BankImpl(restApi, context);

    }

    public void initRest() {

        firstInitRest("");

    }

    public void firstInitRest(String baseUrl) {

        if (baseUrl.isEmpty()) {
            baseUrl = DataManager.getPref().getServer();
        }

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        restApi = retrofit.create(RestApi.class);
    }

    public MainModel getMainNetwork() {
        return mainNetwork;
    }

    public LoginModel getLoginNetwork() {
        return loginNetwork;
    }

    public ProjectModel getProjectNetwork() {
        return projectNetwork;
    }

    public SupportModel getSupportNetwork() {
        return supportNetwork;
    }

    public SettingsModel getSettingNetwork() {
        return settingNetwork;
    }

    public BankModel getBankNetwork() {
        return bankNetwork;
    }

    private class DateDeserializer implements JsonDeserializer<Date> {

        @Override
        public Date deserialize(JsonElement jsonElement, Type typeOF,
                                JsonDeserializationContext context) throws JsonParseException {
            for (String format : DATE_FORMATS) {
                try {
                    return new SimpleDateFormat(format, Locale.US).parse(jsonElement.getAsString());
                } catch (ParseException e) {
                    Log.deb("errorData = " + e.getMessage());
                }
            }
            throw new JsonParseException("Unparseable date: \"" + jsonElement.getAsString()
                    + "\". Supported formats: " + Arrays.toString(DATE_FORMATS));
        }
    }
}

