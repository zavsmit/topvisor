package ru.handh.topvisor.data.network.model.currentProject;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Group implements Parcelable {

    public static final Creator<Group> CREATOR = new Creator<Group>() {
        public Group createFromParcel(Parcel source) {
            return new Group(source);
        }

        public Group[] newArray(int size) {
            return new Group[size];
        }
    };
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("on")
    @Expose
    private String on;
    @SerializedName("status")
    @Expose
    private String status;
    private boolean isEnable;

    public Group() {
    }

    protected Group(Parcel in) {
        this.id = in.readString();
        this.name = in.readString();
        this.on = in.readString();
        this.status = in.readString();
        this.isEnable = in.readByte() != 0;
    }

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The on
     */
    public String getOn() {
        return on;
    }

    /**
     * @param on The on
     */
    public void setOn(String on) {
        this.on = on;
    }

    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isEnable() {
        return isEnable;
    }

    public void setIsEnable(boolean isEnamble) {
        this.isEnable = isEnamble;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.name);
        dest.writeString(this.on);
        dest.writeString(this.status);
        dest.writeByte(isEnable ? (byte) 1 : (byte) 0);
    }
}
