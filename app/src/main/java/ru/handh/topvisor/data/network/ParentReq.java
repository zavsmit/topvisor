package ru.handh.topvisor.data.network;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.handh.topvisor.data.network.model.ReturnResponce;

/**
 * Created by Zavsmit on 11.05.2016.
 */
public abstract class ParentReq<T> implements Callback<T> {

    private ReturnResponce<T> resp;

    public ParentReq(ReturnResponce<T> resp){
        this.resp = resp;
    }



//    @Override
//    public void onResponse(Call<T> call, Response<T> response) {
//
//    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        resp.setIsFailure(true);
        resp.setErrorMessage("Не удалось получить транзакции");
        t.getMessage();
        onFail(resp);
    }


    abstract void onFail(ReturnResponce<T> resp);
}
