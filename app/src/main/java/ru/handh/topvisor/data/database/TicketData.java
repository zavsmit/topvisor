package ru.handh.topvisor.data.database;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by sergey on 12.04.16.
 */
public class TicketData extends RealmObject {

    @PrimaryKey
    private String id;
    private String user;
    private String social_user_id;
    private String email;
    private String type;
    private String text;
    private String user_data;
    private String status;
    private String owner_readed;
    private String answerer_readed;
    private Date time;
    private String admin_writing;
    private String task_id;
    private String last_message;
    private String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getSocial_user_id() {
        return social_user_id;
    }

    public void setSocial_user_id(String social_user_id) {
        this.social_user_id = social_user_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUser_data() {
        return user_data;
    }

    public void setUser_data(String user_data) {
        this.user_data = user_data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOwner_readed() {
        return owner_readed;
    }

    public void setOwner_readed(String owner_readed) {
        this.owner_readed = owner_readed;
    }

    public String getAnswerer_readed() {
        return answerer_readed;
    }

    public void setAnswerer_readed(String answerer_readed) {
        this.answerer_readed = answerer_readed;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getAdmin_writing() {
        return admin_writing;
    }

    public void setAdmin_writing(String admin_writing) {
        this.admin_writing = admin_writing;
    }

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public String getLast_message() {
        return last_message;
    }

    public void setLast_message(String last_message) {
        this.last_message = last_message;
    }
}
