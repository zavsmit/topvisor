
package ru.handh.topvisor.data.network.model.projecPhrases;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Phrase implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("phrase_id")
    @Expose
    private String phraseId;
    @SerializedName("group_id")
    @Expose
    private String groupId;
    @SerializedName("phrase")
    @Expose
    private String phrase;
    @SerializedName("target_status")
    @Expose
    private String targetStatus;
    @SerializedName("tag")
    @Expose
    private String tag;
    @SerializedName("frequency1")
    @Expose
    private String frequency1;
    @SerializedName("frequency2")
    @Expose
    private String frequency2;
    @SerializedName("frequency3")
    @Expose
    private String frequency3;
    @SerializedName("price1")
    @Expose
    private String price1;
    @SerializedName("price2")
    @Expose
    private String price2;
    @SerializedName("price3")
    @Expose
    private String price3;
    @SerializedName("dates")
    @Expose
    private List<Date> dates = new ArrayList<>();
    @SerializedName("visitors_for_period")
    @Expose
    private String visitorsForPeriod;

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The phraseId
     */
    public String getPhraseId() {
        return phraseId;
    }

    /**
     * 
     * @param phraseId
     *     The phrase_id
     */
    public void setPhraseId(String phraseId) {
        this.phraseId = phraseId;
    }

    /**
     * 
     * @return
     *     The groupId
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * 
     * @param groupId
     *     The group_id
     */
    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    /**
     * 
     * @return
     *     The phrase
     */
    public String getPhrase() {
        return phrase;
    }

    /**
     * 
     * @param phrase
     *     The phrase
     */
    public void setPhrase(String phrase) {
        this.phrase = phrase;
    }

    /**
     * 
     * @return
     *     The targetStatus
     */
    public String getTargetStatus() {
        return targetStatus;
    }

    /**
     * 
     * @param targetStatus
     *     The target_status
     */
    public void setTargetStatus(String targetStatus) {
        this.targetStatus = targetStatus;
    }

    /**
     * 
     * @return
     *     The tag
     */
    public String getTag() {
        return tag;
    }

    /**
     * 
     * @param tag
     *     The tag
     */
    public void setTag(String tag) {
        this.tag = tag;
    }

    /**
     * 
     * @return
     *     The frequency1
     */
    public String getFrequency1() {
        return frequency1;
    }

    /**
     * 
     * @param frequency1
     *     The frequency1
     */
    public void setFrequency1(String frequency1) {
        this.frequency1 = frequency1;
    }

    /**
     * 
     * @return
     *     The frequency2
     */
    public String getFrequency2() {
        return frequency2;
    }

    /**
     * 
     * @param frequency2
     *     The frequency2
     */
    public void setFrequency2(String frequency2) {
        this.frequency2 = frequency2;
    }

    /**
     * 
     * @return
     *     The frequency3
     */
    public String getFrequency3() {
        return frequency3;
    }

    /**
     * 
     * @param frequency3
     *     The frequency3
     */
    public void setFrequency3(String frequency3) {
        this.frequency3 = frequency3;
    }

    /**
     * 
     * @return
     *     The price1
     */
    public String getPrice1() {
        return price1;
    }

    /**
     * 
     * @param price1
     *     The price1
     */
    public void setPrice1(String price1) {
        this.price1 = price1;
    }

    /**
     * 
     * @return
     *     The price2
     */
    public String getPrice2() {
        return price2;
    }

    /**
     * 
     * @param price2
     *     The price2
     */
    public void setPrice2(String price2) {
        this.price2 = price2;
    }

    /**
     * 
     * @return
     *     The price3
     */
    public String getPrice3() {
        return price3;
    }

    /**
     * 
     * @param price3
     *     The price3
     */
    public void setPrice3(String price3) {
        this.price3 = price3;
    }

    /**
     * 
     * @return
     *     The dates
     */
    public List<Date> getDates() {
        return dates;
    }

    /**
     * 
     * @param dates
     *     The dates
     */
    public void setDates(List<Date> dates) {
        this.dates = dates;
    }

    /**
     * 
     * @return
     *     The visitorsForPeriod
     */
    public String getVisitorsForPeriod() {
        return visitorsForPeriod;
    }

    /**
     * 
     * @param visitorsForPeriod
     *     The visitors_for_period
     */
    public void setVisitorsForPeriod(String visitorsForPeriod) {
        this.visitorsForPeriod = visitorsForPeriod;
    }


    public Phrase() {
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.phraseId);
        dest.writeString(this.groupId);
        dest.writeString(this.phrase);
        dest.writeString(this.targetStatus);
        dest.writeString(this.tag);
        dest.writeString(this.frequency1);
        dest.writeString(this.frequency2);
        dest.writeString(this.frequency3);
        dest.writeString(this.price1);
        dest.writeString(this.price2);
        dest.writeString(this.price3);
        dest.writeTypedList(dates);
        dest.writeString(this.visitorsForPeriod);
    }

    protected Phrase(Parcel in) {
        this.id = in.readString();
        this.phraseId = in.readString();
        this.groupId = in.readString();
        this.phrase = in.readString();
        this.targetStatus = in.readString();
        this.tag = in.readString();
        this.frequency1 = in.readString();
        this.frequency2 = in.readString();
        this.frequency3 = in.readString();
        this.price1 = in.readString();
        this.price2 = in.readString();
        this.price3 = in.readString();
        this.dates = in.createTypedArrayList(Date.CREATOR);
        this.visitorsForPeriod = in.readString();
    }

    public static final Creator<Phrase> CREATOR = new Creator<Phrase>() {
        public Phrase createFromParcel(Parcel source) {
            return new Phrase(source);
        }

        public Phrase[] newArray(int size) {
            return new Phrase[size];
        }
    };
}
