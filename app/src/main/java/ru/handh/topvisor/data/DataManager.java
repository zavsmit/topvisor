package ru.handh.topvisor.data;

import android.content.Context;

import ru.handh.topvisor.data.network.BankModel;
import ru.handh.topvisor.data.network.LoginModel;
import ru.handh.topvisor.data.network.MainModel;
import ru.handh.topvisor.data.network.ProjectModel;
import ru.handh.topvisor.data.network.SettingsModel;
import ru.handh.topvisor.data.network.SupportModel;

/**
 * Created by sergey on 24.02.16.
 * синглтон для работы с данными
 */
public class DataManager {

    private static DataManager mInstance;

    private PreferencesHelper preferencesHelper;
    private NetworkHelper networkHelper;
    private DBHelper dbHelper;

    private DataManager(Context context) {
        preferencesHelper = new PreferencesHelper(context);
        networkHelper = new NetworkHelper(context, preferencesHelper.getServer());
        dbHelper = new DBHelper(context);
    }

    public static void initInstance(Context context) {
        if (mInstance == null) {
            mInstance = new DataManager(context);
        }
    }

    //методы работы с данными
    public static PreferencesHelper getPref() {
        return mInstance.preferencesHelper;
    }

    public static DBHelper getDB() {
        return mInstance.dbHelper;
    }

    public static MainModel getMainRest() {
        return mInstance.networkHelper.getMainNetwork();
    }

    public static LoginModel getLoginRest() {
        return mInstance.networkHelper.getLoginNetwork();
    }

    public static SupportModel getSupportRest() {
        return mInstance.networkHelper.getSupportNetwork();
    }

    public static BankModel getBankRest() {
        return mInstance.networkHelper.getBankNetwork();
    }

    public static ProjectModel getProjectRest() {
        return mInstance.networkHelper.getProjectNetwork();
    }

    public static SettingsModel getSettingsRest() {
        return mInstance.networkHelper.getSettingNetwork();
    }

    public static void initRest() {
        mInstance.networkHelper.initRest();
    }

}
