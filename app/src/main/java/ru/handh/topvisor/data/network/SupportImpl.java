package ru.handh.topvisor.data.network;

import android.content.Context;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.handh.topvisor.R;
import ru.handh.topvisor.data.network.model.ModelItemMessage;
import ru.handh.topvisor.data.network.model.ModelList;
import ru.handh.topvisor.data.network.model.ModelResult;
import ru.handh.topvisor.data.network.model.ModelTicket;
import ru.handh.topvisor.data.network.model.ReturnResponce;
import ru.handh.topvisor.ui.support.SupportEndReq;

/**
 * Created by sergey on 31.03.16.
 */
public class SupportImpl implements SupportModel {
    private RestApi restApi;
    private Context context;

    public SupportImpl(RestApi restApi, Context context) {
        this.restApi = restApi;
        this.context = context;
    }

    @Override
    public void reqDialogList(int page, int count, final SupportEndReq listener) {
        final ReturnResponce<List<ModelTicket>> result = new ReturnResponce<>();

        Call<ModelList<ModelTicket>> call = restApi.reqSupport(page, count);
        call.enqueue(new Callback<ModelList<ModelTicket>>() {
            @Override
            public void onResponse(Call<ModelList<ModelTicket>> call, Response<ModelList<ModelTicket>> response) {

                result.setResult(response.body().getRows());
                listener.listDialogsResponce(result);

            }

            @Override
            public void onFailure(Call<ModelList<ModelTicket>> call, Throwable t) {
                result.setIsFailure(true);
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                t.getMessage();
                listener.listDialogsResponce(result);
            }
        });
    }

    @Override
    public void getMessages(int idTicket, final SupportEndReq listener) {
        final ReturnResponce<List<ModelItemMessage>> result = new ReturnResponce<>();

        Call<List<ModelItemMessage>> call = restApi.getMessages(idTicket);
        call.enqueue(new Callback<List<ModelItemMessage>>() {
            @Override
            public void onResponse(Call<List<ModelItemMessage>> call, Response<List<ModelItemMessage>> response) {

                result.setResult(response.body());
                listener.listMessagesResponce(result);

            }

            @Override
            public void onFailure(Call<List<ModelItemMessage>> call, Throwable t) {
                result.setIsFailure(true);
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                t.getMessage();
                listener.listMessagesResponce(result);
            }
        });
    }

    @Override
    public void removeImage(String name, final SupportEndReq listener) {


        name = name.substring(name.lastIndexOf("/") + 1, name.length());

        final ReturnResponce<ModelResult> result = new ReturnResponce<>();

        Call<ModelResult> call = restApi.reqRemoveImage(name);
        call.enqueue(new Callback<ModelResult>() {
            @Override
            public void onResponse(Call<ModelResult> call, Response<ModelResult> response) {

                result.setResult(response.body());
                listener.removiImageResponce(result);

            }

            @Override
            public void onFailure(Call<ModelResult> call, Throwable t) {
                result.setIsFailure(true);
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                t.getMessage();
                listener.removiImageResponce(result);
            }
        });
    }

    @Override
    public void getImagesList(final SupportEndReq listener) {
        final ReturnResponce<List<String>> result = new ReturnResponce<>();

        Call<List<String>> call = restApi.getLoadImage();
        call.enqueue(new Callback<List<String>>() {
            @Override
            public void onResponse(Call<List<String>> call, Response<List<String>> response) {

                result.setResult(response.body());
                listener.getImagesResponce(result);

            }

            @Override
            public void onFailure(Call<List<String>> call, Throwable t) {
                result.setIsFailure(true);
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                t.getMessage();
                listener.getImagesResponce(result);
            }
        });
    }

    @Override
    public void sendMessage(final String text, int idTicket, final SupportEndReq listener) {
        final ReturnResponce<ModelResult> result = new ReturnResponce<>();

        Call<ModelResult> call = restApi.sendMessage(text, idTicket);
        call.enqueue(new Callback<ModelResult>() {
            @Override
            public void onResponse(Call<ModelResult> call, Response<ModelResult> response) {

                result.setResult(response.body());
                listener.sendMessageResponce(result);

            }

            @Override
            public void onFailure(Call<ModelResult> call, Throwable t) {
                result.setIsFailure(true);
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                t.getMessage();
                listener.sendMessageResponce(result);
            }
        });
    }

    @Override
    public void sendNewDialogMessage(String text, final SupportEndReq listener) {
        final ReturnResponce<ModelResult> result = new ReturnResponce<>();

        Call<ModelResult> call = restApi.sendNewDialogMessage(text);
        call.enqueue(new Callback<ModelResult>() {
            @Override
            public void onResponse(Call<ModelResult> call, Response<ModelResult> response) {

                result.setResult(response.body());
                listener.sendNewDialogMessageResponce(result);

            }

            @Override
            public void onFailure(Call<ModelResult> call, Throwable t) {
                result.setIsFailure(true);
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                t.getMessage();
                listener.sendNewDialogMessageResponce(result);
            }
        });
    }

    @Override
    public void sendPhoto(File file, final SupportEndReq listener) {
        final ReturnResponce<ModelResult> result = new ReturnResponce<>();

        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);

        MultipartBody.Part body =
                MultipartBody.Part.createFormData("file", file.getName(), requestFile);

        Call<ModelResult> call = restApi.loadPhoto(body);
        call.enqueue(new Callback<ModelResult>() {
            @Override
            public void onResponse(Call<ModelResult> call, Response<ModelResult> response) {

                result.setResult(response.body());
                listener.sendPhotoResponce(result);

            }

            @Override
            public void onFailure(Call<ModelResult> call, Throwable t) {
                result.setIsFailure(true);
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                t.getMessage();
                listener.sendPhotoResponce(result);
            }
        });
    }
}
