package ru.handh.topvisor.data.network.model;

/**
 * Created by sergey on 16.02.16.
 * простые ответы о прохождении запроса
 */
public class ModelResult {
    private String result;
    private String message;
    private boolean error;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }
}
