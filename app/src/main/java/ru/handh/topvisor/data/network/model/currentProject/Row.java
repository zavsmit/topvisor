
package ru.handh.topvisor.data.network.model.currentProject;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Row implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("user")
    @Expose
    private String user;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("site")
    @Expose
    private String site;
    @SerializedName("competitor")
    @Expose
    private String competitor;
    @SerializedName("competitor_ord")
    @Expose
    private String competitorOrd;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("date")
    @Expose
    private Date date;
    @SerializedName("update")
    @Expose
    private Date update;
    @SerializedName("last_view")
    @Expose
    private Date lastView;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("status_frequency")
    @Expose
    private Boolean statusFrequency;
    @SerializedName("status_claster")
    @Expose
    private String statusClaster;
    @SerializedName("subdomains")
    @Expose
    private String subdomains;
    @SerializedName("filter")
    @Expose
    private String filter;
    @SerializedName("auto_correct")
    @Expose
    private String autoCorrect;
    @SerializedName("common_traffic")
    @Expose
    private String commonTraffic;
    @SerializedName("on")
    @Expose
    private String on;
    @SerializedName("auto_cond")
    @Expose
    private String autoCond;
    @SerializedName("wait_after_updates")
    @Expose
    private String waitAfterUpdates;
    @SerializedName("time_for_update")
    @Expose
    private String timeForUpdate;
    @SerializedName("with_snippets")
    @Expose
    private String withSnippets;
    @SerializedName("count_keywords")
    @Expose
    private String countKeywords;
    @SerializedName("count_tasks")
    @Expose
    private String countTasks;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("report_on")
    @Expose
    private String reportOn;
    @SerializedName("report_time")
    @Expose
    private String reportTime;
    @SerializedName("report_format")
    @Expose
    private String reportFormat;
    @SerializedName("guest_link_right")
    @Expose
    private String guestLinkRight;
    @SerializedName("cat_y")
    @Expose
    private String catY;
    @SerializedName("cat_dm")
    @Expose
    private String catDm;
    @SerializedName("cat_m")
    @Expose
    private String catM;
    @SerializedName("right")
    @Expose
    private String right;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("favorite")
    @Expose
    private String favorite;
    @SerializedName("tag")
    @Expose
    private String tag;
    @SerializedName("percent_of_parse")
    private String percentOfParse;
    @SerializedName("history")
    @Expose
    private List<History> history = new ArrayList<>();
    @SerializedName("searchers")
    @Expose
    private List<Searcher> searchers = new ArrayList<>();
    @SerializedName("competitors")
    @Expose
    private ArrayList<Competitor> competitors = new ArrayList<>();
    @SerializedName("groups")
    @Expose
    private ArrayList<Group> groups = new ArrayList<>();
    @SerializedName("dynamics_limit")
    @Expose
    private String dynamicsLimit;

    public String getPercentOfParse() {
        return percentOfParse;
    }

    public void setPercentOfParse(String percentOfParse) {
        this.percentOfParse = percentOfParse;
    }

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The user
     */
    public String getUser() {
        return user;
    }

    /**
     * 
     * @param user
     *     The user
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The site
     */
    public String getSite() {
        return site;
    }

    /**
     * 
     * @param site
     *     The site
     */
    public void setSite(String site) {
        this.site = site;
    }

    /**
     * 
     * @return
     *     The competitor
     */
    public String getCompetitor() {
        return competitor;
    }

    /**
     * 
     * @param competitor
     *     The competitor
     */
    public void setCompetitor(String competitor) {
        this.competitor = competitor;
    }

    /**
     * 
     * @return
     *     The competitorOrd
     */
    public String getCompetitorOrd() {
        return competitorOrd;
    }

    /**
     * 
     * @param competitorOrd
     *     The competitor_ord
     */
    public void setCompetitorOrd(String competitorOrd) {
        this.competitorOrd = competitorOrd;
    }

    /**
     * 
     * @return
     *     The comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * 
     * @param comment
     *     The comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * 
     * @return
     *     The date
     */
    public Date getDate() {
        return date;
    }

    /**
     * 
     * @param date
     *     The date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * 
     * @return
     *     The update
     */
    public Date getUpdate() {
        return update;
    }

    /**
     * 
     * @param update
     *     The update
     */
    public void setUpdate(Date update) {
        this.update = update;
    }

    /**
     * 
     * @return
     *     The lastView
     */
    public Date getLastView() {
        return lastView;
    }

    /**
     * 
     * @param lastView
     *     The last_view
     */
    public void setLastView(Date lastView) {
        this.lastView = lastView;
    }

    /**
     * 
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The statusFrequency
     */
    public Boolean getStatusFrequency() {
        return statusFrequency;
    }

    /**
     * 
     * @param statusFrequency
     *     The status_frequency
     */
    public void setStatusFrequency(Boolean statusFrequency) {
        this.statusFrequency = statusFrequency;
    }

    /**
     * 
     * @return
     *     The statusClaster
     */
    public String getStatusClaster() {
        return statusClaster;
    }

    /**
     * 
     * @param statusClaster
     *     The status_claster
     */
    public void setStatusClaster(String statusClaster) {
        this.statusClaster = statusClaster;
    }

    /**
     * 
     * @return
     *     The subdomains
     */
    public String getSubdomains() {
        return subdomains;
    }

    /**
     * 
     * @param subdomains
     *     The subdomains
     */
    public void setSubdomains(String subdomains) {
        this.subdomains = subdomains;
    }

    /**
     * 
     * @return
     *     The filter
     */
    public String getFilter() {
        return filter;
    }

    /**
     * 
     * @param filter
     *     The filter
     */
    public void setFilter(String filter) {
        this.filter = filter;
    }

    /**
     * 
     * @return
     *     The autoCorrect
     */
    public String getAutoCorrect() {
        return autoCorrect;
    }

    /**
     * 
     * @param autoCorrect
     *     The auto_correct
     */
    public void setAutoCorrect(String autoCorrect) {
        this.autoCorrect = autoCorrect;
    }

    /**
     * 
     * @return
     *     The commonTraffic
     */
    public String getCommonTraffic() {
        return commonTraffic;
    }

    /**
     * 
     * @param commonTraffic
     *     The common_traffic
     */
    public void setCommonTraffic(String commonTraffic) {
        this.commonTraffic = commonTraffic;
    }

    /**
     * 
     * @return
     *     The on
     */
    public String getOn() {
        return on;
    }

    /**
     * 
     * @param on
     *     The on
     */
    public void setOn(String on) {
        this.on = on;
    }

    /**
     * 
     * @return
     *     The autoCond
     */
    public String getAutoCond() {
        return autoCond;
    }

    /**
     * 
     * @param autoCond
     *     The auto_cond
     */
    public void setAutoCond(String autoCond) {
        this.autoCond = autoCond;
    }

    /**
     * 
     * @return
     *     The waitAfterUpdates
     */
    public String getWaitAfterUpdates() {
        return waitAfterUpdates;
    }

    /**
     * 
     * @param waitAfterUpdates
     *     The wait_after_updates
     */
    public void setWaitAfterUpdates(String waitAfterUpdates) {
        this.waitAfterUpdates = waitAfterUpdates;
    }

    /**
     * 
     * @return
     *     The timeForUpdate
     */
    public String getTimeForUpdate() {
        return timeForUpdate;
    }

    /**
     * 
     * @param timeForUpdate
     *     The time_for_update
     */
    public void setTimeForUpdate(String timeForUpdate) {
        this.timeForUpdate = timeForUpdate;
    }

    /**
     * 
     * @return
     *     The withSnippets
     */
    public String getWithSnippets() {
        return withSnippets;
    }

    /**
     * 
     * @param withSnippets
     *     The with_snippets
     */
    public void setWithSnippets(String withSnippets) {
        this.withSnippets = withSnippets;
    }

    /**
     * 
     * @return
     *     The countKeywords
     */
    public String getCountKeywords() {
        return countKeywords;
    }

    /**
     * 
     * @param countKeywords
     *     The count_keywords
     */
    public void setCountKeywords(String countKeywords) {
        this.countKeywords = countKeywords;
    }

    /**
     * 
     * @return
     *     The countTasks
     */
    public String getCountTasks() {
        return countTasks;
    }

    /**
     * 
     * @param countTasks
     *     The count_tasks
     */
    public void setCountTasks(String countTasks) {
        this.countTasks = countTasks;
    }

    /**
     * 
     * @return
     *     The price
     */
    public String getPrice() {
        return price;
    }

    /**
     * 
     * @param price
     *     The price
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * 
     * @return
     *     The reportOn
     */
    public String getReportOn() {
        return reportOn;
    }

    /**
     * 
     * @param reportOn
     *     The report_on
     */
    public void setReportOn(String reportOn) {
        this.reportOn = reportOn;
    }

    /**
     * 
     * @return
     *     The reportTime
     */
    public String getReportTime() {
        return reportTime;
    }

    /**
     * 
     * @param reportTime
     *     The report_time
     */
    public void setReportTime(String reportTime) {
        this.reportTime = reportTime;
    }

    /**
     * 
     * @return
     *     The reportFormat
     */
    public String getReportFormat() {
        return reportFormat;
    }

    /**
     * 
     * @param reportFormat
     *     The report_format
     */
    public void setReportFormat(String reportFormat) {
        this.reportFormat = reportFormat;
    }

    /**
     * 
     * @return
     *     The guestLinkRight
     */
    public String getGuestLinkRight() {
        return guestLinkRight;
    }

    /**
     * 
     * @param guestLinkRight
     *     The guest_link_right
     */
    public void setGuestLinkRight(String guestLinkRight) {
        this.guestLinkRight = guestLinkRight;
    }

    /**
     * 
     * @return
     *     The catY
     */
    public String getCatY() {
        return catY;
    }

    /**
     * 
     * @param catY
     *     The cat_y
     */
    public void setCatY(String catY) {
        this.catY = catY;
    }

    /**
     * 
     * @return
     *     The catDm
     */
    public String getCatDm() {
        return catDm;
    }

    /**
     * 
     * @param catDm
     *     The cat_dm
     */
    public void setCatDm(String catDm) {
        this.catDm = catDm;
    }

    /**
     * 
     * @return
     *     The catM
     */
    public String getCatM() {
        return catM;
    }

    /**
     * 
     * @param catM
     *     The cat_m
     */
    public void setCatM(String catM) {
        this.catM = catM;
    }

    /**
     * 
     * @return
     *     The right
     */
    public String getRight() {
        return right;
    }

    /**
     * 
     * @param right
     *     The right
     */
    public void setRight(String right) {
        this.right = right;
    }

    /**
     * 
     * @return
     *     The email
     */
    public String getEmail() {
        return email;
    }

    /**
     * 
     * @param email
     *     The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 
     * @return
     *     The favorite
     */
    public String getFavorite() {
        return favorite;
    }

    /**
     * 
     * @param favorite
     *     The favorite
     */
    public void setFavorite(String favorite) {
        this.favorite = favorite;
    }

    /**
     * 
     * @return
     *     The tag
     */
    public String getTag() {
        return tag;
    }

    /**
     * 
     * @param tag
     *     The tag
     */
    public void setTag(String tag) {
        this.tag = tag;
    }

    /**
     * 
     * @return
     *     The history
     */
    public List<History> getHistory() {
        return history;
    }

    /**
     * 
     * @param history
     *     The history
     */
    public void setHistory(List<History> history) {
        this.history = history;
    }

    /**
     * 
     * @return
     *     The searchers
     */
    public List<Searcher> getSearchers() {
        return searchers;
    }

    /**
     * 
     * @param searchers
     *     The searchers
     */
    public void setSearchers(List<Searcher> searchers) {
        this.searchers = searchers;
    }

    /**
     * 
     * @return
     *     The competitors
     */
    public ArrayList<Competitor> getCompetitors() {
        return competitors;
    }

    /**
     * 
     * @param competitors
     *     The competitors
     */
    public void setCompetitors(ArrayList<Competitor> competitors) {
        this.competitors = competitors;
    }

    /**
     * 
     * @return
     *     The groups
     */
    public ArrayList<Group> getGroups() {
        return groups;
    }

    /**
     * 
     * @param groups
     *     The groups
     */
    public void setGroups(ArrayList<Group> groups) {
        this.groups = groups;
    }

    /**
     * 
     * @return
     *     The dynamicsLimit
     */
    public String getDynamicsLimit() {
        return dynamicsLimit;
    }

    /**
     * 
     * @param dynamicsLimit
     *     The dynamics_limit
     */
    public void setDynamicsLimit(String dynamicsLimit) {
        this.dynamicsLimit = dynamicsLimit;
    }


    public Row() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.user);
        dest.writeString(this.name);
        dest.writeString(this.site);
        dest.writeString(this.competitor);
        dest.writeString(this.competitorOrd);
        dest.writeString(this.comment);
        dest.writeLong(date != null ? date.getTime() : -1);
        dest.writeLong(update != null ? update.getTime() : -1);
        dest.writeLong(lastView != null ? lastView.getTime() : -1);
        dest.writeString(this.status);
        dest.writeValue(this.statusFrequency);
        dest.writeString(this.statusClaster);
        dest.writeString(this.subdomains);
        dest.writeString(this.filter);
        dest.writeString(this.autoCorrect);
        dest.writeString(this.commonTraffic);
        dest.writeString(this.on);
        dest.writeString(this.autoCond);
        dest.writeString(this.waitAfterUpdates);
        dest.writeString(this.timeForUpdate);
        dest.writeString(this.withSnippets);
        dest.writeString(this.countKeywords);
        dest.writeString(this.countTasks);
        dest.writeString(this.price);
        dest.writeString(this.reportOn);
        dest.writeString(this.reportTime);
        dest.writeString(this.reportFormat);
        dest.writeString(this.guestLinkRight);
        dest.writeString(this.catY);
        dest.writeString(this.catDm);
        dest.writeString(this.catM);
        dest.writeString(this.right);
        dest.writeString(this.email);
        dest.writeString(this.favorite);
        dest.writeString(this.tag);
        dest.writeString(this.percentOfParse);
        dest.writeTypedList(history);
        dest.writeTypedList(searchers);
        dest.writeTypedList(competitors);
        dest.writeTypedList(groups);
        dest.writeString(this.dynamicsLimit);
    }

    protected Row(Parcel in) {
        this.id = in.readString();
        this.user = in.readString();
        this.name = in.readString();
        this.site = in.readString();
        this.competitor = in.readString();
        this.competitorOrd = in.readString();
        this.comment = in.readString();
        long tmpDate = in.readLong();
        this.date = tmpDate == -1 ? null : new Date(tmpDate);
        long tmpUpdate = in.readLong();
        this.update = tmpUpdate == -1 ? null : new Date(tmpUpdate);
        long tmpLastView = in.readLong();
        this.lastView = tmpLastView == -1 ? null : new Date(tmpLastView);
        this.status = in.readString();
        this.statusFrequency = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.statusClaster = in.readString();
        this.subdomains = in.readString();
        this.filter = in.readString();
        this.autoCorrect = in.readString();
        this.commonTraffic = in.readString();
        this.on = in.readString();
        this.autoCond = in.readString();
        this.waitAfterUpdates = in.readString();
        this.timeForUpdate = in.readString();
        this.withSnippets = in.readString();
        this.countKeywords = in.readString();
        this.countTasks = in.readString();
        this.price = in.readString();
        this.reportOn = in.readString();
        this.reportTime = in.readString();
        this.reportFormat = in.readString();
        this.guestLinkRight = in.readString();
        this.catY = in.readString();
        this.catDm = in.readString();
        this.catM = in.readString();
        this.right = in.readString();
        this.email = in.readString();
        this.favorite = in.readString();
        this.tag = in.readString();
        this.percentOfParse = in.readString();
        this.history = in.createTypedArrayList(History.CREATOR);
        this.searchers = in.createTypedArrayList(Searcher.CREATOR);
        this.competitors = in.createTypedArrayList(Competitor.CREATOR);
        this.groups = in.createTypedArrayList(Group.CREATOR);
        this.dynamicsLimit = in.readString();
    }

    public static final Creator<Row> CREATOR = new Creator<Row>() {
        public Row createFromParcel(Parcel source) {
            return new Row(source);
        }

        public Row[] newArray(int size) {
            return new Row[size];
        }
    };


}
