package ru.handh.topvisor.data.network.model;

import java.util.List;

/**
 * Created by sergey on 27.03.16.
 */
public class ModelResultType {
    private List<String> result;
    private String message;
    private boolean error;

    public List<String> getResult() {
        return result;
    }

    public void setResult(List<String> result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }
}
