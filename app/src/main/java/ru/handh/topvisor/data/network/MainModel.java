package ru.handh.topvisor.data.network;

import java.util.List;

import ru.handh.topvisor.ui.main.MainEndReq;
import ru.handh.topvisor.ui.menu.MenuEndReq;

/**
 * Created by sergey on 10.02.16.
 * интерфейсы для модел главного экрана
 */
public interface MainModel {

    void reqUserData(MenuEndReq listener);

    void reqTikets(MenuEndReq listener);

    void reqGetProjects(MainEndReq listener);

    void reqPriceProjects(MainEndReq listener, List<Integer> listIds);

    void reqInspectionProjects(MainEndReq listener, int listIds);

    void reqRemoveProjects(MainEndReq listener, int listIds);

    void reqAddProjects(MainEndReq listener, String name);

    void reqRenameProjects(MainEndReq listener, int listIds, String name);

    void reqRecoveryProjects(MainEndReq listener, int listIds);

    void initHeader();

    int getIdResLabel();

    String getHeaderText();

    void sendGCMToken();

    void removeGCMToken();
}
