package ru.handh.topvisor.data.network.model.projecPhrases;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Rows {

    @SerializedName("total")
    @Expose
    private String total;
    @SerializedName("phrases")
    @Expose
    private HashMap<String, Phrase> phrases = new HashMap<>();
//    private List<Phrase> phrases = new ArrayList<>();
    @SerializedName("all_dates")
    @Expose
    private List<String> allDates = new ArrayList<>();
    @SerializedName("is_empty")
    @Expose
    private Boolean isEmpty;
    @SerializedName("compare")
    @Expose
    private String compare;
    @SerializedName("scheme")
    @Expose
    private Scheme scheme;

    /**
     * @return The total
     */
    public String getTotal() {
        return total;
    }

    /**
     * @param total The total
     */
    public void setTotal(String total) {
        this.total = total;
    }

    public HashMap<String, Phrase> getPhrases() {
        return phrases;
    }

    public void setPhrases(HashMap<String, Phrase> phrases) {
        this.phrases = phrases;
    }

    /**
     * @return The allDates
     */
    public List<String> getAllDates() {
        return allDates;
    }

    /**
     * @param allDates The all_dates
     */
    public void setAllDates(List<String> allDates) {
        this.allDates = allDates;
    }

    /**
     * @return The isEmpty
     */
    public Boolean getIsEmpty() {
        return isEmpty;
    }

    /**
     * @param isEmpty The is_empty
     */
    public void setIsEmpty(Boolean isEmpty) {
        this.isEmpty = isEmpty;
    }

    /**
     * @return The compare
     */
    public String getCompare() {
        return compare;
    }

    /**
     * @param compare The compare
     */
    public void setCompare(String compare) {
        this.compare = compare;
    }

    /**
     * @return The scheme
     */
    public Scheme getScheme() {
        return scheme;
    }

    /**
     * @param scheme The scheme
     */
    public void setScheme(Scheme scheme) {
        this.scheme = scheme;
    }

}
