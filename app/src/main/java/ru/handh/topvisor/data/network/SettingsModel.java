package ru.handh.topvisor.data.network;

import java.util.ArrayList;
import java.util.List;

import ru.handh.topvisor.ui.settingsProject.SettingsEndReq;
import ru.handh.topvisor.ui.settingsProject.interval.IntervalReqResult;
import ru.handh.topvisor.ui.settingsProject.searchSystems.SearcherResult;
import ru.handh.topvisor.ui.settingsProject.searchSystems.searchRegion.SearchRegionResult;

/**
 * Created by sergey on 22.03.16.
 */
public interface SettingsModel {
    void reqAddCompetitors(String name, String idProject, SettingsEndReq listener);

    void reqRemoveCompetitors(String idCompetitors, SettingsEndReq listener);

    void reqRemoveGroup(String idGroup,int position, SettingsEndReq listener);

    void reqCheckGroup(String idProject, String on, String idGroup, SettingsEndReq listener);

    void reqAddGroup(String idProject, String name, SettingsEndReq listener);

    void reqRenameGroup(String idProject, String name, String idGroup, SettingsEndReq listener);

    void reqAddPS(String idProject, String searcher, SearcherResult listener);

    void reqCheckedPS(String idSearcher, boolean isChecked, SearcherResult listener, int position);

    void reqRemovePS(String idSearcher, SearcherResult listener, int position, String type);

    void reqChangeItemsPS(String idSearcher, SearcherResult listener, ArrayList<String> listItems, ArrayList<Integer> positions);

    void resSearchRegions(String typeSearcher, String term, SearchRegionResult listener);

    void reqAddRegions(String idSearcher, List<String> term, SearchRegionResult listener);

    void reqChangeInterval(int on, String idProject, String timeForUpdate, String waitAfrepUpdate, IntervalReqResult listener);

    void reqAutoCond(String selcted, String idProject, IntervalReqResult listener);

}
