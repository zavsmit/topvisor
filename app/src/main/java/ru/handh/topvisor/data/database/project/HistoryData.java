package ru.handh.topvisor.data.database.project;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by sergey on 28.04.16.
 * для истории
 */
public class HistoryData extends RealmObject{
    @PrimaryKey
    private String id;
    private String projectId;
    private String cy;
    private String pr;
    private String indexedY;
    private String indexedG;
    private String trafficY;
    private String trafficG;
    private String trafficM;
    private String backlinks;
    private String backlinksAh;
    private Date date;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getCy() {
        return cy;
    }

    public void setCy(String cy) {
        this.cy = cy;
    }

    public String getPr() {
        return pr;
    }

    public void setPr(String pr) {
        this.pr = pr;
    }

    public String getIndexedY() {
        return indexedY;
    }

    public void setIndexedY(String indexedY) {
        this.indexedY = indexedY;
    }

    public String getIndexedG() {
        return indexedG;
    }

    public void setIndexedG(String indexedG) {
        this.indexedG = indexedG;
    }

    public String getTrafficY() {
        return trafficY;
    }

    public void setTrafficY(String trafficY) {
        this.trafficY = trafficY;
    }

    public String getTrafficG() {
        return trafficG;
    }

    public void setTrafficG(String trafficG) {
        this.trafficG = trafficG;
    }

    public String getTrafficM() {
        return trafficM;
    }

    public void setTrafficM(String trafficM) {
        this.trafficM = trafficM;
    }

    public String getBacklinks() {
        return backlinks;
    }

    public void setBacklinks(String backlinks) {
        this.backlinks = backlinks;
    }

    public String getBacklinksAh() {
        return backlinksAh;
    }

    public void setBacklinksAh(String backlinksAh) {
        this.backlinksAh = backlinksAh;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
