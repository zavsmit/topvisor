
package ru.handh.topvisor.data.network.model.db.currentProject;

import io.realm.RealmList;
import io.realm.RealmObject;

public class SearcherDB extends RealmObject {

    private String id;
    private String projectId;
    private String searcher;
    private String domain;
    private String enabled;
    private String ord;
    private String name;
    private RealmList<RegionDB> regionDBs = new RealmList<RegionDB>();

    /**
     *
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     *     The projectId
     */
    public String getProjectId() {
        return projectId;
    }

    /**
     *
     * @param projectId
     *     The project_id
     */
    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    /**
     *
     * @return
     *     The searcher
     */
    public String getSearcher() {
        return searcher;
    }

    /**
     *
     * @param searcher
     *     The searcher
     */
    public void setSearcher(String searcher) {
        this.searcher = searcher;
    }

    /**
     *
     * @return
     *     The domain
     */
    public String getDomain() {
        return domain;
    }

    /**
     *
     * @param domain
     *     The domain
     */
    public void setDomain(String domain) {
        this.domain = domain;
    }

    /**
     *
     * @return
     *     The enabled
     */
    public String getEnabled() {
        return enabled;
    }

    /**
     *
     * @param enabled
     *     The enabled
     */
    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    /**
     *
     * @return
     *     The ord
     */
    public String getOrd() {
        return ord;
    }

    /**
     *
     * @param ord
     *     The ord
     */
    public void setOrd(String ord) {
        this.ord = ord;
    }

    /**
     *
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     *     The regionDBs
     */
    public RealmList<RegionDB> getRegionDBs() {
        return regionDBs;
    }

    /**
     *
     * @param regionDBs
     *     The regionDBs
     */
    public void setRegionDBs(RealmList<RegionDB> regionDBs) {
        this.regionDBs = regionDBs;
    }

}
