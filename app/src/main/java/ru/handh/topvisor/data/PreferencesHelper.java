package ru.handh.topvisor.data;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import java.util.Locale;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.network.model.Labels;
import ru.handh.topvisor.data.network.model.Projects;
import ru.handh.topvisor.ui.login.LoginViewActivity;
import ru.handh.topvisor.utils.Constants;

/**
 * Created by sergey on 27.02.16.
 * работа с шаред преф
 */
public class PreferencesHelper {
    public static final String PREF_FILE_NAME = "android_topvisor_pref_file";

    private final SharedPreferences mPref;
    private Context context;

    public PreferencesHelper(Context context) {
        mPref = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        this.context = context;
    }

    private void clear() {
        mPref.edit().clear().apply();
    }

    public String getTokenApp() {
        return mPref.getString("token", "");
    }

    public void setTokenApp(String token) {
        SharedPreferences.Editor ed = mPref.edit();
        ed.putString("token", token);
        ed.apply();
    }

    public String getServerType() {
        return mPref.getString("serverType", "");
    }

    public void setServerType(String serverType) {
        SharedPreferences.Editor ed = mPref.edit();
        ed.putString("serverType", serverType);
        ed.apply();
    }

    public String getGCMToken() {
        return mPref.getString("gcmToken", "");
    }

    public void setGCMToken(String gcmToken) {
        SharedPreferences.Editor ed = mPref.edit();
        ed.putString("gcmToken", gcmToken);
        ed.apply();
    }

    public String getUserId() {
        return mPref.getString("userId", "");
    }

    public void setUserId(String userId) {
        SharedPreferences.Editor ed = mPref.edit();
        ed.putString("userId", userId);
        ed.apply();
    }

    public Projects getMainSettingsFolder() {

        return new Projects(mPref.getInt("idFolderPosition", 0),
                mPref.getString("FolderPosition", "my"),
                mPref.getString("nameFolderPosition", context.getString(R.string.myProject)));

    }

    public void setMainSettingsFolder(Projects folderProject) {
        SharedPreferences.Editor ed = mPref.edit();
        ed.putString("nameFolderPosition", folderProject.name);
        ed.putString("FolderPosition", folderProject.folder);
        ed.putInt("idFolderPosition", folderProject.id);
        ed.apply();
    }

    public boolean getVisibleHeaderProject() {
        return mPref.getBoolean("VisibleHeaderProject", true);
    }

    public void setVisibleHeaderProject(boolean isVisible) {
        SharedPreferences.Editor ed = mPref.edit();
        ed.putBoolean("VisibleHeaderProject", isVisible);
        ed.apply();
    }

    public Labels getMainSettingsTab() {
        return new Labels(mPref.getInt("idLabels", 0), "", mPref.getInt("iconLabels", R.drawable.label_all_vector));
    }

    public void setMainSettingsTab(Labels labels) {
        SharedPreferences.Editor ed = mPref.edit();
        ed.putInt("idLabels", labels.id);
        ed.putInt("iconLabels", labels.icon);
        ed.apply();
    }

    public void logoutApp(Activity activity) {

        clear();
        DataManager.getDB().removeAll();
        DataManager.getMainRest().removeGCMToken();
        Intent intent = new Intent(activity, LoginViewActivity.class);
        activity.startActivity(intent);
        activity.finish();
    }

    public String getServer() {

        switch (getServerType()) {
            case "1":
                return Constants.SERVER_RU;
            case "2":
                return Constants.SERVER_US;
        }

        if (Locale.getDefault().getDisplayLanguage().equals(Constants.RU_LANGUAGE)) {
            return Constants.SERVER_RU;
        } else {
            return Constants.SERVER_US;
        }
    }

    public String getServerShort() {

        switch (getServerType()) {
            case "1":
                return Constants.RU;
            case "2":
                return Constants.US;
        }

        if (Locale.getDefault().getDisplayLanguage().equals(Constants.RU_LANGUAGE)) {
            return Constants.RU;
        } else {
            return Constants.US;
        }
    }
}
