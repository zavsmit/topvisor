package ru.handh.topvisor.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by sergey on 31.03.16.
 */
public class ModelItemMessage {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("user")
    @Expose
    private String user;
    @SerializedName("ticket_id")
    @Expose
    private String ticketId;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("readed")
    @Expose
    private String readed;
    @SerializedName("data")
    @Expose
    private String data;
    @SerializedName("time")
    @Expose
    private Date time;
    @SerializedName("avatar")
    @Expose
    private String avatar;
    @SerializedName("login")
    @Expose
    private String login;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("online")
    @Expose
    private Boolean online;
    @SerializedName("lastactive")
    @Expose
    private Date lastactive;
    @SerializedName("files")
    @Expose
    private List<String> files = new ArrayList<>();

    /**
     *
     * @return
     * The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The user
     */
    public String getUser() {
        return user;
    }

    /**
     *
     * @param user
     * The user
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     *
     * @return
     * The ticketId
     */
    public String getTicketId() {
        return ticketId;
    }

    /**
     *
     * @param ticketId
     * The ticket_id
     */
    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    /**
     *
     * @return
     * The text
     */
    public String getText() {
        return text;
    }

    /**
     *
     * @param text
     * The text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     *
     * @return
     * The readed
     */
    public String getReaded() {
        return readed;
    }

    /**
     *
     * @param readed
     * The readed
     */
    public void setReaded(String readed) {
        this.readed = readed;
    }

    /**
     *
     * @return
     * The data
     */
    public String getData() {
        return data;
    }

    /**
     *
     * @param data
     * The data
     */
    public void setData(String data) {
        this.data = data;
    }

    /**
     *
     * @return
     * The time
     */
    public Date getTime() {
        return time;
    }

    /**
     *
     * @param time
     * The time
     */
    public void setTime(Date time) {
        this.time = time;
    }

    /**
     *
     * @return
     * The avatar
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     *
     * @param avatar
     * The avatar
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    /**
     *
     * @return
     * The login
     */
    public String getLogin() {
        return login;
    }

    /**
     *
     * @param login
     * The login
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The online
     */
    public Boolean getOnline() {
        return online;
    }

    /**
     *
     * @param online
     * The online
     */
    public void setOnline(Boolean online) {
        this.online = online;
    }

    /**
     *
     * @return
     * The lastactive
     */
    public Date getLastactive() {
        return lastactive;
    }

    /**
     *
     * @param lastactive
     * The lastactive
     */
    public void setLastactive(Date lastactive) {
        this.lastactive = lastactive;
    }

    /**
     *
     * @return
     * The files
     */
    public List<String> getFiles() {
        return files;
    }

    /**
     *
     * @param files
     * The files
     */
    public void setFiles(List<String> files) {
        this.files.addAll(files);
    }

}