package ru.handh.topvisor.data.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sergey on 24.03.16.
 */
public class SearcherItemDialog implements Parcelable {
    private String idSearcher;
    private String nameSearcher;

    public SearcherItemDialog(String idSearcher, String nameSearcher){
        this.idSearcher = idSearcher;
        this.nameSearcher = nameSearcher;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.idSearcher);
        dest.writeString(this.nameSearcher);
    }

    protected SearcherItemDialog(Parcel in) {
        this.idSearcher = in.readString();
        this.nameSearcher = in.readString();
    }

    public static final Parcelable.Creator<SearcherItemDialog> CREATOR = new Parcelable.Creator<SearcherItemDialog>() {
        @Override
        public SearcherItemDialog createFromParcel(Parcel source) {
            return new SearcherItemDialog(source);
        }

        @Override
        public SearcherItemDialog[] newArray(int size) {
            return new SearcherItemDialog[size];
        }
    };

    public String getIdSearcher() {
        return idSearcher;
    }

    public void setIdSearcher(String idSearcher) {
        this.idSearcher = idSearcher;
    }

    public String getNameSearcher() {
        return nameSearcher;
    }

    public void setNameSearcher(String nameSearcher) {
        this.nameSearcher = nameSearcher;
    }
}