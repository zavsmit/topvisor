package ru.handh.topvisor.data.network.model;

import java.util.Date;

/**
 * Created by sergey on 08.02.16.
 * для данных в боковом меню
 */
public class ModelUserData {

    private String id;
    private String auth;
    private Date date_reg;
    private String email;
    private String name;
    private String avatar;
    private Date lastactive;
    private String ref;
    private String domain;
    private double balance;
    private String yandex_xml_balance;
    private String warning_message;
    private Tarif tariff;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAuth() {
        return auth;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }

    public Date getDate_reg() {
        return date_reg;
    }

    public void setDate_reg(Date date_reg) {
        this.date_reg = date_reg;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Date getLastactive() {
        return lastactive;
    }

    public void setLastactive(Date lastactive) {
        this.lastactive = lastactive;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getYandex_xml_balance() {
        return yandex_xml_balance;
    }

    public void setYandex_xml_balance(String yandex_xml_balance) {
        this.yandex_xml_balance = yandex_xml_balance;
    }

    public String getWarning_message() {
        return warning_message;
    }

    public void setWarning_message(String warning_message) {
        this.warning_message = warning_message;
    }

    public Tarif getTariff() {
        return tariff;
    }

    public void setTariff(Tarif tariff) {
        this.tariff = tariff;
    }

    public class Tarif {
        private double balance_t;
        private String tariff;
        private String discount;

        public double getBalance_t() {
            return balance_t;
        }

        public void setBalance_t(double balance_t) {
            this.balance_t = balance_t;
        }

        public String getTariff() {
            return tariff;
        }

        public void setTariff(String tariff) {
            this.tariff = tariff;
        }

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }
    }
}
