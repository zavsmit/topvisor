package ru.handh.topvisor.data.network;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import ru.handh.topvisor.data.network.model.ModelAuth;
import ru.handh.topvisor.data.network.model.ModelBankPrice;
import ru.handh.topvisor.data.network.model.ModelCompetitorsAdd;
import ru.handh.topvisor.data.network.model.ModelItemBank;
import ru.handh.topvisor.data.network.model.ModelItemMessage;
import ru.handh.topvisor.data.network.model.ModelList;
import ru.handh.topvisor.data.network.model.ModelLocation;
import ru.handh.topvisor.data.network.model.ModelPriceForProgects;
import ru.handh.topvisor.data.network.model.ModelPriseInspectFilter;
import ru.handh.topvisor.data.network.model.ModelResult;
import ru.handh.topvisor.data.network.model.ModelResultPS;
import ru.handh.topvisor.data.network.model.ModelResultType;
import ru.handh.topvisor.data.network.model.ModelSearchRegion;
import ru.handh.topvisor.data.network.model.ModelTicket;
import ru.handh.topvisor.data.network.model.ModelUserData;
import ru.handh.topvisor.data.network.model.currentProject.ModelCurrentProject;
import ru.handh.topvisor.data.network.model.projecDates.ModelProjectDates;
import ru.handh.topvisor.data.network.model.projecPhrases.ModelProjectPhrases;

/**
 * Created by sergey on 02.02.16.
 * запросы к серверу
 */
public interface RestApi {

    @FormUrlEncoded
    @POST("/ajax/get.php?module=user&func=auth")
    Call<ModelAuth> getAuth(@Field("authorisation_login") String login, @Field("authorisation_pass") String pass);

    @FormUrlEncoded
    @POST("/ajax/get.php?getFormat=json_object&module=user&func=domain")
    Call<ModelLocation> getLocale(@Field("email") String email);

    @FormUrlEncoded
    @POST("/ajax/edit.php?getFormat=json_object&module=user&func=retrievePass")
    Call<ModelResult> getSendForgetPass(@Field("email") String email, @Field("oper") String edit, @Field("id") String id);


    @FormUrlEncoded
    @POST("/ajax/edit.php?getFormat=json_object&module=user&func=registration")
    Call<ModelResult> registration(@Field("email") String email, @Field("oper") String add, @Field("id") String id);

    @POST("/ajax/get.php?getFormat=json&module=user&func=mobile")
    Call<ModelUserData> getUserData();

    @FormUrlEncoded
    @POST("/login/")
    Call<Void> getAuthSocialNetwork(@Field("token") String token);

    @FormUrlEncoded
    @POST("/login/")
    Call<Void> getAuthOnToken(@Field("email") String email, @Field("code") String code,
                              @Field("fromApp") int fromApp, @Field("params") String params);

    @FormUrlEncoded
    @POST("/ajax/get.php?getFormat=jqgrid&module=mod_tickets&func=")
//    @POST("/ajax/get.php?getFormat=json_object&module=mod_tickets&func=")
    Call<ModelList<ModelTicket>> getTikets(@Field("page") String add, @Field("rows") String id);

    @FormUrlEncoded
    @POST("/ajax/get.php")
    Call<ModelCurrentProject> getProjects(
            @Field("folder") String folder,
            @Field("getFormat") String getFormat,
            @Field("module") String module
            , @Field("page") String page
            , @Field("rows") String rows
            , @Field("tag") String tag
            , @Field("func") String func);

    @FormUrlEncoded
    @POST("/ajax/get.php?getFormat=json&module=mod_keywords&func=multi_parse_price&apply_discount=1")
    Call<ModelPriceForProgects> getProjectPrice(@Field("search_json") String search);

    @FormUrlEncoded
    @POST("/ajax/edit.php?getFormat=json&module=mod_keywords&func=parse_task")
    Call<ModelResult> inspectionProject(@Field("oper") String edit, @Field("id") String id);

    @FormUrlEncoded
    @POST("/ajax/edit.php?getFormat=json&module=mod_projects")
    Call<ModelResult> removeProject(@Field("oper") String del, @Field("id") String id);

    @FormUrlEncoded
    @POST("/ajax/edit.php?getFormat=json&module=mod_projects")
    Call<ModelResult> renameProject(@Field("oper") String edit, @Field("id") String id, @Field("name") String name);

    @FormUrlEncoded
    @POST("/ajax/edit.php?getFormat=json&module=mod_projects")
    Call<ModelResult> recoveryProject(@Field("oper") String edit, @Field("id") String id, @Field("on") String on);

    @FormUrlEncoded
    @POST("/ajax/edit.php?getFormat=json&module=mod_projects&oper=add&id=")
    Call<ModelResult> addProject(@Field("site") String site);


    @FormUrlEncoded
    @POST("/ajax/get.php?getFormat=json_pager&module=mod_keywords&func=history")
    Call<ModelProjectDates> getListDateProject(
            @Field("project_id") String project_id,
            @Field("searcher") String searcher_data,
            @Field("competitor_id") String competitor_id,
            @Field("region_key") String region_key,
            @Field("region_lang") String region_lang,
            @Field("rows") String rows,
            @Field("page") String page,
            @Field("group_id") String group_id);

    @FormUrlEncoded
    @POST("/ajax/get.php?getFormat=json_pager&module=mod_keywords&func=history")
    Call<ModelProjectPhrases> getListPhrasesProject(
            @Field("rows") String rows,
            @Field("project_id") String project_id,
            @Field("searcher") String searcher_data,
            @Field("region_key") String region_key,
            @Field("region_lang") String region_lang,
            @Field("page") String page,
            @Field("group_id") String group_id,
            @Field("range") String range,
            @Field("date1") String date1,
            @Field("date2") String date2,
            @Field("competitor_id") String competitor_id);

    @FormUrlEncoded
    @POST("/ajax/get.php?getFormat=json_pager&module=mod_projects&func=mobile")
    Call<ModelCurrentProject> getItemProject(@Field("search_json") String searchJson);

    @FormUrlEncoded
    @POST("/ajax/edit.php?getFormat=json_pager&module=mod_keywords")
    Call<ModelResult> reqRemovePhrase(
            @Field("oper") String del,
            @Field("project_id") String projectId,
            @Field("id") String phraseId);

    @FormUrlEncoded
    @POST("/ajax/edit.php?getFormat=json_pager&module=mod_keywords")
    Call<ModelResult> reqAddPhrase(
            @Field("project_id") String projectId,
            @Field("group_id") String groupId,
            @Field("phrase") String phrase,
            @Field("oper") String add,
            @Field("id") String idEmpty);


    @FormUrlEncoded
    @POST("/ajax/get.php?getFormat=json_pager&module=mod_keywords&func=parse_price")
    Call<ModelPriseInspectFilter> loadPriceOnFilter(
            @Field("search_json") String searchJson,
            @Field("apply_discount") String applyDiscount,
            @Field("searcher") String searcher,
            @Field("searcher_domen") String searcherDomen,
            @Field("region_key") String regionKey,
            @Field("region_lang") String regionLang,
            @Field("group_id") String groupId);

    @FormUrlEncoded
    @POST("/ajax/edit.php?getFormat=json&module=mod_keywords&func=parse_task")
    Call<ModelResult> inspectOnFilter(
            @Field("oper") String edit,
            @Field("searcher") String searcher,
            @Field("searcher_domen") String searcherDomen,
            @Field("region_key") String regionKey,
            @Field("region_lang") String regionLang,
            @Field("group_id") String groupId,
            @Field("id") String idProject);

    @FormUrlEncoded
    @POST("/ajax/edit.php?getFormat=json_object&module=mod_projects&func=competitor")
    Call<ModelCompetitorsAdd> addCompetitor(
            @Field("oper") String add,
            @Field("competitors_sites") String name,
            @Field("project_id") String idProject,
            @Field("id") String id);

    @FormUrlEncoded
    @POST("/ajax/edit.php?getFormat=json_object&module=func&func=competitor&oper=del")
    Call<ModelResult> removeCompetitor(
            @Field("id") String id);

    @FormUrlEncoded
    @POST("/ajax/edit.php?getFormat=json_object&module=mod_keywords&func=group")
    Call<ModelResult> addGroup(//1088128
                               @Field("oper") String add,
                               @Field("name") String name,
                               @Field("project_id") String idProject,
                               @Field("ord") String ord,
                               @Field("on") String on);


    @FormUrlEncoded
    @POST("/ajax/edit.php?getFormat=json_object&module=mod_keywords&func=group")
    Call<ModelResult> removeGroup(
            @Field("oper") String del,
            @Field("id") String id);

    @FormUrlEncoded
    @POST("/ajax/edit.php?getFormat=json_object&module=mod_keywords&func=group")
    Call<ModelResult> renameGroup(
            @Field("oper") String edit,
            @Field("name") String name,
            @Field("project_id") String idProject,
            @Field("id") String id);

    @FormUrlEncoded
    @POST("/ajax/edit.php?getFormat=json_object&module=mod_keywords&func=group")
    Call<ModelResult> checkGroup(
            @Field("oper") String edit,
            @Field("project_id") String idProject,
            @Field("on") String on,
            @Field("id") String id);

    @FormUrlEncoded
    @POST("/ajax/edit.php?getFormat=json_object&module=mod_projects&func=searcher")
    Call<ModelResultPS> addPS(//238134
                              @Field("oper") String edit,
                              @Field("project_id") String idProject,
                              @Field("searcher") String searcher,
                              @Field("id") String id);

    @FormUrlEncoded
    @POST("/ajax/edit.php?getFormat=json_object&module=mod_projects&func=searcher")
    Call<ModelResultPS> checkedPS(
            @Field("oper") String edit,
            @Field("enabled") String enabled,
            @Field("id") String idSearcher);

    @FormUrlEncoded
    @POST("/ajax/edit.php?getFormat=json_object&module=mod_projects&func=searcher")
    Call<ModelResult> removePS(
            @Field("oper") String del,
            @Field("id") String idSearcher);

    ////
    @FormUrlEncoded
    @POST("/ajax/edit.php?getFormat=json_object&oper=edit&module=mod_projects&func=searcher")
    Call<ModelResultPS> changeRegions(
            @Field("regions[]") List<String> listRegions,
            @Field("id") String idSearcher);

    @FormUrlEncoded
    @POST("/ajax/get.php?getFormat=json_object&func=regions&getList=name&module=mod_common")
    Call<List<ModelSearchRegion>> reqSearchRegions(
            @Field("rows") String rows,
            @Field("term") String term,
            @Field("searcher") String idSearcher);

    @FormUrlEncoded
    @POST("/ajax/edit.php?getFormat=json&module=mod_projects&func=searcher_regions&oper=add&type_result=object")
    Call<ModelResultType> reqAddRegions(
            @Field("searcher_id") String idSearcher,
            @Field("regions[]") List<String> listRegions);

    @FormUrlEncoded
    @POST("/ajax/edit.php?getFormat=json&module=mod_projects&oper=edit")
    Call<ModelResult> reqAddRegions(
            @Field("on") int on,
            @Field("id") String idProject,
            @Field("time_for_update") String timeForUpdate,
            @Field("wait_after_updates") String waitAfterUpdates
    );

    @FormUrlEncoded
    @POST("/ajax/edit.php?getFormat=json&module=mod_projects&oper=edit")
    Call<ModelResult> reqAutoCond(
            @Field("auto_cond") String autoCond,
            @Field("id") String idProject);

    @FormUrlEncoded
    @POST("/ajax/get.php?getFormat=json_pager&module=mod_tickets&func=")
    Call<ModelList<ModelTicket>> reqSupport(
            @Field("page") int page,
            @Field("rows") int rows);

    @FormUrlEncoded
    @POST("/ajax/get.php?getFormat=json&module=mod_tickets&func=post")
    Call<List<ModelItemMessage>> getMessages(
            @Field("ticket_id") int ticket_id);

    @FormUrlEncoded
    @POST("/ajax/edit.php?getFormat=json&func=upload&module=mod_tickets&oper=del&id=")
    Call<ModelResult> reqRemoveImage(
            @Field("name") String name);

    @POST("/ajax/get.php?getFormat=json&func=upload&module=mod_tickets")
    Call<List<String>> getLoadImage();

    @FormUrlEncoded
    @POST("/ajax/edit.php?getFormat=json&module=mod_tickets&func=post&id=&oper=add")
    Call<ModelResult> sendMessage(
            @Field("text") String text,
            @Field("ticket_id") int idTicket
    );

    @FormUrlEncoded
    @POST("/ajax/edit.php?getFormat=json&module=mod_tickets&id=&oper=add&resource=/account/#to_tickets&type=12")
    Call<ModelResult> sendNewDialogMessage(
            @Field("text") String text
    );

    @FormUrlEncoded
    @POST("/ajax/edit.php?module=users&oper=edit&module=user&func=mobile_addToken&id=&device=android")
    Call<ModelResult> sendGCMToken(
            @Field("token") String token
    );

    @FormUrlEncoded
    @POST("/ajax/edit.php?module=users&oper=edit&module=user&func=mobile_removeToken&id=&device=android")
    Call<ModelResult> removeGCMToken(
            @Field("token") String token
    );

    @Multipart
    @POST("/ajax/edit.php?getFormat=json&func=upload&module=mod_tickets&oper=edit&id=")
    Call<ModelResult> loadPhoto(@Part MultipartBody.Part file);


    @FormUrlEncoded
    @POST("/ajax/get.php?getFormat=json&module=bank&func=history&getFormat=jqgrid")
    Call<ModelList<ModelItemBank>> reqBankList(
            @Field("rows") int rows,
            @Field("sidx") String sidx,
            @Field("sord") String desc,
            @Field("page") int page,
            @Field("search_json[and][date][]") List<String> dates
    );

    @FormUrlEncoded
    @POST("/ajax/get.php?getFormat=json&module=bank&funcsetOrder=history&getFormat=jqgrid")
    Call<ModelList<ModelItemBank>> reqBankListParam(
            @Field("rows") int rows,
            @Field("sidx") String sidx,
            @Field("sord") String desc,
            @Field("page") int page,
            @Field("search_json[and][target_id]") String idProject,
            @Field("search_json[and][date][]") List<String> dates
    );

    @FormUrlEncoded
    @POST("/ajax/get.php?getFormat=json&module=bank&func=history_summary")
    Call<List<ModelBankPrice>> reqBankPrice(@Field("search_json[and][date][]") List<String> dates);

    @FormUrlEncoded
    @POST("/ajax/get.php?getFormat=json&module=bank&func=history_summary")
    Call<List<ModelBankPrice>> reqBankPriceParam(
            @Field("search_json[and][target_id]") String idProject,
            @Field("search_json[and][date][]") List<String> dates
    );
}
