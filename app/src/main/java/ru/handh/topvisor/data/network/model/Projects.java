package ru.handh.topvisor.data.network.model;

/**
 * Created by sergey on 12.02.16.
 * модель сохранения выбранных проектов на экране проектов
 */
public class Projects {
    public String name;
    public boolean isChecked;
    public String folder;
    public int id;

    public Projects(int id,  String folder, String name) {
        this.name = name;
        this.folder = folder;
        this.id = id;
        this.isChecked = false;
    }
}
