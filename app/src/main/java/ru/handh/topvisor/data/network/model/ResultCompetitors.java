package ru.handh.topvisor.data.network.model;

/**
 * Created by sergey on 22.03.16.
 */
public class ResultCompetitors {
    private String id;
    private String site;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
