package ru.handh.topvisor.data.network.model;

/**
 * Created by sergey on 07.04.16.
 */
public class ModelBankPrice {
    private double sum;
    private String target;
    private String info;

    public double isSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
