package ru.handh.topvisor.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by sergey on 27.03.16.
 */
public class ModelSearchRegion {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("google_id")
    @Expose
    private String googleId;
    @SerializedName("mail_id")
    @Expose
    private String mailId;
    @SerializedName("coords")
    @Expose
    private String coords;
    @SerializedName("countryCode")
    @Expose
    private String countryCode;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("name_ru")
    @Expose
    private String nameRu;
    @SerializedName("areaName_ru")
    @Expose
    private String areaNameRu;
    @SerializedName("name_en")
    @Expose
    private String nameEn;
    @SerializedName("areaName_en")
    @Expose
    private String areaNameEn;
    @SerializedName("childrenCount")
    @Expose
    private String childrenCount;
    @SerializedName("parentId")
    @Expose
    private String parentId;
    @SerializedName("areaName")
    @Expose
    private String areaName;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("domain")
    @Expose
    private String domain;
    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("value")
    @Expose
    private String value;

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The googleId
     */
    public String getGoogleId() {
        return googleId;
    }

    /**
     * @param googleId The google_id
     */
    public void setGoogleId(String googleId) {
        this.googleId = googleId;
    }

    /**
     * @return The mailId
     */
    public String getMailId() {
        return mailId;
    }

    /**
     * @param mailId The mail_id
     */
    public void setMailId(String mailId) {
        this.mailId = mailId;
    }

    /**
     * @return The coords
     */
    public String getCoords() {
        return coords;
    }

    /**
     * @param coords The coords
     */
    public void setCoords(String coords) {
        this.coords = coords;
    }

    /**
     * @return The countryCode
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * @param countryCode The countryCode
     */
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    /**
     * @return The type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return The nameRu
     */
    public String getNameRu() {
        return nameRu;
    }

    /**
     * @param nameRu The name_ru
     */
    public void setNameRu(String nameRu) {
        this.nameRu = nameRu;
    }

    /**
     * @return The areaNameRu
     */
    public String getAreaNameRu() {
        return areaNameRu;
    }

    /**
     * @param areaNameRu The areaName_ru
     */
    public void setAreaNameRu(String areaNameRu) {
        this.areaNameRu = areaNameRu;
    }

    /**
     * @return The nameEn
     */
    public String getNameEn() {
        return nameEn;
    }

    /**
     * @param nameEn The name_en
     */
    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    /**
     * @return The areaNameEn
     */
    public String getAreaNameEn() {
        return areaNameEn;
    }

    /**
     * @param areaNameEn The areaName_en
     */
    public void setAreaNameEn(String areaNameEn) {
        this.areaNameEn = areaNameEn;
    }

    /**
     * @return The childrenCount
     */
    public String getChildrenCount() {
        return childrenCount;
    }

    /**
     * @param childrenCount The childrenCount
     */
    public void setChildrenCount(String childrenCount) {
        this.childrenCount = childrenCount;
    }

    /**
     * @return The parentId
     */
    public String getParentId() {
        return parentId;
    }

    /**
     * @param parentId The parentId
     */
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    /**
     * @return The areaName
     */
    public String getAreaName() {
        return areaName;
    }

    /**
     * @param areaName The areaName
     */
    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The domain
     */
    public String getDomain() {
        return domain;
    }

    /**
     * @param domain The domain
     */
    public void setDomain(String domain) {
        this.domain = domain;
    }

    /**
     * @return The label
     */
    public String getLabel() {
        return label;
    }

    /**
     * @param label The label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * @return The value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value The value
     */
    public void setValue(String value) {
        this.value = value;
    }
}
