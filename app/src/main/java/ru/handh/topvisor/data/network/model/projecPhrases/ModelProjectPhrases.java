
package ru.handh.topvisor.data.network.model.projecPhrases;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelProjectPhrases {

    @SerializedName("page")
    @Expose
    private Integer page;
    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("records")
    @Expose
    private Integer records;
    @SerializedName("rows")
    @Expose
    private Rows rows;


    /**
     * 
     * @return
     *     The page
     */
    public Integer getPage() {
        return page;
    }

    /**
     * 
     * @param page
     *     The page
     */
    public void setPage(Integer page) {
        this.page = page;
    }

    /**
     * 
     * @return
     *     The total
     */
    public Integer getTotal() {
        return total;
    }

    /**
     * 
     * @param total
     *     The total
     */
    public void setTotal(Integer total) {
        this.total = total;
    }

    /**
     * 
     * @return
     *     The records
     */
    public Integer getRecords() {
        return records;
    }

    /**
     * 
     * @param records
     *     The records
     */
    public void setRecords(Integer records) {
        this.records = records;
    }

    /**
     * 
     * @return
     *     The rows
     */
    public Rows getRows() {
        return rows;
    }

    /**
     * 
     * @param rows
     *     The rows
     */
    public void setRows(Rows rows) {
        this.rows = rows;
    }

}
