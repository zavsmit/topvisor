
package ru.handh.topvisor.data.network.model.projecDates;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Phrase {

    @SerializedName("visitors_for_period")
    @Expose
    private String visitorsForPeriod;

    /**
     * 
     * @return
     *     The visitorsForPeriod
     */
    public String getVisitorsForPeriod() {
        return visitorsForPeriod;
    }

    /**
     * 
     * @param visitorsForPeriod
     *     The visitors_for_period
     */
    public void setVisitorsForPeriod(String visitorsForPeriod) {
        this.visitorsForPeriod = visitorsForPeriod;
    }

}
