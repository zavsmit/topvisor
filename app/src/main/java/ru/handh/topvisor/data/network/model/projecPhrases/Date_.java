package ru.handh.topvisor.data.network.model.projecPhrases;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Date_ {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("top_percent")
    @Expose
    private String topPercent;
    @SerializedName("all_visitors")
    @Expose
    private String allVisitors;

    /**
     * @return The date
     */
    public String getDate() {
        return date;
    }

    /**
     * @param date The date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * @return The topPercent
     */
    public String getTopPercent() {
        return topPercent;
    }

    /**
     * @param topPercent The top_percent
     */
    public void setTopPercent(String topPercent) {
        this.topPercent = topPercent;
    }

    /**
     * @return The allVisitors
     */
    public String getAllVisitors() {
        return allVisitors;
    }

    /**
     * @param allVisitors The all_visitors
     */
    public void setAllVisitors(String allVisitors) {
        this.allVisitors = allVisitors;
    }

}
