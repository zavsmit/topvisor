package ru.handh.topvisor.data.database.project;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by sergey on 28.04.16.
 */
public class ProjectData extends RealmObject {

    @PrimaryKey
    private String id;
    private String user;
    private String name;
    private String site;
    private String competitor;
    private String competitorOrd;
    private String comment;
    private Date date;
    private Date update;
    private Date lastView;
    private String status;
    private Boolean statusFrequency;
    private String statusClaster;
    private String subdomains;
    private String filter;
    private String autoCorrect;
    private String commonTraffic;
    private String on;
    private String autoCond;
    private String waitAfterUpdates;
    private String timeForUpdate;
    private String withSnippets;
    private String countKeywords;
    private String countTasks;
    private String price;
    private String reportOn;
    private String reportTime;
    private String reportFormat;
    private String guestLinkRight;
    private String catY;
    private String catDm;
    private String catM;
    private String right;
    private String email;
    private String favorite;
    private String tag;
    private String percentOfParse;
    private RealmList<HistoryData> history;
    private RealmList<SearchData> searchers;
    private RealmList<CompetitorData> competitors;
    private RealmList<GroupData> groups;
    private String dynamicsLimit;

    public RealmList<HistoryData> getHistory() {
        return history;
    }

    public void setHistory(RealmList<HistoryData> history) {
        this.history = history;
    }

    public RealmList<SearchData> getSearchers() {
        return searchers;
    }

    public void setSearchers(RealmList<SearchData> searchers) {
        this.searchers = searchers;
    }

    public RealmList<CompetitorData> getCompetitors() {
        return competitors;
    }

    public void setCompetitors(RealmList<CompetitorData> competitors) {
        this.competitors = competitors;
    }

    public RealmList<GroupData> getGroups() {
        return groups;
    }

    public void setGroups(RealmList<GroupData> groups) {
        this.groups = groups;
    }

    public String getDynamicsLimit() {
        return dynamicsLimit;
    }

    public void setDynamicsLimit(String dynamicsLimit) {
        this.dynamicsLimit = dynamicsLimit;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getCompetitor() {
        return competitor;
    }

    public void setCompetitor(String competitor) {
        this.competitor = competitor;
    }

    public String getCompetitorOrd() {
        return competitorOrd;
    }

    public void setCompetitorOrd(String competitorOrd) {
        this.competitorOrd = competitorOrd;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getUpdate() {
        return update;
    }

    public void setUpdate(Date update) {
        this.update = update;
    }

    public Date getLastView() {
        return lastView;
    }

    public void setLastView(Date lastView) {
        this.lastView = lastView;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getStatusFrequency() {
        return statusFrequency;
    }

    public void setStatusFrequency(Boolean statusFrequency) {
        this.statusFrequency = statusFrequency;
    }

    public String getStatusClaster() {
        return statusClaster;
    }

    public void setStatusClaster(String statusClaster) {
        this.statusClaster = statusClaster;
    }

    public String getSubdomains() {
        return subdomains;
    }

    public void setSubdomains(String subdomains) {
        this.subdomains = subdomains;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public String getAutoCorrect() {
        return autoCorrect;
    }

    public void setAutoCorrect(String autoCorrect) {
        this.autoCorrect = autoCorrect;
    }

    public String getCommonTraffic() {
        return commonTraffic;
    }

    public void setCommonTraffic(String commonTraffic) {
        this.commonTraffic = commonTraffic;
    }

    public String getOn() {
        return on;
    }

    public void setOn(String on) {
        this.on = on;
    }

    public String getAutoCond() {
        return autoCond;
    }

    public void setAutoCond(String autoCond) {
        this.autoCond = autoCond;
    }

    public String getWaitAfterUpdates() {
        return waitAfterUpdates;
    }

    public void setWaitAfterUpdates(String waitAfterUpdates) {
        this.waitAfterUpdates = waitAfterUpdates;
    }

    public String getTimeForUpdate() {
        return timeForUpdate;
    }

    public void setTimeForUpdate(String timeForUpdate) {
        this.timeForUpdate = timeForUpdate;
    }

    public String getWithSnippets() {
        return withSnippets;
    }

    public void setWithSnippets(String withSnippets) {
        this.withSnippets = withSnippets;
    }

    public String getCountKeywords() {
        return countKeywords;
    }

    public void setCountKeywords(String countKeywords) {
        this.countKeywords = countKeywords;
    }

    public String getCountTasks() {
        return countTasks;
    }

    public void setCountTasks(String countTasks) {
        this.countTasks = countTasks;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getReportOn() {
        return reportOn;
    }

    public void setReportOn(String reportOn) {
        this.reportOn = reportOn;
    }

    public String getReportTime() {
        return reportTime;
    }

    public void setReportTime(String reportTime) {
        this.reportTime = reportTime;
    }

    public String getReportFormat() {
        return reportFormat;
    }

    public void setReportFormat(String reportFormat) {
        this.reportFormat = reportFormat;
    }

    public String getGuestLinkRight() {
        return guestLinkRight;
    }

    public void setGuestLinkRight(String guestLinkRight) {
        this.guestLinkRight = guestLinkRight;
    }

    public String getCatY() {
        return catY;
    }

    public void setCatY(String catY) {
        this.catY = catY;
    }

    public String getCatDm() {
        return catDm;
    }

    public void setCatDm(String catDm) {
        this.catDm = catDm;
    }

    public String getCatM() {
        return catM;
    }

    public void setCatM(String catM) {
        this.catM = catM;
    }

    public String getRight() {
        return right;
    }

    public void setRight(String right) {
        this.right = right;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFavorite() {
        return favorite;
    }

    public void setFavorite(String favorite) {
        this.favorite = favorite;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getPercentOfParse() {
        return percentOfParse;
    }

    public void setPercentOfParse(String percentOfParse) {
        this.percentOfParse = percentOfParse;
    }
}
