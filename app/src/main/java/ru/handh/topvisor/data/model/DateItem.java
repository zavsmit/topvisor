package ru.handh.topvisor.data.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sergey on 15.03.16.
 */
public class DateItem implements Parcelable {
    private String name;
    private String shot;
    private boolean isEnable;

    public DateItem(String name, boolean isEnable){
        this.name = name;
        this.isEnable = isEnable;
    }

    public DateItem(String name, boolean isEnable, String shot){
        this.name = name;
        this.shot = shot;
        this.isEnable = isEnable;
    }


    public void setShot(String shot) {
        this.shot = shot;
    }

    public String getShot() {
        return shot;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEnable() {
        return isEnable;
    }

    public void setIsEnable(boolean isEnable) {
        this.isEnable = isEnable;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.shot);
        dest.writeByte(isEnable ? (byte) 1 : (byte) 0);
    }

    protected DateItem(Parcel in) {
        this.name = in.readString();
        this.shot = in.readString();
        this.isEnable = in.readByte() != 0;
    }

    public static final Creator<DateItem> CREATOR = new Creator<DateItem>() {
        @Override
        public DateItem createFromParcel(Parcel source) {
            return new DateItem(source);
        }

        @Override
        public DateItem[] newArray(int size) {
            return new DateItem[size];
        }
    };
}
