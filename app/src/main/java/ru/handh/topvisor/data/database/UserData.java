package ru.handh.topvisor.data.database;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by sergey on 08.04.16.
 */
public class UserData extends RealmObject {
    @PrimaryKey
    private String id;
    private String auth;
    private Date date_reg;
    private String email;
    private String name;
    private String avatar;
    private Date lastactive;
    private String ref;
    private String domain;
    private double balance;
    private String yandex_xml_balance;
    private String warning_message;
    private UserDataTarif tariff;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAuth() {
        return auth;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }

    public Date getDate_reg() {
        return date_reg;
    }

    public void setDate_reg(Date date_reg) {
        this.date_reg = date_reg;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Date getLastactive() {
        return lastactive;
    }

    public void setLastactive(Date lastactive) {
        this.lastactive = lastactive;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getYandex_xml_balance() {
        return yandex_xml_balance;
    }

    public void setYandex_xml_balance(String yandex_xml_balance) {
        this.yandex_xml_balance = yandex_xml_balance;
    }

    public String getWarning_message() {
        return warning_message;
    }

    public void setWarning_message(String warning_message) {
        this.warning_message = warning_message;
    }

    public UserDataTarif getTariff() {
        return tariff;
    }

    public void setTariff(UserDataTarif tariff) {
        this.tariff = tariff;
    }
}
