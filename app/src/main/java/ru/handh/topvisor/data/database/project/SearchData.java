package ru.handh.topvisor.data.database.project;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by sergey on 28.04.16.
 */
public class SearchData extends RealmObject {

    @PrimaryKey
    private String id;
    private String projectId;
    private String searcher;
    private String domain;
    private String enabled;
    private String ord;
    private String name;
    private RealmList<RegionData> regions;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getSearcher() {
        return searcher;
    }

    public void setSearcher(String searcher) {
        this.searcher = searcher;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    public String getOrd() {
        return ord;
    }

    public void setOrd(String ord) {
        this.ord = ord;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RealmList<RegionData> getRegions() {
        return regions;
    }

    public void setRegions(RealmList<RegionData> regions) {
        this.regions = regions;
    }
}
