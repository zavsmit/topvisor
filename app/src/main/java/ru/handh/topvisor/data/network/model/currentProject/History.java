
package ru.handh.topvisor.data.network.model.currentProject;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class History implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("project_id")
    @Expose
    private String projectId;
    @SerializedName("cy")
    @Expose
    private String cy;
    @SerializedName("pr")
    @Expose
    private String pr;
    @SerializedName("indexed_y")
    @Expose
    private String indexedY;
    @SerializedName("indexed_g")
    @Expose
    private String indexedG;
    @SerializedName("traffic_y")
    @Expose
    private String trafficY;
    @SerializedName("traffic_g")
    @Expose
    private String trafficG;
    @SerializedName("traffic_m")
    @Expose
    private String trafficM;
    @SerializedName("backlinks")
    @Expose
    private String backlinks;
    @SerializedName("backlinks_ah")
    @Expose
    private String backlinksAh;
    @SerializedName("date")
    @Expose
    private Date date;

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The projectId
     */
    public String getProjectId() {
        return projectId;
    }

    /**
     * 
     * @param projectId
     *     The project_id
     */
    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    /**
     * 
     * @return
     *     The cy
     */
    public String getCy() {
        return cy;
    }

    /**
     * 
     * @param cy
     *     The cy
     */
    public void setCy(String cy) {
        this.cy = cy;
    }

    /**
     * 
     * @return
     *     The pr
     */
    public String getPr() {
        return pr;
    }

    /**
     * 
     * @param pr
     *     The pr
     */
    public void setPr(String pr) {
        this.pr = pr;
    }

    /**
     * 
     * @return
     *     The indexedY
     */
    public String getIndexedY() {
        return indexedY;
    }

    /**
     * 
     * @param indexedY
     *     The indexed_y
     */
    public void setIndexedY(String indexedY) {
        this.indexedY = indexedY;
    }

    /**
     * 
     * @return
     *     The indexedG
     */
    public String getIndexedG() {
        return indexedG;
    }

    /**
     * 
     * @param indexedG
     *     The indexed_g
     */
    public void setIndexedG(String indexedG) {
        this.indexedG = indexedG;
    }

    /**
     * 
     * @return
     *     The trafficY
     */
    public String getTrafficY() {
        return trafficY;
    }

    /**
     * 
     * @param trafficY
     *     The traffic_y
     */
    public void setTrafficY(String trafficY) {
        this.trafficY = trafficY;
    }

    /**
     * 
     * @return
     *     The trafficG
     */
    public String getTrafficG() {
        return trafficG;
    }

    /**
     * 
     * @param trafficG
     *     The traffic_g
     */
    public void setTrafficG(String trafficG) {
        this.trafficG = trafficG;
    }

    /**
     * 
     * @return
     *     The trafficM
     */
    public String getTrafficM() {
        return trafficM;
    }

    /**
     * 
     * @param trafficM
     *     The traffic_m
     */
    public void setTrafficM(String trafficM) {
        this.trafficM = trafficM;
    }

    /**
     * 
     * @return
     *     The backlinks
     */
    public String getBacklinks() {
        return backlinks;
    }

    /**
     * 
     * @param backlinks
     *     The backlinks
     */
    public void setBacklinks(String backlinks) {
        this.backlinks = backlinks;
    }

    /**
     * 
     * @return
     *     The backlinksAh
     */
    public Object getBacklinksAh() {
        return backlinksAh;
    }

    /**
     * 
     * @param backlinksAh
     *     The backlinks_ah
     */
    public void setBacklinksAh(String backlinksAh) {
        this.backlinksAh = backlinksAh;
    }

    /**
     * 
     * @return
     *     The date
     */
    public Date getDate() {
        return date;
    }

    /**
     * 
     * @param date
     *     The date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    public History() {
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.projectId);
        dest.writeString(this.cy);
        dest.writeString(this.pr);
        dest.writeString(this.indexedY);
        dest.writeString(this.indexedG);
        dest.writeString(this.trafficY);
        dest.writeString(this.trafficG);
        dest.writeString(this.trafficM);
        dest.writeString(this.backlinks);
        dest.writeString(this.backlinksAh);
        dest.writeLong(date != null ? date.getTime() : -1);
    }

    protected History(Parcel in) {
        this.id = in.readString();
        this.projectId = in.readString();
        this.cy = in.readString();
        this.pr = in.readString();
        this.indexedY = in.readString();
        this.indexedG = in.readString();
        this.trafficY = in.readString();
        this.trafficG = in.readString();
        this.trafficM = in.readString();
        this.backlinks = in.readString();
        this.backlinksAh = in.readString();
        long tmpDate = in.readLong();
        this.date = tmpDate == -1 ? null : new Date(tmpDate);
    }

    public static final Creator<History> CREATOR = new Creator<History>() {
        public History createFromParcel(Parcel source) {
            return new History(source);
        }

        public History[] newArray(int size) {
            return new History[size];
        }
    };
}
