
package ru.handh.topvisor.data.network.model.db.currentProject;

import io.realm.RealmList;
import io.realm.RealmObject;

public class ModelCurrentProjectDB extends RealmObject{

    private Integer page;
    private Integer total;
    private String records;
    private RealmList<RowDB> rowDBs = new RealmList<>();

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getTotal() {
        return total;
    }

    /**
     * 
     * @param total
     *     The total
     */
    public void setTotal(Integer total) {
        this.total = total;
    }

    /**
     * 
     * @return
     *     The records
     */
    public String getRecords() {
        return records;
    }

    /**
     * 
     * @param records
     *     The records
     */
    public void setRecords(String records) {
        this.records = records;
    }

    /**
     * 
     * @return
     *     The rowDBs
     */
    public RealmList<RowDB> getRowDBs() {
        return rowDBs;
    }

    /**
     * 
     * @param rowDBs
     *     The rowDBs
     */
    public void setRowDBs(RealmList<RowDB> rowDBs) {
        this.rowDBs = rowDBs;
    }

}
