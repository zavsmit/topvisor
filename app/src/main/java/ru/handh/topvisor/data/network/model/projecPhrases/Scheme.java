package ru.handh.topvisor.data.network.model.projecPhrases;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Scheme {

    @SerializedName("dates")
    @Expose
    private List<Date_> dates = new ArrayList<Date_>();
    @SerializedName("visitors_exists")
    @Expose
    private String visitorsExists;
    @SerializedName("searchers")
    @Expose
    private List<Searcher> searchers = new ArrayList<Searcher>();

    /**
     * @return The dates
     */
    public List<Date_> getDates() {
        return dates;
    }

    /**
     * @param dates The dates
     */
    public void setDates(List<Date_> dates) {
        this.dates = dates;
    }

    /**
     * @return The visitorsExists
     */
    public String getVisitorsExists() {
        return visitorsExists;
    }

    /**
     * @param visitorsExists The visitors_exists
     */
    public void setVisitorsExists(String visitorsExists) {
        this.visitorsExists = visitorsExists;
    }

    /**
     * @return The searchers
     */
    public List<Searcher> getSearchers() {
        return searchers;
    }

    /**
     * @param searchers The searchers
     */
    public void setSearchers(List<Searcher> searchers) {
        this.searchers = searchers;
    }

}
