package ru.handh.topvisor.data.network.model;

/**
 * Created by sergey on 12.02.16.
 * модель сохранения иконок на экране проектов
 */
public class Labels {
    public String name;
    public int icon;
    public int id;
    public boolean isChecked;

    public Labels(int id, String name, int icon) {
        this.name = name;
        this.icon = icon;
        this.id = id;
        this.isChecked = false;
    }
}
