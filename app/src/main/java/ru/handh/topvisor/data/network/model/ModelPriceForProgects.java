package ru.handh.topvisor.data.network.model;

/**
 * Created by sergey on 16.02.16.
 * кол-во денег
 */
public class ModelPriceForProgects {
    private double price;
    private double xml_for_use;

    public double getXml_for_use() {
        return xml_for_use;
    }

    public void setXml_for_use(double xml_for_use) {
        this.xml_for_use = xml_for_use;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
