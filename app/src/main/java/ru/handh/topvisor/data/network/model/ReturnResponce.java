package ru.handh.topvisor.data.network.model;

/**
 * Created by sergey on 09.02.16.
 * класс передаваемый из модели в презентер с данными
 */
public class ReturnResponce<T> {

    private boolean isFailure;
    private String errorMessage;
    private T result;

    public boolean isFailure() {
        return isFailure;
    }

    public void setIsFailure(boolean isFailure) {
        this.isFailure = isFailure;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public void clean() {
        isFailure = false;
        errorMessage = "";
        result = null;
    }
}
