package ru.handh.topvisor.data.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sergey on 14.03.16.
 */
public class InspectDialogData implements Parcelable {
    private String name;
    private String region;
    private String ps;
    private String group;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getPs() {
        return ps;
    }

    public void setPs(String ps) {
        this.ps = ps;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.region);
        dest.writeString(this.ps);
        dest.writeString(this.group);
    }

    public InspectDialogData() {
    }

    protected InspectDialogData(Parcel in) {
        this.name = in.readString();
        this.region = in.readString();
        this.ps = in.readString();
        this.group = in.readString();
    }

    public static final Parcelable.Creator<InspectDialogData> CREATOR = new Parcelable.Creator<InspectDialogData>() {
        public InspectDialogData createFromParcel(Parcel source) {
            return new InspectDialogData(source);
        }

        public InspectDialogData[] newArray(int size) {
            return new InspectDialogData[size];
        }
    };
}
