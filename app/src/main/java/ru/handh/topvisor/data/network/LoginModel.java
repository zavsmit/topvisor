package ru.handh.topvisor.data.network;

import ru.handh.topvisor.ui.login.LoginEndLoad;

/**
 * Created by sergey on 08.02.16.
 * интерфейс модели для входа
 */
public interface LoginModel {

    void reqServerTypeAuth(final String email, final String pass, LoginEndLoad listener);

    void reqServerTypeForget(final String email, LoginEndLoad listener);

    void reqRegistration(final String email, LoginEndLoad listener);

    void reqAuthorizationOnToken(final String email, String code, String params, int domain, LoginEndLoad listener);

    void authSotialNetwork(String token, LoginEndLoad listener);

}
