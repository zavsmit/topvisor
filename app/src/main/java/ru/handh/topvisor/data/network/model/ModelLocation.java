package ru.handh.topvisor.data.network.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sergey on 02.02.16.
 */
public class ModelLocation {

    @SerializedName("0")
    private String key;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
