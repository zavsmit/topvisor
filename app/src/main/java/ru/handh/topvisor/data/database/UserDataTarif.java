package ru.handh.topvisor.data.database;

import io.realm.RealmObject;

/**
 * Created by sergey on 08.04.16.
 */
public class UserDataTarif extends RealmObject{
    private double balance_t;
    private String tariff;
    private String discount;

    public double getBalance_t() {
        return balance_t;
    }

    public void setBalance_t(double balance_t) {
        this.balance_t = balance_t;
    }

    public String getTariff() {
        return tariff;
    }

    public void setTariff(String tariff) {
        this.tariff = tariff;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }
}
