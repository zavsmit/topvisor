package ru.handh.topvisor.data.gcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.gcm.GcmListenerService;

import ru.handh.topvisor.R;
import ru.handh.topvisor.data.DataManager;
import ru.handh.topvisor.ui.pay.ActivityRefill;
import ru.handh.topvisor.ui.support.listMessages.ActivityListMessages;

public class MyGcmListenerService extends GcmListenerService {

    public final static int TICKET_NOTIFICATION = 9909;
    public final static int WEB_NOTIFICATION = 9910;
    public final static int BALANCE_NOTIFICATION = 9911;
    private static final String TAG = "MyGcmListenerService";

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle data) {

        if (DataManager.getPref().getUserId().isEmpty()) {
            return;
        }

        String message = data.getString("messages");
        String idTicket = data.getString("idTicket");
        String link = data.getString("link");
        String type = data.getString("type");
        String badge = data.getString("badge");

        int endText = message.indexOf("\",\"title\":\"");
        String text = message.substring(8, endText);


        int startTitle = message.indexOf("title\":\"") + 8;
        String title = message.substring(startTitle, message.length() - 2);


        switch (type) {
            case "1":
                sendNotificationWeb(text, title, link);
                break;
            case "2":
                // balance
                //запрос на баланс
                sendNotificationBalance(text, title);
                break;
            case "3":
                // обновление бейджа
                sendNotificationTicket(text, title, idTicket);

                //если запущен диалог - показать новое собщение
                // иначе показать пуш при тапе на который перход в диалог
                // tickets (см. mod_tickets::protected_add_post())
                break;
            case "4":
                sendNotificationWeb(text, title, link);
                // показать диалог?????
                // yandex-update (см. mod_keywords::cron_check_updates())
                break;
            case "5":
                sendNotificationWeb(text, title, link);
                // comments
                break;
            case "6":
                sendNotificationWeb(text, title, link);
                // 'INFORMER_Type_api'
                break;
        }
//        Bundle[{idTicket=24073, messages={"text":"вапвап","title":"Вы получили новый ответ на тикет #24073:" +
//                " \"ttyyyttyttt fghgd prived\""}, link=24073, type=3, badge=2, collapse_key=do_not_collapse}]


        // [START_EXCLUDE]
        /**
         * Production applications would usually process the message here.
         * Eg: - Syncing with server.
         *     - Store message in local database.
         *     - Update UI.
         */

        /**
         * In some cases it may be useful to show a notification indicating to the user
         * that a message was received.
         */


        updateUI(text, idTicket);

        // [END_EXCLUDE]
    }
// [END receive_message]

    private void sendNotificationTicket(String message, String title, String idTicket) {
        Intent intent = new Intent(this, ActivityListMessages.class);
        intent.putExtra("nameId", idTicket);
        sendNotification(title, message, intent, TICKET_NOTIFICATION);
    }

    private void sendNotificationWeb(String message, String title, String link) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
        sendNotification(title, message, intent, WEB_NOTIFICATION);
    }

    private void sendNotificationBalance(String message, String title) {
        Intent intent = new Intent(this, ActivityRefill.class);
        sendNotification(title, message, intent, BALANCE_NOTIFICATION);
    }

    private void updateUI(String text, String idTicket) {
        Intent intent = new Intent(ActivityListMessages.BROADCAST_MESSAGE);
        intent.putExtra(ActivityListMessages.PARAM_STATUS, ActivityListMessages.STATYS_NEW_GCM_MESSAGE);
        intent.putExtra(ActivityListMessages.NEW_MESSAGE, text);
        intent.putExtra(ActivityListMessages.ID_TICKET, idTicket);
        sendBroadcast(intent);
    }

    private void sendNotification(String title, String message, Intent intent, int idNotification) {
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(idNotification, notificationBuilder.build());
    }


}