package ru.handh.topvisor.data;

import android.content.Context;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmList;
import io.realm.RealmResults;
import ru.handh.topvisor.R;
import ru.handh.topvisor.data.database.ItemProject;
import ru.handh.topvisor.data.database.TicketData;
import ru.handh.topvisor.data.database.UserData;
import ru.handh.topvisor.data.database.UserDataTarif;
import ru.handh.topvisor.data.database.project.CompetitorData;
import ru.handh.topvisor.data.database.project.GroupData;
import ru.handh.topvisor.data.database.project.HistoryData;
import ru.handh.topvisor.data.database.project.ProjectData;
import ru.handh.topvisor.data.database.project.RegionData;
import ru.handh.topvisor.data.database.project.SearchData;
import ru.handh.topvisor.data.database.projectPhraseDates.PhraseDatesData;
import ru.handh.topvisor.data.database.projectPhraseDates.RealmString;
import ru.handh.topvisor.data.network.model.ModelTicket;
import ru.handh.topvisor.data.network.model.ModelUserData;
import ru.handh.topvisor.data.network.model.currentProject.Competitor;
import ru.handh.topvisor.data.network.model.currentProject.Group;
import ru.handh.topvisor.data.network.model.currentProject.Region;
import ru.handh.topvisor.data.network.model.currentProject.Row;
import ru.handh.topvisor.data.network.model.currentProject.Searcher;

/**
 * Created by sergey on 08.04.16.
 * методы для работы с базой
 */
public class DBHelper {

    private Realm realm;
    private RealmConfiguration config;
    private Context context;


    public DBHelper(Context context) {
        this.context = context;
        config = new RealmConfiguration.Builder(context)
                .deleteRealmIfMigrationNeeded()
                .build();
        initDB();

    }

    public void initDB() {
        if (realm == null) {
            Realm.setDefaultConfiguration(config);
            realm = Realm.getDefaultInstance();
        } else {
            if (realm.isClosed()) {
                Realm.setDefaultConfiguration(config);
                realm = Realm.getDefaultInstance();
            }
        }


    }

    public void userDataWriteDatabase(final ModelUserData udm) {

        if (udm.getId() == null) {

            return;
        }
        UserData ud = new UserData();
        ud.setAuth(udm.getAuth());
        ud.setAvatar(udm.getAvatar());
        ud.setBalance(udm.getBalance());
        ud.setDate_reg(udm.getDate_reg());
        ud.setDomain(udm.getDomain());
        ud.setEmail(udm.getEmail());
        ud.setId(udm.getId());
        ud.setLastactive(udm.getLastactive());
        ud.setName(udm.getName());
        ud.setRef(udm.getRef());
        ud.setWarning_message(udm.getWarning_message());
        ud.setYandex_xml_balance(udm.getYandex_xml_balance());


        UserDataTarif udt = new UserDataTarif();
        udt.setBalance_t(udm.getTariff().getBalance_t());
        udt.setDiscount(udm.getTariff().getDiscount());
        udt.setTariff(udm.getTariff().getTariff());

        ud.setTariff(udt);


        realm.beginTransaction();
        realm.copyToRealmOrUpdate(ud);
        realm.commitTransaction();
    }

    public Date getUserStartDate() {

        RealmResults<UserData> ud = realm.where(UserData.class).findAll();

        return ud.get(0).getDate_reg();
    }

    public UserData getUser() {

        RealmResults<UserData> ud = realm.where(UserData.class).findAll();
        if (ud.size() == 0) {
            return new UserData();
        }

        return ud.get(0);
    }

    public void projectWriteDatabase(ArrayList<Row> list) {
        realm.beginTransaction();
        for (int i = 0; i < list.size(); i++) {
            ItemProject ip = new ItemProject();
            ip.setName(list.get(i).getName());
            ip.setId(list.get(i).getId());
            realm.copyToRealmOrUpdate(ip);
        }
        realm.commitTransaction();
    }

    public void ticketWriteDatabase(List<ModelTicket> modelTickets) {
        if (modelTickets == null) {
            return;
        }
        realm.beginTransaction();


        for (int i = 0; i < modelTickets.size(); i++) {
            ModelTicket mt = modelTickets.get(i);


            TicketData td = new TicketData();

            td.setId(mt.getId());
            td.setUser(mt.getUser());
            td.setSocial_user_id(mt.getSocial_user_id());
            td.setEmail(mt.getEmail());
            td.setType(mt.getType());
            td.setText(mt.getText());
            td.setUser_data(mt.getUser_data());
            td.setStatus(mt.getStatus());
            td.setOwner_readed(mt.getOwner_readed());
            td.setAnswerer_readed(mt.getAnswerer_readed());
            td.setTime(mt.getTime());
            td.setAdmin_writing(mt.getAdmin_writing());
            td.setTask_id(mt.getTask_id());
            td.setLast_message(mt.getLast_message());
            td.setUserName(mt.getUserName());


            realm.copyToRealmOrUpdate(td);
        }


        realm.commitTransaction();
    }

    public List<TicketData> getTickets() {

        return realm.where(TicketData.class).findAll();
    }

    ////////////////////////////////////// //////////////////////////////////////
    public void projectRowWriteDatabase(ProjectData pd) {
        if (pd == null) {
            return;
        }
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(pd);
        realm.commitTransaction();
    }


    public ProjectData getProject(String idProject) {

        RealmResults<ProjectData> list = realm.where(ProjectData.class).findAll();

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getId().equals(idProject)) {
                return list.get(i);
            }
        }

        return null;
    }

    public void projectDatesWriteDatabase(List<String> dates, String idProject, String regionKey) {

        PhraseDatesData pdd = new PhraseDatesData();
        pdd.setIdProjectRegionKey(idProject + regionKey);

        RealmList<RealmString> realmStrings = new RealmList<>();

        for (int i = 0; i < dates.size(); i++) {
            RealmString realmString = new RealmString();
            realmString.setName(dates.get(i));
            realmStrings.add(realmString);
        }

        pdd.setAllDates(realmStrings);

        realm.beginTransaction();
        realm.copyToRealmOrUpdate(pdd);
        realm.commitTransaction();
    }


    public void addGroupToProject(RealmList<GroupData> list, GroupData group) {
        realm.beginTransaction();
        list.add(group);
        realm.commitTransaction();
    }

    public void renameGroupProject(RealmList<GroupData> list, int position, String newName) {
        realm.beginTransaction();
        list.get(position).setName(newName);
        realm.commitTransaction();
    }

    public void checkGroupProject(RealmList<GroupData> list, int position, String on) {
        realm.beginTransaction();
        list.get(position).setOn(on);
        realm.commitTransaction();
    }

    public void removeGroupFromProject(RealmList<GroupData> list, int position) {
        realm.beginTransaction();
        list.remove(position);
        realm.commitTransaction();
    }

    public void addCompetitorToProject(RealmList<CompetitorData> list, CompetitorData competitor) {
        realm.beginTransaction();
        list.add(competitor);
        realm.commitTransaction();
    }

    public void removeCompetitorFromProject(RealmList<CompetitorData> list, int position) {
        realm.beginTransaction();
        list.remove(position);
        realm.commitTransaction();
    }

    public void changeLangDepthDevice(RealmList<RegionData> regions, int position,
                                      String lang, String depth, String device) {
        realm.beginTransaction();

        regions.get(position).setLang(lang);
        regions.get(position).setDepth(depth);
        regions.get(position).setDevice(device);

        realm.commitTransaction();
    }

    public void checkSearchSystem(SearchData searchData, String enabled) {
        realm.beginTransaction();
        searchData.setEnabled(enabled);
        realm.commitTransaction();
    }

    public void checkIntervalInspection(ProjectData projectData, String inspection) {
        realm.beginTransaction();
        projectData.setOn(inspection);
        realm.commitTransaction();
    }

    public void changeYandexIntervalInspection(String idProject, String id) {
        realm.beginTransaction();
        getProject(idProject).setWaitAfterUpdates(id);
        realm.commitTransaction();
    }

    public void changeDateIntervalInspection(String idProject, String timeForUpdate, String autoCord) {
        realm.beginTransaction();
        getProject(idProject).setTimeForUpdate(timeForUpdate);
        getProject(idProject).setAutoCond(autoCord);
        realm.commitTransaction();
    }


    public void selectItemGroup(RealmList<GroupData> groups, String idGroup) {
        realm.beginTransaction();

        for (int i = 0; i < groups.size(); i++) {

            if (groups.get(i).getId().equals(idGroup)) {
                groups.get(i).setEnable(true);
            } else {
                groups.get(i).setEnable(false);
            }
        }

        realm.commitTransaction();
    }

    public void selectItemRegion(RealmList<SearchData> searchDatas, String idRegion) {
        realm.beginTransaction();

        for (int i = 0; i < searchDatas.size(); i++) {

            RealmList<RegionData> regions = searchDatas.get(i).getRegions();

            for (int j = 0; j < regions.size(); j++) {

                if (idRegion.equals(regions.get(j).getId())) {
                    regions.get(j).setSelected(true);
                } else {
                    regions.get(j).setSelected(false);
                }

            }
        }

        realm.commitTransaction();
    }

    ////////////////////////////////////// //////////////////////////////////////
    public List<ItemProject> getProjectsStartDate() {

        return realm.where(ItemProject.class).findAll();
    }


    public void closeDB() {
        if (!realm.isClosed())
            realm.close();
    }

    public void removeAll() {
        realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.deleteAll();
        realm.commitTransaction();
    }


    public ProjectData rowTpProgect(Row currentRow) {
        ProjectData pd = new ProjectData();

        pd.setId(currentRow.getId());
        pd.setUser(currentRow.getUser());
        pd.setName(currentRow.getName());
        pd.setSite(currentRow.getSite());
        pd.setCompetitor(currentRow.getCompetitor());
        pd.setCompetitorOrd(currentRow.getCompetitorOrd());
        pd.setComment(currentRow.getComment());
        pd.setDate(currentRow.getDate());
        pd.setUpdate(currentRow.getUpdate());
        pd.setLastView(currentRow.getLastView());
        pd.setStatus(currentRow.getStatus());
        pd.setStatusFrequency(currentRow.getStatusFrequency());
        pd.setStatusClaster(currentRow.getStatusClaster());
        pd.setSubdomains(currentRow.getSubdomains());
        pd.setFilter(currentRow.getFilter());
        pd.setAutoCorrect(currentRow.getAutoCorrect());
        pd.setCommonTraffic(currentRow.getCommonTraffic());
        pd.setOn(currentRow.getOn());
        pd.setAutoCond(currentRow.getAutoCond());
        pd.setWaitAfterUpdates(currentRow.getWaitAfterUpdates());
        pd.setTimeForUpdate(currentRow.getTimeForUpdate());
        pd.setWithSnippets(currentRow.getWithSnippets());
        pd.setCountKeywords(currentRow.getCountKeywords());
        pd.setCountTasks(currentRow.getCountTasks());
        pd.setPrice(currentRow.getPrice());
        pd.setReportOn(currentRow.getReportOn());
        pd.setReportTime(currentRow.getReportTime());
        pd.setReportFormat(currentRow.getReportFormat());
        pd.setGuestLinkRight(currentRow.getGuestLinkRight());
        pd.setCatY(currentRow.getCatY());
        pd.setCatM(currentRow.getCatM());
        pd.setCatDm(currentRow.getCatDm());
        pd.setRight(currentRow.getRight());
        pd.setEmail(currentRow.getEmail());
        pd.setFavorite(currentRow.getFavorite());
        pd.setTag(currentRow.getTag());
        pd.setPercentOfParse(currentRow.getPercentOfParse());
        pd.setDynamicsLimit(currentRow.getDynamicsLimit());

        RealmList<HistoryData> hdList = new RealmList<>();
        for (int i = 0; i < currentRow.getHistory().size(); i++) {
            HistoryData hd = new HistoryData();

            if (currentRow.getHistory().get(i).getId() == null) {
                hd.setId(String.valueOf(i));
            } else {
                hd.setId(currentRow.getHistory().get(i).getId());
            }

            hd.setProjectId(currentRow.getHistory().get(i).getProjectId());
            hd.setCy(currentRow.getHistory().get(i).getCy());
            hd.setPr(currentRow.getHistory().get(i).getPr());
            hd.setIndexedY(currentRow.getHistory().get(i).getIndexedY());
            hd.setIndexedG(currentRow.getHistory().get(i).getIndexedG());
            hd.setTrafficG(currentRow.getHistory().get(i).getTrafficG());
            hd.setTrafficM(currentRow.getHistory().get(i).getTrafficM());
            hd.setTrafficY(currentRow.getHistory().get(i).getTrafficY());
            hd.setBacklinks(currentRow.getHistory().get(i).getBacklinks());
            hd.setBacklinksAh(currentRow.getHistory().get(i).getBacklinks());
            hd.setDate(currentRow.getHistory().get(i).getDate());
            hdList.add(hd);
        }
        pd.setHistory(hdList);

        RealmList<SearchData> sdList = new RealmList<>();
        for (int i = 0; i < currentRow.getSearchers().size(); i++) {
            Searcher searcher = currentRow.getSearchers().get(i);
            SearchData sd = new SearchData();
            if (searcher.getId() == null) {
                sd.setId(String.valueOf(i));
            } else {
                sd.setId(searcher.getId());
            }

            sd.setProjectId(searcher.getProjectId());
            sd.setSearcher(searcher.getSearcher());
            sd.setDomain(searcher.getDomain());
            sd.setEnabled(searcher.getEnabled());
            sd.setOrd(searcher.getOrd());
            sd.setName(searcher.getName());


            RealmList<RegionData> rdList = new RealmList<>();
            for (int j = 0; j < currentRow.getSearchers().get(i).getRegions().size(); j++) {
                Region region = currentRow.getSearchers().get(i).getRegions().get(j);
                RegionData rd = new RegionData();

                if (region.getId() == null) {
                    rd.setId(String.valueOf(i));
                } else {
                    rd.setId(region.getId());
                }

                rd.setDepth(region.getDepth());
                rd.setDevice(region.getDevice());
                rd.setKey(region.getKey());
                rd.setLang(region.getLang());
                rd.setCountryCode(region.getCountryCode());
                rd.setDomain(region.getDomain());
                rd.setAreaName(region.getAreaName());
                rd.setName(region.getName());
                rd.setSelected(region.isSelected());
                rdList.add(rd);
            }
            sd.setRegions(rdList);


            sdList.add(sd);
        }
        pd.setSearchers(sdList);


        RealmList<CompetitorData> cdList = new RealmList<>();
        for (int j = 0; j < currentRow.getCompetitors().size(); j++) {
            Competitor competitor = currentRow.getCompetitors().get(j);
            CompetitorData cd = new CompetitorData();

            if (competitor.getId() == null) {
                cd.setId(String.valueOf(j));
            } else {
                cd.setId(competitor.getId());
            }

            cd.setName(competitor.getName());
            cd.setSite(competitor.getSite());
            cd.setOn(competitor.getOn());
            cdList.add(cd);
        }
        pd.setCompetitors(cdList);

        RealmList<GroupData> gdList = new RealmList<>();
        for (int j = 0; j < currentRow.getGroups().size(); j++) {
            Group group = currentRow.getGroups().get(j);


            if (j == 0 && !group.getId().equals("-1")) {
                GroupData gd = new GroupData();

                gd.setId("-1");

                gd.setName(context.getString(R.string.allGroup));
                gd.setStatus(group.getStatus());
                gd.setOn("1");
                gd.setEnable(true);
                gdList.add(gd);
            }

            GroupData gd = new GroupData();

            if (group.getId() == null) {
                gd.setId(String.valueOf(j));
            } else {
                gd.setId(group.getId());
            }

            gd.setName(group.getName());
            gd.setStatus(group.getStatus());
            gd.setOn(group.getOn());
            gd.setEnable(group.isEnable());
            gdList.add(gd);
        }
        pd.setGroups(gdList);


        if (pd.getSearchers().size() != 0 && pd.getSearchers().get(0).getRegions().size() != 0) {
            pd.getSearchers().get(0).getRegions().get(0).setSelected(true);
        }

        return pd;
    }
}

