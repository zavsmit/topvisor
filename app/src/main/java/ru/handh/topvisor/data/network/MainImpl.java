package ru.handh.topvisor.data.network;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.handh.topvisor.R;
import ru.handh.topvisor.data.DataManager;
import ru.handh.topvisor.data.network.model.Labels;
import ru.handh.topvisor.data.network.model.ModelList;
import ru.handh.topvisor.data.network.model.ModelPriceForProgects;
import ru.handh.topvisor.data.network.model.ModelResult;
import ru.handh.topvisor.data.network.model.ModelTicket;
import ru.handh.topvisor.data.network.model.ModelUserData;
import ru.handh.topvisor.data.network.model.Projects;
import ru.handh.topvisor.data.network.model.ReturnResponce;
import ru.handh.topvisor.data.network.model.currentProject.ModelCurrentProject;
import ru.handh.topvisor.data.network.model.currentProject.Row;
import ru.handh.topvisor.ui.main.MainEndReq;
import ru.handh.topvisor.ui.menu.MenuEndReq;
import ru.handh.topvisor.utils.Constants;

/**
 * Created by sergey on 10.02.16.
 * реализация запросов
 */
public class MainImpl implements MainModel {

    private RestApi restApi;

    private Projects projects;
    private Labels labels;
    private Context context;

    public MainImpl(RestApi restApi, Context context) {
        this.restApi = restApi;
        this.context = context;
    }

    @Override
    public void reqUserData(final MenuEndReq listener) {
        final ReturnResponce<ModelUserData> result = new ReturnResponce<>();

        Call<ModelUserData> call = restApi.getUserData();
        call.enqueue(new Callback<ModelUserData>() {
            @Override
            public void onResponse(Call<ModelUserData> call, Response<ModelUserData> response) {

                result.setResult(response.body());

                if (response.body() != null) {
                    DataManager.getPref().setUserId(response.body().getId());
                }

                listener.getUserDataResponce(result);

            }

            @Override
            public void onFailure(Call<ModelUserData> call, Throwable t) {
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                result.setIsFailure(true);
                t.getMessage();
                listener.getUserDataResponce(result);
            }
        });
    }

    @Override
    public void reqTikets(final MenuEndReq listener) {
        final ReturnResponce<ModelList<ModelTicket>> result = new ReturnResponce<>();

        Call<ModelList<ModelTicket>> call = restApi.getTikets("1", Constants.COUNT_IN_PAGE);

        call.enqueue(new Callback<ModelList<ModelTicket>>() {
            @Override
            public void onResponse(Call<ModelList<ModelTicket>> call, Response<ModelList<ModelTicket>> response) {

                result.setResult(response.body());
                listener.getTicketsResponce(result);
            }

            @Override
            public void onFailure(Call<ModelList<ModelTicket>> call, Throwable t) {
                result.setIsFailure(true);
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                t.getMessage();
                listener.getTicketsResponce(result);
            }
        });
    }

    @Override
    public void reqGetProjects(final MainEndReq listener) {

        final ReturnResponce<ArrayList<Row>> result = new ReturnResponce<>();

        Call<ModelCurrentProject> call = restApi.getProjects(
                projects.folder,
                "json_pager",
                "mod_projects",
                "1",
                Constants.COUNT_IN_PAGE,
                String.valueOf(labels.id),
                "v2"
        );
        call.enqueue(new Callback<ModelCurrentProject>() {
            @Override
            public void onResponse(Call<ModelCurrentProject> call, Response<ModelCurrentProject> response) {

                final ModelCurrentProject body = response.body();

                if (body.getRows() == null) {
                    result.setIsFailure(true);
                    listener.getProjectsResponce(result);
                    return;
                }

                ArrayList<Row> list = new ArrayList<>();

                for (int i = 0; i < body.getRows().size(); i++) {
                    list.add(body.getRows().get(i));
                }

                result.setResult(list);
                listener.getProjectsResponce(result);

            }

            @Override
            public void onFailure(Call<ModelCurrentProject> call, Throwable t) {
                result.setIsFailure(true);
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                t.getMessage();
                listener.getProjectsResponce(result);
            }
        });
    }

    @Override
    public void reqPriceProjects(final MainEndReq listener, List<Integer> listIds) {
        final ReturnResponce<ModelPriceForProgects> result = new ReturnResponce<>();

        String responce = "{\"id\":[";

        for (int i = 0; i < listIds.size(); i++) {

            if (i == listIds.size() - 1) {
                responce = responce + listIds.get(i);
            } else {
                responce = responce + listIds.get(i) + ",";
            }

        }
        responce = responce + "]}";


        Call<ModelPriceForProgects> call = restApi.getProjectPrice(responce);
        call.enqueue(new Callback<ModelPriceForProgects>() {
            @Override
            public void onResponse(Call<ModelPriceForProgects> call, Response<ModelPriceForProgects> response) {


                result.setResult(response.body());
                listener.getPriceProgect(result);

            }

            @Override
            public void onFailure(Call<ModelPriceForProgects> call, Throwable t) {
                result.setIsFailure(true);
                t.getMessage();
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                listener.getPriceProgect(result);
            }
        });
    }

    @Override
    public void reqInspectionProjects(final MainEndReq listener, int listIds) {
        final ReturnResponce<ModelResult> result = new ReturnResponce<>();

        Call<ModelResult> call = restApi.inspectionProject("edit", String.valueOf(listIds));

        call.enqueue(new Callback<ModelResult>() {
            @Override
            public void onResponse(Call<ModelResult> call, Response<ModelResult> response) {

                result.setResult(response.body());
                listener.getInspectProjectResult(result);
            }

            @Override
            public void onFailure(Call<ModelResult> call, Throwable t) {
                result.setIsFailure(true);
                t.getMessage();
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                listener.getInspectProjectResult(result);
            }
        });
    }

    @Override
    public void reqRemoveProjects(final MainEndReq listener, int listIds) {
        final ReturnResponce<ModelResult> result = new ReturnResponce<>();

        Call<ModelResult> call = restApi.removeProject("del", String.valueOf(listIds));

        call.enqueue(new Callback<ModelResult>() {
            @Override
            public void onResponse(Call<ModelResult> call, Response<ModelResult> response) {

                result.setResult(response.body());
                listener.getInspectProjectResult(result);
            }

            @Override
            public void onFailure(Call<ModelResult> call, Throwable t) {
                result.setIsFailure(true);
                t.getMessage();
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                listener.getInspectProjectResult(result);
            }
        });
    }

    @Override
    public void reqAddProjects(final MainEndReq listener, String name) {
        final ReturnResponce<ModelResult> result = new ReturnResponce<>();

        Call<ModelResult> call = restApi.addProject(name);

        call.enqueue(new Callback<ModelResult>() {
            @Override
            public void onResponse(Call<ModelResult> call, Response<ModelResult> response) {

                result.setResult(response.body());
                listener.addProjectsResponce(result);
            }

            @Override
            public void onFailure(Call<ModelResult> call, Throwable t) {
                result.setIsFailure(true);
                t.getMessage();
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                listener.addProjectsResponce(result);
            }
        });
    }

    @Override
    public void reqRenameProjects(final MainEndReq listener, int listIds, String name) {
        final ReturnResponce<ModelResult> result = new ReturnResponce<>();

        Call<ModelResult> call = restApi.renameProject("edit", String.valueOf(listIds), name);

        call.enqueue(new Callback<ModelResult>() {
            @Override
            public void onResponse(Call<ModelResult> call, Response<ModelResult> response) {

                result.setResult(response.body());
                listener.getInspectProjectResult(result);
            }

            @Override
            public void onFailure(Call<ModelResult> call, Throwable t) {
                result.setIsFailure(true);
                t.getMessage();
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                listener.getInspectProjectResult(result);
            }
        });
    }

    @Override
    public void reqRecoveryProjects(final MainEndReq listener, int listIds) {
        final ReturnResponce<ModelResult> result = new ReturnResponce<>();

        Call<ModelResult> call = restApi.recoveryProject("edit", String.valueOf(listIds), "0");

        call.enqueue(new Callback<ModelResult>() {
            @Override
            public void onResponse(Call<ModelResult> call, Response<ModelResult> response) {

                result.setResult(response.body());
                listener.getInspectProjectResult(result);
            }

            @Override
            public void onFailure(Call<ModelResult> call, Throwable t) {
                result.setIsFailure(true);
                t.getMessage();
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                listener.getInspectProjectResult(result);
            }
        });
    }

    @Override
    public void initHeader() {
        projects = DataManager.getPref().getMainSettingsFolder();
        labels = DataManager.getPref().getMainSettingsTab();
    }

    @Override
    public int getIdResLabel() {
        return labels.icon;
    }

    @Override
    public String getHeaderText() {
        return projects.name;
    }

    @Override
    public void sendGCMToken() {
        final ReturnResponce<ModelResult> result = new ReturnResponce<>();

        String token = DataManager.getPref().getGCMToken();

        Call<ModelResult> call = restApi.sendGCMToken(token);

        call.enqueue(new Callback<ModelResult>() {
            @Override
            public void onResponse(Call<ModelResult> call, Response<ModelResult> response) {

                result.setResult(response.body());
            }

            @Override
            public void onFailure(Call<ModelResult> call, Throwable t) {
                result.setIsFailure(true);
                t.getMessage();
            }
        });
    }

    @Override
    public void removeGCMToken() {
//        final ReturnResponce<ModelResult> result = new ReturnResponce<>();

        Call<ModelResult> call = restApi.removeGCMToken(DataManager.getPref().getGCMToken());

        call.enqueue(new Callback<ModelResult>() {
            @Override
            public void onResponse(Call<ModelResult> call, Response<ModelResult> response) {


                DataManager.getPref().setGCMToken("");

//                result.setResult(response.body());
//                listener.getInspectProjectResult(result);
            }

            @Override
            public void onFailure(Call<ModelResult> call, Throwable t) {
//                result.setIsFailure(true);
//                t.getMessage();
//                result.setErrorMessage(context.getString(R.string.notGetServerData));
//                listener.getInspectProjectResult(result);
            }
        });
    }
}
