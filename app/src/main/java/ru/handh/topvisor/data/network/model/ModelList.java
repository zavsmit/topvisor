package ru.handh.topvisor.data.network.model;

import java.util.List;

/**
 * Created by sergey on 10.02.16.
 */
public class ModelList<T> {

    private String page;
    private String records;
    private List<T> rows;
    private String total;

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getRecords() {
        return records;
    }

    public void setRecords(String records) {
        this.records = records;
    }

    public List<T> getRows() {
        return rows;
    }

    public void setRows(List<T> rows) {
        this.rows = rows;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}
