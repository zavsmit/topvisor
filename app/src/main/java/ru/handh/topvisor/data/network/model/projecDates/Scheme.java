
package ru.handh.topvisor.data.network.model.projecDates;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Scheme {

    @SerializedName("dates")
    @Expose
    private List<Date> dates = new ArrayList<Date>();
    @SerializedName("visitors_exists")
    @Expose
    private Integer visitorsExists;
    @SerializedName("searchers")
    @Expose
    private List<Searcher> searchers = new ArrayList<Searcher>();

    /**
     * 
     * @return
     *     The dates
     */
    public List<Date> getDates() {
        return dates;
    }

    /**
     * 
     * @param dates
     *     The dates
     */
    public void setDates(List<Date> dates) {
        this.dates = dates;
    }

    /**
     * 
     * @return
     *     The visitorsExists
     */
    public Integer getVisitorsExists() {
        return visitorsExists;
    }

    /**
     * 
     * @param visitorsExists
     *     The visitors_exists
     */
    public void setVisitorsExists(Integer visitorsExists) {
        this.visitorsExists = visitorsExists;
    }

    /**
     * 
     * @return
     *     The searchers
     */
    public List<Searcher> getSearchers() {
        return searchers;
    }

    /**
     * 
     * @param searchers
     *     The searchers
     */
    public void setSearchers(List<Searcher> searchers) {
        this.searchers = searchers;
    }

}
