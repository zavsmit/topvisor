package ru.handh.topvisor.data.network;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.handh.topvisor.R;
import ru.handh.topvisor.data.network.model.ModelCompetitorsAdd;
import ru.handh.topvisor.data.network.model.ModelResult;
import ru.handh.topvisor.data.network.model.ModelResultPS;
import ru.handh.topvisor.data.network.model.ModelResultType;
import ru.handh.topvisor.data.network.model.ModelSearchRegion;
import ru.handh.topvisor.data.network.model.ResultCompetitors;
import ru.handh.topvisor.data.network.model.ReturnResponce;
import ru.handh.topvisor.ui.settingsProject.SettingsEndReq;
import ru.handh.topvisor.ui.settingsProject.interval.IntervalReqResult;
import ru.handh.topvisor.ui.settingsProject.searchSystems.SearcherResult;
import ru.handh.topvisor.ui.settingsProject.searchSystems.searchRegion.SearchRegionResult;

/**
 * Created by sergey on 22.03.16.
 * работа с сетью экрана настроек
 */
public class SettingsImpl implements SettingsModel {
    private RestApi restApi;
    private Context context;

    public SettingsImpl(RestApi restApi, Context context) {
        this.restApi = restApi;
        this.context = context;
    }

    @Override
    public void reqAddCompetitors(String name, String idProject, final SettingsEndReq listener) {
        final ReturnResponce<ResultCompetitors> result = new ReturnResponce<>();

        Call<ModelCompetitorsAdd> call = restApi.addCompetitor("add", name, idProject, "");
        call.enqueue(new Callback<ModelCompetitorsAdd>() {
            @Override
            public void onResponse(Call<ModelCompetitorsAdd> call, Response<ModelCompetitorsAdd> response) {

                final ModelCompetitorsAdd body = response.body();

                if (body.isError()) {
                    result.setIsFailure(true);
                    result.setErrorMessage(body.getMessage());
                    return;
                } else {
                    result.setResult(response.body().getResult().get(0));
                }

                listener.addCompetitorsResponce(result);

            }

            @Override
            public void onFailure(Call<ModelCompetitorsAdd> call, Throwable t) {
                result.setIsFailure(true);
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                t.getMessage();
                listener.addCompetitorsResponce(result);
            }
        });
    }

    @Override
    public void reqRemoveCompetitors(String idCompetitors, final SettingsEndReq listener) {
        final ReturnResponce<ModelResult> result = new ReturnResponce<>();

        Call<ModelResult> call = restApi.removeCompetitor(idCompetitors);
        call.enqueue(new Callback<ModelResult>() {
            @Override
            public void onResponse(Call<ModelResult> call, Response<ModelResult> response) {

                final ModelResult body = response.body();

                if (body.isError()) {
                    result.setIsFailure(true);
                    result.setErrorMessage(body.getMessage());
                    return;
                }

                listener.removeCompetitorsResponce(result);
            }

            @Override
            public void onFailure(Call<ModelResult> call, Throwable t) {
                result.setIsFailure(true);
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                t.getMessage();
                listener.removeCompetitorsResponce(result);
            }
        });
    }

    @Override
    public void reqRemoveGroup(String idGroup, final int position, final SettingsEndReq listener) {
        final ReturnResponce<ModelResult> result = new ReturnResponce<>();

        Call<ModelResult> call = restApi.removeGroup("del", idGroup);
        call.enqueue(new Callback<ModelResult>() {
            @Override
            public void onResponse(Call<ModelResult> call, Response<ModelResult> response) {

                final ModelResult body = response.body();

                if (body.isError()) {
                    result.setIsFailure(true);
                    result.setErrorMessage(body.getMessage());
                    return;
                }

                listener.removeGroupResponce(result, position);
            }

            @Override
            public void onFailure(Call<ModelResult> call, Throwable t) {
                result.setIsFailure(true);
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                t.getMessage();
                listener.removeGroupResponce(result, position);
            }
        });
    }

    @Override
    public void reqCheckGroup(String idProject, String on, String idGroup, final SettingsEndReq listener) {
        final ReturnResponce<ModelResult> result = new ReturnResponce<>();

        Call<ModelResult> call = restApi.checkGroup("edit", idProject, on, idGroup);
        call.enqueue(new Callback<ModelResult>() {
            @Override
            public void onResponse(Call<ModelResult> call, Response<ModelResult> response) {

                final ModelResult body = response.body();

                if (body.isError()) {
                    result.setIsFailure(true);
                    result.setErrorMessage(body.getMessage());
                    return;
                }

                listener.checkGroupResponce(result);
            }

            @Override
            public void onFailure(Call<ModelResult> call, Throwable t) {
                result.setIsFailure(true);
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                t.getMessage();
                listener.checkGroupResponce(result);
            }
        });
    }

    @Override
    public void reqAddGroup(String idProject, String name, final SettingsEndReq listener) {
        final ReturnResponce<ModelResult> result = new ReturnResponce<>();

        Call<ModelResult> call = restApi.addGroup("add", name, idProject, "0", "1");
        call.enqueue(new Callback<ModelResult>() {
            @Override
            public void onResponse(Call<ModelResult> call, Response<ModelResult> response) {

                final ModelResult body = response.body();

                if (body.isError()) {
                    result.setIsFailure(true);
                    result.setErrorMessage(body.getMessage());
                    return;
                } else {
                    result.setResult(response.body());
                }

                listener.addGroupResponce(result);
            }

            @Override
            public void onFailure(Call<ModelResult> call, Throwable t) {
                result.setIsFailure(true);
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                t.getMessage();
                listener.addGroupResponce(result);
            }
        });
    }

    @Override
    public void reqRenameGroup(String idProject, String name, String idGroup, final SettingsEndReq listener) {
        final ReturnResponce<ModelResult> result = new ReturnResponce<>();

        Call<ModelResult> call = restApi.renameGroup("edit", name, idProject, idGroup);
        call.enqueue(new Callback<ModelResult>() {
            @Override
            public void onResponse(Call<ModelResult> call, Response<ModelResult> response) {

                final ModelResult body = response.body();

                if (body.isError()) {
                    result.setIsFailure(true);
                    result.setErrorMessage(body.getMessage());
                    return;
                }

                listener.renameGroupResponce(result);
            }

            @Override
            public void onFailure(Call<ModelResult> call, Throwable t) {
                result.setIsFailure(true);
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                t.getMessage();
                listener.renameGroupResponce(result);
            }
        });
    }

    @Override
    public void reqAddPS(String idProject, String searcher, final SearcherResult listener) {
        final ReturnResponce<ModelResultPS> result = new ReturnResponce<>();

        Call<ModelResultPS> call = restApi.addPS("edit", idProject, searcher, "");
        call.enqueue(new Callback<ModelResultPS>() {
            @Override
            public void onResponse(Call<ModelResultPS> call, Response<ModelResultPS> response) {

                final ModelResultPS body = response.body();

                if (body.isError()) {
                    result.setIsFailure(true);
                    result.setErrorMessage(body.getMessage());
                    return;
                } else {
                    result.setResult(body);
                }

                listener.addPSResponce(result);
            }

            @Override
            public void onFailure(Call<ModelResultPS> call, Throwable t) {
                result.setIsFailure(true);
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                t.getMessage();
                listener.addPSResponce(result);
            }
        });
    }

    @Override
    public void reqCheckedPS(String idSearcher, boolean isChecked, final SearcherResult listener, final int position) {
        final ReturnResponce<ModelResultPS> result = new ReturnResponce<>();


        String check = "0";

        if (isChecked)
            check = "1";

        Call<ModelResultPS> call = restApi.checkedPS("edit", check, idSearcher);
        call.enqueue(new Callback<ModelResultPS>() {
            @Override
            public void onResponse(Call<ModelResultPS> call, Response<ModelResultPS> response) {

                final ModelResultPS body = response.body();

                if (body.isError()) {
                    result.setIsFailure(true);
                    result.setErrorMessage(body.getMessage());
                    return;
                } else {
                    result.setResult(body);
                }

                listener.checkedPSResponce(result, position);
            }

            @Override
            public void onFailure(Call<ModelResultPS> call, Throwable t) {
                result.setIsFailure(true);
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                t.getMessage();
                listener.checkedPSResponce(result, position);
            }
        });
    }

    @Override
    public void reqRemovePS(String idSearcher, final SearcherResult listener, final int position, final String type) {
        final ReturnResponce<ModelResult> result = new ReturnResponce<>();

        Call<ModelResult> call = restApi.removePS("del", idSearcher);
        call.enqueue(new Callback<ModelResult>() {
            @Override
            public void onResponse(Call<ModelResult> call, Response<ModelResult> response) {

                final ModelResult body = response.body();

                if (body.isError()) {
                    result.setIsFailure(true);
                    result.setErrorMessage(body.getMessage());
                    return;
                }

                listener.removePSResponce(result, position, type);
            }

            @Override
            public void onFailure(Call<ModelResult> call, Throwable t) {
                result.setIsFailure(true);
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                t.getMessage();
                listener.removePSResponce(result, position, type);
            }
        });
    }

    @Override
    public void reqChangeItemsPS(String idSearcher,
                                 final SearcherResult listener,
                                 ArrayList<String> listItems,
                                 final ArrayList<Integer> positions) {

        final ReturnResponce<ModelResultPS> result = new ReturnResponce<>();

        Call<ModelResultPS> call = restApi.changeRegions(listItems, idSearcher);
        call.enqueue(new Callback<ModelResultPS>() {
            @Override
            public void onResponse(Call<ModelResultPS> call, Response<ModelResultPS> response) {

                final ModelResultPS body = response.body();

                if (body.isError()) {
                    result.setIsFailure(true);
                    result.setErrorMessage(body.getMessage());
                    return;
                } else {
                    result.setResult(body);
                }

                listener.changeItemsPSResponce(result, positions);
            }

            @Override
            public void onFailure(Call<ModelResultPS> call, Throwable t) {
                result.setIsFailure(true);
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                t.getMessage();
                listener.changeItemsPSResponce(result, positions);
            }
        });
    }

    @Override
    public void resSearchRegions(String idSearcher, final String term, final SearchRegionResult listener) {
        final ReturnResponce<List<ModelSearchRegion>> result = new ReturnResponce<>();

        Call<List<ModelSearchRegion>> call = restApi.reqSearchRegions("20", term, idSearcher);
        call.enqueue(new Callback<List<ModelSearchRegion>>() {
            @Override
            public void onResponse(Call<List<ModelSearchRegion>> call, Response<List<ModelSearchRegion>> response) {

                final List<ModelSearchRegion> body = response.body();


                if (response.errorBody() != null) {
                    result.setIsFailure(true);
                    return;
                } else {
                    result.setResult(body);
                }

                listener.searchRegionResponce(result, term);
            }

            @Override
            public void onFailure(Call<List<ModelSearchRegion>> call, Throwable t) {
                result.setIsFailure(true);
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                t.getMessage();
                listener.searchRegionResponce(result, term);
            }
        });
    }

    @Override
    public void reqAddRegions(String idSearcher, List<String> regions, final SearchRegionResult listener) {
        final ReturnResponce<ModelResultType> result = new ReturnResponce<>();

        Call<ModelResultType> call = restApi.reqAddRegions(idSearcher, regions);
        call.enqueue(new Callback<ModelResultType>() {
            @Override
            public void onResponse(Call<ModelResultType> call, Response<ModelResultType> response) {

                final ModelResultType body = response.body();


                if (body.isError()) {
                    result.setIsFailure(true);
                    result.setErrorMessage(body.getMessage());
                    return;
                } else {
                    result.setResult(body);
                }

                listener.addRegionsResponce(result);
            }

            @Override
            public void onFailure(Call<ModelResultType> call, Throwable t) {
                result.setIsFailure(true);
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                t.getMessage();
                listener.addRegionsResponce(result);
            }
        });
    }

    @Override
    public void reqChangeInterval(int on, String idProject, String timeForUpdate, String waitAfrepUpdate, final IntervalReqResult listener) {
        final ReturnResponce<ModelResult> result = new ReturnResponce<>();

        Call<ModelResult> call = restApi.reqAddRegions(on, idProject, timeForUpdate, waitAfrepUpdate);
        call.enqueue(new Callback<ModelResult>() {
            @Override
            public void onResponse(Call<ModelResult> call, Response<ModelResult> response) {

                final ModelResult body = response.body();

                if (body.isError()) {
                    result.setIsFailure(true);
                    result.setErrorMessage(body.getMessage());
                    return;
                }

                listener.changeIntervalResponce(result);
            }

            @Override
            public void onFailure(Call<ModelResult> call, Throwable t) {
                result.setIsFailure(true);
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                t.getMessage();
                listener.changeIntervalResponce(result);
            }
        });
    }

    @Override
    public void reqAutoCond(String selected, String idProject, final IntervalReqResult listener) {
        final ReturnResponce<ModelResult> result = new ReturnResponce<>();

        Call<ModelResult> call = restApi.reqAutoCond(selected, idProject);
        call.enqueue(new Callback<ModelResult>() {
            @Override
            public void onResponse(Call<ModelResult> call, Response<ModelResult> response) {

                final ModelResult body = response.body();

                if (body.isError()) {
                    result.setIsFailure(true);
                    result.setErrorMessage(body.getMessage());
                    return;
                }

                listener.changeIntervalResponce(result);
            }

            @Override
            public void onFailure(Call<ModelResult> call, Throwable t) {
                result.setIsFailure(true);
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                t.getMessage();
                listener.changeIntervalResponce(result);
            }
        });
    }


}
