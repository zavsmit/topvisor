package ru.handh.topvisor.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by sergey on 07.04.16.
 */
public class ModelItemBank {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("target")
    @Expose
    private String target;
    @SerializedName("target_id")
    @Expose
    private String targetId;
    @SerializedName("sum")
    @Expose
    private double sum;
    @SerializedName("plus")
    @Expose
    private String plus;
    @SerializedName("info")
    @Expose
    private String info;
    @SerializedName("date")
    @Expose
    private Date date;
    @SerializedName("date_day")
    @Expose
    private Date dateDay;
    @SerializedName("to_ref")
    @Expose
    private String toRef;

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId The user_id
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return The target
     */
    public String getTarget() {
        return target;
    }

    /**
     * @param target The target
     */
    public void setTarget(String target) {
        this.target = target;
    }

    /**
     * @return The targetId
     */
    public String getTargetId() {
        return targetId;
    }

    /**
     * @param targetId The target_id
     */
    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    /**
     * @return The sum
     */
    public double getSum() {
        return sum;
    }

    /**
     * @param sum The sum
     */
    public void setSum(double sum) {
        this.sum = sum;
    }

    /**
     * @return The plus
     */
    public String getPlus() {
        return plus;
    }

    /**
     * @param plus The plus
     */
    public void setPlus(String plus) {
        this.plus = plus;
    }

    /**
     * @return The info
     */
    public String getInfo() {
        return info;
    }

    /**
     * @param info The info
     */
    public void setInfo(String info) {
        this.info = info;
    }

    /**
     * @return The date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param date The date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @return The dateDay
     */
    public Date getDateDay() {
        return dateDay;
    }

    /**
     * @param dateDay The date_day
     */
    public void setDateDay(Date dateDay) {
        this.dateDay = dateDay;
    }

    /**
     * @return The toRef
     */
    public String getToRef() {
        return toRef;
    }

    /**
     * @param toRef The to_ref
     */
    public void setToRef(String toRef) {
        this.toRef = toRef;
    }

}
