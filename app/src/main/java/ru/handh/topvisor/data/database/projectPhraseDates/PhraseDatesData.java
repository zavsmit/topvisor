package ru.handh.topvisor.data.database.projectPhraseDates;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by sergey on 29.04.16.
 */
public class PhraseDatesData extends RealmObject {

    @PrimaryKey
    private String idProjectRegionKey;
    private RealmList<RealmString> allDates;

    public String getIdProjectRegionKey() {
        return idProjectRegionKey;
    }

    public void setIdProjectRegionKey(String idProjectRegionKey) {
        this.idProjectRegionKey = idProjectRegionKey;
    }

    public RealmList<RealmString> getAllDates() {
        return allDates;
    }

    public void setAllDates(RealmList<RealmString> allDates) {
        this.allDates = allDates;
    }
}
