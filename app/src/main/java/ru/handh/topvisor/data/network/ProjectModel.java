package ru.handh.topvisor.data.network;

import java.util.List;

import ru.handh.topvisor.ui.project.ProjectEndReq;

/**
 * Created by sergey on 19.02.16.
 * интерфейсы модели для экрана заросов
 */
public interface ProjectModel {

    void reqItemProject(String jsonSearch, ProjectEndReq listener);

    void loadDatesProject(String idProject,
                          String competitorId,
                          String regionKey,
                          String SearchData,
                          String group,
                          ProjectEndReq listener);

    void loadPhrasesProject(String competitorid, String idProject, String regionKey,
                            List<String> dates, final ProjectEndReq listener,
                            final int currentNumberPage, String groupId,
                            String searcher, String page);

    void reqRemovePhrare(String idProject, String idPharse, final ProjectEndReq listener);

    void reqAddPhrase(String phrase, String projectId, String groupId, ProjectEndReq listener);

    void loadPriceOnFilter(String searchJson,
                           String searcher,
                           String searcherDomen,
                           String regionKey,
                           String regionLang,
                           String groupId,
                           final ProjectEndReq listener);

    void reqInspectOnFilter(String id,
                            String searcher,
                            String searcherDomen,
                            String regionKey,
                            String regionLang,
                            String groupId,
                            final ProjectEndReq listener);
}
