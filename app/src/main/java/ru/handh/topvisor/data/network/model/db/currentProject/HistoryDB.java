
package ru.handh.topvisor.data.network.model.db.currentProject;

import java.util.Date;

import io.realm.RealmObject;

public class HistoryDB extends RealmObject {

    private String id;
    private String projectId;
    private String cy;
    private String pr;
    private String indexedY;
    private String indexedG;
    private String trafficY;
    private String trafficG;
    private String trafficM;
    private String backlinks;
    private String backlinksAh;
    private Date date;

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The projectId
     */
    public String getProjectId() {
        return projectId;
    }

    /**
     * 
     * @param projectId
     *     The project_id
     */
    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    /**
     * 
     * @return
     *     The cy
     */
    public String getCy() {
        return cy;
    }

    /**
     * 
     * @param cy
     *     The cy
     */
    public void setCy(String cy) {
        this.cy = cy;
    }

    /**
     * 
     * @return
     *     The pr
     */
    public String getPr() {
        return pr;
    }

    /**
     * 
     * @param pr
     *     The pr
     */
    public void setPr(String pr) {
        this.pr = pr;
    }

    /**
     * 
     * @return
     *     The indexedY
     */
    public String getIndexedY() {
        return indexedY;
    }

    /**
     * 
     * @param indexedY
     *     The indexed_y
     */
    public void setIndexedY(String indexedY) {
        this.indexedY = indexedY;
    }

    /**
     * 
     * @return
     *     The indexedG
     */
    public String getIndexedG() {
        return indexedG;
    }

    /**
     * 
     * @param indexedG
     *     The indexed_g
     */
    public void setIndexedG(String indexedG) {
        this.indexedG = indexedG;
    }

    /**
     * 
     * @return
     *     The trafficY
     */
    public String getTrafficY() {
        return trafficY;
    }

    /**
     * 
     * @param trafficY
     *     The traffic_y
     */
    public void setTrafficY(String trafficY) {
        this.trafficY = trafficY;
    }

    /**
     * 
     * @return
     *     The trafficG
     */
    public String getTrafficG() {
        return trafficG;
    }

    /**
     * 
     * @param trafficG
     *     The traffic_g
     */
    public void setTrafficG(String trafficG) {
        this.trafficG = trafficG;
    }

    /**
     * 
     * @return
     *     The trafficM
     */
    public String getTrafficM() {
        return trafficM;
    }

    /**
     * 
     * @param trafficM
     *     The traffic_m
     */
    public void setTrafficM(String trafficM) {
        this.trafficM = trafficM;
    }

    /**
     * 
     * @return
     *     The backlinks
     */
    public String getBacklinks() {
        return backlinks;
    }

    /**
     * 
     * @param backlinks
     *     The backlinks
     */
    public void setBacklinks(String backlinks) {
        this.backlinks = backlinks;
    }

    /**
     * 
     * @return
     *     The backlinksAh
     */
    public String getBacklinksAh() {
        return backlinksAh;
    }

    /**
     * 
     * @param backlinksAh
     *     The backlinks_ah
     */
    public void setBacklinksAh(String backlinksAh) {
        this.backlinksAh = backlinksAh;
    }

    /**
     * 
     * @return
     *     The date
     */
    public Date getDate() {
        return date;
    }

    /**
     * 
     * @param date
     *     The date
     */
    public void setDate(Date date) {
        this.date = date;
    }

}
