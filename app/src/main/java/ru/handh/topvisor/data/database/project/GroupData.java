package ru.handh.topvisor.data.database.project;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by sergey on 28.04.16.
 * для групп
 */
public class GroupData extends RealmObject {
    @PrimaryKey
    private String id;
    private String name;
    private String on;
    private String status;
    private boolean isEnable;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOn() {
        return on;
    }

    public void setOn(String on) {
        this.on = on;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isEnable() {
        return isEnable;
    }

    public void setEnable(boolean enable) {
        isEnable = enable;
    }
}
