package ru.handh.topvisor.data.network.model;

import java.util.ArrayList;

import ru.handh.topvisor.data.network.model.currentProject.Region;

/**
 * Created by sergey on 23.03.16.
 */
public class ModelResultPS {

    private ResultPS result;
    private String message;
    private boolean error;

    public ResultPS getResult() {
        return result;
    }

    public void setResult(ResultPS result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public class ResultPS {
        private String id;
        private String project_id;
        private String searcher;
        private String enabled;
        private String ord;
        private String name;
        private ArrayList<Region> regions;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getProject_id() {
            return project_id;
        }

        public void setProject_id(String project_id) {
            this.project_id = project_id;
        }

        public String getSearcher() {
            return searcher;
        }

        public void setSearcher(String searcher) {
            this.searcher = searcher;
        }

        public String getEnabled() {
            return enabled;
        }

        public void setEnabled(String enabled) {
            this.enabled = enabled;
        }

        public String getOrd() {
            return ord;
        }

        public void setOrd(String ord) {
            this.ord = ord;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public ArrayList<Region> getRegions() {
            return regions;
        }

        public void setRegions(ArrayList<Region> regions) {
            this.regions = regions;
        }
    }

}
