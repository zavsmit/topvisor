package ru.handh.topvisor.data.network;

import java.util.List;

import ru.handh.topvisor.ui.bank.BankEndReq;

/**
 * Created by sergey on 07.04.16.
 */
public interface BankModel {
    void getBankListDate(int page, BankEndReq listener, List<String> dates);
    void getBankListAllParam(int page, BankEndReq listener, String idProject, List<String> dates);
    void getBankPriceDate(BankEndReq listener, List<String> dates);
    void getBankPriceAllParam(BankEndReq listener, String idProject, List<String> dates);

}
