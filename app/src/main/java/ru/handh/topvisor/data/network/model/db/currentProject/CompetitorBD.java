
package ru.handh.topvisor.data.network.model.db.currentProject;

import io.realm.RealmObject;

public class CompetitorBD extends RealmObject {

    private String id;
    private String name;
    private String site;
    private String on;

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The site
     */
    public String getSite() {
        return site;
    }

    /**
     * 
     * @param site
     *     The site
     */
    public void setSite(String site) {
        this.site = site;
    }

    /**
     * 
     * @return
     *     The on
     */
    public String getOn() {
        return on;
    }

    /**
     * 
     * @param on
     *     The on
     */
    public void setOn(String on) {
        this.on = on;
    }

}
