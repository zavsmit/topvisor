package ru.handh.topvisor.data.network.model;

import java.util.ArrayList;

/**
 * Created by sergey on 02.02.16.
 * авторизация
 */
public class ModelAuth {
    private int page;
    private int total;
    private int records;
    private ArrayList<Integer> rows;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getRecords() {
        return records;
    }

    public void setRecords(int records) {
        this.records = records;
    }

    public ArrayList<Integer> getRows() {
        return rows;
    }

    public void setRows(ArrayList<Integer> rows) {
        this.rows = rows;
    }
}
