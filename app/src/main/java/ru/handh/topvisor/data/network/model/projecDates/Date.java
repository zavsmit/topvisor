
package ru.handh.topvisor.data.network.model.projecDates;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Date {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("all_visitors")
    @Expose
    private Integer allVisitors;
    @SerializedName("top_percent")
    @Expose
    private Integer topPercent;

    /**
     * 
     * @return
     *     The date
     */
    public String getDate() {
        return date;
    }

    /**
     * 
     * @param date
     *     The date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * 
     * @return
     *     The allVisitors
     */
    public Integer getAllVisitors() {
        return allVisitors;
    }

    /**
     * 
     * @param allVisitors
     *     The all_visitors
     */
    public void setAllVisitors(Integer allVisitors) {
        this.allVisitors = allVisitors;
    }

    /**
     * 
     * @return
     *     The topPercent
     */
    public Integer getTopPercent() {
        return topPercent;
    }

    /**
     * 
     * @param topPercent
     *     The top_percent
     */
    public void setTopPercent(Integer topPercent) {
        this.topPercent = topPercent;
    }

}
