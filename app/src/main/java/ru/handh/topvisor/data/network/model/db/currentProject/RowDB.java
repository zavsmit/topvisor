
package ru.handh.topvisor.data.network.model.db.currentProject;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;

public class RowDB extends RealmObject {

    private String id;
    private String user;
    private String name;
    private String site;
    private String competitor;
    private String competitorOrd;
    private String comment;
    private Date date;
    private Date update;
    private Date lastView;
    private String status;
    private Boolean statusFrequency;
    private String statusClaster;
    private String subdomains;
    private String filter;
    private String autoCorrect;
    private String commonTraffic;
    private String on;
    private String autoCond;
    private String waitAfterUpdates;
    private String timeForUpdate;
    private String withSnippets;
    private String countKeywords;
    private String countTasks;
    private String price;
    private String reportOn;
    private String reportTime;
    private String reportFormat;
    private String guestLinkRight;
    private String catY;
    private String catDm;
    private String catM;
    private String right;
    private String email;
    private String favorite;
    private String tag;
    private String percentOfParse;
    private RealmList<HistoryDB> historyDB = new RealmList<HistoryDB>();
    private RealmList<SearcherDB> searcherDBs = new RealmList<SearcherDB>();
    private RealmList<CompetitorBD> competitorBDs = new RealmList<CompetitorBD>();
    private RealmList<GroupDB> groupDBs = new RealmList<GroupDB>();
    private String dynamicsLimit;

    public String getPercentOfParse() {
        return percentOfParse;
    }

    public void setPercentOfParse(String percentOfParse) {
        this.percentOfParse = percentOfParse;
    }

    /**
     *
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     *     The user
     */
    public String getUser() {
        return user;
    }

    /**
     *
     * @param user
     *     The user
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     *
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     *     The site
     */
    public String getSite() {
        return site;
    }

    /**
     *
     * @param site
     *     The site
     */
    public void setSite(String site) {
        this.site = site;
    }

    /**
     *
     * @return
     *     The competitor
     */
    public String getCompetitor() {
        return competitor;
    }

    /**
     *
     * @param competitor
     *     The competitor
     */
    public void setCompetitor(String competitor) {
        this.competitor = competitor;
    }

    /**
     *
     * @return
     *     The competitorOrd
     */
    public String getCompetitorOrd() {
        return competitorOrd;
    }

    /**
     *
     * @param competitorOrd
     *     The competitor_ord
     */
    public void setCompetitorOrd(String competitorOrd) {
        this.competitorOrd = competitorOrd;
    }

    /**
     *
     * @return
     *     The comment
     */
    public String getComment() {
        return comment;
    }

    /**
     *
     * @param comment
     *     The comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     *
     * @return
     *     The date
     */
    public Date getDate() {
        return date;
    }

    /**
     *
     * @param date
     *     The date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     *
     * @return
     *     The update
     */
    public Date getUpdate() {
        return update;
    }

    /**
     *
     * @param update
     *     The update
     */
    public void setUpdate(Date update) {
        this.update = update;
    }

    /**
     *
     * @return
     *     The lastView
     */
    public Date getLastView() {
        return lastView;
    }

    /**
     *
     * @param lastView
     *     The last_view
     */
    public void setLastView(Date lastView) {
        this.lastView = lastView;
    }

    /**
     *
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     *     The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return
     *     The statusFrequency
     */
    public Boolean getStatusFrequency() {
        return statusFrequency;
    }

    /**
     *
     * @param statusFrequency
     *     The status_frequency
     */
    public void setStatusFrequency(Boolean statusFrequency) {
        this.statusFrequency = statusFrequency;
    }

    /**
     *
     * @return
     *     The statusClaster
     */
    public String getStatusClaster() {
        return statusClaster;
    }

    /**
     *
     * @param statusClaster
     *     The status_claster
     */
    public void setStatusClaster(String statusClaster) {
        this.statusClaster = statusClaster;
    }

    /**
     *
     * @return
     *     The subdomains
     */
    public String getSubdomains() {
        return subdomains;
    }

    /**
     *
     * @param subdomains
     *     The subdomains
     */
    public void setSubdomains(String subdomains) {
        this.subdomains = subdomains;
    }

    /**
     *
     * @return
     *     The filter
     */
    public String getFilter() {
        return filter;
    }

    /**
     *
     * @param filter
     *     The filter
     */
    public void setFilter(String filter) {
        this.filter = filter;
    }

    /**
     *
     * @return
     *     The autoCorrect
     */
    public String getAutoCorrect() {
        return autoCorrect;
    }

    /**
     *
     * @param autoCorrect
     *     The auto_correct
     */
    public void setAutoCorrect(String autoCorrect) {
        this.autoCorrect = autoCorrect;
    }

    /**
     *
     * @return
     *     The commonTraffic
     */
    public String getCommonTraffic() {
        return commonTraffic;
    }

    /**
     *
     * @param commonTraffic
     *     The common_traffic
     */
    public void setCommonTraffic(String commonTraffic) {
        this.commonTraffic = commonTraffic;
    }

    /**
     *
     * @return
     *     The on
     */
    public String getOn() {
        return on;
    }

    /**
     *
     * @param on
     *     The on
     */
    public void setOn(String on) {
        this.on = on;
    }

    /**
     *
     * @return
     *     The autoCond
     */
    public String getAutoCond() {
        return autoCond;
    }

    /**
     *
     * @param autoCond
     *     The auto_cond
     */
    public void setAutoCond(String autoCond) {
        this.autoCond = autoCond;
    }

    /**
     *
     * @return
     *     The waitAfterUpdates
     */
    public String getWaitAfterUpdates() {
        return waitAfterUpdates;
    }

    /**
     *
     * @param waitAfterUpdates
     *     The wait_after_updates
     */
    public void setWaitAfterUpdates(String waitAfterUpdates) {
        this.waitAfterUpdates = waitAfterUpdates;
    }

    /**
     *
     * @return
     *     The timeForUpdate
     */
    public String getTimeForUpdate() {
        return timeForUpdate;
    }

    /**
     *
     * @param timeForUpdate
     *     The time_for_update
     */
    public void setTimeForUpdate(String timeForUpdate) {
        this.timeForUpdate = timeForUpdate;
    }

    /**
     *
     * @return
     *     The withSnippets
     */
    public String getWithSnippets() {
        return withSnippets;
    }

    /**
     *
     * @param withSnippets
     *     The with_snippets
     */
    public void setWithSnippets(String withSnippets) {
        this.withSnippets = withSnippets;
    }

    /**
     *
     * @return
     *     The countKeywords
     */
    public String getCountKeywords() {
        return countKeywords;
    }

    /**
     *
     * @param countKeywords
     *     The count_keywords
     */
    public void setCountKeywords(String countKeywords) {
        this.countKeywords = countKeywords;
    }

    /**
     *
     * @return
     *     The countTasks
     */
    public String getCountTasks() {
        return countTasks;
    }

    /**
     *
     * @param countTasks
     *     The count_tasks
     */
    public void setCountTasks(String countTasks) {
        this.countTasks = countTasks;
    }

    /**
     *
     * @return
     *     The price
     */
    public String getPrice() {
        return price;
    }

    /**
     *
     * @param price
     *     The price
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     *
     * @return
     *     The reportOn
     */
    public String getReportOn() {
        return reportOn;
    }

    /**
     *
     * @param reportOn
     *     The report_on
     */
    public void setReportOn(String reportOn) {
        this.reportOn = reportOn;
    }

    /**
     *
     * @return
     *     The reportTime
     */
    public String getReportTime() {
        return reportTime;
    }

    /**
     *
     * @param reportTime
     *     The report_time
     */
    public void setReportTime(String reportTime) {
        this.reportTime = reportTime;
    }

    /**
     *
     * @return
     *     The reportFormat
     */
    public String getReportFormat() {
        return reportFormat;
    }

    /**
     *
     * @param reportFormat
     *     The report_format
     */
    public void setReportFormat(String reportFormat) {
        this.reportFormat = reportFormat;
    }

    /**
     *
     * @return
     *     The guestLinkRight
     */
    public String getGuestLinkRight() {
        return guestLinkRight;
    }

    /**
     *
     * @param guestLinkRight
     *     The guest_link_right
     */
    public void setGuestLinkRight(String guestLinkRight) {
        this.guestLinkRight = guestLinkRight;
    }

    /**
     *
     * @return
     *     The catY
     */
    public String getCatY() {
        return catY;
    }

    /**
     *
     * @param catY
     *     The cat_y
     */
    public void setCatY(String catY) {
        this.catY = catY;
    }

    /**
     *
     * @return
     *     The catDm
     */
    public String getCatDm() {
        return catDm;
    }

    /**
     *
     * @param catDm
     *     The cat_dm
     */
    public void setCatDm(String catDm) {
        this.catDm = catDm;
    }

    /**
     *
     * @return
     *     The catM
     */
    public String getCatM() {
        return catM;
    }

    /**
     *
     * @param catM
     *     The cat_m
     */
    public void setCatM(String catM) {
        this.catM = catM;
    }

    /**
     *
     * @return
     *     The right
     */
    public String getRight() {
        return right;
    }

    /**
     *
     * @param right
     *     The right
     */
    public void setRight(String right) {
        this.right = right;
    }

    /**
     *
     * @return
     *     The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     *     The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     *     The favorite
     */
    public String getFavorite() {
        return favorite;
    }

    /**
     *
     * @param favorite
     *     The favorite
     */
    public void setFavorite(String favorite) {
        this.favorite = favorite;
    }

    /**
     *
     * @return
     *     The tag
     */
    public String getTag() {
        return tag;
    }

    /**
     *
     * @param tag
     *     The tag
     */
    public void setTag(String tag) {
        this.tag = tag;
    }

    /**
     *
     * @return
     *     The historyDB
     */
    public RealmList<HistoryDB> getHistoryDB() {
        return historyDB;
    }

    /**
     *
     * @param historyDB
     *     The historyDB
     */
    public void setHistoryDB(RealmList<HistoryDB> historyDB) {
        this.historyDB = historyDB;
    }

    /**
     *
     * @return
     *     The searcherDBs
     */
    public RealmList<SearcherDB> getSearcherDBs() {
        return searcherDBs;
    }

    /**
     *
     * @param searcherDBs
     *     The searcherDBs
     */
    public void setSearcherDBs(RealmList<SearcherDB> searcherDBs) {
        this.searcherDBs = searcherDBs;
    }

    /**
     *
     * @return
     *     The competitorBDs
     */
    public RealmList<CompetitorBD> getCompetitorBDs() {
        return competitorBDs;
    }

    /**
     *
     * @param competitorBDs
     *     The competitorBDs
     */
    public void setCompetitorBDs(RealmList<CompetitorBD> competitorBDs) {
        this.competitorBDs = competitorBDs;
    }

    /**
     *
     * @return
     *     The groupDBs
     */
    public RealmList<GroupDB> getGroupDBs() {
        return groupDBs;
    }

    /**
     *
     * @param groupDBs
     *     The groupDBs
     */
    public void setGroupDBs(RealmList<GroupDB> groupDBs) {
        this.groupDBs = groupDBs;
    }

    /**
     *
     * @return
     *     The dynamicsLimit
     */
    public String getDynamicsLimit() {
        return dynamicsLimit;
    }

    /**
     *
     * @param dynamicsLimit
     *     The dynamics_limit
     */
    public void setDynamicsLimit(String dynamicsLimit) {
        this.dynamicsLimit = dynamicsLimit;
    }



}
