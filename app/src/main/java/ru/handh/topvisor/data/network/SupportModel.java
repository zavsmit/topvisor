package ru.handh.topvisor.data.network;

import java.io.File;

import ru.handh.topvisor.ui.support.SupportEndReq;

/**
 * Created by sergey on 31.03.16.
 */
public interface SupportModel {
    void reqDialogList(int page, int count, SupportEndReq listener);

    void getMessages(int idTicket, SupportEndReq listener);

    void removeImage(String name, SupportEndReq listener);

    void getImagesList(SupportEndReq listener);

    void sendMessage(String text, int idTicket, SupportEndReq listener);

    void sendNewDialogMessage(String text, SupportEndReq listener);

    void sendPhoto(File file, SupportEndReq listener);
}
