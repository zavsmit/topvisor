package ru.handh.topvisor.data.network;

import android.content.Context;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.handh.topvisor.R;
import ru.handh.topvisor.data.network.model.ModelBankPrice;
import ru.handh.topvisor.data.network.model.ModelItemBank;
import ru.handh.topvisor.data.network.model.ModelList;
import ru.handh.topvisor.data.network.model.ReturnResponce;
import ru.handh.topvisor.ui.bank.BankEndReq;
import ru.handh.topvisor.utils.Constants;

/**
 * Created by sergey on 07.04.16.
 */
public class BankImpl implements BankModel {

    private RestApi restApi;
    private Context context;

    public BankImpl(RestApi restApi, Context context) {
        this.restApi = restApi;
        this.context = context;
    }

    @Override
    public void getBankListDate(int page, final BankEndReq listener, List<String> dates) {
        final ReturnResponce<ModelList<ModelItemBank>> result = new ReturnResponce<>();

        Call<ModelList<ModelItemBank>> call = restApi.reqBankList(Constants.UPLOADING, "date", "desc", page, dates);
        call.enqueue(new Callback<ModelList<ModelItemBank>>() {
            @Override
            public void onResponse(Call<ModelList<ModelItemBank>> call, Response<ModelList<ModelItemBank>> response) {

                result.setResult(response.body());
                listener.listBanksResponce(result);

            }

            @Override
            public void onFailure(Call<ModelList<ModelItemBank>> call, Throwable t) {
                result.setIsFailure(true);
                result.setErrorMessage(context.getString(R.string.notGetTransaction));
                t.getMessage();
                listener.listBanksResponce(result);
            }
        });
    }

    @Override
    public void getBankListAllParam(int page, final BankEndReq listener, String idProject, List<String> dates) {
        final ReturnResponce<ModelList<ModelItemBank>> result = new ReturnResponce<>();

        Call<ModelList<ModelItemBank>> call = restApi.reqBankListParam(Constants.UPLOADING,  "date", "desc", 1, idProject, dates);
        call.enqueue(new Callback<ModelList<ModelItemBank>>() {
            @Override
            public void onResponse(Call<ModelList<ModelItemBank>> call, Response<ModelList<ModelItemBank>> response) {

                result.setResult(response.body());
                listener.listBanksResponce(result);

            }

            @Override
            public void onFailure(Call<ModelList<ModelItemBank>> call, Throwable t) {
                result.setIsFailure(true);
                result.setErrorMessage(context.getString(R.string.notGetTransaction));
                t.getMessage();
                listener.listBanksResponce(result);
            }
        });
    }

    @Override
    public void getBankPriceDate(final BankEndReq listener, List<String> dates) {
        final ReturnResponce<List<ModelBankPrice>> result = new ReturnResponce<>();

        Call<List<ModelBankPrice>> call = restApi.reqBankPrice(dates);
        call.enqueue(new Callback<List<ModelBankPrice>>() {
            @Override
            public void onResponse(Call<List<ModelBankPrice>> call, Response<List<ModelBankPrice>> response) {

                result.setResult(response.body());
                listener.priceBanksResponce(result);

            }

            @Override
            public void onFailure(Call<List<ModelBankPrice>> call, Throwable t) {
                result.setIsFailure(true);
                result.setErrorMessage(context.getString(R.string.notGetData));
                t.getMessage();
                listener.priceBanksResponce(result);
            }
        });
    }

    @Override
    public void getBankPriceAllParam(final BankEndReq listener, String idProject, List<String> dates) {
        final ReturnResponce<List<ModelBankPrice>> result = new ReturnResponce<>();

        Call<List<ModelBankPrice>> call = restApi.reqBankPriceParam(idProject, dates);
        call.enqueue(new Callback<List<ModelBankPrice>>() {
            @Override
            public void onResponse(Call<List<ModelBankPrice>> call, Response<List<ModelBankPrice>> response) {
                result.setResult(response.body());
                listener.priceBanksResponce(result);
            }

            @Override
            public void onFailure(Call<List<ModelBankPrice>> call, Throwable t) {
                result.setIsFailure(true);
                result.setErrorMessage(context.getString(R.string.notGetData));
                t.getMessage();
                listener.priceBanksResponce(result);
            }
        });
    }
}
