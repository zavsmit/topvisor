
package ru.handh.topvisor.data.network.model.currentProject;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Searcher implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("project_id")
    @Expose
    private String projectId;
    @SerializedName("searcher")
    @Expose
    private String searcher;
    @SerializedName("domain")
    @Expose
    private String domain;
    @SerializedName("enabled")
    @Expose
    private String enabled;
    @SerializedName("ord")
    @Expose
    private String ord;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("regions")
    @Expose
    private ArrayList<Region> regions = new ArrayList<>();

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The projectId
     */
    public String getProjectId() {
        return projectId;
    }

    /**
     * 
     * @param projectId
     *     The project_id
     */
    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    /**
     * 
     * @return
     *     The searcher
     */
    public String getSearcher() {
        return searcher;
    }

    /**
     * 
     * @param searcher
     *     The searcher
     */
    public void setSearcher(String searcher) {
        this.searcher = searcher;
    }

    /**
     * 
     * @return
     *     The domain
     */
    public String getDomain() {
        return domain;
    }

    /**
     * 
     * @param domain
     *     The domain
     */
    public void setDomain(String domain) {
        this.domain = domain;
    }

    /**
     * 
     * @return
     *     The enabled
     */
    public String getEnabled() {
        return enabled;
    }

    /**
     * 
     * @param enabled
     *     The enabled
     */
    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    /**
     * 
     * @return
     *     The ord
     */
    public String getOrd() {
        return ord;
    }

    /**
     * 
     * @param ord
     *     The ord
     */
    public void setOrd(String ord) {
        this.ord = ord;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The regions
     */
    public ArrayList<Region> getRegions() {
        return regions;
    }

    /**
     * 
     * @param regions
     *     The regions
     */
    public void setRegions(ArrayList<Region> regions) {
        this.regions = regions;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.projectId);
        dest.writeString(this.searcher);
        dest.writeString(this.domain);
        dest.writeString(this.enabled);
        dest.writeString(this.ord);
        dest.writeString(this.name);
        dest.writeTypedList(regions);
    }

    public Searcher() {
    }

    protected Searcher(Parcel in) {
        this.id = in.readString();
        this.projectId = in.readString();
        this.searcher = in.readString();
        this.domain = in.readString();
        this.enabled = in.readString();
        this.ord = in.readString();
        this.name = in.readString();
        this.regions = in.createTypedArrayList(Region.CREATOR);
    }

    public static final Parcelable.Creator<Searcher> CREATOR = new Parcelable.Creator<Searcher>() {
        public Searcher createFromParcel(Parcel source) {
            return new Searcher(source);
        }

        public Searcher[] newArray(int size) {
            return new Searcher[size];
        }
    };
}
