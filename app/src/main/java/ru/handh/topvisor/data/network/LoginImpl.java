package ru.handh.topvisor.data.network;

import android.content.Context;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.handh.topvisor.BuildConfig;
import ru.handh.topvisor.R;
import ru.handh.topvisor.data.DataManager;
import ru.handh.topvisor.data.network.model.ModelAuth;
import ru.handh.topvisor.data.network.model.ModelLocation;
import ru.handh.topvisor.data.network.model.ModelResult;
import ru.handh.topvisor.data.network.model.ReturnResponce;
import ru.handh.topvisor.ui.login.LoginEndLoad;
import ru.handh.topvisor.ui.login.LoginViewActivity;
import ru.handh.topvisor.utils.Log;

/**
 * Created by sergey on 08.02.16.
 * реализация модели экрана входа
 */
public class LoginImpl implements LoginModel {

    private RestApi restApi;
    private Context context;

    public LoginImpl(Context context, RestApi restApi) {
        this.context = context;
        this.restApi = restApi;
    }

    @Override
    public void reqServerTypeAuth(final String email, final String pass, final LoginEndLoad listener) {
        final ReturnResponce<String> result = new ReturnResponce<>();


        Call<ModelLocation> call;
        if (BuildConfig.TVDEBUG) {
            call = restApi.getLocale("app@topvisor.ru");
//            call = restApi.getLocale("app@topvisor.com");
        } else {
            call = restApi.getLocale(email);
        }

        call.enqueue(new Callback<ModelLocation>() {
            @Override
            public void onResponse(Call<ModelLocation> call, Response<ModelLocation> response) {

                String serverType = response.body().getKey();
                if (serverType != null && !serverType.equals("0")) {
                    DataManager.getPref().setServerType(serverType);
                    result.setResult(serverType);
                    reqAuthorization(email, pass, result, listener);
                }

                if (serverType != null && serverType.equals("0")) {
                    result.setErrorMessage(context.getString(R.string.emailNotRegister));
                    listener.openMainAndSaveAuth(result);
                }

                if (serverType == null) {
                    result.setErrorMessage(context.getString(R.string.faileLogin));
                    listener.openMainAndSaveAuth(result);
                }

            }

            @Override
            public void onFailure(Call<ModelLocation> call, Throwable t) {
                result.setIsFailure(true);
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                t.getMessage();
                listener.openMainAndSaveAuth(result);
            }
        });
    }

    public void reqAuthorization(String email, String pass, final ReturnResponce<String> result, final LoginEndLoad listener) {

        result.setResult("");
        DataManager.initRest();

        Call<ModelAuth> call;
        if (BuildConfig.TVDEBUG) {
            call = restApi.getAuth("app@topvisor.ru", "Qej)30bH");
//            call = restApi.getAuth("app@topvisor.com", "topvisor");
        } else {
            call = restApi.getAuth(email, pass);
        }

        call.enqueue(new Callback<ModelAuth>() {
            @Override
            public void onResponse(Call<ModelAuth> call, Response<ModelAuth> response) {

                if (response.body().getPage() == 1) {
                    List<String> listHeaders = response.headers().toMultimap().get("Set-Cookie");

                    for (int i = 0; i < listHeaders.size(); i++) {
                        if (listHeaders.get(i).contains("auth")) {

                            String auth = listHeaders.get(i);

                            auth = auth.substring(auth.indexOf("auth") + 5, auth.indexOf(";"));

                            result.setResult(auth);

                            Log.deb("auth = " + auth);
                            DataManager.getPref().setTokenApp(result.getResult());
                        }
                    }

                } else {
                    result.setErrorMessage(context.getString(R.string.falseEnter));
                }

                listener.openMainAndSaveAuth(result);
            }

            @Override
            public void onFailure(Call<ModelAuth> call, Throwable t) {
                result.setIsFailure(true);
                t.getMessage();
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                listener.openMainAndSaveAuth(result);
            }
        });

    }

    @Override
    public void reqServerTypeForget(final String email, final LoginEndLoad listener) {

        final ReturnResponce<Boolean> result = new ReturnResponce<>();

        Call<ModelLocation> call;
        if (BuildConfig.TVDEBUG) {
            call = restApi.getLocale("app@topvisor.ru");
        } else {
            call = restApi.getLocale(email);
        }

        call.enqueue(new Callback<ModelLocation>() {
            @Override
            public void onResponse(Call<ModelLocation> call, Response<ModelLocation> response) {

                String serverType = response.body().getKey();

                if (serverType != null && !serverType.equals("0")) {
                    DataManager.getPref().setServerType(serverType);
                    reqForgetPass(email, listener, result);

                }

                if (serverType != null && serverType.equals("0")) {
                    result.setErrorMessage(context.getString(R.string.emailNotRegister));
                    listener.sendReqAndShowDialog(result, 0);
                }

            }

            @Override
            public void onFailure(Call<ModelLocation> call, Throwable t) {
                result.setIsFailure(true);
                t.getMessage();
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                listener.sendReqAndShowDialog(result, 0);
            }
        });
    }

    public void reqForgetPass(String email, final LoginEndLoad listener, final ReturnResponce<Boolean> result) {
        DataManager.initRest();

        Call<ModelResult> call;
        if (BuildConfig.TVDEBUG) {
            call = restApi.getSendForgetPass("app@topvisor.ru", "edit", "");
        } else {
            call = restApi.getSendForgetPass(email, "edit", "");
        }

        call.enqueue(new Callback<ModelResult>() {
            @Override
            public void onResponse(Call<ModelResult> call, Response<ModelResult> response) {
                result.setResult(!response.body().isError());
                result.setErrorMessage(response.body().getMessage());
                listener.sendReqAndShowDialog(result, LoginViewActivity.DIALOG_SEND_FORGET);
            }

            @Override
            public void onFailure(Call<ModelResult> call, Throwable t) {
                t.getMessage();
                result.setIsFailure(true);
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                listener.sendReqAndShowDialog(result, 0);
            }
        });
    }

    @Override
    public void reqRegistration(String email, final LoginEndLoad listener) {
        final ReturnResponce<Boolean> responce = new ReturnResponce<>();

        Call<ModelResult> call;
        if (BuildConfig.TVDEBUG) {
            call = restApi.registration("app@topvisor.ru", "add", "");
        } else {
            call = restApi.registration(email, "add", "");
        }
        call.enqueue(new Callback<ModelResult>() {
            @Override
            public void onResponse(Call<ModelResult> call, Response<ModelResult> response) {

                responce.setResult(!response.body().isError());
                responce.setErrorMessage(response.body().getMessage());
                listener.sendReqAndShowDialog(responce, LoginViewActivity.DIALOG_SEND_REGISTRATION);
            }

            @Override
            public void onFailure(Call<ModelResult> call, Throwable t) {
                t.getMessage();
                responce.setIsFailure(true);
                responce.setErrorMessage(context.getString(R.string.notGetServerData));
                listener.sendReqAndShowDialog(responce, 0);
            }
        });
    }

    @Override
    public void reqAuthorizationOnToken(String email, String code, String params, int domain, final LoginEndLoad listener) {

        final ReturnResponce<String> responce = new ReturnResponce<>();

        Call<Void> call = restApi.getAuthOnToken(email, code, 1, params);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                successAuth(response, responce, listener);
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                t.getMessage();
                responce.setIsFailure(true);
                listener.openMainAndSaveAuth(responce);
                responce.setErrorMessage(context.getString(R.string.notGetServerData));
            }
        });
    }

    @Override
    public void authSotialNetwork(String token, final LoginEndLoad listener) {

        final ReturnResponce<String> responce = new ReturnResponce<>();
        Log.deb("send token coockies = " + token);
        Call<Void> call = restApi.getAuthSocialNetwork(token);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {

                List<String> listHeaders = response.headers().toMultimap().get("Set-Cookie");

                for (int i = 0; i < listHeaders.size(); i++) {

                    Log.deb("request cookies = " + listHeaders.get(i));
                    if (listHeaders.get(i).contains("auth")) {
                        String auth = listHeaders.get(i);

                        auth = auth.substring(auth.indexOf("auth") + 5, auth.indexOf(";"));
                        Log.deb("auth = " + auth);
                    }
                }

                successAuth(response, responce, listener);
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                t.getMessage();
                responce.setIsFailure(true);
                listener.openMainAndSaveAuth(responce);
                responce.setErrorMessage(context.getString(R.string.notGetServerData));
            }
        });
    }

    private void successAuth(Response<Void> response, ReturnResponce<String> responce, LoginEndLoad listener) {
        String token = getAuthFromResult(response);

        responce.setResult(token);

        if (token.isEmpty()) {
            responce.setErrorMessage(context.getString(R.string.falseAuthFromNetwork));
        } else {
            DataManager.getPref().setTokenApp(token);
        }

        listener.openMainAndSaveAuth(responce);
    }


    private String getAuthFromResult(Response<Void> response) {

        List<String> listHeaders = response.headers().toMultimap().get("Set-Cookie");

        String authSuccess = "auth=";
        for (int i = 0; i < listHeaders.size(); i++) {
            if (listHeaders.get(i).contains(authSuccess)) {

                String auth = listHeaders.get(i);
                if (auth.contains(authSuccess)) {
                    auth = auth.substring(auth.indexOf(authSuccess) + authSuccess.length(), auth.indexOf(";"));
                }
                return auth;
            }
        }

        String socialSuccess = "set_auth_cookie=";
        for (int i = 0; i < listHeaders.size(); i++) {
            if (listHeaders.get(i).contains(socialSuccess)) {

                String auth = listHeaders.get(i);
                if (auth.contains(socialSuccess)) {
                    auth = auth.substring(auth.indexOf(socialSuccess) + socialSuccess.length(), auth.indexOf(";"));
                }
                return auth;
            }
        }
        return "";
    }
}
