
package ru.handh.topvisor.data.network.model.db.currentProject;

import io.realm.RealmObject;

public class GroupDB extends RealmObject {

    private String id;
    private String name;
    private String on;
    private String status;

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The on
     */
    public String getOn() {
        return on;
    }

    /**
     * 
     * @param on
     *     The on
     */
    public void setOn(String on) {
        this.on = on;
    }

    /**
     * 
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(String status) {
        this.status = status;
    }
}
