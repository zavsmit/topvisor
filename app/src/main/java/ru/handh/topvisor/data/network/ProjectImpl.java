package ru.handh.topvisor.data.network;

import android.content.Context;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.handh.topvisor.R;
import ru.handh.topvisor.data.DataManager;
import ru.handh.topvisor.data.network.model.ModelPriseInspectFilter;
import ru.handh.topvisor.data.network.model.ModelResult;
import ru.handh.topvisor.data.network.model.ReturnResponce;
import ru.handh.topvisor.data.network.model.currentProject.ModelCurrentProject;
import ru.handh.topvisor.data.network.model.currentProject.Row;
import ru.handh.topvisor.data.network.model.projecDates.ModelProjectDates;
import ru.handh.topvisor.data.network.model.projecDates.Rows;
import ru.handh.topvisor.data.network.model.projecPhrases.ModelProjectPhrases;
import ru.handh.topvisor.ui.project.ProjectEndReq;
import ru.handh.topvisor.utils.Constants;

/**
 * Created by sergey on 19.02.16.
 * реализация заросов с экрана проектов
 */
public class ProjectImpl implements ProjectModel {

    private RestApi restApi;
    private Context context;

    public ProjectImpl(RestApi restApi, Context context) {
        this.restApi = restApi;
        this.context = context;
    }

    @Override
    public void reqItemProject(String jsonSearch, final ProjectEndReq listener) {
        final ReturnResponce<Row> result = new ReturnResponce<>();

        Call<ModelCurrentProject> call = restApi.getItemProject(jsonSearch);
        call.enqueue(new Callback<ModelCurrentProject>() {
            @Override
            public void onResponse(Call<ModelCurrentProject> call, Response<ModelCurrentProject> response) {

                final ModelCurrentProject body = response.body();

                if (body.getRows() == null) {
                    result.setIsFailure(true);
                    listener.itemProjectResponce(result);
                    return;
                }

                result.setResult(response.body().getRows().get(0));
                listener.itemProjectResponce(result);

            }

            @Override
            public void onFailure(Call<ModelCurrentProject> call, Throwable t) {
                result.setIsFailure(true);
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                t.getMessage();
                listener.itemProjectResponce(result);

            }
        });
    }


    public void loadDatesProject(final String idProject, String competitorId, final String regionKey, String SearchData, String group,
                                 final ProjectEndReq listener) {

        final ReturnResponce<Rows> result = new ReturnResponce<>();

        Call<ModelProjectDates> call = restApi.getListDateProject(
                idProject,
                SearchData,
                competitorId,
                regionKey,
                DataManager.getPref().getServerShort(),
                Constants.COUNT_IN_PAGE,
                "1",//page// выбирать из данных
                group);


        call.enqueue(new Callback<ModelProjectDates>() {
            @Override
            public void onResponse(Call<ModelProjectDates> call, Response<ModelProjectDates> response) {

                final ModelProjectDates body = response.body();

                if (body.getRows() == null) {
                    result.setIsFailure(true);
                    listener.datesProjectResponce(result, idProject, regionKey);
                    return;
                }

                result.setResult(response.body().getRows());
                listener.datesProjectResponce(result, idProject, regionKey);
            }

            @Override
            public void onFailure(Call<ModelProjectDates> call, Throwable t) {
                result.setIsFailure(true);
                t.getMessage();
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                listener.datesProjectResponce(result, idProject, regionKey);
            }
        });
    }

    @Override
    public void loadPhrasesProject(String competitorid, String idProject, String regionKey,
                                   final List<String> dates, final ProjectEndReq listener,
                                   final int currentNumberPage, String groupId,
                                   String searcher, String page) {


        final ReturnResponce<ModelProjectPhrases> result = new ReturnResponce<>();

        Call<ModelProjectPhrases> call = restApi.getListPhrasesProject(
                Constants.COUNT_IN_PAGE,
                idProject,
                searcher,
                regionKey,
                DataManager.getPref().getServerShort(),
                page,
                groupId,
                "3",
                dates.get(0),
                dates.get(1),
                competitorid);


        call.enqueue(new Callback<ModelProjectPhrases>() {
            @Override
            public void onResponse(Call<ModelProjectPhrases> call, Response<ModelProjectPhrases> response) {


                if (response.body().getRows() == null) {
                    result.setIsFailure(true);
                    listener.phrasesProjectResponce(result, currentNumberPage, dates);
                    return;
                }


                result.setResult(response.body());
                listener.phrasesProjectResponce(result, currentNumberPage, dates);


            }

            @Override
            public void onFailure(Call<ModelProjectPhrases> call, Throwable t) {
                result.setIsFailure(true);
                t.getMessage();
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                listener.phrasesProjectResponce(result, currentNumberPage, dates);
            }
        });
    }

    public void reqRemovePhrare(String idProject, String idPharse, final ProjectEndReq listener) {

        Call<ModelResult> call = restApi.reqRemovePhrase(
                "del",
                idProject,
                idPharse);

        final ReturnResponce<ModelResult> result = new ReturnResponce<>();

        call.enqueue(new Callback<ModelResult>() {
            @Override
            public void onResponse(Call<ModelResult> call, Response<ModelResult> response) {
                result.setResult(response.body());
                listener.removePharseResult(result);
            }

            @Override
            public void onFailure(Call<ModelResult> call, Throwable t) {
                result.setIsFailure(true);
                t.getMessage();
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                listener.removePharseResult(result);
            }
        });
    }

    @Override
    public void reqAddPhrase(String phrase, String projectId, String groupId, final ProjectEndReq listener) {
        Call<ModelResult> call = restApi.reqAddPhrase(
                projectId,
                groupId,
                phrase,
                "add",
                "");

        final ReturnResponce<ModelResult> result = new ReturnResponce<>();

        call.enqueue(new Callback<ModelResult>() {
            @Override
            public void onResponse(Call<ModelResult> call, Response<ModelResult> response) {
                result.setResult(response.body());
                listener.addPharseResult(result);
            }

            @Override
            public void onFailure(Call<ModelResult> call, Throwable t) {
                result.setIsFailure(true);
                t.getMessage();
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                listener.addPharseResult(result);
            }
        });
    }

    @Override
    public void loadPriceOnFilter(String searchJson,
                                  String searcher,
                                  String searcherDomen,
                                  String regionKey,
                                  String regionLang,
                                  String groupId,
                                  final ProjectEndReq listener) {
        Call<ModelPriseInspectFilter> call = restApi.loadPriceOnFilter(
                searchJson,
                "1",
                searcher,
                searcherDomen,
                regionKey,
                regionLang,
                groupId);


        final ReturnResponce<ModelPriseInspectFilter> result = new ReturnResponce<>();

        call.enqueue(new Callback<ModelPriseInspectFilter>() {
            @Override
            public void onResponse(Call<ModelPriseInspectFilter> call, Response<ModelPriseInspectFilter> response) {
                result.setResult(response.body());
                listener.priceOnFilter(result);
            }

            @Override
            public void onFailure(Call<ModelPriseInspectFilter> call, Throwable t) {
                result.setIsFailure(true);
                t.getMessage();
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                listener.priceOnFilter(result);
            }
        });
    }

    @Override
    public void reqInspectOnFilter(String id,
                                   String searcher,
                                   String searcherDomen,
                                   String regionKey,
                                   String regionLang,
                                   String groupId,
                                   final ProjectEndReq listener) {
        Call<ModelResult> call = restApi.inspectOnFilter(
                "edit",
                searcher,
                searcherDomen,
                regionKey,
                regionLang,
                groupId,
                id);


        final ReturnResponce<ModelResult> result = new ReturnResponce<>();

        call.enqueue(new Callback<ModelResult>() {
            @Override
            public void onResponse(Call<ModelResult> call, Response<ModelResult> response) {
                result.setResult(response.body());
                listener.inspectOnFilter(result);
            }

            @Override
            public void onFailure(Call<ModelResult> call, Throwable t) {
                result.setIsFailure(true);
                t.getMessage();
                result.setErrorMessage(context.getString(R.string.notGetServerData));
                listener.inspectOnFilter(result);
            }
        });
    }
}
