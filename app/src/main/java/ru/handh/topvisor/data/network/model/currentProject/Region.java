package ru.handh.topvisor.data.network.model.currentProject;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Region implements Parcelable {

    public static final Creator<Region> CREATOR = new Creator<Region>() {
        public Region createFromParcel(Parcel source) {
            return new Region(source);
        }

        public Region[] newArray(int size) {
            return new Region[size];
        }
    };
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("depth")
    @Expose
    private String depth;
    @SerializedName("device")
    @Expose
    private String device;
    @SerializedName("key")
    @Expose
    private String key;
    @SerializedName("lang")
    @Expose
    private String lang;
    @SerializedName("countryCode")
    @Expose
    private String countryCode;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("areaName")
    @Expose
    private String areaName;
    @SerializedName("domain")
    @Expose
    private String domain;
    private boolean isSelected;

    public Region() {
    }

    protected Region(Parcel in) {
        this.id = in.readString();
        this.depth = in.readString();
        this.device = in.readString();
        this.key = in.readString();
        this.lang = in.readString();
        this.countryCode = in.readString();
        this.name = in.readString();
        this.areaName = in.readString();
        this.domain = in.readString();
        this.isSelected = in.readByte() != 0;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The depth
     */
    public String getDepth() {
        return depth;
    }

    /**
     * @param depth The depth
     */
    public void setDepth(String depth) {
        this.depth = depth;
    }

    /**
     * @return The device
     */
    public String getDevice() {
        return device;
    }

    /**
     * @param device The device
     */
    public void setDevice(String device) {
        this.device = device;
    }

    /**
     * @return The key
     */
    public String getKey() {
        return key;
    }

    /**
     * @param key The key
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * @return The lang
     */
    public String getLang() {
        return lang;
    }

    /**
     * @param lang The lang
     */
    public void setLang(String lang) {
        this.lang = lang;
    }

    /**
     * @return The countryCode
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * @param countryCode The countryCode
     */
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The areaName
     */
    public String getAreaName() {
        return areaName;
    }

    /**
     * @param areaName The areaName
     */
    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    /**
     * @return The domain
     */
    public String getDomain() {
        return domain;
    }

    /**
     * @param domain The domain
     */
    public void setDomain(String domain) {
        this.domain = domain;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.depth);
        dest.writeString(this.device);
        dest.writeString(this.key);
        dest.writeString(this.lang);
        dest.writeString(this.countryCode);
        dest.writeString(this.name);
        dest.writeString(this.areaName);
        dest.writeString(this.domain);
        dest.writeByte(isSelected ? (byte) 1 : (byte) 0);
    }
}
