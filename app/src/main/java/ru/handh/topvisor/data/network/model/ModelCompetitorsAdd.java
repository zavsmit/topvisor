package ru.handh.topvisor.data.network.model;

import java.util.ArrayList;

/**
 * Created by sergey on 22.03.16.
 */
public class ModelCompetitorsAdd {
    private ArrayList<ResultCompetitors> result;
    private String message;
    private boolean error;

    public ArrayList<ResultCompetitors> getResult() {
        return result;
    }

    public void setResult(ArrayList<ResultCompetitors> result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }
}
