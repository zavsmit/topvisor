package ru.handh.topvisor.data.network.model;

/**
 * Created by sergey on 14.03.16.
 */
public class ModelPriseInspectFilter {

    private String page;
    private String total;
    private String records;
    private Prise rows;

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getRecords() {
        return records;
    }

    public void setRecords(String records) {
        this.records = records;
    }

    public Prise getPrise() {
        return rows;
    }

    public void setPrise(Prise rows) {
        this.rows = rows;
    }

    public class Prise {
        private double price;
        private double xml_for_use;
        private String count_tasks;
        private String name;
        private String user;
        private String id;

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        public double getXml_for_use() {
            return xml_for_use;
        }

        public void setXml_for_use(double xml_for_use) {
            this.xml_for_use = xml_for_use;
        }

        public String getCount_tasks() {
            return count_tasks;
        }

        public void setCount_tasks(String count_tasks) {
            this.count_tasks = count_tasks;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUser() {
            return user;
        }

        public void setUser(String user) {
            this.user = user;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
}
