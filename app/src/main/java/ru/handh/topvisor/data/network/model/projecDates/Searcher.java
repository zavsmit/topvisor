
package ru.handh.topvisor.data.network.model.projecDates;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Searcher {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("searcher")
    @Expose
    private String searcher;
    @SerializedName("regions")
    @Expose
    private List<Region> regions = new ArrayList<Region>();

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The searcher
     */
    public String getSearcher() {
        return searcher;
    }

    /**
     * 
     * @param searcher
     *     The searcher
     */
    public void setSearcher(String searcher) {
        this.searcher = searcher;
    }

    /**
     * 
     * @return
     *     The regions
     */
    public List<Region> getRegions() {
        return regions;
    }

    /**
     * 
     * @param regions
     *     The regions
     */
    public void setRegions(List<Region> regions) {
        this.regions = regions;
    }

}
