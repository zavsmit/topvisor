
package ru.handh.topvisor.data.network.model.projecPhrases;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Date implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("phrase_id")
    @Expose
    private String phraseId;
    @SerializedName("page")
    @Expose
    private String page;
    @SerializedName("position")
    @Expose
    private String position;
    @SerializedName("visitors")
    @Expose
    private String visitors;
    @SerializedName("status")
    @Expose
    private String status;

    /**
     * 
     * @return
     *     The id
     */
    public String getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * @return
     *     The phraseId
     */
    public String getPhraseId() {
        return phraseId;
    }

    /**
     * 
     * @param phraseId
     *     The phrase_id
     */
    public void setPhraseId(String phraseId) {
        this.phraseId = phraseId;
    }

    /**
     * 
     * @return
     *     The page
     */
    public String getPage() {
        return page;
    }

    /**
     * 
     * @param page
     *     The page
     */
    public void setPage(String page) {
        this.page = page;
    }

    /**
     * 
     * @return
     *     The position
     */
    public String getPosition() {
        return position;
    }

    /**
     * 
     * @param position
     *     The position
     */
    public void setPosition(String position) {
        this.position = position;
    }

    /**
     * 
     * @return
     *     The visitors
     */
    public String getVisitors() {
        return visitors;
    }

    /**
     * 
     * @param visitors
     *     The visitors
     */
    public void setVisitors(String visitors) {
        this.visitors = visitors;
    }

    /**
     * 
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(String status) {
        this.status = status;
    }


    public Date() {
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.phraseId);
        dest.writeString(this.page);
        dest.writeString(this.position);
        dest.writeString(this.visitors);
        dest.writeString(this.status);
    }

    protected Date(Parcel in) {
        this.id = in.readString();
        this.phraseId = in.readString();
        this.page = in.readString();
        this.position = in.readString();
        this.visitors = in.readString();
        this.status = in.readString();
    }

    public static final Creator<Date> CREATOR = new Creator<Date>() {
        public Date createFromParcel(Parcel source) {
            return new Date(source);
        }

        public Date[] newArray(int size) {
            return new Date[size];
        }
    };
}
