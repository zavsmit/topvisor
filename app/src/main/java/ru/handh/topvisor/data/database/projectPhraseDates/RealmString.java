package ru.handh.topvisor.data.database.projectPhraseDates;

import io.realm.RealmObject;

/**
 * Created by sergey on 29.04.16.
 */
public class RealmString extends RealmObject {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
