
package ru.handh.topvisor.data.network.model.currentProject;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ModelCurrentProject implements Parcelable {

    @SerializedName("page")
    @Expose
    private Integer page;
    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("records")
    @Expose
    private String records;
    @SerializedName("rows")
    @Expose
    private List<Row> rows = new ArrayList<>();

    /**
     * 
     * @return
     *     The page
     */
    public Integer getPage() {
        return page;
    }

    /**
     * 
     * @param page
     *     The page
     */
    public void setPage(Integer page) {
        this.page = page;
    }

    /**
     * 
     * @return
     *     The total
     */
    public Integer getTotal() {
        return total;
    }

    /**
     * 
     * @param total
     *     The total
     */
    public void setTotal(Integer total) {
        this.total = total;
    }

    /**
     * 
     * @return
     *     The records
     */
    public String getRecords() {
        return records;
    }

    /**
     * 
     * @param records
     *     The records
     */
    public void setRecords(String records) {
        this.records = records;
    }

    /**
     * 
     * @return
     *     The rows
     */
    public List<Row> getRows() {
        return rows;
    }

    /**
     * 
     * @param rows
     *     The rows
     */
    public void setRows(List<Row> rows) {
        this.rows = rows;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.page);
        dest.writeValue(this.total);
        dest.writeString(this.records);
        dest.writeTypedList(rows);
    }

    public ModelCurrentProject() {
    }

    protected ModelCurrentProject(Parcel in) {
        this.page = (Integer) in.readValue(Integer.class.getClassLoader());
        this.total = (Integer) in.readValue(Integer.class.getClassLoader());
        this.records = in.readString();
        this.rows = in.createTypedArrayList(Row.CREATOR);
    }

    public static final Parcelable.Creator<ModelCurrentProject> CREATOR = new Parcelable.Creator<ModelCurrentProject>() {
        public ModelCurrentProject createFromParcel(Parcel source) {
            return new ModelCurrentProject(source);
        }

        public ModelCurrentProject[] newArray(int size) {
            return new ModelCurrentProject[size];
        }
    };
}
